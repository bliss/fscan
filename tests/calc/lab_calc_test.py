# --- test raw user calc
from fscan.mussttools import MusstChanUserRawCalc

class CalcStepPeriod(MusstChanUserRawCalc):
    @property
    def dest_name(self):
        return f"{self.master.name}:mot2_step_period"

    def select_channel(self, channels, pars):
        channel = channels.get("mot2", None)
        return channel

    def prepare(self):
        self._data = numpy.array((), dtype=numpy.int32)

    def calculate(self, data):
        self._data = numpy.append(self._data, data)
        if len(self._data) < 3:
            return
        calc_data = self._data[2::2] - self._data[0:-2:2]
        self._data = self._data[2 * len(calc_data) :]
        return calc_data

my_raw_calc = CalcStepPeriod()
fscan.master.add_user_calc(my_raw_calc)

# --- test user calc

from fscan.mussttools import MusstChanUserCalc

class SpeedUserCalc(MusstChanUserCalc):
    def select_input_channels(self, channels, pars):
        used = list()
        for chan in channels:
            if chan in ["mot2_period", "timer_period", "timer_delta"]:
                used.append(chan)
        return used

    def get_output_channels(self):
        return ["mot2_speed", "dead_time"]

    def calculate(self, data):
        calc_data = dict()
        calc_data["mot2_speed"] = data["mot2_period"]/data["timer_period"]
        calc_data["dead_time"] = data["timer_period"]-data["timer_delta"]
        return calc_data

my_user_calc = SpeedUserCalc()
fscan.master.add_user_calc(my_user_calc)



import numpy
import tabulate

def mca_timing(fscan, mcaobj):
    mca = None
    for node in fscan.scan.acq_chain.nodes_list:
        if node.name == mcaobj.name:
            mca = node
            break
    if mca is None:
        print(f"{name} not found in acq chain")

    tap = numpy.array(mca._timing_append)
    tap *= 1000.0
    tpu = numpy.array(mca._timing_publish)
    tpu /= mca.block_size
    tpu *= 1000.0
   
    infos = list()
    infos.append(["append", tap.min(), tap.max(), tap.mean(), tap.std()])
    infos.append(["publish", tpu.min(), tpu.max(), tpu.mean(), tpu.std()])

    heads = ["operation", "min", "max", "mean", "std"]
    print(tabulate.tabulate(infos, headers=heads))
    print()

def mca_tests(expotime, npoints, mcaobj):
    name = mcaobj.name

    ACTIVE_MG.enable(f"{name}*")
    ftimescan(expotime, npoints)
    print("\n>>> MCA = SPECTRUM + STATS + ROIS")
    mca_timing(ftimescan, mercury)

    ACTIVE_MG.disable(f"{name}:roi*")
    ftimescan(expotime, npoints)
    print("\n>>> MCA = SPECTRUM + STATS")
    mca_timing(ftimescan, mercury)

    ACTIVE_MG.disable(f"{name}:*")
    ACTIVE_MG.enable(f"{name}:spectrum*")
    ftimescan(expotime, npoints)
    print("\n>>> MCA = SPECTRUM")
    mca_timing(ftimescan, mercury)


**Decription:**

An _ftimescan_ is executed at every point of a _lookupscan_:

- Move motors to first positions of lookup table
- Execute ftimescan-like acquisition (npoints * acq_time)
- Move motors to second positions of lookup table
- Execute ftimescan-like acquisition
- ... to the end of lookup table ...

Detectors are started only at the beginning and configured with all points of the entire scan.
When using one lima device, CAMERA scanning and LIMA accumulation modes are supported.

**Parameters:**

All parameters are the same as an **ftimescan**. It only adds the lookup parameter *motor_lookup*. *motor_lookup* is defined in the same manner in bliss standard *lookupscan* : a list of tuple of motor / positions array.

***Example:***

```
MANU [2]: ftimescanlookup.pars.motor_lookup = [(rot1, [0.1, 0.2, 0.3]), (rot2, [90., 95., 100.])]                             
MANU [3]: ftimescanlookup.pars                                                                                                
 Out [3]: Parameters [redis-key=fscan_manu_ftimescanlookup_pars]                                                              
                                                                                                                              
            .acq_time      = 0.1                                                                                              
            .npoints       = 10                                                                                               
            .period        = 0                                                                                                
            .motor_lookup  =                                                                                                  
              - rot1 :   0.1   0.2    0.3                                                                                     
              - rot2 :  90    95    100                                                                                       
            .start_delay   = 0.1                                                                                              
            .latency_time  = 0.0                                                                                              
            .sampling_time = 0.5                                                                                              
            .scan_mode     = TIME                                                                                             
            .camera_signal = EDGE                                                                                             
            .save_flag     = True                                                                                             
            .display_flag  = True                                                                                             
```


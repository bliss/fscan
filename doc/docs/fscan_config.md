
## Beacon Configuration ##

### Minimum Configuration ###

Following is an example of basic fscan configuration:

```
- name: fscan_eh3
  class: FScanConfig
  package: fscan.fscanconfig
  devices: 
    synchro: $musst_eh3                  	[ mandatory ]
    OR musst: $musst_eh3                        [ backward compatibility ]
    measgroup: $mg_eh3				[ optional ]
    rotation_motors: $hrrz			[ optional ]
    external_motors: $hry                       [ optional ]
    
```

Configuration devices parameters :

  - *synchro* [mandatory] : mandatory reference to at least one musst or maestrio object. Alternatively, multiple synchro boards can be given as a list. In that case, fscan searches the device motors (encoders) involved in the scan on the referenced boards. Only that synchro device, musst or maestrio, will be used. Signal multiplexing must be setup specifically. Example of such a config:

    ``` musst: [ $musst_eh3, $maestrio_eh3 ] ```

  - *musst* [backward compatibility] : It replaces *synchro* key for musst only devices.
  - *measgroup* [optional] : reference to a bliss measurement group for the scans. If no measurement group is defined, the default one (*ACTIVE_MG*) will be used.
  - *fast_measgroup* [optional] : when using multiple rate fast scans, reference a bliss measurement group to be used on fast acquisition rate. See doc of mtimescan for details.
  - *rotation_motors* [optional] : list of motors which can **rotate indefinitely**. In some scans, they are used as *finterlaced* in *FORWARD* mode or in all fscan* when home_rotation parameter is set. See scan reference for details.
  - *external_motors* [optional] : list of motors which will trigger the musst/maestrio. No need to have their encoders plugged on the synchro board. Only supported on *fscan/fscan2d/fscan3d*. Typically this is used for piezos or speed goat controlled motors. Motor acquisition master class for these motors can be overwritten. See Session Setup/External Motor Class section.

### Additional Configuration ###

#### Chain Configuration ####
Default device settings may be changed in the configuration. For example, fscan is using lima EXTERNAL_TRIGGER_MULTI, but the Perkin Elmer detector does not support it. Here one can customise the acquisition chain to change the trigger mode as follow:

```
  chain_config:
    perkin:
      acq_params:
        acq_trigger_mode: EXTERNAL_START_STOP
```

*acq_params* and *ctrl_params* can be specified on any device (Lima, MCAs, ...). These parameters must defined within a bliss acquisition object.


#### Lima Calibration ####
Fscan holds the readout time calibration default values for some of the lima detectors. However one can adjust them in the configuration. Example:

```
  lima_calib:
    eiger:
      internal_time_correction: 0.0000373265
    marana:
      trigger_latency: 0.0005
```

See [Lima Calibration](lima_calib.md) for details.


## Session Setup ##

### Typical Setup ###
FscanConfig objects (see *fscan_eh3* in previous example) give access to all scans. It allows to add ScanPreset or ChainPreset on any scan. To ease scan access to users, shorcuts are generally defined. This is all done in setup. Typical example:

```
fscan_eh3.add_scan_preset(my_scan_preset, "mypreset")
fscan_eh3.add_chain_preset(pilatus_protection_preset, "pilatus")

fscan = fscan_eh3.fscan
fscan2d = fscan_eh3.fscan2d
fscan3d = fscan_eh3.fscan3d
```

In that example, the scan preset and the chain preset are setup for all scans. Fscan, fscan2d, fscan3d shortcuts all use those presets. 
Other scans are always accessible through fscan_eh3 like fscan_eh3.ftimescan(), for example. This is typically used when writing beamline specific macro running fscan. Example:
```
def my360tomo(resolution, expotime):
    fscan_eh3.fscan(rotmot, 0., resolution, int(360./resolution), expotime, scan_mode="TIME")
```

### Presets ###

Presets in fscan are standard bliss presets and therefore need to derivate from **ScanPreset** or **ChainPreset**. Fscan adds an additionnal feature to it : the *set_fscan_master(self, master)* method to get a master fscan reference before *prepare()* is called. This can be used to inspect scan parameters for example:

```
class MyFscanPreset(ScanPreset):
    def set_fscan_master(self, master):
        if master.pars.scan_mode == "TIME":
            self.is_time = True
        else:
            self.is_time = False

    def prepare(self, scan):
        if self.is_time:
            # ...
        else:
            # ...
```

### External Motor Class ###

When using external motor, the acquisition motor class can be defined. Two methods can be overwritten to setup position triggering on the motor controller :

  - *enable_gate_output()* : called by the prepare() method
  - *disable_gate_output()* : called by the stop() method

This class has to derivate from:

  - **ExternalMotorMaster** for *fscan*
  - **ExternalExternalMotorListMaster** for *fscan2d/fscan3d*

To add it on the scans, add in your setup:

  - *fscan.master.set_external_motor_master_class(your_class)*
  - *fscan2d/3d.master.set_external_motor_list_master_class(your_class)*

Example:

```
class MyExternalMotor(ExternalMotorMaster):
    def enable_gate_output(self):
        # self.movable.controller.set_gate_output()

    def disable_gate_output(self):
        # self.movable.controller.unset_gate_output()

my_fscan_config.fscan.master.set_external_motor_master_class(MyExternalMotor)
```


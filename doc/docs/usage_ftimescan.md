
**Description:**

_Continuous scan in time._

When using one lima device, TIME and CAMERA scanning modes are supported, including lima accumulation mode. See [scanning mode details](synchro_signal.md).

**Parameters:**

| parameter        |  | description |
|------------------|--|-------------|
| *acq_time* | [sec] |acquistion time per point.|
| *npoints*  | | number of acquired points|
| *period* | [sec] | period between two start triggers. If set to 0, final detector readout time is estimated to compute period.|
| *start_delay* | [sec] | time delay of first trigger. It is needed for some detectors which have a delay to react to triggers when they start.|
|*scan_mode* | TIME,CAMERA | CAMERA mode can be used when one lima detector is used.|
|*camera_signal* | EDGE,LEVEL | define camera input signal type when using CAMERA mode.|
|*latency_time* | [sec] | additionnal latency time added to detector readout time.|


Common parameters implemented : *save_flag*, *display_flag*, *sampling_time*.


In module **fscan.pepuscope**, some utility scans using **PEPU** board are available. Those scans was primarily written for test purpose at PEL laboratory. It allows to record on **PEPU** encoders and/or interferometer inputs (via V2SSI board) either in time or while a motor moves.


## peputimescope

**description:** 

pepuscope will record all configured inputs of a PEPU board at a given frequency.

**usage:** 

`peputimescan(frequency, duration, pepu=None, pepucalc=None, **kwargs):`

**parameters:**

  - *frequency* : pepu acquisition rate (Hz)
  - *duration* : recorded time lapse in seconds
  - *pepu [optionnal]* : pepu bliss object. Taken from current measurment group otherwise.
  - *pepucalc [optional]* : pepu calc bliss object. Taken from current measurment group otherwise.
  - *kwargs [optional]* :
     - *save [default= True]* : save data in file or not
     - *run [default= True]* : when set, run the scan. If not set, return a bliss scan object to be run later.


## pepumotorscope

**description:** 

pepumotorscope will move a motor at a given speed and register all configured PEPU board inputs while the motor is moving. Motor motion will either be a standard move or a jog motion. The PEPU recording starts before motor movement is launched. There is no hardware synchronization.

**usage:** 

`pepumotorscan(motor, startpos, displacement, speed, frequency, extra_time=0.4, jog=False, pepu=None, pepucalc=None, **kwargs)`

**parameters:**

  - *motor* : motor bliss object
  - *startpos*, *displacement*, *speed* : motor motion parameters
  - *frequency* : pepu acquisition rate (Hz)
  - *extra_time [optionnal]* : the total time of the acquisition is calculed using motor displacement estimation time. This *extra_time* can be added to this estimation.
  - *jog [optionnal]* : launch the movement using the jog command instead a standard move.
  - *pepu [optionnal]* : pepu bliss object. Taken from current measurment group otherwise.
  - *pepucalc [optional]* : pepu calc bliss object. Taken from current measurment group otherwise.
  - *kwargs [optional]* :
     - *save [default= True]* : save data in file or not
     - *run [default= True]* : when set, run the scan. If not set, return a bliss scan object to be run later.


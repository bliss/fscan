
## Description

**fscanloop** is specific to *rotation motors*: it will perform multiple **fscan**-like continuous scan without stopping the rotation motor. Typical usage is fast continuous tomography.
Each loop is defined by a set of parameters : *start_pos*, *step_size*, *step_time* and *loop_range*. The total number of loop executed is controlled by *nloop*. 

We can control how subsequent loop are executed with parameter *loop_trig_mode*:

- **INTERNAL** : next loop is started after rotation motor has moved *loop_range* angle. The minimum *loop_range* is one *step_size*. 0 is not allowed : if not loop delta is desired, you can perform this with a standard fscan.
- **EXTERNAL** : next loop is started when musst board has received a trigger on its *Itrig* input (rising edge).
- **LIMA_READY** : lima cameras can be programmed for the complete scans ie. all loops, or can be programmed for each loop. In some cases, we need to wait that the lima server has finished reading/saving tasks to restart the next loop. This mode is here for that. To use it, user has also to define a trigger functions that will sent a trigger to the *Itrig* of the musst board. This trigger function will be called when lima has finished reading/saving tasks. Typical usage of this mode is continuous tomography with a PCO DiMax. As the PCO DiMax saves images in its internal memory, lima server has to empty this memory before restarting the next acquisition loop.

The lima camera used in the scans can either be programmed to acquire the full scan (all loops) and be stopped/re-started at each loop. This option is defined by *lima_read_mode* parameter:

- **PER_SCAN** : one start at first loop and one stop at end of last loop
- **PER_LOOP** : one start/stop sequence at each acquisition loop.

The lima accumulation mode is not supported.

Example of an *fscanloop* in *INTERNAL* loop trigger mode:

![fscanloop schema](img/fscanloop.svg)

## Parameters

| parameter        |  | description |
|------------------|--|-------------|
| *motor* || bliss motor object (not motor name string !!)
| *start_pos* || start position of first acquisition
| *step_size* || size of one step in position
| *step_time* |[sec]| time of one step 
| *loop_range* || total range of a loop
| *loop_delta* || delta between sub-sequent loop
| *nloop* || total number of loops
| *acq_time* |[sec]| acquisition time
| *loop_trig_mode* | INTERNAL,EXTERNAL,LIMA_READY | loop trigger mode
| *lima_read_mode* | PER_SCAN,PER_LOOP | lima reading mode
| *scan_mode*| TIME,POSITION | scan trigger mode as in fscan
| *gate_mode*| TIME,POSITION | gate trigger mode as in fscan

Common parameters implemented: *save_flag*, *display_flag*, *sync_encoder*, *home_rotation*, *acc_margin*, *sampling_time*.


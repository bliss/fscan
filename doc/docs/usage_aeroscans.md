**Aeroscans** use trajectories on Aerotech controllers (available with Ensemble controllers). They have been developed for diffraction tomography experiments employing sample rotation and y-displacement motors. Both motors need to be driven by the same Aerotech controller. Their encoders must be plugged into the musst board. The motors are synchronised by the trajectory.

Two variants of scan exists:

### aeroystepscan
**Description** : the rotation is scanned continuously and the translation is moved step-by-step. The acquisition can restart when the translation step is finished without the rotation being stopped.

**Parameters**
### aeroycontscan 
**Description** : both motors, rotation and translation are moved continuously throughout the scans. Rotation motor moves at constant speed while different velocity region can be set for translation motor.

**Parameters**


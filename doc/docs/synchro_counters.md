
For each point, the **synchro** board (**musst** or **maestrio**) registers the timer and the motor position. They are stored at the beginning and end of the acquisition, excluding the readout and/or latency time. In addition to timer, an **epoch** counter is estimated from the synchro board timer, in order to scope with the software timer of the sampling counters. The values read out from the synchro board are registered in data as **raw** values, which helps only for debugging. For standard plotting, some calculated counters are provided for timer, motor position and epoch as follow:

![musst calc counters](img/fscan_musst_calc.svg)

For musst counters (not timer, not encoders), only **delta** is computed. Following is an example for an fscan of 10 points. Note that all the calculated counters have 10 values while the raw counters have 21 values (2 * npoints + 1).

- musst_bcu2:mu_cnt4/5/6 are musst counters
- musst_bcu2:rot2 is the motor encoder used in the scan

![fscan counter example](img/silx_musst_calc.png)

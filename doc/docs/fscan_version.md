
Version are managed by specific branch in the git repository.

| fscan branch | bliss compatible version |
| ------------ | ------------- |
| 2.1.x        | >= 2.1        |
| 2.0.x        | >= 2.0        |
| 1.0.x        | >= 2.0        |
| bliss_1.11   | 1.11.x        |
| bliss_1.10   | 1.10.x        |

## version 2.1.x ##
- add support for Lima2 devices

## version 2.0.x ##
- add support for maestrio synchro
- refactoring of musst/maestrio synchro device and calc channels

The following table indicates which scans has been implemented on maestrio:

| scan | musst | maestrio |
| ---- | ----- | -------- |
| ftimescan | YES | YES |
| ftimescanlookup | YES | YES |
| fscan | YES | YES |
| finterlaced | YES | NO |
| fsweep | YES | NO |
| fscanloop | YES | NO |
| fscan2d | YES | YES |
| f2scan | YES | YES |
| fscan3d | YES | YES |
| timescope | YES | YES |
| motorscope | YES | YES |
| limascope | YES | YES |


## version 1.0.x ##
- compatibility with bliss 2.0.x
- add mtimescan
- add external motors

## for older bliss version ##

These branches includes all standard fscans : ftimescan*, fscan*, finterlaced, fsweep, ...

- branch *bliss_1.11* for compatibility with bliss 1.11.x
- branch *bliss_1.10* for compatibility with bliss 1.10.x

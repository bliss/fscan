
## Description

**finterlaced** is a 1-motor continuous scan, like **fscan**, executed in 2 passes. In the _first pass_, only one acquisition is performed every second point; _second pass_, the remaning acquisitions are performed. The objective is to cover the entire scan distance without gaps, during readout phase as it exist in fscan. Consequently, lima detectors are used in *EXTERNAL_GATE* mode and triggers are always in *POSITION*. This ensures that the start of the second phase exposures match exactly the end of first phase exposures.

**finterlaced** supports 3 specific modes based on the **mode** parameter. Due to the acquisition schema, images are not taken in natural order, which imposes care, when processing the images. The following diagrams show the frame orders (acq number) and motor movements for the 3 modes.
  
  - **REWIND**: motor is rewound to the start position after 1st pass
    ![finterlaced rewind_schema](img/finterlaced_rewind.svg)
  - **ZIGZAG**: motor perform the second pass in opposite direction. This avoids rewinding the motor to the start position and saves time.
    ![finterlaced_zigzag_schema](img/finterlaced_zigzag.svg)
  - **FORWARD**: only supported on *rotation motors* (i.e. motors which can rotate indefinitely). The motor is not stopped after the first pass and continues its rotation to perform the second pass in one movement. This saves a lot of travel time, when performing acquisitions on a 360 degree basis.
    ![finterlaced_forward_schema](img/finterlaced_forward.svg)

Common parameters implemented: *save_flag*, *display_flag*, *sync_encoder*, *home_rotation*, *acc_margin*, *sampling_time*

## Parameters

| parameter        |  | description |
|------------------|--|-------------|
| *motor* || bliss motor object (not motor name string !!)
| *start_pos* || start position of first acquisition
| *acq_size* || size of one acquisition in position
| *acq_time* |[sec]| acquisition time
| *npoints* || total number of points
| *mode* |REWIND,ZIGZAG,FORWARD| interlaced mode as described above.
| *shift_pos* |default=0.0| position shift of start position of second interlaced pass.

Common parameters implemented: *save_flag*, *display_flag*, *sync_encoder*, *home_rotation*, *acc_margin*, *sampling_time*.

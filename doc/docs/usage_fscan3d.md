
## Description

**fscan3d** adds a second stepper motion loop on top of **fscan2d**. Whence the same scanning mode and options as **fscan2d** (refer to [fscan2d documentation](usage_fscan2d.md) for details) are available. As in **fscan2d**, all acquisition devices are programmed with the total number of acquisition points at the start of the scan in order to lower the deadtime between continuous parts to be only motor motion time.

The three motors involved are called *slow1_motor*, *slow2_motor* and *fast_motor*; the are used as follow:
```
for each position of slow1_motor:
    for each position of slow2_motor:
        do continuous scan of fast_motor
```

## Parameters

| parameter        |  | description |
|------------------|--|-------------|
| *slow1_motor* | | bliss motor object for motor to step
| *slow1_start_pos* | | start position of first slow motor
| *slow1_step_size* | | step size in position of first slow motor
| *slow1_npoints* | | number of step movement performed on first slow motor
| *slow2_motor* | | bliss motor object for motor to step
| *slow2_start_pos* | | start position of second slow motor
| *slow2_step_size* | | step size in position of second slow motor
| *slow2_npoints* | | number of step movement performed on second slow motor
| *fast_motor* | | bliss motor object for motor scanned continuously
| *fast_start_pos* | | start position
| *fast_step_size* | | step in position during one point
| *fast_step_time* | [sec] | time to move one *fast_step_size*. As in **fscan**, it can be set to 0. and readout time will be estimated [see fscan description](usage_fscan.md)
| *fast_npoints* | | number of points to acquire in one continuous part
| *acq_time* | [sec] | acquisition time
| *fast_motor_mode* | REWIND,FORWARD | fast motor return mode
| *scan_mode* | TIME,POSITION,CAMERA | scanning mode
| *gate_mode* | TIME,POSITION | gate mode used while scanning in POSITION
| *camera_signal* | EDGE,LEVEL | input camera signal type when in CAMERA mode
| *latency_time* | [sec] | additionnal latency time added to detector readout time
| *scatter_plot* | True,False | ask flint to add a scatter plot or not

Common parameters implemented: *save_flag*, *display_flag*, *sync_encoder*, *home_rotation*, *acc_margin* (named *fast_acc_margin* here), *sampling_time*.

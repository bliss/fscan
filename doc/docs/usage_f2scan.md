
## Description

**f2scan** is an **fscan** performed on 2 motors at the same time. The two motors are not synchronized by hardware. This works fine when the two motors are driven by the same controller which allows multiple-axis start like *icepap*. When using it on different controllers, a position shift can be observed. Typical usage is a tomography diffraction scan using rotation as first motor and y-displacement as second motor, the slave motor.

As in **fscan**, the first acquisition is always triggered by position. The position is read from the first motor (*motor* parameter), which needs to be connected to the musst board. The second motor is called *slave_motor* as it is not used for acquisition synchronisation. If possible both motor positions are recorded on the musst.

The two motors are started from their real start positions at the same time. The real start positions are calculated, so that they reach their constant speed at the start position of the first acquisition. When using **f2scan** with a large velocity difference, the slower motor may travel a long distance before starting.

All scanning mode of **fscan** are supported : TIME, POSITION and CAMERA. Lima accumulation mode is also supported.

## Parameters

| parameter        |  | description |
|------------------|--|-------------|
| *motor* | | bliss motor object (not motor name string !!)
| *start_pos* | | start position of first acquisition
| *step_size* | | step size in position of one acquisition
| *step_time* | [sec] | if set to non-zero, fix time between start triggers
| *slave_motor* | | bliss motor object of second motor
| *slave_start_pos* | | start position of second motor
| *slave_step_size* | | step of second mode during one acquisition
| *acq_time* | [sec] | acquisition time per point
| *npoints* | | total number of points
| *scan_mode* | TIME,POSITION,CAMERA | scanning mode
| *gate_mode* | TIME,POSITION | gate mode used while scanning mode is in POSITION.
| *camera_signal* | EDGE,LEVEL | input camera signal type when in CAMERA mode.
| *latency_time* | [sec] | additional latency time added to detector readout time

Common parameters implemented: *save_flag*, *display_flag*, *sync_encoder*, *home_rotation*, *acc_margin*, *sampling_time*.


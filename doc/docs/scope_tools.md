
Some tools are available in module **fscan.scope**. The aim of these tools is to record either motor positions or camera signals for diagnostic pupose. They can be used with either a **musst** or a **maestrio** board.

## timescope

**description:** 

timescope will record musst counters/encoders inputs with a given time period. Can be used to monitor motor stability for example.

**usage:** 

`timescope(syncdev, npts, period, *motors, **kwargs)`

**parameters:**

  - *syncdev* : musst or maestrio bliss object
  - *npts* : number of points to record
  - *period* : point period in seconds
  - *motors [optional]* : motors/encoders to record (bliss motor object)
  - *kwargs [optional]* :
     - *save [default= True]* : save data in file or not
     - *sync_motors [default= True]* : when using motors, synchronise musst motor steps with current motor position before starting

**example:** 

```
MANU [11]: from fscan.scope import timescope                                                              
MANU [12]: timescope(musst_bcu2, 100, 0.01, save=False)                                                        
Scan 1 Fri Jun 11 11:32:50 2021  manu user = blissadm                                                          
timescope musst_bcu2 100 0.01                                                                                  
                                                                                                               
Preparing ...                                                                                                  
                                                                                                               
Running ...                                                                                                    
trig 100                                                                                                       
                                                                                                               
Stopping ...                                                                                                   
                                                                                                               
Finished (took 0:00:01.486697)      

MANU [13]:  
```

## motorscope

**description:** 

motorscope will move a motor at a given speed and register its encoder/steps while moving. It can be used to check how a motor accelerates (check overshoot for example), or if its speed is really constant (closed loop stability for example).

**usage:** 

`motorscope(syncdev, motor, start, stop, speed, period, **kwargs)`

**parameters:**

  - *syncdev* : musst or maestrio bliss object
  - *motor* : motor bliss object
  - *start*, *stop*, *speed* : motor motion parameters
  - *period* : time period in seconds to register musst inputs
  - *kwargs [optional]* :
    - *save [default= True]* : save data to file or not
    - *sync_motors [default= True]* : synchronise musst motor steps with current motor position before starting
    - *acc_margin [default= 0.]* : add a margin (in motor units) to acceleration delta
    - *extra_time [default= 0]* : register longer time than computed move time
    - *constant_speed [default= True]* : start recording after motor acceleration time when motor should be at constant speed
    - *zigzag [default= False]* : move 2 times : from *start* to *stop* then go back

**example:**
```
MANU [16]: from fscan.scope import motorscope
MANU [17]: motorscope(musst_bcu2, rot2, 3800, 3805, 1.5, 0.010)                                                
                                                                                                               
motorscope type : simple move from 3800.000000 unit to 3805.000000 unit                                        
    * maximum speed  = 1.500000 unit/sec                                                                       
    * estimated time = 4.833333 sec                                                                            
                                                                                                               
Scan 174 Fri Jun 11 11:34:30 2021 /nobackup/lid00limace1/scans/manu/data.h5 manu user = blissadm               
motorscope musst_bcu2 rot2 3800 3805 1.5 0.01                                                                  
                                                                                                               
Preparing ...                                                                                                  
rot2 3800.000                                                                                                  
Running ...                                                                                                    
rot2 3804.9900  trig 483                                                                                       
                                                                                                               
Stopping ...                                                                                                   
rot2 3805.000                                                                                                  
Finished (took 0:00:39.723543)                                                                                 
                                                                                                               
MANU [18]:
```

## limascope

**description:** 

limascope records the time of the output signal of a camera plugged into the input trigger of the synchronization board. Lima acquisitions can be started by software or external trigger. The timer can be recorded either by input signal edge or level. This tool can be used to investigate camera readout time, frame frequency, ...
Following signal are used:

signal type | *MUSST* | *MAESTRIO*
----------- | ------- | ----------
input trigger | **Itrig** | **DI1**
external trigger | **OutA** | **DO1**



**usage:** 

`limascope(musst, limadev, expotime, npoints, **kwargs)`

**parameters:**

  - *musst* : musst bliss object
  - *limadev* : lima bliss object
  - *expotime*, *npoints* : parameters of lima acquisition
  - *kwargs [optional]*:
    - *external_trigger [default= False]* : start camera acquisition with one external trigger (on musst **OutA**)
    - *record [default= RISE_AND_FALL]* : specify signal type recording. Can be one of **RISE_AND_FALL**, **RISE**, **FALL** or **HIGH_AND_LOW**
    - *save [default= True]* : save data to file or not

**example:**
```
MANU [20]: from fscan.scope import limascope      
MANU [21]: limascope(musst_bcu2, bcu_mpx_tpxatl25, 0.01, 20, record="RISE_AND_FALL")                           
Scan 175 Fri Jun 11 11:37:37 2021 /nobackup/lid00limace1/scans/manu/data.h5 manu user = blissadm
limascope musst_bcu2 bcu_mpx_tpxatl25 0.01 20

Preparing ...

Running ...
trig 20  bcu_mpx_tpxatl25 20 (S  +0)  

Stopping ...

Finished (took 0:00:00.684139)

Limascope recorded signals on musst:
====================================
                       Mean       Min       Max
--------------  -----------  --------  --------
AcqTime [sec]   0.0102876    0.010287  0.010288
Period [sec]    0.0103001    0.010299  0.010301
DeadTime [sec]  1.24211e-05  1.2e-05   1.3e-05

MANU [22]:  
```



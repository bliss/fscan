
## Description

**fsweep** is a 1-motor continuous scan like **fscan**. Unlike **fscan**, **fsweep** covers all angular space by rewinding the motor for each acquisition. Each acquisition can be viewed as a 1-dimensional scan of a single point. To align the end of one exposure with the start of the following one, triggers are always in *POSITION* and lima detectors' trigger modes are set to *EXTERNAL_GATE*.

This scan is typically used on single crystal diffraction experiment. Particularly, when the exposure time is longer than the readout time and the readout time exceeds the motor rewind time. **Finterlaced** will be faster, if this does not apply.

The lima accumulation mode is not supported.

![fsweep schema](img/fsweep.svg)

## Parameters

| parameter        |  | description |
|------------------|--|-------------|
| *motor* || bliss motor object (not motor name string !!)
| *start_pos* || start position of first acquisition
| *acq_size* || size of one acquisition in position
| *acq_time* |[sec]| acquisition time
| *npoints* || total number of points

Common parameters implemented: *save_flag*, *display_flag*, *sync_encoder*, *home_rotation*, *acc_margin*, *sampling_time*.


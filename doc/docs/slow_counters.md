
Bliss counters controllers, which inherit from *SamplingCounterController*, can be used as slow counters. Typically, they are storage ring current, eurotherms, wagos ... Beside continuous triggered scans, a software timer loop is launched. The frequency of the software loop is driven by the common parameter *sampling_time*. The counter values registered are saved in a separate scan.

To be able to match synchronized counters and slow counters, we can use the *epoch* in software loop and *epoch_trig* from the musst. *epoch_trig* is the epoch time at each point start trigger. It is usually shifted by about 100 msec compared to *epoch*.


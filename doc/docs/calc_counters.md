
The active measurement group may contain bliss calculation counters. These counters will be automatically integrated in fscan, if the counters they depend on are synchronised in the scan. If not, such a calculation counter will be ignored.

Example:

```yaml
- plugin: bliss
  module: expression_based_calc
  class: ExpressionCalcCounter
  name: count_per_pixel
  expression: cnt / (516*516)
  inputs:
    - counter: $bcu_mpx_tpxatl25.roi_counters.counters.all_sum
      tags: cnt
```

This calculation counter depends on the roi counter of the maxipix detector. So if the roi counter is enabled, the calculation counter will be used. Else, it will be ignored.

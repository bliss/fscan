
### Bliss maestrio configuration ###

An example for a typical maestrio configuration for fscan can be:

```yaml
- name: maestrioeu6
  plugin: bliss
  class: Maestrio
  host: maestrioeu6
  channels:
  - channel: CH1
    label: mot1

  - channel: CH2
    label: mot2
    steps_per_unit: 200
    offset: 0.

  - channel: CH3
    switch: $icebcu2_switch
```

The configuration should declare all accessible encoder input, either encoder direcly plug onto the board or via a switch. Following configuration keys can be used:

- **channel** : define which channel input is used (CH1 to CH6)
- **label** : name to reference this channel. for fscan, the associated motor name is used.
- **switch** : if a switch is connected to the channel, give a reference to the bliss switch object (icepap or pi712 switch for example). If *swicth* is used, no **label** has to be defined. The switch object itself will gives the reference names.

Additiionnal configuration related to encoder conversion can be added. Those configuration are used when encoder has a different resolution than the associated motor. These parameters are:

- **steps_per_unit** : steps per unit of the input encoder
- **offset** : offset of the input encoder (this is not the motor offset)
- **modulo** : if the encoder works on a modulo base. For example, absolute encoder on an infinite rotation which delivers position in 360 range only.
- **sign** : sign of the encoder (may be opposite to linked motor direction)
- **unit** : unit string used for display

Refer to [bliss maestrio documentation](https://bliss.gitlab-pages.esrf.fr/bliss/master/config_maestrio.html) for more details.

### Checking encoder input ###

For each motor, fscan expects either encoder or motor steps to be provided by one maestrio channel. To convert these steps to positions, fscan is using in preference order :

- the resolution configured on the maestrio channel
- the motor encoder steps_per_unit parameter (in bliss, *mymotor.encoder.steps_per_unit*)
- the motor steps_per_unit (in bliss, *mymotor.steps_per_unit*) will be used.
The direction of steps recorded on maestrio **musst match the motor dial steps direction**.

A small script is provided for checking that a motor and its assoicated maestrio channel are working properly. It will launch a movement on the motor in both directions and check direction and number of steps seen by the maestrio board. 

```python
FSCANTEST [1]: from fscan.utils import check_motor_on_maestrio
FSCANTEST [2]: check_motor_on_maestrio(maestrioeu6, mot1)
motor resolution   = 200.0 steps/unit
encoder resolution = 200.0 steps/unit

> Search motor on MAESTRIO:
found on channel     = 1
channel type         = QUAD
maestrio resolution  = 200.0 steps/unit

> Move relative +1:
Steps seen on MAESTRIO  = 202 steps

> Move relative -1:
Steps seen on MAESTRIO  = -198 steps

> CHECK RESULT:
All tests OK. You can use this motor/maestrio in fscans.

```

The tool will report, if either the number of steps recorded or the direction is wrong.

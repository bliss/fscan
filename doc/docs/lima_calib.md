## Readout time calibration ##

The aim of calibrating a lima detector is to estimate the readout time needed. Fscan distinguishes 2 cases:

- *internal trigger time* : time between 2 exposures in internal trigger mode. Used when *scan_mode* is set to **CAMERA**
- *external trigger time* : time needed after an exposure before sending next trigger. Used when *scan_mode* is set to **POSITION** or **TIME**

By default lima calibration uses:

- internal_trigger_time = readout_time
- external_trigger_time = readout_time + trigger_latency

*readout_time* is estimated from the detector properties. It depends on the detector type and capabilities. For example, a Frelon detector readout time changes, if the detector is used in frame transfer mode, a Marana detector readout time depends on its overlap mode, ...

*trigger_latency* uses the following default values defined in the fscan code:

| detector<BR>type | code defined<BR>latency |
| ---      | ---      |
| pilatus  | 5e-5     |
| andor3   | 1e-4     |
| pco      | 4e-4     |
| frelon   | 1e-4     |
| perkinelmer| 1e-4   |
| other detectors | 0.|

## Configuration ##

Readout time and/or trigger latency can be changed in the configuration. Add the following section to the fscan config:
```yaml
  lima_calib:
    mymarana:
      trigger_latency: 0.0005
    myfrelon:
      readout_time: 0.026
      trigger_latency: 0.001
    myfrelon2:
      trigger_latency: 0.005
```

Note that detectors are referenced by name (not $mymarana !!).

## Eiger detectors ##

Eiger detectors' readout_time is different, for both external and internal triggering and is proportional to the exposure time. So for this detector two extra properties are defined:

```python
  lima_calib:
    myeiger:
      internal_time_correction: 0.000041
      external_time_correction: 0.00006
```

External and internal trigger times are therefore calculated as follows:

- internal_trigger_time = internal_time_correction * acq_time + 1e-7
- external_trigger_time = external_time_correction * acq_time + 0.0001 + latency_time

## Accessing lima calibration details from bliss ##

We can access lima calibration details from the scan object as follow:
```python
FSCANTEST [8]: ftimescan.validate()
FSCANTEST [9]: ftimescan.master.lima_calib
      Out [9]: Calibration for exposure_time = 0.1 sec
               
               Name                Internal    External    Latency Used
               ----------------  ----------  ----------  --------------
               bcu_mpx_tpxatl25      0.0003      0.0003               0
               
               ==> internal trigger time = 0.0003 sec
               ==> external trigger time = 0.0003 sec
               
```


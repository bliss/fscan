
## Description

**fscan2d** is a mesh-like scan doing continuous scans of one motor (called *fast_motor*) and stepping the other (called *slow_motor*). At the beginning of the scan, all synchronized acquisition devices (lima, mca, ...) are programmed with the total number of points. This lowers the deadtime between continuous segments to only step movement time.

**fscan2d** supports the same scanning modes as **fscan** : TIME, POSITION, CAMERA. Lima devices can be used in accumulation mode, too. In addition to that, it provides a **fast_motor_mode** which can be:

- **REWIND** : the fast motor goes back to the start position at each slow motor step. The continuous (fast) scan is always executed in the same direction.
- **ZIGZAG** : the fast motor does go back to the start position at each step. Thus half of the continuous scan are executed in opposite direction.


## Parameters

| parameter        |  | description |
|------------------|--|-------------|
| *slow_motor* || bliss motor object for stepper motor
| *slow_start_pos* || start position of slow motor
| *slow_step_size* || step size in position of slow motor
| *slow_npoints* || number of step movement performed on slow motor
| *fast_motor* || bliss motor object for continuous movement motor
| *fast_start_pos* || start position
| *fast_step_size* || step in position during one point
| *fast_step_time* |[sec]| time to move one *fast_step_size*. As in **fscan**, it can be set to 0, readout time will then be estimated [see fscan description](usage_fscan.md).
| *fast_npoints* || number of points to acquire in one continuous part
| *acq_time* |[sec] | acquisition time
| *fast_motor_mode* |REWIND,FORWARD | fast motor return mode
| *scan_mode* |TIME,POSITION,CAMERA | scanning mode
| *gate_mode* |TIME,POSITION | gate mode used while scanning in POSITION
| *camera_signal* |EDGE,LEVEL | input camera signal type when in CAMERA mode
| *latency_time* |[sec] | additionnal latency time added to detector readout time
| *scatter_plot* |True,False | ask flint to add a scatter plot or not

Common parameters implemented: *save_flag*, *display_flag*, *sync_encoder*, *home_rotation*, *acc_margin* (named *fast_acc_margin* here), *sampling_time*.

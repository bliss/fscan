
## Lauching Scans ##

Scans can be launched in two ways:

1) direct execution
```
fscan(rot, 90., 0.1, 100, 0.02, scan_mode="TIME")
```
2) set parameters and run
```
fscan.pars.motor = rot
fscan.pars.start_pos = 90.
fscan.pars.step_size = 0.1
fscan.pars.npoints = 100
fscan.pars.acq_time = 0.02
fscan.pars.scan_mode = "TIME"
    
fscan.run()
```

## Accessing Parameters ##

All parameters can be accessed through **fscan.pars**.
All parameters except *save_flag* are stored as settings. *save_flag* always defaults to *True* on bliss session startup. Other parameters keep their values accross bliss restarts.

For parameters like *scan_mode* or *gate_mode*, you can retrieve supported modes using *fscan.pars.scan_mode_available* or *fscan.pars.gate_mode_available*.

Available methods on parameters:

  - *fscan.pars.to_dict()/.from_dict()* : get/set all parameters as a dictionnary.
  - *fscan.pars.to_file()/.from_file()* : load/save all parameters to a yml file.
  - *fscan.pars.reset()* : resets all parameters to default.

## Common Parameters ##

The following parameters are common to all scans:

| parameter | default | description |
|-----------|---------|-------------|
| *save_flag* |*True*| save or not scan data including images
| *display_flag* |*True*| call show() before running scans to print out a resume of scan parameters.
| *sync_encoder* |*True*| synchronize musst encoder value with current motor position at the beginning of scan.
| *home_rotation* |*False*| if motor used in scan is configured as *rotation_motors*, a homing is performed at the preparation phase of the scan. The corresponding musst encoder is synchronized just after homing.
| *acc_margin* |0.| add a margin to computed acceleration displacement. This can be used to adjust acceleration size or to overcome speed overshoot on some controllers.
| *sampling_time* |0.5| time used for software timer loop of slow (or sampling) counters

All these parameters are stored as settings **except save_flag** which always default to **True** on bliss session startup.
  
## Standard Methods ##

All scans offer the same methods :

- *validate()* : validate parameters and computes internal parameters (speed, acceleration, ...).
- *show()* : call *validate()* and display a resume of scan calculated parameters
- *prepare()* : prepare the scan and all its devices
- *start()* : launch the scan
- *run()* : shortcut for prepare() + start()
- *chain_show()* : display the acquisition chain as a tree.

Methods available to ease data checking:

- *data_names()* : list all data channel names registered during last scan run.
- *list_data()* : list all data channel and their size.
- *get_data(data_name)* : return numpy array of asked data.


All scans have common properties:

- *pars* : user parameters
- *inpars* : calculated parameters after *validate()* call
- *scan* : bliss underlying scan object
- *chain* : bliss underlying chain object
- *master* : fscan underlying scan master object

## Scan Metadata ##

Scan parameters are saved as scan metadata. They can be found under the *instrument/fscan_parameters* entry of the scan in the HDF5 file. Users can add metadata providing a *scan_info* dictionnary to the *run()* method.

```
fscan.run(my_scan_info)

OR

fscan.prepare(my_scan_info)
fscan.start()
```

## Saving/Loading complete scan state ##

Two methods exists to load and save full scan state. It means saving/loading of:

- scan parameters
- measurement group active counters
- lima detector state:

    - image parameters (roi, binning, ...)
    - roi counters configuration
    - processing tasks configuration

On loading, the scan parameters are set, lima configuration is restored, and the saved active counters are enabled in measurement group. After loading, save can be executed by launching ```fscan.run()```.

Scan state is saved in a yml file. Location of file is specified when calling load/save functions.
```
fscan.save_scan_state("/data/id15/inhouse1/myexp/xrdct_conf.yml")
...
fscan.load_scan_state("/data/id15/inhouse1/myexp/xrdct_conf.yml")
```

## Diagnostic Methods ##

For scans involving motors (not ftimescan-like), two specific run functions exists:

- *scope_run()* : this will launch the exact motors' movement of the scan and the musst is used as a recorder all along the movement. This can be used to check motor acceleration, speed stability, ...
- *dry_run()* : the scan is executed without any counters/detectors. Only the motor(s) and the musst are used. Triggers are generated on the musst. This can be used to debug musst program for instance.

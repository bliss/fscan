
##Description##

**fscan** is a continuous scan of one motor. The real motor start position is computed so that the motor can reach its desired velocity at requested *start_pos*. Acquisition starts at that position when the motor is at constant speed.

**fscan** supports three scanning mode:

- **TIME** : first trigger in position, next ones in time
- **POSITION** : all triggers are in position
- **CAMERA** : (_only when using one lima device_) first trigger in position, then all counters follow camera image acquisition.

See [scanning mode details](synchro_signal.md).

By default, lima devices are set to *EXTERNAL_TRIGGER_MULTI* mode and all other synchronized devices (mca, p201, ...) are used in *GATE* mode, generated in *TIME*. Alternatively, the parameter **gate_mode** can bet set to *POSITION*. In that case, start and end of the gate are generated using motor position and all devices, including lima ones, are programmed in *GATE* mode.

The camera readout time can be handled in two ways:

- by setting *step_time* : this will fix the time between two image start triggers. 
  ![fscan paramaters](img/fscan_pars.svg)
- by setting *step_time=0.* : fscan will sum estimated camera readout time and *latency_time* to define the real *step_time*.
  ![fscan paramaters_with_readout_time](img/fscan_pars_rotime.svg)



##Parameters##

| parameter        |    | description |
|------------------|----|-------------|
| *motor* | | bliss motor object (not motor name string !!)
| *start_pos* | | start position of first acquisition
| *step_size* | | step size in position of one acquisition
| *step_time* | [sec] | if set to non-zero, fix time between start triggers
| *acq_time* | [sec] | acquisition time per point
| *npoints* | | total number of points
| *scan_mode* | TIME,POSITION,CAMERA | scanning mode
| *gate_mode* | TIME,POSITION | gate mode used while scanning mode is in POSITION.
| *camera_signal* | EDGE,LEVEL | input camera signal type when in CAMERA mode.
| *latency_time* | [sec] | additionnal latency time added to detector readout time

Common parameters implemented: *save_flag*, *display_flag*, *sync_encoder*, *home_rotation*, *acc_margin*, *sampling_time*.


### Bliss musst configuration ###

An example for a typical musst configuration for fscan can be:

```yaml
name: musst_tdxrd
class: musst
plugin: generic
gpib:
  url: tango_gpib_device_server://id11/gpib/lid112_0
  pad: 14
  timeout: 10.
musst_prg_root: /musst_prog
channels:
  - label: diffrz
    type: encoder
    channel: 1
  - label: diffry
    type: encoder
    channel: 2
  - type: switch
    channel: 3
    name: $iceid111_switch
  - type: switch
    channel: 4
    name: $iceid113_switch
  - type: adc10
    channel: 5
    label: mch5
    counter_name: mch5
  - type: adc10
    channel: 6
    label: mch6
    counter_name: mch6
```

Configuration keywords:

  - **type = encoder** : corresponds to an encoder plugged into a musst input. It can come from the encoder itself or from the icepap driver board front panel. The *label* field **musst be** the motor name so that musst can locate the encoder. If using a motor alias in your bliss session, this *label* must be set to the real motor name, not to the alias name.
  
  - **type = switch** : the reference to the switch is set in the *name* field. In the case of an *icepap*, the cable connected to the musst channel comes from the icepap rack main module DB9 connector. It allows to send encoder or motor steps of all the motors configured on this rack. The field *label* must not be declared. To avoid confusing the fscan encoder lookup stage, **it is recommended to configure switches after direct encoder channels**.
  
  - **other types** : adc / counters can be configured as stated in the bliss documentation.

For types **encoder** and **switch**, musst channels must be configured as **ENC**. Use the following command:
```python
  mymusst.putget("CHCFG CH1 ENC")
```

If needed, mainly in case of direct encoder connection, the encoder signal can be inverted at the musst channel level:
```python
  mymusst.putget("CHCFG CH2 ENC INV")
```
See the [musst documentation](https://confluence.esrf.fr/display/EUWK/MUSST) for more details.


### Checking encoder input ###

For each motor, fscan expects either encoder or motor steps to be provided by one musst channel. To convert these steps to positions, fscan is using the motor encoder steps_per_unit parameter, if it exists (in bliss, *mymotor.encoder.steps_per_unit*), otherwise the motor steps_per_unit (in bliss, *mymotor.steps_per_unit*) will be used.
The direction of steps recorded on musst **musst match the motor dial steps direction**.

A small script is provided for checking that a motor and its musst channel are working properly. It will launch a movement on the motor in both directions and check direction and number of steps. 

```python
FSCANTEST [1]: from fscan.utils import check_motor_on_musst
FSCANTEST [3]: check_motor_on_musst(musst1, mot2, 1.)
motor resolution   = 200.0 steps/unit
encoder resolution = 200.0 steps/unit

> Search motor on MUSST:
found on channel  = 2
channel type      = ENC

> Move relative +1:
Moving mot2 from -3.55 to -2.55
Steps seen on MUSST  = 200 steps

> Move relative -1:
Moving mot2 from -2.55 to -3.55
Steps seen on MUSST  = -200 steps

> CHECK RESULT:
All tests OK. You can use this motor/musst in fscans.
```

The tool will report, if either the number of steps recorded or the direction is wrong.

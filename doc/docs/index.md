
The Fscan module adds new types of continuous scans to bliss. These scans have originally been developed for ID15 and extended on ID11. They were developed mainly for diffraction experiments, focusing on 2D detector integration and performance. As much as possible, they have been written in a portable way and can be used on other beamlines. Some of the scans rely on specific hardware (like aeroscans, which need an aerotech motor controller).

All scans have a, where possible, common parameters, which have the same meaning across scans.

The scans are using a **musst** or a **maestrio** board for synchronisation. **lima** detectors, **mca** dectectors and **p201** counter/timer boards, if present, are synchronized at each point. Any other counters can be acquired in a software loop started in parallel with the main scan. Thus, a **musst** or a **maestrio** board is mandatory. Each scan can be run with only one lima device or only one p201, or mixing lima, mca and p201 for example.  
Standard bliss scan or chain presets can be used for signal multiplexing, shutter management, etc.

In addition to the scans, some tools based on the **musst** or **maestrio** have been written to help investigating motor or camera signals.

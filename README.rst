=============
fscan project
=============

Tools for continuous scans using 2D detectors
Used to build custom scans on ID15A/B, ID11, ID22, ID10

Latest documentation from master can be found [here](https://bliss.gitlab-pages.esrf.fr/fscan).

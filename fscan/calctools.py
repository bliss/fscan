import numpy
from bliss.scanning.chain import AcquisitionSlave, AcquisitionChannel
from bliss.scanning.acquisition.calc import CalcAcquisitionSlaveBase

class Spectrum2ScatterCalc(CalcAcquisitionSlaveBase):
    def __init__(
            self, 
            axis_channel,
            axis_id,
            spectrum_channel, 
            spectrum_size, 
            prepare_once=False, 
            start_once=False
    ):
        super().__init__(
            None,
            name=f"scatter_{spectrum_channel.short_name}",
            trigger_type=AcquisitionSlave.HARDWARE,
            prepare_once=prepare_once,
            start_once=start_once,
        )

        self.axis_channel = axis_channel
        self.axis_id = int(axis_id)
        self.spectrum_channel = spectrum_channel
        self.spectrum_size = spectrum_size
        dataname = spectrum_channel.short_name
        self.calc_names = dict()
        axis_direction = axis_id == 1 and "y" or "x"
        self.calc_names["axis"] = f"{dataname}_{axis_direction}"
        spec_direction = axis_id == 1 and "x" or "y"
        self.calc_names["spectrum_index"] = f"{dataname}_{spec_direction}"
        self.calc_names["spectrum_value"] = f"{dataname}_value"

        self.channels.append(AcquisitionChannel(self.calc_names["axis"], numpy.float64, ()))
        self.channels.append(AcquisitionChannel(self.calc_names["spectrum_index"], numpy.int32, ()))
        self.channels.append(AcquisitionChannel(self.calc_names["spectrum_value"], numpy.int32, ()))

    def _iter_input_channels(self):
        yield from [self.axis_channel, self.spectrum_channel]

    def compute(self, sender, input_data: dict):
        axis_arr = None
        axis_data = input_data.get(self.axis_channel.short_name)
        if axis_data is not None:
            axis_arr = numpy.array([], numpy.float64)
            for value in axis_data:
                axis_arr = numpy.append(axis_arr, numpy.repeat(value, self.spectrum_size))

        spec_arr = None
        index_arr = None
        spec_data = input_data.get(self.spectrum_channel.short_name)
        if spec_data is not None:
            nspec = spec_data.shape[0]
            spec_arr = spec_data.ravel()
            index_arr = numpy.empty(spec_data.shape, numpy.int32)
            index_arr[:,:] = numpy.arange(0, spec_data.shape[1], 1, numpy.int32)
            index_arr = index_arr.ravel()

        return { self.calc_names["axis"]: axis_arr,
                 self.calc_names["spectrum_index"]: index_arr,
                 self.calc_names["spectrum_value"]: spec_arr,
        }

    def _prepare_calc_input(self, event_dict, signal, sender):
        input_data = super()._prepare_calc_input(event_dict, signal, sender)
        return {sender.short_name: input_data}

    def get_channel_name(self, tag):
        return self.calc_names.get(tag)

class LimaRoiProfile2ScatterCalc(Spectrum2ScatterCalc):
    def __init__(self, axis_channel, roi, roi_channel):
        if roi.mode == "horizontal":
            roi_size = roi.width
            axis_id = 1
        else:
            roi_size = roi.height
            axis_id = 0
        super().__init__(axis_channel, axis_id, roi_channel, roi_size)


import tabulate

from bliss.scanning.scan import ScanPreset
from bliss.scanning.chain import ChainPreset
from .ftimescan import FTimeScanMaster
from .ftimescanlookup import FTimeScanLookupMaster
from .fscan import FScanMaster
from .fscan2d import FScan2DMaster
from .fscan3d import FScan3DMaster
from .f2scan import F2ScanMaster
from .flyscan import FlyScanMaster
from .finterlaced import FInterlacedMaster
from .fsweep import FSweepMaster
from .fscanloop import FScanLoopMaster
from .mtimescan import MTimeScanMaster

def _get_list(value):
    try:
        iter(value)
        return list(value)
    except TypeError:
        return [value,]

class FScanConfig(object):
    DefaultMasterClass = {
       "ftimescan": FTimeScanMaster,
       "ftimescanlookup": FTimeScanLookupMaster,
       "fscan": FScanMaster,
       "fscan2d": FScan2DMaster,
       "fscan3d": FScan3DMaster,
       "f2scan": F2ScanMaster,
       "flyscan": FlyScanMaster,
       "finterlaced": FInterlacedMaster,
       "fsweep": FSweepMaster,
       "fscanloop": FScanLoopMaster,
       "mtimescan": MTimeScanMaster,
    }

    def __init__(self, name, config):
        self.name = name
        self.config = config.to_dict()

        # --- check needed keys
        if "devices" not in self.config:
            raise ValueError(f"Missing \"devices\" section for FScanConfig[{name}]")

        syncdevs = list()
        if "musst" in self.config["devices"]:
            # backward compatibility
            musstdevs = _get_list(self.config["devices"]["musst"])
            syncdevs += musstdevs
            self.config["devices"]["musst"] = musstdevs
        if "synchro" in self.config["devices"]:
            syncdevs += _get_list(self.config["devices"]["synchro"])
        if not len(syncdevs):
            raise ValueError(f"Missing \"synchro\" key in \"devices\" section for FScanConfig[{name}]")
        self.config["devices"]["synchro"] = syncdevs

        if "measgroup" not in self.config["devices"]:
            self.config["devices"]["measgroup"] = None
        if "fast_measgroup" not in self.config["devices"]:
            self.config["devices"]["fast_measgroup"] = None

        rotmots = self.config["devices"].get("rotation_motors", list())
        self.config["devices"]["rotation_motors"] = _get_list(rotmots)

        extmots = self.config["devices"].get("external_motors", tuple())
        self.config["devices"]["external_motors"] = _get_list(extmots)

        self.config["chain_config"] = self.config.get("chain_config", dict())
        self.config["lima_calib"] = self.config.get("lima_calib", dict())

        self.__masters = dict()
        self.__runners = dict()
        self.__scan_presets = dict()
        self.__chain_presets = dict()

        self.__master_class = dict()
        for (scan_name, scan_class) in FScanConfig.DefaultMasterClass.items():
            self.add_scan(scan_name, scan_class)

    def list_scans(self):
        values = [ (scan_name, scan_class.__name__) 
                   for (scan_name, scan_class) in self.__master_class.items() 
        ]
        headers = ["scan name", "scan class"]
        print(tabulate.tabulate(values, headers=headers))

    def add_scan(self, name, master_class):
        if name in self.__master_class:
            raise RuntimeError(f"Scan called {name} already existing !!")
        self.__master_class[name]= master_class

    def __getattr__(self, name):
        if name in self.__master_class:
            return self.get_runner(name)
        elif name in ["{scan}_master" for scan in self.__master_class]:
            return self.get_master(name)
        else:
            return object.__getattr__(self, name)

    def add_scan_preset(self, preset, name=None):
        if not isinstance(preset, ScanPreset):
            raise ValueError("preset should be an instance of ScanPreset !!")
        preset_name = id(preset) if name is None else name
        self.__scan_presets[preset_name] = preset
        for runner in self.__runners.values():
            runner.add_scan_preset(preset, preset_name)

    def remove_scan_preset(self, name):
        if name in self.__scan_presets:
            self.__scan_presets.pop(name)
            for runner in self.__runners.values():
                runner.remove_scan_preset(name)

    def add_chain_preset(self, preset, name=None):
        if not isinstance(preset, ChainPreset):
            raise ValueError("preset should be an instance of ChainPreset !!")
        preset_name = id(preset) if name is None else name
        self.__chain_presets[preset_name] = preset
        for runner in self.__runners.values():
            runner.add_chain_preset(preset, preset_name)

    def remove_chain_preset(self, name):
        if name in self.__chain_presets:
            self.__chain_presets.pop(name)
            for runner in self.__runners.values():
                runner.remove_chain_preset(name)

    def get_runner(self, name, klass=None, *args):
        runner = self.__runners.get(name, None)
        if runner is not None and klass is not None:
            if runner.__class__!=klass:
                runner = None
        if runner is None:
            master = self.__get_master(name)
            if klass is None:
                klass = master.get_runner_class()
            runner = klass(name, master, *args)
            self.__runners[name] = runner
            for (name, scan_preset) in self.__scan_presets.items():
                runner.add_scan_preset(scan_preset, name)
            for (name, chain_preset) in self.__chain_presets.items():
                runner.add_chain_preset(chain_preset, name)
        return runner

    def __get_master(self, name):
        master = self.__masters.get(name, None)
        if master is None:
            master_name = f"{self.name}_{name}"
            master = self.__master_class[name](master_name, self.config)
            self.__masters[name] = master
        return master


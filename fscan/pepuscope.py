
import time
import numpy

from bliss.scanning.acquisition.timer import SoftwareTimerMaster
from bliss.scanning.acquisition.pepu import PepuAcquisitionSlave
from bliss.scanning.acquisition.calc import CalcCounterAcquisitionSlave
from bliss.scanning.acquisition.motor import JogMotorMaster
from bliss.controllers.pepu import PEPU, PepuCalcController, Trigger, Signal
from bliss.scanning.chain import AcquisitionChain, AcquisitionMaster
from bliss.scanning.channel import AcquisitionChannel
from bliss.scanning.scan import Scan
from bliss.scanning.scan_info import ScanInfo
from bliss.scanning.chain import attach_channels
from bliss.common.measurementgroup import get_active

from fscan.chaintools import ChainTool
from fscan.fscantools import FScanDisplay
from fscan.motortools import calc_move_time, MotorMaster

class PepuSoftAcqMaster(AcquisitionMaster):
    def __init__(self, npoints, frequency, name="pepu", **keys):
        AcquisitionMaster.__init__(self, name=name, **keys)
        self.channels.append(
            AcquisitionChannel(f"{self.name}:elapsed_time", float, (), unit="s")
        )
        self.channels.append(
            AcquisitionChannel(f"{self.name}:epoch", float, (), unit="s")
        )
        self._npoints = npoints
        self._frequency = frequency

    def prepare(self):
        pass

    def start(self):
        if self.parent is None:
            self.trigger()

    def trigger(self):
        self.trigger_slaves()
        start_epoch = time.time()
        sample_time = numpy.arange(self._npoints, dtype=float) * (1./self._frequency)
        self.channels[0].emit(sample_time)
        self.channels[1].emit(start_epoch+sample_time)
 
    def stop(self):
        pass

def _find_pepu_in_active_mg():
    measgroup = get_active()
    chaintool = ChainTool(get_active())
    ctrls = chaintool.get_controllers()
    pepu = None
    pepucalc = None
    for ctrl in ctrls:
        if isinstance(ctrl, PEPU):
            pepu = ctrl
        elif isinstance(ctrl, PepuCalcController):
            pepucalc = ctrl
    if not pepu:
        raise RuntimeError("No PEPU device found in active MG !!")
    if pepucalc:
        if not _check_calc_inputs(pepu, pepucalc):
            print(f"WARNING: pepu calc {pepucalc.name} is not using only {pepu.name}. Will not use it.")
            pepucalc = None
    return (pepu, pepucalc)

def _check_calc_inputs(pepu, pepucalc):
    pepucnts = pepu._counters.values()
    for cnt in pepucalc._input_counters:
        if cnt not in pepucnts:
            return False
    return True
    
def peputimescan(frequency, duration, pepu=None, pepucalc=None, **kwargs):
    name = "peputimescan"

    # --- parameters
    run_flag = kwargs.pop("run", True)
    save_flag = kwargs.pop("save", True)
    if len(kwargs):
        raise ValueError("Invalid argument : {0}".format(list(kwargs.keys())))

    # --- find pepu / pepu calc controllers
    if pepu is None:
        (pepu, pepucalc) = _find_pepu_in_active_mg()

    # --- soft master
    npoints = int(frequency * duration)
    soft_master = PepuSoftAcqMaster(npoints, frequency)

    # --- pepu slave
    pepu_slave = PepuAcquisitionSlave(*pepu.counters, 
                                      npoints=npoints,
                                      start=Signal.SOFT,
                                      trigger=Signal.FREQ,
                                      frequency=frequency
    )
    trigger_channel = pepu_slave.channels[0]

    # --- acquisition chain
    chain = AcquisitionChain()
    chain.add(soft_master, pepu_slave)

    # --- pepu calc
    if pepucalc:
        pepucalc_slave = CalcCounterAcquisitionSlave(pepucalc, [pepu_slave,], {"npoints": npoints})
        for cnt in pepucalc.outputs:
            pepucalc_slave.add_counter(cnt)
        chain.add(soft_master, pepucalc_slave)

    # --- scan info
    scan_info = ScanInfo()
    scan_info["title"] = f"{name} {pepu.name} {frequency} {duration}"
    scan_info["type"] = name
    scan_info["npoints"] = npoints
    scan_info["count_time"] = 1./frequency

    pars_dict = dict()
    pars_dict["frequency"] = frequency
    pars_dict["duration"] = duration
    scan_info.setdefault("instrument", {})
    scan_info["instrument"]["fscan_parameters"] = pars_dict

    # --- scan
    scan = Scan(chain, name=name, scan_info=scan_info, save=save_flag,
                scan_progress=FScanDisplay(trigger_channel_name=trigger_channel.name))
    if run_flag is True:
        scan.run()
    else:
        return scan

def pepumotorscan(motor, startpos, displacement, speed, frequency, extra_time=0.4, jog=False, pepu=None, pepucalc=None, **kwargs):
    name = "pepumotorscan"

    # --- parameters
    run_flag = kwargs.pop("run", True)
    save_flag = kwargs.pop("save", True)
    if len(kwargs):
        raise ValueError("Invalid argument : {0}".format(list(kwargs.keys())))

    # --- find pepu / pepu calc controllers
    if pepu is None:
        (pepu, pepucalc) = _find_pepu_in_active_mg()

    # --- motor master
    speed = abs(speed)
    move_time = calc_move_time(motor, abs(displacement), speed)
    stoppos = startpos + displacement
    timepos = abs(displacement) / speed

    if not jog:
        mot_master = MotorMaster(motor, startpos, stoppos, timepos, undershoot=0)

    # --- pepu slave
    tot_time = move_time + extra_time
    npoints = int(frequency * tot_time)
    soft_master = PepuSoftAcqMaster(npoints, frequency)
    pepu_slave = PepuAcquisitionSlave(*pepu.counters,
                                      npoints=npoints,
                                      start=Signal.SOFT,
                                      trigger=Signal.FREQ,
                                      frequency=frequency
    )
    trigger_channel = pepu_slave.channels[0]

    # --- jog master if needed
    if jog:
        def pepu_end_jog(*args):
            pepu_finished = pepu_slave.read_done
            return not pepu_finished

        if displacement < 0:
            jog_speed = -1. * speed
        else:
            jog_speed = speed
        mot_master = JogMotorMaster(motor, startpos, jog_speed, undershoot=0,
                                    end_jog_func=pepu_end_jog, polling_time=0.1)
    
    # --- acquisition chain
    chain = AcquisitionChain()
    chain.add(mot_master, soft_master)
    chain.add(soft_master, pepu_slave)

    # --- pepu calc
    if pepucalc:
        pepucalc_slave = CalcCounterAcquisitionSlave(pepucalc, [pepu_slave,], {"npoints": npoints})
        for cnt in pepucalc.outputs:
            pepucalc_slave.add_counter(cnt)
        chain.add(soft_master, pepucalc_slave)

    # --- scan info
    scan_info = ScanInfo()
    jog_txt = jog and "jog" or ""
    scan_info["title"] = f"{name} {pepu.name} {motor.name} {startpos} {displacement} {speed} {frequency} {extra_time} {jog_txt}"
    scan_info["type"] = name
    scan_info["npoints"] = npoints
    scan_info["count_time"] = 1./frequency

    pars_dict = dict()
    pars_dict["motor"] = motor.name
    pars_dict["startpos"] = startpos
    pars_dict["stoppos"] = stoppos
    pars_dict["displacement"] = displacement
    pars_dict["speed"] = speed
    pars_dict["frequency"] = frequency
    pars_dict["extra_time"] = extra_time
    pars_dict["jog"] = jog
    scan_info.setdefault("instrument", {})
    scan_info["instrument"]["fscan_parameters"] = pars_dict

    # --- scan
    scan = Scan(chain, name=name, scan_info=scan_info, save=save_flag,
                scan_progress=FScanDisplay(trigger_channel_name=trigger_channel.name))
    if run_flag is True:
        scan.run()
    else:
        return scan

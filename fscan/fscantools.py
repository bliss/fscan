
from ruamel.yaml import YAML
import numpy
import datetime

from bliss.config.settings import HashObjSetting
from bliss.scanning.scan_progress import ScanProgress
from bliss.scanning.scan import ScanState
from bliss import setup_globals
from bliss.controllers.lima.lima_base import Lima
from bliss.controllers.lima2.controller import DetectorController as Lima2
from bliss.controllers.mca.base import BaseMCA
from bliss.controllers.mosca.base import McaCounterController
from bliss.controllers.musst import Musst
from bliss.controllers.maestrio import Maestrio
from bliss.shell.formatters.ansi import BOLD


class FScanModeBase(object):
    def __init__(self, name, *args):
        self.__name = name
        self.__values = args
        for arg in args:
            setattr(self, arg, arg)

    def __dir__(self):
        return self.__values + ("get", "values", "name")

    def __repr__(self):
        retstr = object.__repr__(self)
        retstr += "\nPossible Value : {0}".format(self.__values)
        return retstr

    def get(self, value, name=None):
        retval = str(value).upper()
        if retval not in self.__values:
            raise ValueError(
                "{0} can only be one of {1}".format(name or self.__name, self.__values)
            )
        return retval

    def index(self, value):
        return self.__values.index(self.get(value))

    @property
    def values(self):
        return self.__values

    @property
    def name(self):
        return self.__name

FScanMode = FScanModeBase("FScanMode", "TIME", "POSITION", "CAMERA")
FTimeScanMode = FScanModeBase("FTimeScanMode", "TIME", "CAMERA", "EXTSTART")
FScanTrigMode = FScanModeBase("FScanTrigMode", "TIME", "POSITION")
FScanAxisMode = FScanModeBase("FScanAxisMode", "REWIND", "ZIGZAG")
FScanCamSignal = FScanModeBase("FScanCamSignal", "EDGE", "RISE", "FALL", "LEVEL")
FScanCamTrigger = FScanModeBase("FScanCamTrigger", "TRIGGER", "GATE")
FScanLoopMode = FScanModeBase(
    "FScanLoopMode", "NONE", "FORWARD", "REWIND", "ZIGZAG", "EXTERNAL"
)
FScanLoopTrigMode = FScanModeBase(
    "FScanLoopTrigMode", "INTERNAL", "EXTERNAL", "LIMA_READY"
)
FScanLoopLimaMode = FScanModeBase(
    "FScanLoopLimaMode", "PER_SCAN", "PER_LOOP"
)


class FScanParamBase(object):
    def __init__(self, name, default_dict, obj_keys=list(), value_list=dict(), no_settings=list()):
        self._name = f"{name}_pars"
        self._default = default_dict
        self._obj_keys = obj_keys
        self._val_list = value_list
        self._no_settings = no_settings
        self._cache = dict(self._default)

        self._par_keys = list(self._default.keys())
        self._list_keys = ["{0}_available".format(name) for name in self._val_list.keys()]
        self.__init_settings()

    def __init_settings(self):
        self._settings = HashObjSetting(self._name)
        settings_keys = self._settings.keys()
        for key in settings_keys:
            if key in self._default and key not in self._no_settings:
                try:
                    self._cache[key] = self.__str_2_obj(key, self._settings[key])
                except ValueError:
                    self._cache[key] = None
            else:
                self._settings.remove(key)
        for key in self._cache.keys():
            if key not in settings_keys and key not in self._no_settings:
                self._settings[key] = self.__obj_2_str(key, self._cache[key])

    def __dir__(self):
        return self._par_keys + self._list_keys + [
            "show",
            "set",
            "get",
            "to_dict",
            "from_dict",
            "reset",
            "to_file",
            "from_file",
        ]

    def __getattribute__(self, name):
        if name.startswith("_"):
            return object.__getattribute__(self, name)
        elif name.endswith("_available"):
            parname = name[:name.index("_available")]
            return self._val_list[parname]
        else:
            try:
                return self._cache[name]
            except KeyError:
                return object.__getattribute__(self, name)

    def __setattr__(self, name, value):
        if name.startswith("_"):
            return object.__setattr__(self, name, value)
        elif name.endswith("_available"):
            raise KeyError("{0} is not writable".format(name))
        else:
            self.__set_value(name, value)

    def __set_value(self, name, value):
        if name not in self._default:
            raise KeyError("{0} has no parameter [{1}]".format(self._name, name))
        validate = getattr(self, "_validate_{0}".format(name), None)
        if validate:
            value = validate(value)
        if name in self._val_list:
            if value not in self._val_list[name]:
                raise ValueError("{0} can only be one of {1}".format(name, self._val_list[name]))
        if name not in self._no_settings:
            self._settings[name] = self.__obj_2_str(name, value)
        self._cache[name] = value

    def __info__(self):
        return self._get_display_str()

    def _get_display_str(self):
        sstr = f"Parameters [redis-key={self._name}]\n\n"
        key_len = max((0,) + tuple(len(x) for x in self._cache.keys()))
        for (key, val) in self._cache.items():
            sstr += "  .{0:<{1}s} = {2}\n".format(
                key, key_len, self.__obj_2_str(key, val)
            )
        return sstr

    def __obj_2_str(self, name, value):
        if name in self._obj_keys and value is not None:
            try:
                return value.name
            except AttributeError:
                raise ValueError(
                    "Should give bliss object not string for [{0}]".format(name)
                )
        return value

    def __str_2_obj(self, name, value):
        if name in self._obj_keys and value is not None:
            try:
                return getattr(setup_globals, str(value))
            except AttributeError:
                raise ValueError(
                    "Could not get bliss object [{0}] for [{1}]".format(value, name)
                )
        return value

    def show(self):
        print(self._get_display_str())

    def set(self, *args, **kwargs):
        for (name, value) in zip(list(self._default.keys()), list(args)):
            self.__set_value(name, value)
        for (name, value) in kwargs.items():
            self.__set_value(name, value)

    def get(self, *args):
        if not len(args):
            return list(self._cache.values())
        else:
            return [self._cache[key] for key in args]

    def to_dict(self, string_format=False):
        if string_format is False:
            return dict(self._cache)
        else:
            ret_dict = dict(self._cache)
            for (name, value) in ret_dict.items():
                if name in self._obj_keys:
                    ret_dict[name] = self.__obj_2_str(name, value)
                if name == "motor_lookup":
                    ret_dict[name] = "lookup"
            else:
                if isinstance(value, tuple):
                    ret_dict[name] = list(value)
                if isinstance(value, numpy.ndarray):
                    ret_dict[name] = value.tolist()
            return ret_dict

    def from_dict(self, pars_dict, string_format=False):
        for (name, value) in pars_dict.items():
            if string_format is True:
                value = self.__str_2_obj(name, value)
            self.__set_value(name, value)

    def reset(self):
        self._cache = dict(self._default)
        for (name, value) in self._cache.items():
            self._settings[name] = self.__obj_2_str(name, value)

    def to_file(self, filename):
        yaml_dict = self.to_dict(string_format=True)
        yaml = YAML()
        yaml.default_flow_style = False
        with open(filename, "w") as yaml_file:
            yaml.dump(yaml_dict, yaml_file)

    def from_file(self, filename):
        yaml = YAML()
        with open(filename) as yaml_file:
            yaml_dict = yaml.load(yaml_file)
        self.from_dict(yaml_dict, string_format=True)


class FScanParamStruct(object):
    def __init__(self, pars_dict=None):
        self.__keys = list()
        if pars_dict is not None:
            self.from_dict(pars_dict)

    def __setattr__(self, name, value):
        if not name.startswith("_"):
            if name not in self.__keys:
                self.__keys.append(name)
        object.__setattr__(self, name, value)

    def __info__(self):
        return "\n\n" + self._get_display_str()

    def add(self, prename, params):
        if not isinstance(params, FScanParamStruct):
            raise RuntimeError("FScanParamStruct.add: params should be a FScanParamStruct")
        pars_dict = params.to_dict()
        for (name, value) in pars_dict.items():
            setattr(self, f"{prename}_{name}", value)
            
    def from_dict(self, pars_dict):
        for (name, value) in pars_dict.items():
            setattr(self, name, value)

    def to_dict(self):
        ret = dict()
        for key in self.__keys:
            ret[key] = getattr(self, key)
        return ret

    def _get_display_str(self):
        sstr = ""
        key_len = max((0,) + tuple(len(x) for x in self.__keys))
        for key in self.__keys:
            sstr += "  .{0:<{1}s} = {2}\n".format(key, key_len, getattr(self, key))
        return sstr


SCAN_STATE_MSG = {
    ScanState.IDLE: "Preparing",
    ScanState.PREPARING: "Preparing",
    ScanState.STARTING:  "Running",
    ScanState.STOPPING:  "Stopping",
    ScanState.USER_ABORTED: "Stopping",
    ScanState.KILLED: "Stopping",
    ScanState.DONE: "Finished",
}

class FScanDisplayBase(ScanProgress):
    def __init__(self, tracked_channels=None):
        super().__init__(tracked_channels)
        self._frame_rate = 6
        self._start_time = datetime.datetime.now()

    def print_header(self):
        self._start_time = datetime.datetime.fromisoformat(self.scan_info["start_time"])
        if self.scan_info["filename"] is None:
            save_text = "NO SAVING"
        else:
            save_text = "Saving in " + self.scan_info["filename"]
        start_time = self._start_time.strftime("%c")
        scan_nb = self.scan_info["scan_nb"]
        scan_title = self.scan_info["title"]

        print(f"Scan {scan_nb} {start_time} {save_text}\n{scan_title}\n")

    def scan_new_callback(self):
        self.print_header()

    def scan_end_callback(self):
        txt = self.build_display_line()
        print(txt, end="")

        elapsed = datetime.datetime.now(self._start_time.tzinfo) - self._start_time
        msg = "\nFinished (took {0})\n".format(elapsed)
        print(msg)

    def progress_callback(self):
        if self.scan_state in (ScanState.PREPARING, ScanState.IDLE):
            self._update_axes_pos()

        txt = self.build_display_line()
        print(txt, end="")

    def build_display_line(self):
        return ""

    def build_axis_string(self, chan):
        return f" {BOLD(chan.short_name)} {self.data.get(chan.name):8.4f}"

    def build_lima_string(self, cam):
        last_acq = self.data.get(f"{cam.name}:last_image_ready", -1) + 1
        txt = f" {BOLD(cam.name)} {last_acq}"

        if self.scan_info.get("save", False):
            last_saved = self.data.get(f"{cam.name}:last_image_saved", -1) + 1
            diff_saved = last_saved - last_acq
            txt += f" (S {diff_saved:+3d})"
        else:
            txt += f" (S ----)"
        return txt

    def build_lima2_string(self, cam, trig_counts=None):
        nb_frames_acquired = self.data.get(f"{cam.name}:nb_frames_acquired", 0)
        nb_frames_xferred = self.data.get(f"{cam.name}:nb_frames_xferred", 0)
        nb_frames_processed = self.data.get(f"{cam.name}:nb_frames_processed", 0)
        nb_frames_saved = self.data.get(f"{cam.name}:nb_frames_saved", -1)
        last_acq = max(nb_frames_acquired, nb_frames_xferred)

        diff_pro = min(nb_frames_processed - last_acq, 0)
        txt = f" {BOLD(cam.name)} {last_acq} (P {diff_pro:+3d}"

        if self.scan_info.get("save", False):
            diff_saved = 0
            if nb_frames_saved > 0:
                # processing saving
                diff_saved = min(nb_frames_saved - last_acq, 0)
            else:
                # detector saving
                if trig_counts is not None:
                    diff_saved = min(last_acq - trig_counts, 0)
            txt += f" S {diff_saved:+3d})"
        else:
            txt += f" S ----)"
        return txt

    def build_mca_string(self, mca):
        last_pixel = self.data.get(f"{mca.name}:last_pixel_triggered", -1) #+ 1
        return f" {BOLD(mca.name)} {last_pixel}"

    def build_mosca_string(self, mosca):
        last_pixel = self.data.get(f"{mosca.name}:curr_pixel", -1)
        return f" {BOLD(mosca.name)} {last_pixel}"

class FScanDisplay(FScanDisplayBase):

    def __init__(self, trigger_channel_name=None, tracked_channels=None):
        super().__init__(tracked_channels)
        self._trigger_name = trigger_channel_name
        self._trigger_counts = 0
        self._limas = list()
        self._lima2s = list()
        self._mcas  = list()
        self._moscas = list()

    def scan_init_callback(self):
        for acq in self.acq_objects:
            if isinstance(acq.device, Lima):
                self._limas.append(acq.device)
            elif isinstance(acq.device, Lima2):
                self._lima2s.append(acq.device)
            elif isinstance(acq.device, BaseMCA):
                self._mcas.append(acq.device)
            elif isinstance(acq.device, McaCounterController):
                self._moscas.append(acq.device)
            elif isinstance(acq.device, Musst) or isinstance(acq.device, Maestrio):
                if self._trigger_name is None:
                    self._trigger_name = f"{acq.device.name}:timer_trig"

        if self._trigger_name is not None:
            self._tracked_channels.append(self._trigger_name)

    def build_display_line(self):
        status = f"{SCAN_STATE_MSG.get(self.scan_state, '')}:"
        txt = f"{status}"

        for ch in self._axes_chan2acq:
            txt += self.build_axis_string(ch)

        if self.scan_state in (ScanState.PREPARING, ScanState.IDLE):
            return txt + "\r"

        if self._trigger_name:
            txt += f" {BOLD('trig')} {self._trigger_counts}"

        for cam in self._limas:
            txt += self.build_lima_string(cam)

        for cam in self._lima2s:
            txt += self.build_lima2_string(cam, self._trigger_counts)

        for mca in self._mcas:
            txt += self.build_mca_string(mca)

        for mosca in self._moscas:
            txt += self.build_mosca_string(mosca)

        # to clear end of line
        txt += " "*4
        return txt + "\r"

    def new_data_received(self, data=None, signal=None, sender=None):
        super().new_data_received(data, signal, sender)

        if sender.name == self._trigger_name:
            self._trigger_counts += len(data)

class MScanDisplay(FScanDisplayBase):

    def __init__(self, fast_trigger_name, fast_acq_nodes, slow_trigger_name, slow_acq_nodes, tracked_channels=None):
        super().__init__(tracked_channels)
        self._trigger_names = dict(fast=fast_trigger_name, slow=slow_trigger_name)
        self._trigger_count = dict(fast=0, slow=0)
        self._tracked_channels.append(fast_trigger_name)
        self._tracked_channels.append(slow_trigger_name)

        self._limas = dict(fast=list(), slow=list())
        self._lima2s = dict(fast=list(), slow=list())
        self._mcas = dict(fast=list(), slow=list())
        self._moscas = dict(fast=list(), slow=list())

        acq_nodes = dict(fast=fast_acq_nodes, slow=slow_acq_nodes)
        for name, node_list in acq_nodes.items():
            for node in node_list:
                dev = node.acquisition_obj.device
                if isinstance(dev, Lima):
                    self._limas[name].append(dev)
                elif isinstance(dev, Lima2):
                    self._lima2s[name].append(dev)
                elif isinstance(dev, BaseMCA):
                    self._mcas[name].append(dev)
                elif isinstance(dev, McaCounterController):
                    self._moscas[name].append(dev)

        self._first_line = True

    def new_data_received(self, data=None, signal=None, sender=None):
        super().new_data_received(data, signal, sender)

        for (key, trig_name) in self._trigger_names.items():
            if sender.name == trig_name:
                self._trigger_count[key] += len(data)

    def build_display_line(self):
        if self._first_line:
            pre_txt = ""
            self._first_line = False
        else:
            pre_txt = "\033[F" * 2

        status = f"{SCAN_STATE_MSG.get(self.scan_state, '')}:"
        head_txt = f"{status}"

        for ch in self._axes_chan2acq:
            head_txt += self.build_axis_string(ch)

        if self.scan_state in (ScanState.PREPARING, ScanState.IDLE):
            return pre_txt + head_txt + "\n\n"

        head_len = len(head_txt)
        trig_txt = dict(fast="", slow="")

        for name in trig_txt:
            trig_txt[name] += f" {BOLD(name)} {self._trigger_count[name]}"

            for cam in self._limas[name]:
                trig_txt[name] += self.build_lima_string(cam)

            for cam in self._lima2s[name]:
                trig_txt[name] += self.build_lima2_string(cam)

            for mca in self._mcas[name]:
                trig_txt[name] += self.build_mca_string(mca)

            for mosca in self._moscas[name]:
                trig_txt[name] += self.build_mosca_string(mosca)

        txt = pre_txt + head_txt + trig_txt["fast"] + " "*4 + "\n"
        txt += " "*head_len + trig_txt["slow"] + " "*4 + "\n"

        return txt


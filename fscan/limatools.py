import tabulate
import copy
import math

from fscan.fscantools import FScanParamStruct
from bliss.controllers.lima.lima_base import Lima
from bliss.controllers.lima2.controller import DetectorController as Lima2


def get_lima_version(dev):
    if isinstance(dev, Lima):
        return 1
    elif isinstance(dev, Lima2):
        return 2
    return None

def has_accumulation_mode(devs):
    has_acc = [dev.acquisition.mode == "ACCUMULATION" for dev in devs]
    if True in has_acc:
        if len(has_acc) > 1:
            raise RuntimeError("When using ACCUMULATION mode, only ONE lima device can be used")
        return True
    return False


class LimaCalib:

    DefaultTriggerLatency = {
        "lima1_pilatus": 5e-5,
        "lima1_andor3": 1e-4,
        "lima1_pco": 4e-4,
        "lima1_eiger": 0.,
        "lima1_frelon": 1e-4,
        "lima1_slsdetector": 0.,
        "lima1_perkinelmer": 1e-4,
        "lima1_lambda": 0.,
        "lima1_basler": 1e-4,
        "lima2_dectris": 0.,
    }
    
    def __init__(self, config={}):
        self.__config_origin = config
        self.reset()

    def reset(self):
        self.config = copy.deepcopy(self.__config_origin)
        self._calib = dict()
        self._int_time = 0.
        self._ext_time = 0.

    def set_camera_readout_time(self, dev, readout):
        self.set_camera_config(dev, "readout_time", readout)

    def set_camera_trigger_latency(self, dev, latency):
        self.set_camera_config(dev, "trigger_latency", latency)

    def set_eiger_internal_time_correction(self, dev, correction):
        self.set_camera_config(dev, "internal_time_correction", correction)

    def set_eiger_external_time_correction(self, dev, correction):
        self.set_camera_config(dev, "external_time_correction", correction)

    def set_camera_config(self, dev, name, value):
        self.config.setdefault(dev.name, {})
        self.config[dev.name][name] = value

    def get_camera_config(self, dev):
        return self.config.get(dev.name, {})

    def reset_camera_config(self, dev):
        orig_cfg = copy.deepcopy(self.__config_origin.get(dev.name, {}))
        self.config[dev.name] = orig_cfg

    def __info__(self):
        if not len(self._calib):
            return "No calibration performed."

        _int_rate = 1./(self._acq_time + self._int_time)
        _ext_rate = 1./(self._acq_time + self._ext_time)
        values = [ (key,)+val for key,val in self._calib.items() ]
        headers = ["Name", "Internal", "External", "Latency Used"]
        infos = f"Calibration for exposure_time = {self._acq_time} sec\n\n"
        infos += tabulate.tabulate(values, headers=headers)
        infos += f"\n\ninternal trigger ==> readout= {self._int_time*1000.:.6f} msec ; frame rate= {_int_rate:.2f} Hz\n"
        infos += f"external trigger ==> readout= {self._ext_time*1000.:.6f} msec ; frame rate= {_ext_rate:.2f} Hz\n"
        return infos
            
    def validate(self, limas, pars, use_acc=True):
        pars.lima_ndev = len(limas)
        pars.lima_acc_used = False
        pars.lima_acq_mode = "SINGLE"

        if pars.scan_mode == "CAMERA":
            if pars.lima_ndev > 1:
                raise RuntimeError("With scan_mode == CAMERA, only ONE lima device can be used")
            if pars.lima_ndev == 0:
                raise RuntimeError("With scan_mode == CAMERA, need ONE lima device active")

        if pars.lima_ndev == 0:
            return

        if use_acc:
            lima1_devs = [ dev for dev in limas if get_lima_version(dev)==1 ]
            dev_in_acc = [dev.acquisition.mode == "ACCUMULATION" for dev in lima1_devs]
            if True in dev_in_acc:
                if dev_in_acc.count(True) > 1:
                    raise RuntimeError("When using ACCUMULATION mode, only ONE lima device can be used")
                pars.lima_acc_used = True

        if pars.lima_acc_used:
            maxtime = limas[0].accumulation.max_expo_time
            pars.lima_acc_nb = math.ceil(pars.acq_time/maxtime)
            pars.lima_acc_time = pars.acq_time / pars.lima_acc_nb
            pars.lima_acq_mode = "ACCUMULATION"
            (internal_time, external_time) = self.calibrate(limas, pars.lima_acc_time)
            if pars.scan_mode == "CAMERA":
                pars.lima_acc_period = pars.lima_acc_time + internal_time
                pars.lima_min_period = pars.lima_acc_nb * pars.lima_acc_period
            else:
                pars.lima_acc_period = pars.lima_acc_time + external_time
                pars.lima_min_period = pars.lima_acc_nb * pars.lima_acc_period + pars.latency_time
        else:
            (internal_time, external_time) = self.calibrate(limas, pars.acq_time)
            if pars.scan_mode == "CAMERA":
                pars.lima_min_period = pars.acq_time + internal_time
            else:
                pars.lima_min_period = pars.acq_time + external_time + pars.latency_time

    def calibrate(self, devs, acq_time):
        self.reset()
        self._acq_time = acq_time
        for dev in devs:
            self._calib[dev.name] = self.calibrate_device(dev, acq_time)
        
        self._int_time = max([ val[0] for val in self._calib.values() ])
        self._ext_time = max([ val[1] for val in self._calib.values() ])
        return (self._int_time, self._ext_time)

    @property
    def internal_time(self):
        return self._int_time

    @property
    def external_time(self):
        return self._ext_time

    def calibrate_device(self, dev, acq_time):
        cam_name = dev.name
        cam_vers = get_lima_version(dev)
        if cam_vers == 1:
            cam_type = dev._proxy.lima_type
            cam_type = "lima1_" + cam_type.lower()
        else:
            cam_type = dev.device.det_info.get("plugin")
            cam_type = "lima2_" + cam_type.lower()
        cfg = self.config.get(cam_name, {})
        cfg_readout = cfg.get("readout_time", None)
        def_latency = self.DefaultTriggerLatency.get(cam_type, 0.)
        cfg_latency = cfg.get("trigger_latency", def_latency)
        if cfg_readout is not None:
            return (cfg_readout, cfg_readout+cfg_latency, cfg_latency)
        else:
            try:
                func = getattr(self, f"_calibrate_{cam_type}")
            except AttributeError:
                raise RuntimeError(f"No LIMA calibration for {cam_type} camera [{dev.name}]")
            (int_time, ext_time) = func(dev, acq_time, cfg_latency)
            return (int_time, ext_time, cfg_latency)

    def _calibrate_lima1_simulator(self, dev, acq_time, latency):
        cam_time = 5e-6
        ext_time = cam_time + latency
        return (cam_time, ext_time)

    def _calibrate_lima1_slsdetector(self, dev, acq_time, latency):
        cam_time = 5e-6
        ext_time = cam_time + latency
        return (cam_time, ext_time)

    def _calibrate_lima1_pilatus(self, dev, acq_time, latency):
        dev._proxy.latency_time = 0.
        cam_time = dev._proxy.latency_time
        ext_time = cam_time + latency
        return (cam_time, ext_time)

    def _calibrate_lima1_imxpad(self, dev, acq_time, latency):
        dev._proxy.latency_time = 5e-2
        cam_time = dev._proxy.latency_time
        # cam_time = 0.002
        ext_time = cam_time + latency
        return (cam_time, ext_time)    

    def _calibrate_lima1_maxipix(self, dev, acq_time, latency):
        dev._proxy.latency_time = 0.
        cam_time = dev._proxy.latency_time
        ext_time = cam_time + latency
        return (cam_time, ext_time)

    def _calibrate_lima1_pco(self, dev, acq_time, latency):
        dev._proxy.saving_mode = "MANUAL"
        dev._proxy.acq_expo_time = acq_time
        dev._proxy.prepareAcq()
        coc_time = dev.camera.coc_run_time
        cam_time = coc_time - acq_time
        ext_time = cam_time + latency
        return (cam_time, ext_time)

    def _calibrate_lima1_andor(self, dev, acq_time, latency):
        cam_time = 0.5
        ext_time = cam_time + latency
        return (cam_time, ext_time)

    def _calibrate_lima1_andor3(self, dev, acq_time, latency):
        model = dev._proxy.camera_model
        if model.startswith("MARANA"):
            return self._calibrate_lima1_andor3_marana(dev, acq_time, latency)
        elif model.startswith("BLR"):
            return self._calibrate_lima1_andor3_balor(dev, acq_time, latency)
        else:
            cam_time = dev.camera.readout_time
            ext_time = cam_time + latency
            return (cam_time, ext_time)

    def _calibrate_lima1_andor3_marana(self, dev, acq_time, latency):
        cam_time = dev.camera.readout_time
        ext_time = cam_time + 2*cam_time/2048 + latency
        if dev.camera.overlap == "ON":
            if acq_time > cam_time:
                int_time = cam_time/2048
            else:
                int_time = (cam_time-acq_time) + 2*cam_time/2048
        else:
            int_time = cam_time + 2*cam_time/2048
        return (int_time, ext_time)

    def _calibrate_lima1_andor3_balor(self, dev, acq_time, latency):
        shut_mode = dev.camera.electronic_shutter_mode
        frame_time = dev.camera.readout_time
        row_read = 1. * dev.image.raw_roi[3]
        row_time = frame_time * 4. / row_read
        if shut_mode == "GLOBAL":
            if acq_time < (frame_time + 21*row_time):
                cam_time = 2*frame_time + 13*row_time
            else:
                cam_time = frame_time + 6*row_time
            ext_time = cam_time
        elif shut_mode == "GLOBAL___100%_DUTY_CYCLE":
            if acq_time <= (2*frame_time + 27*row_time):
                raise RuntimeError(f"Acquisition time too short for andor3/balor in {shut_mode}")
            cam_time = 0.
            ext_time = cam_time
        elif shut_mode == "ROLLING":
            cam_time = max(acq_time + 4*row_time, frame_time + 2*row_time) - acq_time
            ext_time = cam_time + row_time
        elif shut_mode == "ROLLING___100%_DUTY_CYCLE":
            if acq_time <= (frame_time + 3*row_time):
                raise RuntimeError(f"Acquisition time too short for andor3/balor in {shut_mode}")
            cam_time = 0.
            ext_time = cam_time
        return (cam_time, ext_time + latency)

    def _calibrate_lima1_eiger(self, dev, acq_time, latency):
        cfg = self.config.get(dev.name, {})
        model = dev._proxy.camera_model
        if "PILATUS4" in model:
            internal_time_correction = cfg.get("internal_time_correction", 0.000015)
            cam_time = internal_time_correction * acq_time + 1e-7
            ext_time = max(1.0e-6, cam_time)
        else: # EIGER2
            internal_time_correction = cfg.get("internal_time_correction", 0.000041)
            external_time_correction = cfg.get("external_time_correction", 0.00006)
            cam_time = internal_time_correction * acq_time + 1e-7
            ext_time = external_time_correction * acq_time + 0.0001 + latency
        return (cam_time, ext_time)

    def _calibrate_lima1_frelon(self, dev, acq_time, latency):
        dev._proxy.saving_mode = "MANUAL"
        dev._proxy.acq_expo_time = acq_time
        dev._proxy.prepareAcq()
        if dev.camera.image_mode == "FRAME TRANSFER":
            cam_time = dev.camera.transfer_time
            min_expo = dev.camera.readout_time
            if acq_time < min_expo:
                raise RuntimeError(f"Frelon camera [{dev.name}] in frame transfer:"
                                   f" min expo time = {min_expo}")
        else:
            cam_time = dev.camera.readout_time + dev.shutter.close_time
        ext_time = cam_time + latency
        return (cam_time, ext_time)

    def _calibrate_lima1_perkinelmer(self, dev, acq_time, latency):
        # this is valid for bin 1x1
        # should be /2 for bin 2x2, but not working with id15 detector I tested
        cam_time = 0.0665
        ext_time = cam_time + latency
        if acq_time <= cam_time:
            raise RuntimeError(f"PerkinElmer acq_time must be > {cam_time}")
        return (cam_time, ext_time)

    def _calibrate_lima1_lambda(self, dev, acq_time, latency):
        cam_time = 0.002
        ext_time = cam_time + latency
        return (cam_time, ext_time)

    def _calibrate_lima1_basler(self, dev, acq_time, latency):
        dev._proxy.saving_mode = "MANUAL"
        dev._proxy.image_bin = dev.image.binning
        dev._proxy.image_roi = dev.image.roi
        dev._proxy.acq_expo_time = acq_time
        dev._proxy.prepareAcq()
        frame_rate = dev.camera.frame_rate
        min_period = 1./frame_rate
        if acq_time < min_period:
            cam_time = min_period - acq_time
            ext_time = cam_time + latency
        else:
            cam_time = latency
            ext_time = latency
        return (cam_time, ext_time)

    def _calibrate_lima2_simulator(self, dev, acq_time, latency):
        cam_time = 5e-6
        ext_time = cam_time + latency
        return (cam_time, ext_time)

    def _calibrate_lima2_dectris(self, dev, acq_time, latency):
        cfg = self.config.get(dev.name, {})
        model = dev.device.det_info["model"]
        if "PILATUS4" in model:
            internal_time_correction = cfg.get("internal_time_correction", 0.000015)
            cam_time = internal_time_correction * acq_time + 1e-7
            ext_time = max(1.1e-6, cam_time)
        else: # EIGER2
            internal_time_correction = cfg.get("internal_time_correction", 0.000041)
            external_time_correction = cfg.get("external_time_correction", 0.00006)
            cam_time = internal_time_correction * acq_time + 1e-7
            ext_time = external_time_correction * acq_time + 0.0001 + latency
        return (cam_time, ext_time)

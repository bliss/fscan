from bliss.scanning.scan import Scan
from bliss.controllers.ct2.device import AcqMode as CT2AcqMode
from bliss.scanning.chain import AcquisitionSlave

from fscan.synchro.base import SyncDeviceList
from fscan.synchro.maestrio.programs import (
    MaestrioProgFScan,
    MaestrioProgFScanCamera,
)
from fscan.synchro.musst.programs import (
    MusstProgFScan,
    MusstProgFScanCamera,
    MusstProgTimeTrigger,
)
from fscan.musstcalc import MusstCalcAcqSlave

from fscan.fscantools import (
    FScanMode,
    FScanTrigMode,
    FScanCamSignal,
    FScanParamBase,
    FScanParamStruct,
    FScanDisplay,
)
from fscan.mcatools import get_fscan_mca_params
from fscan.motortools import (
    TwoMotorMaster,
    RotationMotorChainPreset, 
    calc_move_time,
    check_motor_limits,
    check_motor_velocity,
)
from fscan.chaintools import ChainTool
from fscan.fscanrunner import FScanDiagRunner
from fscan.fscaninfo import FScanInfo
from fscan.limatools import LimaCalib


class F2ScanPars(FScanParamBase):
    DEFAULT = {
        "motor": None,
        "start_pos": 0,
        "step_size": 0.1,
        "step_time": 1.0,
        "slave_motor": None,
        "slave_start_pos": 0.,
        "slave_step_size": 0.01,
        "acq_time": 0.8,
        "npoints": 10,
        "scan_mode": FScanMode.TIME,
        "gate_mode": FScanTrigMode.TIME,
        "camera_signal": FScanCamSignal.EDGE,
        "min_gate_low_time": 0.,
        "save_flag": True,
        "display_flag": True,
        "sync_encoder": True,
        "home_rotation": False,
        "acc_margin": 0.,
        "slave_acc_margin": 0.,
        "sampling_time": 0.5,
        "latency_time": 1e-5,
    }
    OBJKEYS = ["motor", "slave_motor"]
    LISTVAL = {
        "scan_mode": FScanMode.values,
        "gate_mode": FScanTrigMode.values,
        "camera_signal": FScanCamSignal.values,
    }
    NOSETTINGS = ["save_flag",]

    def __init__(self, name):
        FScanParamBase.__init__(
            self, 
            name, 
            F2ScanPars.DEFAULT, 
            F2ScanPars.OBJKEYS, 
            F2ScanPars.LISTVAL,
            F2ScanPars.NOSETTINGS,
        )

    def _validate_npoints(self, value):
        if not int(value) > 0:
            raise ValueError("npoints should be > 0")
        return int(value)

    def _validate_save_flag(self, value):
        return value and True or False

    def _validate_display_flag(self, value):
        return value and True or False

    def _validate_sync_encoder(self, value):
        return value and True or False

    def _validate_scan_mode(self, value):
        return FScanMode.get(value, "scan_mode")

    def _validate_gate_mode(self, value):
        return FScanTrigMode.get(value, "gate_mode")

    def _validate_camera_signal(self, value):
        return FScanCamSignal.get(value, "camera_signal")

    def _validate_acc_margin(self, value):
        return abs(value)


class F2ScanMaster(object):
    def __init__(self, name, config):
        self.name = name
        self.config = config

        self.sync_list = SyncDeviceList(config["devices"]["synchro"])
        self.meas_group = config["devices"]["measgroup"]
        self.rot_motors = config["devices"]["rotation_motors"]
        self.chain_config = config["chain_config"]
        self.lima_calib = LimaCalib(config["lima_calib"])
        self.musst_data_rate_factor = config.get("musst_data_rate_factor", 0)

        self.pars = F2ScanPars(self.name)
        self.inpars = None
        self.mgchain = None

    def validate(self):
        self.inpars = FScanParamStruct(self.pars.to_dict())
        pars = self.inpars

        # --- check master and slave motors are on same sync device
        self.sync_dev = self.sync_list.find_device_for_motors(pars.motor, pars.slave_motor)
        self.scan_motors = [ pars.motor, pars.slave_motor ]

        # --- check lima and minimum period
        limas = self.get_controllers_found("lima", "lima2")
        self.lima_calib.validate(limas, pars, use_acc=True)

        if pars.lima_ndev:
            if pars.step_time < pars.lima_min_period:
                pars.step_time = pars.lima_min_period
        else:
            if pars.step_time < pars.acq_time:
                pars.step_time = pars.acq_time + pars.latency_time

        pars.dead_time = pars.step_time - pars.acq_time

        # --- check minimum gate low time
        pars.gate_low_time = 0.
        if pars.scan_mode == FScanMode.CAMERA:
            if pars.camera_signal == FScanCamSignal.FALL:
                pars.gate_low_time = max(pars.min_gate_low_time, pars.dead_time)
            elif pars.camera_signal == FScanCamSignal.RISE:
                pars.gate_low_time = pars.min_gate_low_time
            if pars.gate_low_time >= pars.step_time:
                raise ValueError("Minimum gate low time >= period is not possible")

        # --- calc params
        pars.motor_name = pars.motor.name
        pars.motor_unit = pars.motor.unit is None and "unit" or pars.motor.unit
        pars.stop_pos = pars.start_pos + pars.npoints * pars.step_size
        pars.move_time = pars.npoints * pars.step_time

        pars.slave_motor_name = pars.slave_motor.name
        pars.slave_motor_unit = pars.slave_motor.unit is None and "unit" or pars.slave_motor.unit
        pars.slave_stop_pos = pars.slave_start_pos + pars.npoints * pars.slave_step_size

        mot_master = TwoMotorMaster(pars.motor, pars.start_pos, pars.stop_pos,
                                    pars.slave_motor, pars.slave_start_pos, pars.slave_stop_pos,
                                    pars.move_time,
                                    master_undershoot_start_margin= pars.acc_margin,
                                    slave_undershoot_start_margin= pars.slave_acc_margin,
                                   )
        self.mot_master = mot_master

        pars.speed = mot_master.master["velocity"]
        pars.real_start = mot_master.master["real_start"]
        pars.real_stop = mot_master.master["real_end"]
        pars.slave_speed = mot_master.slave["velocity"]
        pars.slave_real_start = mot_master.slave["real_start"]
        pars.slave_real_stop = mot_master.slave["real_end"]
  
 
        pars.acq_size = pars.acq_time * pars.speed
        pars.scan_time = calc_move_time(pars.motor, abs(pars.real_stop-pars.real_start), pars.speed)
        if pars.step_size > 0:
            pars.scan_dir = 1
        else:
            pars.scan_dir = -1

        # --- check motor limits
        check_motor_limits(pars.motor, pars.real_start, pars.real_stop)
        check_motor_velocity(pars.motor, pars.speed)
        check_motor_limits(pars.slave_motor, pars.slave_real_start, pars.slave_real_stop)
        check_motor_velocity(pars.slave_motor, pars.slave_speed)

        # --- rotation specific
        pars.nrotations = 1
        if pars.motor in self.rot_motors and abs(pars.stop_pos - pars.start_pos) > 360.:
            pars.npoints_per_rotation = abs(int(360. / pars.step_size))
            pars.slave_step_size_per_rotation = pars.slave_step_size * pars.npoints_per_rotation
            pars.nrotations = pars.npoints / pars.npoints_per_rotation

    def show(self):
        if self.inpars is None:
            raise RuntimeError(f"F2ScanMaster[{self.name}]: Parameters not validated yet !!")

        if self.inpars.nrotations > 1:
            txt = """
Rotation Motor : {motor_name}
    start pos = {start_pos:.4f} {motor_unit}
    stop pos  = {stop_pos:.4f} {motor_unit}\t[ {nrotations} full-turn ]
    step size = {step_size:.4f} {motor_unit}
    step time = {step_time:.6f} sec
    velocity  = {speed:.4f} {motor_unit}/sec

Slave Motor : {slave_motor_name}
    start pos = {slave_start_pos:.5f} {slave_motor_unit}
    stop pos  = {slave_stop_pos:.5f} {slave_motor_unit}
    step size = {slave_step_size:.5f} {slave_motor_unit}\t[ {slave_step_size_per_rotation} {slave_motor_unit} / turn ]
    velocity  = {slave_speed:.5f} {slave_motor_unit}/sec

Acquisition :
    scan mode   = {scan_mode} {signal_txt}
    camera mode = {lima_acq_mode}
    acq time  = {acq_time:.6f} sec
    acq size  = {acq_size:.4f} {motor_unit}
    dead time = {dead_time:.6f} sec
    nb points = {npoints}\t[ {npoints_per_rotation} / turn ]
"""
        else:
            txt = """
Motor : {motor_name}
    start pos = {start_pos:.4f} {motor_unit}
    stop pos  = {stop_pos:.4f} {motor_unit}
    step size = {step_size:.4f} {motor_unit}
    step time = {step_time:.6f} sec
    velocity  = {speed:.4f} {motor_unit}/sec

Slave Motor : {slave_motor_name}
    start pos = {slave_start_pos:.5f} {slave_motor_unit}
    stop pos  = {slave_stop_pos:.5f} {slave_motor_unit}
    step size = {slave_step_size:.5f} {slave_motor_unit}
    velocity  = {slave_speed:.5f} {slave_motor_unit}/sec

Acquisition :
    scan mode   = {scan_mode} {signal_txt}
    camera mode = {lima_acq_mode}
    acq time  = {acq_time:.6f} sec
    acq size  = {acq_size:.4f} {motor_unit}
    dead time = {dead_time:.6f} sec
    nb points = {npoints}
"""
        if self.inpars.scan_mode == FScanMode.CAMERA:
            signal_txt = f"({self.inpars.camera_signal} signal)"
        else:
            signal_txt = ""

        print(txt.format(signal_txt=signal_txt, **self.inpars.to_dict()))

    def init_scan(self):
        self.mgchain = ChainTool(self.meas_group, self.chain_config)
        self.lima_used = list()
        self.mca_used = list()
        self.home_preset = None

    def get_controllers_found(self, *ctrl_type):
        mgchain = ChainTool(self.meas_group)
        return mgchain.get_controllers(*ctrl_type)

    def get_controllers_used(self, ctrl_type):
        if self.mgchain is None:
            return []
        else:
            return self.mgchain.get_controllers_done(ctrl_type)

    def setup_acq_chain(self, chain):
        mot_master = self.setup_motor_master(chain)
        acq_master = self.setup_acq_master(chain, mot_master)
        self.setup_acq_slaves(chain, acq_master)

    def setup_motor_master(self, chain):
        self.home_preset = dict()
        if self.inpars.home_rotation is True:
            for motor in (self.inpars.motor, self.inpars.slave_motor):
                if motor in self.rot_motors:
                    self.home_preset[motor] = RotationMotorChainPreset(motor, homing=True, rounding=False)
                    chain.add_preset(self.home_preset[motor])

        # --- return motor master created in validate()
        return self.mot_master

    def setup_acq_master(self, chain, mot_master):
        pars = self.inpars

        if pars.scan_mode == FScanMode.CAMERA:
            if self.sync_dev.is_musst():
                syncprog = MusstProgFScanCamera(self.sync_dev.board, self.scan_motors)
                syncprog.check_max_timer(pars.scan_time)
                syncprog.set_max_data_rate(self.musst_data_rate_factor * 2./pars.step_time)
            else:
                syncprog = MaestrioProgFScanCamera(self.sync_dev.board, self.scan_motors)
            syncprog.set_params(pars.start_pos, pars.scan_dir, pars.npoints, pars.camera_signal, pars.gate_low_time)
            if pars.lima_acc_used:
                syncprog.set_acc_params(pars.lima_acc_nb)
        else:
            # --- musst prog for either TIME or POSITION mode
            if self.sync_dev.is_musst():
                syncprog = MusstProgFScan(self.sync_dev.board, self.scan_motors)
                syncprog.check_max_timer(pars.scan_time)
                syncprog.set_max_data_rate(self.musst_data_rate_factor * 2./pars.step_time)
            else:
                syncprog = MaestrioProgFScan(self.sync_dev.board, self.scan_motors)

            syncprog.set_modes(pars.scan_mode, pars.gate_mode)
            if pars.scan_mode == FScanTrigMode.TIME:
                delta = pars.step_time
            else:
                delta = pars.step_size
            if pars.gate_mode == FScanTrigMode.TIME:
                gatewidth = pars.acq_time
            else:
                gatewidth = pars.acq_size
            syncprog.set_params(pars.start_pos, pars.scan_dir, delta, gatewidth, pars.npoints)
            if pars.lima_acc_used:
                syncprog.set_acc_params(pars.lima_acc_nb, pars.lima_acc_period)

        # --- sync motors on musst
        if pars.sync_encoder:
            syncprog.sync_motors()
        for (motor, preset) in self.home_preset.items():
            if motor in self.scan_motors:
                preset.set_musst_sync_cb(syncprog.sync_one_motor)

        # --- add to chain
        syncprog.setup(chain, mot_master)

        # --- calc device
        synccalc = MusstCalcAcqSlave(syncprog.acq_data_master, syncprog.channels, syncprog.DataPerPoint)
        synccalc.add_default_calc()
        for motor in self.scan_motors:
            if motor in self.rot_motors:
                calctype = "cen360"
            else:
                calctype = "center"
            synccalc.add_calc(calctype, motor.name)
            motchan = synccalc.get_calc_channel(calctype, motor.name)
            mot_master.add_external_channel(synccalc, motchan.short_name, f"axis:{motor.name}")

        chain.add(syncprog.acq_master, synccalc)

        self.syncprog = syncprog
        self.synccalc = synccalc

        return syncprog.acq_master

    def setup_acq_slaves(self, chain, acq_master):
        pars = self.inpars

        # --- ct2 counters
        ct2_params={
            "npoints": pars.npoints,
            "acq_mode": CT2AcqMode.ExtGate,
            "prepare_once": True,
            "start_once": True           
        }
        self.mgchain.setup("ct2", chain, acq_master, ct2_params)

        # --- lima devices
        if pars.scan_mode == FScanMode.CAMERA:
            trig_mode = "EXTERNAL_TRIGGER"
        else:
            trig_mode = "EXTERNAL_TRIGGER_MULTI"

        if pars.nrotations > 1:
            scan_frame_per_file = pars.npoints_per_rotation
        else:
            scan_frame_per_file = pars.npoints

        lima_params = {
            "acq_mode": pars.lima_acq_mode,
            "acq_trigger_mode": trig_mode,
            "acq_expo_time": pars.acq_time,
            "acq_nb_frames": pars.npoints,
            "wait_frame_id": [pars.npoints-1,],
            "prepare_once": True,
            "start_once": True,
            "scan_frame_per_file": scan_frame_per_file,
        }
        if pars.scan_mode == FScanMode.CAMERA:
            if pars.step_time > pars.lima_min_period:
                lat_time = pars.step_time - pars.acq_time
                lima_params["latency_time"] = lat_time
        self.mgchain.setup("lima", chain, acq_master, lima_params)
        self.lima_used = self.mgchain.get_controllers_done("lima")

        # --- lima2 devices
        lima_params = {
            "trigger_mode": "external",
            "expo_time": pars.acq_time,
            "nb_frames": pars.npoints,
            "nb_frames_per_trigger": 1,
            "wait_frame_id": [ pars.npoints - 1, ],
            "prepare_once": True,
            "start_once": True,
        }
        if pars.scan_mode == FScanMode.CAMERA:
            lima_params["nb_frames_per_trigger"] = pars.npoints
            if pars.step_time > pars.lima_min_period:
                lat_time = pars.step_time - pars.acq_time
                lima_params["latency_time"] = lat_time
        self.mgchain.setup("lima2", chain, acq_master, lima_params)

        # --- mca devices
        for mca_ctrl in self.mgchain.get_controllers_not_done("mca"):
            mca_params = get_fscan_mca_params(mca_ctrl, self.inpars.npoints, self.inpars.step_time)
            self.mgchain.setup("mca", chain, acq_master, mca_params, mca_ctrl.name)
            if "trigger_mode" in mca_params:
                # --- in spectrum reading mode
                self.mca_used.append(mca_ctrl)

        # --- mosca devices
        mosca_params = {
            "trigger_mode": "GATE",
            "npoints": pars.npoints,
            "preset_time": pars.acq_time,
            "wait_frame_id": [pars.npoints,],
            "start_once": True,
        }
        self.mgchain.setup("mosca", chain, acq_master, mosca_params)
        
        # --- mythen
        mythen_params = {
            "trigger_type": AcquisitionSlave.HARDWARE,
            "npoints": pars.npoints,
            "count_time": pars.acq_time,
        }
        self.mgchain.setup("mythen", chain, acq_master, mythen_params)
           
        # --- calc
        self.mgchain.setup_calc_counters(chain, acq_master)
 
        # --- sampling
        self.mgchain.setup_sampling(chain, pars.sampling_time)

    def setup_acq_scope(self, chain, mot_master):
        acqtime = max(0.001, self.inpars.step_time / 10.)
        npts = int((self.inpars.scan_time + 0.1) / acqtime + 0.5)

        # --- musst time prog
        musstprog = MusstProgTimeTrigger(self.sync_dev.board, self.scan_motors)
        musstprog.set_params(npts, acqtime)

        # --- sync motors on musst
        if self.inpars.sync_encoder:
            musstprog.sync_motors()
        for (motor, preset) in self.home_preset.items():
            if motor in self.scan_motors:
                preset.set_musst_sync_cb(musstprog.sync_one_motor)

        # --- acq chain
        musstprog.setup(chain, mot_master)
        musstprog.add_default_calc(chain)

    def setup_scan(self, chain, scan_name, user_info=None):
        inpars = self.inpars
        if inpars.display_flag:
            self.show()

        scan_info = FScanInfo(scan_name, "f2scan", inpars.npoints)
        scan_info.set_fscan_pars(self.pars)
        title = "{motor_name} {start_pos:g} {step_size:g} " +\
                "{slave_motor_name} {slave_start_pos}, {slave_step_size:g} " +\
                "{npoints:d} {acq_time:g} {step_time:g}"
        scan_info.set_fscan_title(title, inpars)

        if inpars.nrotations > 1:
            # --- add a scatter plot
            xname = f"axis:{inpars.motor_name}"
            xpoints = inpars.npoints_per_rotation
            scan_info.set_channel_meta(xname,
                                       start= 0. + inpars.step_size/2.,
                                       stop= 360. - inpars.step_size/2.,
                                       points= inpars.npoints,
                                       axis_id= 0,
                                       axis_points= xpoints,
                                      )
            yname = f"axis:{inpars.slave_motor_name}"
            ypoints = int(inpars.npoints / xpoints)
            scan_info.set_channel_meta(yname,
                                       start= inpars.slave_start_pos,
                                       stop= inpars.slave_stop_pos,
                                       points= inpars.npoints,
                                       axis_id= 1,
                                       axis_points= ypoints,
                                      )
            scan_info.add_scatter_plot(x=xname, y=yname)
        else:
            # --- no scatter plot
            scan_info.set_channel_meta(f"axis:{inpars.motor_name}",
                                       start= inpars.start_pos,
                                       stop= inpars.stop_pos,
                                       points= inpars.npoints,
                                      )
            scan_info.set_channel_meta(f"axis:{inpars.slave_motor_name}",
                                       start= inpars.slave_start_pos,
                                       stop= inpars.slave_stop_pos,
                                       points= inpars.npoints,
                                      )
        scan_info.set_fscan_info(user_info)
        scan_info.add_curve_plot(x=f"axis:{inpars.motor_name}")

        trig_name = self.synccalc.get_channel_name("trig", "timer")

        scan = Scan(
            chain,
            name=scan_name,
            save=inpars.save_flag,
            scan_info=scan_info,
            scan_progress=FScanDisplay(trig_name),
        )

        return scan

    def get_runner_class(self):
        return F2ScanCustomRunner

class F2ScanCustomRunner(FScanDiagRunner):
    def __call__(self, motor, start_pos, step_size, 
                       slave_motor, slave_start_pos, slave_step_size, 
                       npoints, acq_time, step_time=0., **kwargs):
        pars = dict(motor=motor,
                    start_pos=start_pos,
                    step_size=step_size,
                    slave_motor=slave_motor,
                    slave_start_pos=slave_start_pos,
                    slave_step_size=slave_step_size,
                    npoints=npoints,
                    acq_time=acq_time,
                    step_time=step_time,
               )
        pars.update(kwargs)
        self.run_with_pars(pars)


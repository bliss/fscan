from bliss.controllers.motor import CalcController
from bliss.scanning.scan import Scan
from bliss.controllers.ct2.device import AcqMode as CT2AcqMode
from bliss.scanning.acquisition.motor import MotorMaster, CalcAxisTrajectoryMaster
from bliss.physics.trajectory import LinearTrajectory

from fscan.synchro.base import SyncDeviceList
from fscan.synchro.musst.legacy.timetrig import MusstProgTimeTrigger
from fscan.synchro.musst.legacy.ftimescan import MusstProgFTimeScan
from fscan.synchro.musst.legacy.fscan import MusstProgFScan

from fscan.fscantools import (
    FScanModeBase,
    FScanParamBase,
    FScanTrigMode,
    FScanParamStruct,
    FScanDisplay,
)
from fscan.trajtools import (
    CalcTrajTool,
    MusstLinearTrajCalcFromTime,
    MusstTrajCalcFromOneAxis,
    MusstTrajCalcFromTime,
    MusstTrajCalcFromAllAxis,
)
from fscan.chaintools import ChainTool
from fscan.fscanrunner import FScanDiagRunner
from fscan.fscaninfo import FScanInfo
from fscan.limatools import LimaCalib
from fscan.mcatools import get_fscan_mca_params


PosAxisMode = FScanModeBase("CalcAxisMode", 
                            "CALC_FROM_ALL_AXIS", 
                            "CALC_FROM_ONE_AXIS", 
                            "CALC_FROM_TIME",
                            "REAL_FROM_AXIS",
                            "REAL_FROM_TIME"
                           )

class FlyScanPars(FScanParamBase):
    DEFAULT = {
        "motor": None,
        "start": 1.0,
        "stop": 2.0,
        "intervals": 10,
        "count_time": 0.5,
        "latency_time": 0.,
        "synchro_delay": 0.05,
        "sampling_time": 0.5,
        "save_flag": True,
        "display_flag": True,
    }
    OBJKEYS = ["motor"]
    LISTVAL = dict()

    def __init__(self, name):
        FScanParamBase.__init__(self, name, FlyScanPars.DEFAULT, FlyScanPars.OBJKEYS, FlyScanPars.LISTVAL)

class FlyScanMaster(object):
    def __init__(self, name, config):
        self.name = name
        self.config = config

        self.musst_list = SyncDeviceList(config["devices"]["synchro"])
        self.meas_group = config["devices"]["measgroup"]
        self.chain_config = config["chain_config"]
        self.lima_calib = LimaCalib(config["lima_calib"])

        self.pars = FlyScanPars(self.name)
        self.inpars = None
        self.mgchain = None

    def get_controllers_found(self, ctrl_type):
        mgchain = ChainTool(self.meas_group)
        return mgchain.get_controllers_not_done(ctrl_type)

    def get_controllers_used(self, ctrl_type):
        if self.mgchain is None:
            return []
        else:
            return self.mgchain.get_controllers_done(ctrl_type)

    def validate(self):
        self.inpars = FScanParamStruct(self.pars.to_dict())
        pars = self.inpars

        pars.motor_name = pars.motor.name
        pars.motor_unit = pars.motor.unit is None and "unit" or pars.motor.unit
        pars.step_size = (pars.stop - pars.start) / pars.intervals

        limas = self.get_controllers_found("lima")
        if len(limas):
            self.lima_calib.calibrate(limas, pars.count_time)
            pars.readout_time = self.lima_calib.external_time
        else:
            pars.readout_time = 1e-6

        pars.step_time = pars.count_time + pars.readout_time + pars.latency_time
        pars.npoints = pars.intervals + 1

        pars.start_pos = pars.start - pars.step_size / 2.
        pars.stop_pos = pars.stop + pars.step_size / 2.

        pars.speed = abs(pars.step_size) / pars.step_time
        pars.move_time = pars.npoints * pars.step_time

    def show(self):
        pass

    def setup_motor_master(self, chain):
        if isinstance(self.inpars.motor.controller, CalcController):
            return self.setup_calc_motor_master(chain)
        else:
            return self.setup_real_motor_master(chain)
            
    def init_scan(self):
        self.mgchain = ChainTool(self.meas_group, self.chain_config)
        self.lima_used = list()
        self.mca_used = list()

    def setup_calc_motor_master(self, chain):
        pars = self.inpars

        mot_master = CalcAxisTrajectoryMaster(pars.motor, pars.start_pos, pars.stop_pos,
                                              pars.npoints, pars.step_time)

        self.trajcalc = CalcTrajTool(pars.motor, mot_master.trajectory)
        axis_list = self.trajcalc.axis_ordered_list()
        axis_musst = self.musst_list.check_musst_for_motors(*axis_list)

        if all(map(lambda m: m is None, axis_musst)):
            # no motors on musst
            self.musst = self.musst_list.first_musst()
            self.musst_motors = []
            pars.calc_mode = PosAxisMode.CALC_FROM_TIME
        else:
            if all(map(lambda m: m == axis_musst[0], axis_musst)):
                # all motors on same musst, check channel_id
                musst = axis_musst[0]
                chans = [ musst.get_channel_by_name(axis.name).channel_id for axis in axis_list ]
                if any(map(lambda v: chans.count(v) > 1, chans)) is False:
                    # all on a different channel
                    self.musst = musst
                    self.musst_motors = axis_list
                    pars.calc_mode = PosAxisMode.CALC_FROM_ALL_AXIS
                else:
                    self.musst = musst
                    self.musst_motors = [axis_list[0],]
                    pars.calc_mode = PosAxisMode.CALC_FROM_ONE_AXIS
            else:
                for axis, musst in zip(axis_list, axis_musst):
                    if musst is not None:
                        self.musst = musst
                        self.musst_motors = [axis,]
                        pars.calc_mode = PosAxisMode.CALC_FROM_ONE_AXIS
                        break

            trig_axis = self.musst_motors[0]
            pars.trig_pos = self.trajcalc.get_trigger_position(trig_axis)
            pars.trig_dir = self.trajcalc.get_trigger_direction(trig_axis)

        pars.scan_time = self.trajcalc.total_time()

        return mot_master

    def setup_real_motor_master(self, chain):
        pars = self.inpars

        pars.acc_time = pars.speed / pars.motor.acceleration
        pars.acc_disp = 0.5 * pars.speed ** 2 / pars.motor.acceleration
        pars.scan_time = 2 * pars.acc_time + pars.move_time

        try:
            self.musst = self.musst_list.find_musst_for_motors(pars.motor)
            self.musst_motors = [ pars.motor, ]
            pars.calc_mode = PosAxisMode.REAL_FROM_AXIS
            pars.trig_pos = pars.start_pos
            if pars.step_size > 0:
                pars.trig_dir = 1
            else:
                pars.trig_dir = -1
        except ValueError:
            self.musst = self.musst_list.first_musst()
            self.musst_motors = []
            pars.calc_mode = PosAxisMode.REAL_FROM_TIME
            self.trajcalc = LinearTrajectory(pars.start_pos - pars.acc_disp,
                                                pars.stop_pos + pars.acc_disp,
                                                pars.speed,
                                                pars.motor.acceleration,
                                                0.,
                                               )

        mot_master = MotorMaster(pars.motor, pars.start_pos, pars.stop_pos, pars.move_time)

        return mot_master

    def setup_acq_master(self, chain, mot_master):
        pars = self.inpars

        if pars.calc_mode in (PosAxisMode.REAL_FROM_TIME, PosAxisMode.CALC_FROM_TIME):
            musstprog = MusstProgFTimeScan(self.musst)
            musstprog.set_time_params(pars.synchro_delay + pars.acc_time, pars.npoints, 
                                      pars.count_time, pars.step_time)
        else:
            musstprog = MusstProgFScan(self.musst, self.musst_motors)
            musstprog.set_modes(FScanTrigMode.TIME, FScanTrigMode.TIME)
            musstprog.set_params(pars.trig_pos, pars.trig_dir, pars.step_time, 
                                 pars.count_time, pars.npoints)
            musstprog.sync_motors()

        musstprog.check_max_timer(pars.scan_time)
        musstprog.setup(chain, mot_master)
        musstprog.add_default_calc(chain)

        if pars.calc_mode == PosAxisMode.REAL_FROM_AXIS:
            pass
        
        elif pars.calc_mode == PosAxisMode.REAL_FROM_TIME:
            calc = MusstLinearTrajCalcFromTime(self.trajcalc, pars.motor,
                                               musstprog.musst_device,
                                               musstprog.get_timer_channel(),
                                               musstprog.DataPerPoint,
                                               pars.synchro_delay,
                                              )
            musstprog.add_calc_dev(chain, calc)

        elif pars.calc_mode == PosAxisMode.CALC_FROM_TIME:
            calc = MusstTrajCalcFromTime(self.trajcalc,
                                         musstprog.musst_device,
                                         musstprog.get_timer_channels(),
                                         musstprog.DataPerPoint,
                                         pars.synchro_delay,
                                        )
            musstprog.add_calc_dev(chain, calc)

        elif pars.calc_mode == PosAxisMode.CALC_FROM_ONE_AXIS:
            axis = self.musst_motors[0]
            calc = MusstTrajCalcFromOneAxis(self.trajcalc, axis,
                                            musstprog.musst_device,
                                            musstprog.get_channel(axis),
                                            musstprog.DataPerPoint,
                                           )
            musstprog.add_calc_dev(chain, calc)
            
        elif pars.calc_mode == PosAxisMode.CALC_FROM_ALL_AXIS:
            calc = MusstTrajCalcFromAllAxis(pars.motor,
                                            self.musst_motors,
                                            musstprog,
                                           )
            musstprog.add_calc_dev(chain, calc)

        (calc_dev, calc_name) = musstprog.get_calc_dev(pars.motor.name, "trig")
        mot_master.add_external_channel(calc_dev, calc_name, f"axis:{pars.motor.name}")
            
        self.musstprog = musstprog
        return musstprog.musst_master

    def setup_acq_slaves(self, chain, acq_master):
        pars = self.inpars

        # --- ct2 counters
        ct2_params={
            "npoints": pars.npoints,
            "acq_mode": CT2AcqMode.ExtGate,
            "prepare_once": True,
            "start_once": True
        }
        self.mgchain.setup("ct2", chain, acq_master, ct2_params)

        # --- lima devices
        lima_params = {
            "acq_trigger_mode": "EXTERNAL_TRIGGER_MULTI",
            "acq_expo_time": pars.count_time,
            "acq_nb_frames": pars.npoints,
            "wait_frame_id": [pars.npoints-1,],
            "prepare_once": True,
            "start_once": True,
            "scan_frame_per_file": pars.npoints,
        }
        self.mgchain.setup("lima", chain, acq_master, lima_params)
        self.lima_used = self.mgchain.get_controllers_done("lima")

        # --- mca devices
        for mca_ctrl in self.mgchain.get_controllers_not_done("mca"):
            mca_params = get_fscan_mca_params(mca_ctrl, self.inpars.npoints, self.inpars.period)
            self.mgchain.setup("mca", chain, acq_master, mca_params, mca_ctrl.name)
            if "trigger_mode" in mca_params:
                # --- in spectrum reading mode
                self.mca_used.append(mca_ctrl)

        # --- mosca devices
        mosca_params = {
            "trigger_mode": "GATE",
            "npoints": pars.npoints,
            "preset_time": pars.count_time,
            "wait_frame_id": [pars.npoints,],
            "start_once": True,
        }
        self.mgchain.setup("mosca", chain, acq_master, mosca_params)

        # --- sampling
        self.mgchain.setup_sampling(chain, pars.sampling_time)

    def setup_acq_scope(self, chain, mot_master):
        acqtime = max(0.001, self.inpars.step_time / 10.)
        npts = int((self.inpars.scan_time + 0.1) / acqtime + 0.5)

        # --- musst time prog
        musstprog = MusstProgTimeTrigger(self.musst, self.musst_motors)
        musstprog.set_params(npts, acqtime)
        musstprog.sync_motors()

        # --- acq chain
        musstprog.setup(chain, mot_master)
        musstprog.add_default_calc(chain)

    def setup_scan(self, chain, scan_name, user_info=None):
        inpars = self.inpars
        if inpars.display_flag:
            self.show()

        scan_info = FScanInfo(scan_name, "flyscan", inpars.npoints)
        scan_info.set_fscan_pars(self.pars)
        scan_info.set_fscan_title(
            "{motor_name} {start:g} {stop:g} {intervals:d} {count_time:g}",
            inpars,
        )
        scan_info.set_fscan_info(user_info)

        scan = Scan(
            chain,
            name=scan_name,
            save=inpars.save_flag,
            scan_info=scan_info,
            scan_progress=FScanDisplay(),
        )

        return scan

    def get_runner_class(self):
        return FlyScanCustomRunner

class FlyScanCustomRunner(FScanDiagRunner):
    def __call__(self, motor, start, stop, intervals, count_time, **kwargs):
        pars = dict(motor = motor,
                    start = start,
                    stop = stop,
                    intervals = intervals,
                    count_time = count_time,
               )
        pars.update(kwargs)
        self.run_with_pars(pars)

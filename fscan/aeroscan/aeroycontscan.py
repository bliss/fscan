import numpy
import tabulate

from bliss.scanning.scan import Scan
from bliss.controllers.ct2.device import AcqMode as CT2AcqMode

from fscan.fscantools import (
    FScanModeBase,
    FScanMode,
    FScanParamBase,
    FScanParamStruct,
    FScanDisplay,
)
from fscan.motortools import (
    calc_move_time,
    check_motor_limits,
)
from fscan.synchro.musst.programs import MusstProgTimeTrigger
from fscan.synchro.musst.difftomo import MusstProgDiffTomoCont
from fscan.musstcalc import (
    MusstCalcAcqSlave,
    MusstCalcFilterIndexes,
)
from fscan.chaintools import ChainTool
from fscan.fscanrunner import FScanDiagRunner
from fscan.fscaninfo import FScanInfo
from fscan.aeroscan.aeromaster import AeroXYProgMaster
from fscan.aeroscan.aeroprog import AeroProgXYPVT
from fscan.limatools import LimaCalib


FScanTransitionMode = FScanModeBase("FScanTransitionMode", "DURING_ACQ", "FIXED_ACC", "FIXED_STEP")

class AeroScanXYCPars(FScanParamBase):
    DEFAULT = {
        "x_start_pos": 0,
        "x_step_size": 1.0,
        "x_step_time": 0.,
        "x_range": 360.,
        "y_start_pos": 0.,
        "y_step_size_arr": (.2, .1),
        "y_npoints_arr": (6, 4),
        "acq_time": 0.008,
        "scan_mode": FScanMode.TIME,
        "transition_mode": FScanTransitionMode.DURING_ACQ,
        "min_gate_low_time": 0.,
        "x_acc_margin": 0.,
        "y_acc_margin": 0.,
        "save_flag": True,
        "display_flag": True,
        "sampling_time": 0.5,
        "scatter_plot": False,
    }
    OBJKEYS = []
    LISTVAL = {
        "transition_mode": FScanTransitionMode.values,
        "scan_mode": FScanMode.values,
    }
    NOSETTINGS = ["save_flag",]

    def __init__(self, name):
        FScanParamBase.__init__(self, 
                                name,
                                AeroScanXYCPars.DEFAULT,
                                AeroScanXYCPars.OBJKEYS, 
                                AeroScanXYCPars.LISTVAL,
                                AeroScanXYCPars.NOSETTINGS,
                               )

    def _validate_x_range(self, value):
        if value < 0.:
            print("WARNING : Automatically set x_range positive. Direction is given by size of x_step_size only.")
        return abs(value)

    def _validate_y_npoints_arr(self, value):
        try:
            iter_val = iter(value)
        except TypeError:
            iter_val = iter((value,))
        return tuple(int(val) for val in iter_val)

    def _validate_y_step_size_arr(self, value):
        try:
            iter_val = iter(value)
        except TypeError:
            iter_val = iter((value,))
        return tuple(float(val) for val in iter_val)

    def _validate_transition_mode(self, value):
        return FScanTransitionMode.get(value, "transition_mode")

    def _validate_scan_mode(self, value):
        return FScanMode.get(value, "scan_mode")

    def _validate_save_flag(self, value):
        return value and True or False

    def _validate_display_flag(self, value):
        return value and True or False

    def _validate_scatter_plot(self, value):
        return value and True or False

class AeroScanXYCMaster(object):
    def __init__(self, xmotor, ymotor, measgroup, musstdev):
        self.name = "aeroycontscan"
        self.xmotor = xmotor
        self.ymotor = ymotor
        self.meas_group = measgroup
        self.musst = musstdev
        self.lima_calib = LimaCalib()

        self.pars = AeroScanXYCPars(self.name)
        self.inpars = None

    def validate(self):
        self.inpars = FScanParamStruct(self.pars.to_dict())
        pars = self.inpars

        # --- only positive direction
        if pars.x_step_size < 0.:
            raise ValueError("only positive x_step_size implemented !!")

        # --- motor generic params
        pars.x_motor_name = self.xmotor.name
        pars.x_motor_unit = self.xmotor.unit
        pars.y_motor_name = self.ymotor.name
        pars.y_motor_unit = self.ymotor.unit

        # --- tuple to numpy arrays
        pars.y_step_size_arr = numpy.array(pars.y_step_size_arr)
        pars.y_npoints_arr = numpy.array(pars.y_npoints_arr)
        pars.y_npoints = pars.y_npoints_arr.sum()

        # --- basic checks
        pars.x_npoints = abs(pars.x_range / pars.x_step_size)
        if int(pars.x_npoints) != pars.x_npoints:
            raise ValueError("x_range musst be a multiple of x_step_size")
        pars.x_npoints = int(pars.x_npoints)
        if len(pars.y_step_size_arr) != len(pars.y_npoints_arr):
            raise ValueError("y_step_size_arr and y_npoints_arr arrays must have the same length")

        # --- check lima minimum period
        limas = self.get_controllers_found("lima", "lima2")
        pars.latency_time = 0.0
        self.lima_calib.validate(limas, pars, use_acc=True)

        # --- check step size
        if pars.lima_ndev:
            if pars.x_step_time <= 0:
                pars.x_step_time = pars.lima_min_period
            elif pars.x_step_time < pars.lima_min_period:
                raise ValueError(f"x_step_time should be null or >= lima minimum period of {pars.lima_min_period}")
        else:
            if pars.x_step_time < pars.lima_min_period:
                pars.x_step_time = pars.acq_time

        # --- check gate time
        pars.gate_time = min(pars.x_step_time - pars.min_gate_low_time, pars.acq_time)

        # --- y motor direction
        pars.y_nzone = len(pars.y_npoints_arr)
        scan_dir_check = sum(step > 0. for step in pars.y_step_size_arr)
        if scan_dir_check == pars.y_nzone:
            pars.y_scan_dir = 1
        elif scan_dir_check == 0:
            pars.y_scan_dir = -1
        else:
            raise ValueError("All y step size must have the same direction/sign")

        # --- x motor speed / acceleration
        pars.x_speed = abs(pars.x_step_size) / pars.x_step_time
        pars.x_acc_time = pars.x_acc_margin / pars.x_speed + pars.x_speed / self.xmotor.acceleration
        pars.x_acc_disp = pars.x_acc_margin + 0.5 * pars.x_speed ** 2 / self.xmotor.acceleration
        pars.x_real_start = pars.x_start_pos - pars.x_acc_disp

        # --- y motor speed / acceleration
        pars.y_speed_arr = abs(pars.y_step_size_arr) / (pars.x_range / pars.x_speed)
        y_speed_tmp = numpy.array((0,) + tuple(pars.y_speed_arr) + (0,))
        pars.y_acc_disp_arr = abs(y_speed_tmp[1:]**2 - y_speed_tmp[:-1]**2) / (2 * self.ymotor.acceleration)
        pars.y_acc_time_arr = abs(numpy.diff(y_speed_tmp)) / self.ymotor.acceleration

        pars.y_acc_disp_arr[0] += pars.y_acc_margin
        pars.y_acc_disp_arr[-1] += pars.y_acc_margin

        pars.y_acc_time_arr[0] += pars.y_acc_margin / pars.y_speed_arr[0]
        pars.y_acc_time_arr[-1] += pars.y_acc_margin / pars.y_speed_arr[-1]

        pars.y_real_start = pars.y_start_pos - pars.y_scan_dir * pars.y_acc_disp_arr[0]

        # --- x/y ranges
        pars.y_range_arr = abs(pars.y_step_size_arr) * pars.y_npoints_arr
        pars.x_range_arr = pars.y_npoints_arr * pars.x_range

        # --- acq size
        pars.dead_time = pars.x_step_time - pars.acq_time
        pars.x_acq_size = pars.acq_time * pars.x_speed
        pars.y_acq_size_arr = pars.y_scan_dir * pars.acq_time * pars.y_speed_arr
        pars.npoints = int(pars.x_npoints * pars.y_npoints)

        # --- sign array with directions
        pars.y_acc_disp_arr *= pars.y_scan_dir
        pars.y_speed_arr *= pars.y_scan_dir
        pars.y_range_arr *= pars.y_scan_dir

        # --- pvt array
        pars.pvt_array = numpy.zeros((1+2*pars.y_nzone, 5), numpy.float64)
        # initial acceleration
        pars.pvt_array[0,:] = [ pars.x_acc_disp, pars.x_speed,
                                pars.y_acc_disp_arr[0], pars.y_speed_arr[0],
                                max(pars.x_acc_time, pars.y_acc_time_arr[0]),
                              ]
        # first constant speed segment
        pars.pvt_array[1,:] = [ pars.x_range_arr[0], pars.x_speed,
                                pars.y_range_arr[0], pars.y_speed_arr[0],
                                pars.x_range_arr[0] / pars.x_speed
                              ]
        # final deceleration
        pars.pvt_array[-1,:] = [ pars.x_acc_disp, 0.,
                                 pars.y_acc_disp_arr[-1], 0.,
                                 max(pars.x_acc_time, pars.y_acc_time_arr[-1]),
                               ]

        # other segments depends on transition_mode
        pars.x_delta_time = 0.
        pars.x_delta_size = 0.
        if pars.y_nzone > 1:
            y_max_acc = pars.y_acc_time_arr[1:-1].max()
            if pars.transition_mode == FScanTransitionMode.FIXED_ACC:
                pars.x_delta_time = y_max_acc
            elif pars.transition_mode == FScanTransitionMode.FIXED_STEP:
                pars.x_delta_time = (int(y_max_acc / pars.x_step_time) + 1) * pars.x_step_time
            pars.x_delta_size = pars.x_delta_time * pars.x_speed

        for iz in range(1, pars.y_nzone):
            if pars.transition_mode == FScanTransitionMode.DURING_ACQ:
                pars.pvt_array[2*iz,:] = [ pars.y_acc_time_arr[iz] * pars.x_speed, pars.x_speed,
                                           pars.y_acc_disp_arr[iz], pars.y_speed_arr[iz],
                                           pars.y_acc_time_arr[iz],
                                         ]
                remain_time = pars.x_range_arr[iz] / pars.x_speed - pars.y_acc_time_arr[iz]
                pars.pvt_array[2*iz+1,:] = [ remain_time * pars.x_speed, pars.x_speed,
                                             remain_time * pars.y_speed_arr[iz], pars.y_speed_arr[iz],
                                             remain_time,
                                           ]
            else:
                ydisp = pars.y_acc_disp_arr[iz] + (pars.x_delta_time - pars.y_acc_time_arr[iz])*pars.y_speed_arr[iz]
                pars.pvt_array[2*iz,:] = [ pars.x_delta_size, pars.x_speed,
                                           ydisp, pars.y_speed_arr[iz],
                                           pars.x_delta_time,
                                         ]
                pars.pvt_array[2*iz+1,:] = [ pars.x_range_arr[iz], pars.x_speed,
                                             pars.y_range_arr[iz], pars.y_speed_arr[iz],
                                             pars.x_range_arr[iz] / pars.x_speed,
                                           ]

        # --- final positions
        pars.x_pos_arr = pars.x_real_start + pars.pvt_array[:,0].cumsum()
        pars.y_pos_arr = pars.y_real_start + pars.pvt_array[:,2].cumsum()
        pars.x_real_stop = pars.x_pos_arr[-1]
        pars.y_real_stop = pars.y_pos_arr[-1]
        pars.x_stop = pars.x_pos_arr[-2]
        pars.y_stop = pars.y_pos_arr[-2]

        # --- time
        pars.time_arr = pars.pvt_array[:,4]
        pars.total_time = pars.time_arr.sum()

        # --- check motor limits
        check_motor_limits(self.xmotor, pars.x_real_start, pars.x_real_stop)
        check_motor_limits(self.ymotor, pars.y_real_start, pars.y_real_stop)


    def show(self):
        if self.inpars is None:
            raise RuntimeError(f"{self.name}: Parameters not validated yet !!")

        pars = self.inpars
        nz = pars.y_nzone
        xstart = tuple(pars.x_pos_arr[::2][0:nz])
        xstop = tuple(pars.x_pos_arr[1::2][0:nz])
        ystart = tuple(pars.y_pos_arr[::2][0:nz])
        ystop = tuple(pars.y_pos_arr[1::2][0:nz])

            
        tab = list()
        tab.append(("x start",f"[{pars.x_motor_unit}]") + xstart)
        tab.append(("x stop",f"[{pars.x_motor_unit}]") + xstop)
        tab.append(("x speed",f"[{pars.x_motor_unit}/sec]") + (pars.x_speed,)*nz)
        tab.append(("y start",f"[{pars.y_motor_unit}]") + ystart)
        tab.append(("y stop", f"[{pars.y_motor_unit}]") + ystop)
        tab.append(("y speed",f"[{pars.y_motor_unit}/sec]") + tuple(pars.y_speed_arr))
        tab.append(("y step",f"[{pars.y_motor_unit}]") + tuple(pars.y_step_size_arr))
        tab.append(("npoints", "[nx*ny]") + tuple(pars.x_npoints*pars.y_npoints_arr))
        tab.append(("time", "[sec]",) + tuple(pars.time_arr[1::2]))

        print("Motors:")
        print(f"    x (rotation)    = {pars.x_motor_name}")
        print(f"    y (translation) = {pars.y_motor_name}")
        print(f"    transition mode = {pars.transition_mode}")
        print("\nAcquisition zones:")
        print(tabulate.tabulate(tab))
        if nz > 1 and pars.transition_mode != FScanTransitionMode.DURING_ACQ:
            xtrans = pars.pvt_array[2:2*nz:2,0][0]
            ytrans = pars.pvt_array[2:2*nz:2,2][0]
            print("\nTransition size:")
            print(f"     {xtrans:.6} [{pars.x_motor_unit}] in X ; {ytrans:.6f} [{pars.y_motor_unit}] in Y")
        print("\nAcquisition images:")
        print(f"    step size = {pars.x_step_size:.3f} [{pars.x_motor_unit}]")
        print(f"    acq size  = {pars.x_acq_size:.3f} [{pars.x_motor_unit}]")
        print(f"    acq time  = {pars.acq_time:.6f} [sec]")
        print(f"    dead time = {pars.dead_time:.6f} [sec]")
        print(f"    nb points = {pars.npoints} (X:{pars.x_npoints} * Y:{pars.y_npoints})")
        print(f"\nTotal time = {pars.total_time:.3f} [sec]")
        print()

    def get_controllers_found(self, *ctrl_type):
        mgchain = ChainTool(self.meas_group)
        return mgchain.get_controllers(*ctrl_type)

    def get_controllers_used(self, ctrl_type):
        if self.mgchain is None:
            return []
        else:
            return self.mgchain.get_controllers_done(ctrl_type)

    def init_scan(self):
        self.mgchain = ChainTool(self.meas_group)
        self.lima_used = list()
        self.lima2_used = list()
        self.mca_used = list()

    def setup_acq_chain(self, chain):
        mot_master = self.setup_motor_master(chain)
        acq_master = self.setup_acq_master(chain, mot_master)
        self.setup_acq_slaves(chain, acq_master)

    def setup_motor_master(self, chain):
        pars = self.inpars
        self.musst_motors = [ self.xmotor, self.ymotor ]
        self.aero_prog = AeroProgXYPVT(self.xmotor.controller)
        self.aero_prog.set("pvt", pars.pvt_array)

        return AeroXYProgMaster(
                  self.xmotor,
                  pars.x_real_start,
                  self.ymotor,
                  pars.y_real_start,
                  self.aero_prog
               )

    def setup_acq_master(self, chain, mot_master):
        pars = self.inpars

        # --- musst prog
        musstprog = MusstProgDiffTomoCont(self.musst, self.musst_motors)
        musstprog.check_max_timer(pars.total_time)

        start = pars.x_start_pos
        if pars.scan_mode == FScanMode.POSITION:
            step = pars.x_step_size
        else:
            step = pars.x_step_time
        gate = pars.gate_time
        looppulses = pars.y_npoints_arr * pars.x_npoints
        loopdelta = pars.y_npoints_arr * pars.x_range + pars.x_delta_size

        musstprog.set_params(pars.scan_mode, start, step, gate, looppulses.tolist(), loopdelta.tolist())

        # --- sync y motor on musst
        musstprog.sync_one_motor(self.ymotor)

        # --- sync x motor on musst after homing
        mot_master.set_xhome_cb(musstprog.sync_one_motor)

        # --- add to  chain
        musstprog.setup(chain, mot_master)

        # --- calc device
        musstcalc = MusstCalcAcqSlave(musstprog.acq_data_master, musstprog.channels, musstprog.DataPerPoint)
        musstcalc.add_default_calc()
        musstcalc.add_calc("cen360", pars.x_motor_name)

        # --- filter out transition points
        filter_idx = (looppulses + 1).cumsum() - 1
        musstcalc.set_output_filter(MusstCalcFilterIndexes, filter_idx)

        chain.add(musstprog.acq_master, musstcalc)

        # --- add motor external channels
        x_calc_name = musstcalc.get_channel_short_name("cen360", pars.x_motor_name)
        y_calc_name = musstcalc.get_channel_short_name("trig", pars.y_motor_name)
        mot_master.add_external_channel(musstcalc, x_calc_name, f"axis:{pars.x_motor_name}")
        mot_master.add_external_channel(musstcalc, y_calc_name, f"axis:{pars.y_motor_name}")

        self.syncprog = musstprog
        self.synccalc = musstcalc

        return musstprog.acq_master

    def setup_acq_slaves(self, chain, acq_master):
        pars = self.inpars

        # --- ct2 counters 
        ct2_params={
            "npoints": pars.npoints,
            "acq_mode": CT2AcqMode.ExtGate,
            "prepare_once": True,
            "start_once": True
        }
        self.mgchain.setup("ct2", chain, acq_master, ct2_params)

        # --- lima devices
        lima_params = {
            "acq_mode": "SINGLE",
            "acq_trigger_mode": "EXTERNAL_TRIGGER_MULTI",
            "acq_expo_time": pars.acq_time,
            "acq_nb_frames": pars.npoints,
            "wait_frame_id": [pars.npoints-1,],
            "prepare_once": True,
            "start_once": True,
            "scan_frame_per_file": pars.x_npoints,
        }
        self.mgchain.setup("lima", chain, acq_master, lima_params)
        self.lima_used = self.mgchain.get_controllers_done("lima")

        # --- lima2 devices
        lima_params = {
            "trigger_mode": "external",
            "expo_time": pars.acq_time,
            "nb_frames": pars.npoints,
            "nb_frames_per_trigger": 1,
            "wait_frame_id": [ pars.npoints - 1, ],
            "prepare_once": True,
            "start_once": True,
        }
        self.mgchain.setup("lima2", chain, acq_master, lima_params)
        self.lima2_used = self.mgchain.get_controllers_done("lima2")

        # --- sampling counters
        self.mgchain.setup_sampling(chain, pars.sampling_time)

    def setup_acq_scope(self, chain, mot_master):
        pars = self.inpars
        acqtime = max(0.010, pars.x_step_time / 10.)
        x_move_time = calc_move_time(self.xmotor, pars.x_real_stop - pars.x_real_start, pars.x_speed)
        npts = int((x_move_time + 0.1) / acqtime + 0.5)

        # --- musst time prog
        musstprog = MusstProgTimeTrigger(self.musst, self.musst_motors)
        musstprog.set_params(npts, acqtime)

        # --- sync motors on musst
        musstprog.sync_one_motor(self.ymotor)

        # --- allways sync x after homing
        mot_master.set_xhome_cb(musstprog.sync_one_motor)

        # --- acq chain
        musstprog.setup(chain, mot_master)
        musstprog.add_default_calc(chain)
        musstprog.add_cen360_calc(chain, pars.x_motor_name)

        # --- add motor external channels
        musstprog.set_motor_external_channel(pars.x_motor_name, mot_master, "cen360")
        musstprog.set_motor_external_channel(pars.y_motor_name, mot_master)

    def setup_scan(self, chain, scan_name, user_info=None):
        inpars = self.inpars
        if inpars.display_flag:
            self.show()

        scan_info = FScanInfo(scan_name, "aeroycontscan", inpars.npoints)
        scan_info.set_fscan_pars(self.pars)
        if inpars.x_range == 360.:
            xname = f"axis:{inpars.x_motor_name}"
            scan_info.set_channel_meta(xname,
                                       start= 0,
                                       stop= 360,
                                       points= inpars.npoints,
                                       axis_id= 0,
                                       axis_points= inpars.x_npoints,
                                      )
            yname = f"axis:{inpars.y_motor_name}"
            y_min_step = min(inpars.y_step_size_arr)
            scan_info.set_channel_meta(yname,
                                       start=inpars.y_start_pos,
                                       stop=inpars.y_stop,
                                       points= inpars.npoints,
                                       axis_id= 1,
                                       axis_points= int((inpars.y_stop-inpars.y_start_pos)/y_min_step)+1,
                                      )

            if inpars.scatter_plot is True:
                scan_info.add_scatter_plot(x=xname, y=yname)

        timer_name = self.synccalc.get_channel_name("trig", "timer")
        scan_info.add_curve_plot(x=timer_name)

        scan_info.set_fscan_info(user_info)

        scan = Scan(
            chain,
            name=scan_name,
            save=inpars.save_flag,
            scan_info=scan_info,
            scan_progress=FScanDisplay(timer_name),
        )

        return scan

    def get_runner(self, name=None):
        if name is None:
            name = self.name
        return FScanDiagRunner(name, self)


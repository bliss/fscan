import gevent

from bliss.scanning.chain import AcquisitionMaster
from bliss.common.motor_group import Group
from bliss.common.logtools import log_info
from bliss.comm.tcp import SocketTimeout
from bliss.common.logtools import disable_print

from fscan.motortools import rotation_home

def test_start_position(current_position, start_position):
    pos = (current_position + 180)%360 - 180
    disp = start_position - pos
    offset = (disp + 180)//360*360
    new_position = pos + offset
    new_disp = start_position - new_position
    print(f"modulo position = {pos}")
    print(f"set offset      = {offset}")
    print(f"new position    = {new_position}")
    print(f"displacement    = {new_disp}")
    print(f"start position  = {new_position + new_disp}")
    
class AeroXYProgMaster(AcquisitionMaster):
    def __init__(self, xmot, xstart, ymot, ystart, aero_prog, trigger_type=AcquisitionMaster.HARDWARE, **keys):
        self.x_axis = xmot
        self.x_start = xstart
        self.y_axis = ymot
        self.y_start = ystart
        self.aero_prog = aero_prog
        self.x_home_cb = None
        self.x_offset = None

        self._polling_time = max(self.x_axis._polling_time, self.y_axis._polling_time, 0.1)

        mot_group = Group(xmot, ymot)
        AcquisitionMaster.__init__(
            self, mot_group, trigger_type=trigger_type, **keys
        )

    def set_xhome_cb(self, func):
        self.x_home_cb = func

    def prepare(self):
        self.aero_prog.prepare()
        print("\nHoming {0} ...".format(self.x_axis.name))
        rotation_home(self.x_axis)
        if self.x_home_cb is not None:
            # round current position in [-180, 180] deg
            x_pos = (self.x_axis.position + 180)%360 - 180
            # calculate new offset to remove full 360 rotations
            x_disp = self.x_start - x_pos
            x_offset = (x_disp + 180)//360*360
            # set new offset
            if x_offset != 0:
                with disable_print():
                    self.x_axis.position = x_pos + x_offset
                self.x_offset = x_offset
            # call sync callback (musst)
            self.x_home_cb(self.x_axis)
        self.device.move(self.x_axis, self.x_start, self.y_axis, self.y_start)

    def start(self):
        if self.trigger_type == AcquisitionMaster.SOFTWARE:
            return
        self.trigger()

    def trigger(self):
        if self.trigger_type == AcquisitionMaster.SOFTWARE:
            self.trigger_slaves()

        self.aero_prog.start()
        state = self.aero_prog.state()
        if state != "RUNNING":
            error = self.aero_prog.error()
            raise RuntimeError(f"Failed to start aerotech program [state={state}, err={error}]")

    def trigger_ready(self):
        return self.aero_prog.state() == "READY"

    def wait_ready(self):
        state = "RUNNING"
        while state == "RUNNING":
            gevent.sleep(self._polling_time)
            try:
                state = self.aero_prog.state()
            except SocketTimeout:
                log_info(self.x_axis.controller, "Trap one timeout on aerotech wait_ready")
                gevent.sleep(0.01)
                state = self.aero_prog.state()

    def stop(self):
        if self.aero_prog.state() == "RUNNING":
            self.aero_prog.stop()
            self.wait_ready()
            gevent.sleep(0.2)
        if self.device.is_moving:
            self.device.stop()
        while self.device.is_moving:
            gevent.sleep(self._polling_time)
        gevent.sleep(0.25)
        for axis in [self.x_axis, self.y_axis]:
            axis.sync_hard()
        if self.x_offset is not None:
            with disable_print():
                self.x_axis.position -= self.x_offset


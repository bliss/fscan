import numpy
import gevent.lock

AeroProgErrors = {
    "0": "TaskError_NoError",
    "1": "TaskError_ArgumentOutOfBounds",
    "2": "TaskError_InvalidRegisterType",
    "3": "TaskError_PortAlreadyOpen",
    "4": "TaskError_InvalidSyncTask",
    "5": "TaskError_InvalidProgramPassword",
    "6": "TaskError_InvalidEmbeddedCommandIssued",
    "7": "TaskError_CorruptFlashMemoryFound",
    "8": "TaskError_UnimplementedCommand",
    "10": "TaskError_FeatureNotSupportedByHardware",
    "11": "TaskError_AxisCurrentlyInFault",
    "12": "TaskError_InvalidEmbeddedFunctionRev",
    "82": "TaskError_PortNotOpen",
    "13": "TaskError_TaskNotEnabled",
    "14": "TaskError_CodeSectionIsTooSmall",
    "15": "TaskError_DataSectionIsTooSmall",
    "16": "TaskError_CompilerVersionMismatch",
    "17": "TaskError_InvalidAxisGiven",
    "18": "TaskError_InvalidAxisConfiguration",
    "19": "TaskError_CNCFiveError",
    "20": "TaskError_NoJoystickPairsEnabled",
    "76": "TaskError_FeatureNotEnabled",
    "94": "TaskError_RequiredStageSerialMismatch",
    "21": "TaskError_FileNameIsTooLarge",
    "22": "TaskError_InsufficientFileSystemMemory",
    "23": "TaskError_FileCurrentlyExists",
    "24": "TaskError_FileDoesNotExist",
    "25": "TaskError_CorruptFileFound",
    "26": "TaskError_FileOptimizeInterrupt",
    "27": "TaskError_FileAccessPastEndOfFile",
    "28": "TaskError_FileNotOpen",
    "29": "TaskError_FileOpenedAsReadOnly",
    "30": "TaskError_FileOpenedAsWriteOnly",
    "31": "TaskError_NoFileHandlesAvailable",
    "32": "TaskError_FileAlreadyOpen",
    "79": "TaskError_InvalidFileName",
    "95": "TaskError_FileNameIsEmpty",
    "33": "TaskError_ReadFileBufferTooSmall",
    "34": "TaskError_FileWriteWithCRCOn",
    "35": "TaskError_HeapAllocationFailure",
    "37": "TaskError_AxisNotInTasksPlane",
    "38": "TaskError_ErrorOnFirmwareUpload",
    "84": "TaskError_FlashConfigCommitFailed",
    "86": "TaskError_OneWireDeviceNotFound",
    "87": "TaskError_OneWireDeviceCopyFailure",
    "88": "TaskError_OneWireDeviceWriteBusy",
    "89": "TaskError_OneWireDeviceReadBusy",
    "39": "TaskError_MotionActive",
    "40": "TaskError_ProfileEntryError",
    "41": "TaskError_InvalidHomeConfiguration",
    "42": "TaskError_AmplifierNotEnabled",
    "43": "TaskError_RadiusSpecifiedWithFullCircle",
    "44": "TaskError_RadiusTooShortForArc",
    "45": "TaskError_PlaneProfiling",
    "46": "TaskError_PVLengthError",
    "48": "TaskError_CamAbsIndexInCurrentPlane",
    "49": "TaskError_CamNotEnabled",
    "50": "TaskError_CamNotMonotonic",
    "51": "TaskError_CamPreviouslyEnabled",
    "52": "TaskError_CamSearchCountExceeded",
    "53": "TaskError_CamTimeOverRun",
    "54": "TaskError_FailedCamCommToSlave",
    "55": "TaskError_InvalidCamConfiguration",
    "56": "TaskError_InvalidCamContextCmd",
    "57": "TaskError_InvalidCamListSize",
    "58": "TaskError_MasterNotACammingAxis",
    "59": "TaskError_NoCamContext",
    "60": "TaskError_NotFindCamSegment",
    "61": "TaskError_InvalidFirstLastCamPos",
    "62": "TaskError_NoGantrySlaveMotion",
    "75": "TaskError_CircularRadiusError",
    "77": "TaskError_MasterMotionSuppressed",
    "78": "TaskError_InvalidTimeSpecified",
    "80": "TaskError_InvalidMotorType",
    "81": "TaskError_MXReprogramming",
    "83": "TaskError_JoystickInterlockOpen",
    "85": "TaskError_PVCannotExecute",
    "90": "TaskError_PiezoStageNotConnected",
    "92": "TaskError_AxisNumberConfigurationNotSupported",
    "93": "TaskError_AutofocusNotEnabled",
    "96": "TaskError_TrajectoryTableAlreadyRunning",
    "67": "TaskError_StackOverflow",
    "68": "TaskError_ArrayOutOfBounds",
    "69": "TaskError_DivisionByZero",
    "70": "TaskError_StringAssignmentOverflow",
    "71": "TaskError_SyncTimeOverrun",
    "72": "TaskError_TaskMonitorError",
    "73": "TaskError_OnTaskError",
    "74": "TaskError_SemaphoreStarvation",
}

def _get_var_type(vtype):
    if vtype not in ("DGLOBAL", "IGLOBAL"):
        raise ValueError("Invalid aero prog variable type")
    return vtype

def _get_array_type(vtype):
    if vtype == "IGLOBAL":
        return numpy.int32
    else:
        return numpy.float64

class AeroProgVariable(object):
    def __init__(self, name, vtype, vaddr):
        self.name = name
        self.vtype = _get_var_type(vtype)
        self.vaddr = int(vaddr)

    def convert(self, value):
        if self.vtype == "IGLOBAL":
            return int(value)
        else:
            return float(value)

    def write(self, ctrl, value):
        cmd = "{0}({1}) = {2}".format(self.vtype, self.vaddr, self.convert(value))
        ctrl.raw_write(cmd)

    def read(self, ctrl):
        cmd = "{0}({1})".format(self.vtype, self.vaddr)
        val = ctrl.raw_write_read(cmd)
        return self.convert(val)

class AeroProgFixedArray(object):
    def __init__(self, name, vtype, vaddr, vsize):
        self.name = name
        self.vtype = _get_var_type(vtype)
        self.vaddr = int(vaddr)
        self.vsize = int(vsize)
        self.atype = _get_array_type(vtype)

    def write(self, ctrl, value):
        arr = value.flatten()
        if arr.shape[0] != self.vsize:
            raise ValueError("Aero Prog Value [{0}] has not the correct size".format(self.name))
        if arr.dtype != self.atype:
            raise ValueError("Aero Prog Value [{0}] has not the correct type".format(self.name))
        for (idx, val) in enumerate(arr):
            cmd = "{0}({1}) = {2}".format(self.vtype, self.vaddr + idx, val)
            ctrl.raw_write(cmd)

    def read(self, ctrl):
        values = list()
        for idx in range(self.vsize):
            cmd = "{0}({1})".format(self.vtype, self.vaddr + idx)
            val = ctrl.raw_write_read(cmd)
            values.append(val)
        return numpy.array(values, dtype=self.atype)

class AeroProgFreeArray(object):
    def __init__(self, name, vtype, vaddr, sizevar):
        self.name = name
        self.vtype = _get_var_type(vtype)
        self.vaddr = int(vaddr)
        self.sizevar = str(sizevar)
        self.atype = _get_array_type(vtype)

    def write(self, ctrl, value):
        arr = value.flatten()
        size = arr.shape[0]
        if arr.dtype != self.atype:
            raise ValueError("Aero Prog Value [{0}] has not the correct type".format(self.name))
        # -- write the array size
        cmd = "{0} = {1:d}".format(self.sizevar, size)
        ctrl.raw_write(cmd)
        # -- write the array data
        for (idx, val) in enumerate(arr):
            cmd = "{0}({1}) = {2}".format(self.vtype, self.vaddr + idx, val)
            ctrl.raw_write(cmd)

    def read(self, ctrl):
        # -- read the array size
        val = ctrl.raw_write_read(self.sizevar)
        size = int(val)
        # -- read the array data
        values = list()
        for idx in range(size):
            cmd = "{0}({1})".format(self.vtype, self.vaddr + idx)
            val = ctrl.raw_write_read(cmd)
            values.append(val)
        return numpy.array(values, dtype=self.atype)

class AeroProgBase(object):
    def __init__(self, controller, progname, progvars=list(), progtask=1):
        self.controller = controller
        self.progname = progname
        self.progtask = progtask
        self.lock = gevent.lock.Semaphore()
        self._par_cmd = dict()
        for par in progvars:
            self._par_cmd[par.name] = par
        self.reset()

    def __get_par(self, name):
        par = self._par_cmd.get(name, None)
        if par is None:
            raise ValueError("Invalid aero prog parameter [{0}]".format(name))
        return par

    def names(self):
        return tuple(self._par_cmd.keys())

    def reset(self):
        self._par_val = dict()
        for name in self._par_cmd.keys():
            self._par_val[name] = None
        self.__prepared = False

    def set(self, name, value):
        self.__get_par(name)
        self._par_val[name] = value

    def read(self, name):
        par = self.__get_par(name)
        return par.read(self.controller)

    def read_all(self):
        values = dict()
        for (name, par) in self._par_cmd.items():
            values[name] = par.read(self.controller)
        return values

    def prepare(self):
        for (name, par) in self._par_cmd.items():
            value = self._par_val[name]
            if value is None:
                raise ValueError("Missing value for parameter [{0}]".format(name))
            par.write(self.controller, value)
        self.__prepared = True
         
    def start(self):
        if not self.__prepared:
            raise RuntimeError("Cannot start AeroProg not yet prepared")
        cmd = 'PROGRAM RUN {0},"{1}"'.format(self.progtask, self.progname)
        with self.lock:
            self.controller.raw_write(cmd)

    def stop(self):
        cmd = "PROGRAM STOP {0}".format(self.progtask)
        with self.lock:
            self.controller.raw_write(cmd)

    def state(self):
        cmd = "TASKSTATE({0})".format(self.progtask)
        with self.lock:
            state = self.controller.raw_write_read(cmd)
        istate = int(state)
        if istate == 3 or istate == 4:
            return "RUNNING"
        elif istate == 6:
            return "ERROR"
        elif istate == 0:
            return "DISABLED"
        else:
            return "READY"

    def error(self):
        cmd = "TASKERROR({0})".format(self.progtask)
        with self.lock:
            val = self.controller.raw_write_read(cmd)
        err = AeroProgErrors.get(val, "Unknown [{0}]".format(val))
        return err

class AeroProgXYContTest(AeroProgBase):
    def __init__(self, controller):
        progname = "xyconttest.bcx"
        progtask = 1
        progvars = [
            AeroProgVariable("xdist", "DGLOBAL", 0),
            AeroProgVariable("xvel", "DGLOBAL", 1),
            AeroProgVariable("xacc", "DGLOBAL", 2),
            AeroProgVariable("ydist", "DGLOBAL", 3),
            AeroProgVariable("yvel", "DGLOBAL", 4),
            AeroProgVariable("yacc", "DGLOBAL", 5),
            AeroProgVariable("acqt", "DGLOBAL", 6),
        ]
        AeroProgBase.__init__(self, controller, progname, progvars, progtask)

class AeroProgXYPVT(AeroProgBase):
    def __init__(self, controller):
        progname = "xypvt.bcx"
        progtask = 1
        progvars = [
            AeroProgFreeArray("pvt", "DGLOBAL", 0, "IGLOBAL(0)"),
        ]
        AeroProgBase.__init__(self, controller, progname, progvars, progtask)

class AeroProgXContYStep(AeroProgBase):
    def __init__(self, controller):
        progname = "xcontystep.bcx"
        progtask = 1
        progvars = [
            AeroProgFixedArray("xpvt", "DGLOBAL", 0, 4*3),
            AeroProgFreeArray("nstep", "IGLOBAL", 1, "IGLOBAL(0)"),
            AeroProgFreeArray("ystep", "DGLOBAL", 12, "IGLOBAL(0)"),
        ]
        AeroProgBase.__init__(self, controller, progname, progvars, progtask)


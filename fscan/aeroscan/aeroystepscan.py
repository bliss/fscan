import numpy
import tabulate

from bliss.scanning.scan import Scan
from bliss.controllers.ct2.device import AcqMode as CT2AcqMode

from fscan.fscantools import (
    FScanMode,
    FScanParamBase,
    FScanParamStruct,
    FScanDisplay,
)
from fscan.synchro.musst.difftomo import MusstProgDiffTomoStep
from fscan.musstcalc import (
    MusstCalcAcqSlave, 
    MusstCalcFilterEveryNPoints,
)
from fscan.motortools import (
    calc_move_time,
    check_motor_limits,
)
from fscan.mcatools import get_fscan_mca_params
from fscan.synchro.musst.programs import MusstProgTimeTrigger
from fscan.chaintools import ChainTool
from fscan.fscanrunner import FScanDiagRunner
from fscan.fscaninfo import FScanInfo
from fscan.aeroscan.aeromaster import AeroXYProgMaster
from fscan.aeroscan.aeroprog import AeroProgXContYStep
from fscan.limatools import LimaCalib

class AeroScanXYSPars(FScanParamBase):
    DEFAULT = {
        "x_start_pos": 0,
        "x_step_size": 1.0,
        "x_step_time": 0.,
        "x_range": 360.,
        "y_start_pos": 0.,
        "y_step_size_arr": (.2, .1),
        "y_npoints_arr": (6, 4),
        "acq_time": 0.008,
        "scan_mode": FScanMode.TIME,
        "min_gate_low_time": 0.,
        "x_acc_margin": 0.,
        "y_time_margin": 0.,
        "save_flag": True,
        "display_flag": True,
        "sampling_time": 0.5,
        "scatter_plot": False,
    }
    OBJKEYS = []
    LISTVAL = {
        "scan_mode": FScanMode.values,
    }
    NOSETTINGS = ["save_flag",]

    def __init__(self, name):
        FScanParamBase.__init__(self, 
                                name,
                                AeroScanXYSPars.DEFAULT,
                                AeroScanXYSPars.OBJKEYS, 
                                AeroScanXYSPars.LISTVAL,
                                AeroScanXYSPars.NOSETTINGS,
                               )
    def _validate_x_range(self, value):
        if value < 0.:
            print("WARNING : Automatically set x_range positive.")
        return abs(value)

    def _validate_y_npoints_arr(self, value):
        try:
            iter_val = iter(value)
        except TypeError:
            iter_val = iter((value,))
        return tuple(int(val) for val in iter_val)

    def _validate_y_step_size_arr(self, value):
        try:
            iter_val = iter(value)
        except TypeError:
            iter_val = iter((value,))
        return tuple(float(val) for val in iter_val)

    def _validate_scan_mode(self, value):
        return FScanMode.get(value, "scan_mode")

    def _validate_save_flag(self, value):
        return value and True or False

    def _validate_display_flag(self, value):
        return value and True or False

    def _validate_scatter_plot(self, value):
        return value and True or False

class AeroScanXYSMaster(object):
    def __init__(self, xmotor, ymotor, measgroup, musstdev):
        self.name = "aeroystepscan"
        self.xmotor = xmotor
        self.ymotor = ymotor
        self.meas_group = measgroup
        self.musst = musstdev
        self.lima_calib = LimaCalib()

        self.pars = AeroScanXYSPars(self.name)
        self.inpars = None

    def validate(self):
        self.inpars = FScanParamStruct(self.pars.to_dict())
        pars = self.inpars

        # --- only positive direction
        if pars.x_step_size < 0.:
            raise ValueError("only positive x_step_size implemented !!")

        # --- motor generic params
        pars.x_motor_name = self.xmotor.name
        pars.x_motor_unit = self.xmotor.unit
        pars.y_motor_name = self.ymotor.name
        pars.y_motor_unit = self.ymotor.unit

        # --- tuple to numpy arrays
        pars.y_step_size_arr = numpy.array(pars.y_step_size_arr)
        pars.y_npoints_arr = numpy.array(pars.y_npoints_arr)
        pars.y_total_npoints = pars.y_npoints_arr.sum()

        # --- check number of points
        pars.x_npoints = abs(pars.x_range / pars.x_step_size)
        if int(pars.x_npoints) != pars.x_npoints:
            raise ValueError("x_range musst be a multiple of x_step_size")
        pars.x_npoints = int(pars.x_npoints)
        if len(pars.y_step_size_arr) != len(pars.y_npoints_arr):
            raise ValueError("y_step_size_arr and y_npoints_arr arrays must have the same length")

        # --- check lima minimum period
        limas = self.get_controllers_found("lima", "lima2")
        pars.latency_time = 0.0
        self.lima_calib.validate(limas, pars, use_acc=False)

        # --- check step time         
        if pars.lima_ndev:
            if pars.x_step_time <= 0:
                pars.x_step_time = pars.lima_min_period
            elif pars.x_step_time < pars.lima_min_period:
                raise ValueError(f"x_step_time should be null or >= lima minimum period of {pars.lima_min_period}")
        else:
            if pars.x_step_time < pars.acq_time:
                pars.x_step_time = pars.acq_time

        # --- check gate time
        pars.gate_time = min(pars.x_step_time - pars.min_gate_low_time, pars.acq_time)

        # --- x motor speed / acceleration
        pars.x_speed = abs(pars.x_step_size) / pars.x_step_time
        pars.x_acc_time = pars.x_acc_margin / pars.x_speed + pars.x_speed / self.xmotor.acceleration
        pars.x_acc_disp = pars.x_acc_margin + 0.5 * pars.x_speed ** 2 / self.xmotor.acceleration
        pars.x_real_start = pars.x_start_pos - pars.x_acc_disp

        # --- y motor step time
        pars.y_step_time_arr = numpy.array([calc_move_time(self.ymotor, ystep) for ystep in pars.y_step_size_arr])
        pars.y_step_time_arr += pars.y_time_margin

        # --- x delta
        pars.x_delta_time = (int(pars.y_step_time_arr.max() / pars.x_step_time) + 1) * pars.x_step_time
        pars.x_delta_size = pars.x_delta_time * pars.x_speed
        if pars.x_range < 360.:
            delta_size = 360. - pars.x_range
            if delta_size > pars.x_delta_size:
                pars.x_delta_size = delta_size
                pars.x_delta_time = delta_size / pars.x_speed

        # --- acq size
        pars.dead_time = pars.x_step_time - pars.acq_time
        pars.x_acq_size = pars.acq_time * pars.x_speed
        pars.y_npoints = pars.y_npoints_arr.sum()
        pars.npoints = int(pars.x_npoints * pars.y_npoints_arr.sum())

        # --- x range
        pars.x_range_arr = pars.y_npoints_arr * (pars.x_range + pars.x_delta_size)
        pars.x_stop_pos = pars.x_start_pos + pars.x_range_arr.sum() - pars.x_delta_size
        pars.x_real_stop = pars.x_stop_pos + pars.x_acc_disp

        # --- x pvt array
        pars.x_pvt_arr = numpy.zeros((4, 3), numpy.float64)
        # acc phase
        pars.x_pvt_arr[0,:] = [ pars.x_acc_disp, pars.x_speed, pars.x_acc_time ]
        # acq phase
        pars.x_pvt_arr[1,:] = [ pars.x_range, pars.x_speed, pars.x_range / pars.x_speed ]
        # y step phase
        pars.x_pvt_arr[2,:] = [ pars.x_delta_size, pars.x_speed, pars.x_delta_time ]
        # dec phase
        pars.x_pvt_arr[3,:] = [ pars.x_acc_disp, 0., pars.x_acc_time ]

        # --- y nstep array
        pars.y_nstep_arr = numpy.array(pars.y_npoints_arr, numpy.int32)
        pars.y_nstep_arr[0] -= 1
        pars.y_start_pos_arr = numpy.array((0,) + tuple(pars.y_nstep_arr * pars.y_step_size_arr))
        pars.y_start_pos_arr = pars.y_start_pos + pars.y_start_pos_arr.cumsum()
        pars.y_stop_pos = pars.y_start_pos_arr[-1]

        # --- time array
        pars.time_arr = pars.y_npoints_arr * ( pars.x_range / pars.x_speed ) + pars.y_nstep_arr * pars.x_delta_time
        pars.total_time = pars.time_arr.sum() + 2 * pars.x_acc_time

        # --- check motor limits
        check_motor_limits(self.xmotor, pars.x_real_start, pars.x_real_stop)
        check_motor_limits(self.ymotor, pars.y_start_pos, pars.y_stop_pos)
        
    def show(self):
        if self.inpars is None:
            raise RuntimeError(f"{self.name}: Parameters not validated yet !!")

        rot_txt  = """
X - Rotation Motor : {x_motor_name}
    start pos     = {x_start_pos:.4f} [{x_motor_unit}]
    range         = {x_range} [{x_motor_unit}]
    stop pos      = {x_stop_pos:.4f} [{x_motor_unit}]
    step size     = {x_step_size:.4f} [{x_motor_unit}]
    velocity      = {x_speed:.2f} [{x_motor_unit}/sec]
    accelerarion  = {x_acc_disp:.4f} [{x_motor_unit}] in {x_acc_time:.3f} [sec]
"""

        acq_txt = """
Rotation movement during y-steps:
    delta size = {x_delta_size} [{x_motor_unit}]
    delta time = {x_delta_time} [sec]

Acquisition :
    acq size  = {x_acq_size:.4f} [{x_motor_unit}]
    acq time  = {acq_time:.6f} [sec]
    dead time = {dead_time:.6f} [sec]
    nb points = {npoints} (X:{x_npoints} * Y:{y_total_npoints})

Total time = {total_time:.3f} [sec]
"""

        pars = self.inpars
        nzone = len(pars.y_step_size_arr)
        tab = list()
        tab.append(("from position", f"[{pars.y_motor_unit}]") + tuple(pars.y_start_pos_arr[:nzone]))
        tab.append(("to position", f"[{pars.y_motor_unit}]") + tuple(pars.y_start_pos_arr[1:]))
        tab.append(("step size", f"[{pars.y_motor_unit}]") + tuple(pars.y_step_size_arr))
        tab.append(("npoints", "",) + tuple(pars.y_npoints_arr))
        tab.append(("time", "[sec]",) + tuple(pars.time_arr))

        print(rot_txt.format(**pars.to_dict()))
        print("Y - Translation Motor : {0}".format(pars.y_motor_name))
        print(tabulate.tabulate(tab, stralign="rigth"))
        print(acq_txt.format(**pars.to_dict()))

    def get_controllers_found(self, *ctrl_type):
        mgchain = ChainTool(self.meas_group)
        return mgchain.get_controllers(*ctrl_type)

    def get_controllers_used(self, ctrl_type):
        if self.mgchain is None:
            return []
        else:
            return self.mgchain.get_controllers_done(ctrl_type)

    def init_scan(self):
        self.mgchain = ChainTool(self.meas_group)
        self.lima_used = list()
        self.lima2_used = list()
        self.mca_used = list()

    def setup_acq_chain(self, chain):
        mot_master = self.setup_motor_master(chain)
        acq_master = self.setup_acq_master(chain, mot_master)
        self.setup_acq_slaves(chain, acq_master)

    def setup_motor_master(self, chain):
        pars = self.inpars
        self.musst_motors = [ self.xmotor, self.ymotor ]
        self.aero_prog = AeroProgXContYStep(self.xmotor.controller)
        self.aero_prog.set("xpvt", pars.x_pvt_arr)
        self.aero_prog.set("nstep", pars.y_nstep_arr)
        self.aero_prog.set("ystep", pars.y_step_size_arr)

        return AeroXYProgMaster(
                  self.xmotor,
                  pars.x_real_start,
                  self.ymotor,
                  pars.y_start_pos,
                  self.aero_prog
               )

    def setup_acq_master(self, chain, mot_master):
        pars = self.inpars

        # --- musst prog
        musstprog = MusstProgDiffTomoStep(self.musst, self.musst_motors)
        musstprog.check_max_timer(pars.total_time)

        start = pars.x_start_pos
        if pars.scan_mode == FScanMode.POSITION:
            step = pars.x_step_size
        else:
            step = pars.x_step_time
        if pars.scan_mode == FScanMode.CAMERA:
            gate = pars.min_gate_low_time
        else:
            gate = pars.gate_time
        npulses = pars.x_npoints
        nloop = pars.y_total_npoints
        delta = pars.x_range + pars.x_delta_size

        musstprog.set_params(pars.scan_mode, start, step, gate, npulses, nloop, delta)

        # --- sync y motor on musst
        musstprog.sync_one_motor(self.ymotor)

        # --- sync x on musst after homing
        mot_master.set_xhome_cb(musstprog.sync_one_motor)

        # --- add to  chain
        musstprog.setup(chain, mot_master)

        # --- calc device
        musstcalc = MusstCalcAcqSlave(musstprog.acq_data_master, musstprog.channels, musstprog.DataPerPoint)
        musstcalc.add_default_calc()
        musstcalc.add_calc("cen360", pars.x_motor_name)
        musstcalc.set_output_filter(MusstCalcFilterEveryNPoints, npulses)

        chain.add(musstprog.acq_master, musstcalc)

        # --- add motor external channels
        x_calc_name = musstcalc.get_channel_short_name("cen360", pars.x_motor_name)
        y_calc_name = musstcalc.get_channel_short_name("trig", pars.y_motor_name)
        mot_master.add_external_channel(musstcalc, x_calc_name, f"axis:{pars.x_motor_name}")
        mot_master.add_external_channel(musstcalc, y_calc_name, f"axis:{pars.y_motor_name}")

        self.syncprog = musstprog
        self.synccalc = musstcalc

        return musstprog.acq_master

    def setup_acq_slaves(self, chain, acq_master):
        pars = self.inpars

        # --- ct2 counters 
        ct2_params={
            "npoints": pars.npoints,
            "acq_mode": CT2AcqMode.ExtGate,
            "prepare_once": True,
            "start_once": True
        }
        self.mgchain.setup("ct2", chain, acq_master, ct2_params)

        # --- lima devices
        lima_params = {
            "acq_mode": "SINGLE",
            "acq_trigger_mode": "EXTERNAL_TRIGGER_MULTI",
            "acq_expo_time": pars.acq_time,
            "acq_nb_frames": pars.npoints,
            "wait_frame_id": [pars.npoints-1,],
            "prepare_once": True,
            "start_once": True,
            "scan_frame_per_file": pars.x_npoints,
        }
        self.mgchain.setup("lima", chain, acq_master, lima_params)
        self.lima_used = self.mgchain.get_controllers_done("lima")

        # --- lima2 devices
        lima_params = {
            "trigger_mode": "external",
            "expo_time": pars.acq_time,
            "nb_frames": pars.npoints,
            "nb_frames_per_trigger": 1,
            "wait_frame_id": [ pars.npoints - 1, ],
            "prepare_once": True,
            "start_once": True,
        }
        self.mgchain.setup("lima2", chain, acq_master, lima_params)
        self.lima2_used = self.mgchain.get_controllers_done("lima2")

        # --- mca devices
        for mca_ctrl in self.mgchain.get_controllers_not_done("mca"):
            mca_params = get_fscan_mca_params(mca_ctrl, pars.npoints, pars.x_step_time)
            self.mgchain.setup("mca", chain, acq_master, mca_params, mca_ctrl.name)
            if "trigger_mode" in mca_params:
                # --- in spectrum reading mode
                self.mca_used.append(mca_ctrl)

        # --- sampling counters
        self.mgchain.setup_sampling(chain, pars.sampling_time)

    def setup_acq_scope(self, chain, mot_master):
        pars = self.inpars
        acqtime = max(0.010, pars.x_step_time / 10.)
        x_move_time = calc_move_time(self.xmotor, pars.x_real_stop - pars.x_real_start, pars.x_speed)
        npts = int((x_move_time + 0.1) / acqtime + 0.5)

        # --- musst time prog
        musstprog = MusstProgTimeTrigger(self.musst, self.musst_motors)
        musstprog.set_params(npts, acqtime)

        # --- sync y motor on musst
        musstprog.sync_one_motor(self.ymotor)

        # --- sync x motor on musst after homing
        mot_master.set_xhome_cb(musstprog.sync_one_motor)

        # --- acq chain
        musstprog.setup(chain, mot_master)
        musstprog.add_default_calc(chain)

    def setup_scan(self, chain, scan_name, user_info=None):
        inpars = self.inpars
        if inpars.display_flag:
            self.show()

        scan_info = FScanInfo(scan_name, "aeroystepscan", inpars.npoints)
        scan_info.set_fscan_pars(self.pars)

        if inpars.scatter_plot is True:
            xname = f"axis:{inpars.x_motor_name}"
            if inpars.x_range >= 360.:
                xstart = 0
                xstop = 360
            else:
                xstart = inpars.x_start_pos%360.
                xstop = xstart + inpars.x_range
            scan_info.set_channel_meta(xname,
                                       start= xstart,
                                       stop= xstop,
                                       points= inpars.npoints,
                                       axis_id= 0,
                                       axis_points= inpars.x_npoints,
                                      )
            yname = f"axis:{inpars.y_motor_name}"
            y_min_step = inpars.y_step_size_arr.min()
            scan_info.set_channel_meta(yname,
                                       start=inpars.y_start_pos,
                                       stop=inpars.y_stop_pos,
                                       points= inpars.npoints,
                                       axis_id= 1,
                                       axis_points= int((inpars.y_stop_pos - inpars.y_start_pos)/y_min_step)+1
                                      )
            scan_info.add_scatter_plot(x=xname, y=yname)

        timer_name = self.synccalc.get_channel_name("trig", "timer")
        scan_info.add_curve_plot(x=timer_name)

        scan_info.set_fscan_info(user_info)

        scan = Scan(
            chain,
            name=scan_name,
            save=inpars.save_flag,
            scan_info=scan_info,
            scan_progress=FScanDisplay(timer_name),
        )

        return scan

    def get_runner(self, name=None):
        if name is None:
            name = self.name
        return FScanDiagRunner(name, self)


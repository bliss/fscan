import math
import gevent
import numpy

from bliss.scanning.chain import AcquisitionMaster, ChainPreset, AcquisitionChannel
from bliss.common.axis import Trajectory
from bliss.common.motor_group import TrajectoryGroup
from bliss.common.logtools import disable_print

def estimate_move_time(motor, disp):
    try:
        vel = motor.velocity
        acc = motor.acceleration
        return calc_move_time(motor, disp)
    except NotImplementedError:
        return 0.


def calc_move_time(motor, disp, speed=None, display=False):
    """
    Calculate scan movement durations (ex: f2scan scan_time and sweepscan back_time).
    """
    if speed is None:
        speed = motor.velocity
    acc = motor.acceleration

    if acc == 0:
        tot_time = disp / speed

        if display is True:
            print("Null acceleration !!")
            print(f"Move time = {tot_time:.3f} sec")
        return tot_time

    acc_disp = 0.5 * speed**2 / acc

    if 2*acc_disp <= disp:
        acc_time = speed / acc
        cst_disp = disp - 2*acc_disp
        cst_time = cst_disp / speed
        tot_time = 2*acc_time + cst_time
    else:
        speed = math.sqrt(2*acc*(disp/2.))
        acc_time = speed / acc
        acc_disp = 0.5 * speed**2 / acc
        cst_disp = 0.
        tot_time = 2*acc_time

    if display is True:
        mot_unit = motor.unit is None and "unit" or motor.unit
        print("Acceleration displacement = {acc_disp:.3f} {mot_unit}".format(acc_disp, mot_unit))
        print("Acceleration time         = {acc_time:.3f} sec".format(acc_time))
        print("Maximum speed reached     = {speed:.3f} {mot_unit}/sec".format(speed, mot_unit))
        print("Constant speed displ.     = {cst_disp:.3f} {mot_unit}".format(cst_disp, mot_unit))
        print("TOTAL move time           = {tot_time:.3f} sec".format(tot_time))

    return tot_time

def rotation_home(motor, homedir=-1, homepos=None):
    mot_pos = motor.dial
    round_pos = -1.0 * mot_pos%360.0
    round_neg = round_pos - 360.0
    if abs(round_neg) < abs(round_pos):
        rmove_pos = round_neg
    else:
        rmove_pos = round_pos
    rmove_pos += (-1)*homedir*2.0
    with disable_print():
        motor.rmove(rmove_pos)
        motor.home(homedir, wait=True)
    if homepos is not None:
        motor.position = homepos

def rotation_round(motor, user_only=False):
    mot_pos = motor.position
    round_pos = mot_pos%360.0
    if round_pos != mot_pos:
        if round_pos > 180.0:
            round_pos -= 360.0
        if user_only is False:
            motor.dial = round_pos
            motor.position = motor.dial
        else:
            motor.position = round_pos

def check_motor_limits(axis, *positions):
    min_pos = min(positions)
    max_pos = max(positions)
    ll, hl = axis.limits
    if min_pos < ll:
        # get motion object, this will raise ValueError exception
        axis._get_motion(min_pos)
    elif max_pos > hl:
        # get motion object, this will raise ValueError exception
        axis._get_motion(max_pos)

def check_motor_velocity(axis, velocity):
    if axis.velocity_high_limit != numpy.inf:
        if velocity > axis.velocity_high_limit:
            raise ValueError(f"Velocity too high for {axis.name}. "
                             f"Max is {axis.velocity_high_limit:.3f} deg/sec.")
    if axis.velocity_low_limit != numpy.inf:
        if velocity < axis.velocity_low_limit:
            raise ValueError(f"Velocity too low for {axis.name}. "
                             f"Min is {axis.velocity_low_limit:.3f} deg/sec.")


class UndershootMixin:
    def __init__(self, undershoot=None, start_margin=0, end_margin=0):
        self._undershoot = undershoot
        self._undershoot_start_margin = start_margin
        self._undershoot_end_margin = end_margin

    @property
    def undershoot(self):
        if self._undershoot is not None:
            return self._undershoot
        
        acc = self.movable.acceleration
        if acc > 0:
            inv_acc = 1 / acc
        else:
            inv_acc = 0
        acctime = float(self.velocity) * inv_acc
        return self.velocity * acctime / 2

    def _calculate_undershoot(self, pos, end=False):
        d = 1 if self.end_pos >= self.start_pos else -1
        d *= -1 if end else 1
        pos -= d * self.undershoot
        if end:
            margin = d * self._undershoot_end_margin
        else:
            margin = d * self._undershoot_start_margin
        return pos - margin
    

class RotationMotorChainPreset(ChainPreset):
    def __init__(self, motor, homing=True, rounding=False, homepars=dict(), roundpars=dict()):
        self._motor = motor
        self._homing = homing
        self._home_pars = homepars
        self._rounding = rounding
        self._round_pars = roundpars
        self._sync_musst_cb = None

    def set_musst_sync_cb(self, func):
        self._sync_musst_cb  = func

    def prepare(self, chain):
        if self._homing is True:
            print("Homing {0} ...".format(self._motor.name))
            rotation_home(self._motor, **self._home_pars)
            if self._sync_musst_cb is not None:
                self._sync_musst_cb(self._motor)

    def stop(self, chain):
        if self._rounding is True:
            rotation_round(self._motor, **self._round_pars)


class MotorMaster(AcquisitionMaster, UndershootMixin):
    def __init__(
        self,
        axis,
        start,
        end,
        time=0,
        undershoot=None,
        undershoot_start_margin=0,
        undershoot_end_margin=0,
        trigger_type=AcquisitionMaster.SOFTWARE,
        **keys,
    ):
        AcquisitionMaster.__init__(self, axis, trigger_type=trigger_type, **keys)
        UndershootMixin.__init__(self, undershoot, undershoot_start_margin, undershoot_end_margin)

        self.movable = axis
        self.start_pos = start
        self.end_pos = end
        self.time = time

        if self.time <= 0:
            raise ValueError("Null travel time in MotorMaster")

        self.velocity = abs(self.end_pos - self.start_pos) / float(self.time)

    def prepare(self):
        start = self._calculate_undershoot(self.start_pos)
        self.movable.move(start)

    def start(self):
        if self.parent:
            return
        self.trigger()

    def trigger(self):
        self.trigger_slaves()
        return self._start_move()

    def _start_move(self):
        initial_velocity = self.movable.velocity
        try:
            with disable_print():
                self.movable.velocity = self.velocity
            end = self._calculate_undershoot(self.end_pos, end=True)
            self.movable.move(end)
        finally:
            with disable_print():
                self.movable.velocity = initial_velocity

    def trigger_ready(self):
        return not self.movable.is_moving

    def wait_ready(self):
        self.movable.wait_move()

    def stop(self):
        self.movable.stop()

class ExternalMotorMaster(MotorMaster):
    """
    MotorMaster class for fscan with fast motor started by an hardware signal.
    """
    def __init__(
        self,
        axis,
        start,
        end,
        time,
        fast_npoints,
        undershoot=None,
        undershoot_start_margin=0,
        undershoot_end_margin=0,
        trigger_type=AcquisitionMaster.SOFTWARE,
        **keys,
    ):
        
        self.fast_npoints = fast_npoints

        super().__init__(
            axis=axis, 
            start=start, 
            end=end, 
            time=time,
            undershoot=undershoot, 
            undershoot_start_margin=undershoot_start_margin,
            undershoot_end_margin=undershoot_end_margin,
            trigger_type=trigger_type,
            **keys,
            )
        
        self.axis_channame = f"axis:{self.movable.name}"
        self.channels.append(AcquisitionChannel(self.axis_channame, numpy.float64, ()))
        self.build_theoritical_motor_positions()
        
    def build_theoritical_motor_positions(self):
        """ Calculate time-based centered positions of fast axis """
        self.axis_pos = numpy.linspace(self.start_pos, self.end_pos, self.fast_npoints + 1)[:-1] 
        delta = self.axis_pos[1]-self.axis_pos[0]
        self.axis_pos += delta/2.

    def prepare(self):
        self.enable_gate_output()
        start = self._calculate_undershoot(self.start_pos)
        self.movable.move(start)
        
    def trigger(self):
        self.trigger_slaves()
        self.channels.update({self.axis_channame: self.axis_pos})
        return self._start_move()
        
    def stop(self):
        self.movable.stop()
        self.disable_gate_output()

    def enable_gate_output(self, **keys):
        """
        Motor-depending method to generate external trigger to start scan acquisition.
        To overload depending on controller.
        """
        pass

    def disable_gate_output(self, **keys):
        """
        To overload depending on controller.
        """
        pass

class MotorMasterWithTrajectory(AcquisitionMaster):
    def __init__(
        self,
        axis,
        start,
        end,
        time=0,
        undershoot=None,
        undershoot_start_margin=0,
        undershoot_end_margin=0,
        trigger_type=AcquisitionMaster.SOFTWARE,
        **keys,
    ):
        AcquisitionMaster.__init__(self, axis, trigger_type=trigger_type, **keys)

        self.movable = axis
        self.start_pos = start
        self.end_pos = end
        self.time = time
        self.undershoot = undershoot

        if self.time <= 0:
            raise ValueError("Null travel time in MotorMaster")

        self.velocity = abs(self.end_pos - self.start_pos) / float(self.time)
        self.acctime = self.velocity / axis.acceleration
        self.accdisp = self.velocity * self.acctime / 2.

        if undershoot is None:
            self.undershoot = self.accdisp
        else:
            if self.undershoot > self.accdisp:
                self.acctime += (self.accdisp - self.undershoot) / self.velocity

        sign = 1 if self.end_pos >= self.start_pos else -1
        self.start_undershoot = sign * (self.undershoot + undershoot_start_margin)
        self.end_undershoot = sign * (self.undershoot + undershoot_end_margin)
        self.start_time = self.acctime + undershoot_start_margin / self.velocity
        self.end_time = self.acctime + undershoot_end_margin / self.velocity

        pvt = numpy.zeros(4, dtype=[("position", "f8"), ("velocity", "f8"), ("time", "f8")])
        pvt[0] = (self.start_pos - self.start_undershoot, 0., 0.)
        pvt[1] = (self.start_pos, self.velocity, self.start_time)
        pvt[2] = (self.end_pos, self.velocity, self.start_time + self.time)
        pvt[3] = (self.end_pos + self.end_undershoot, 0., self.start_time + self.time + self.end_time)

        axis_trajectory = Trajectory(self.movable, pvt)
        self.trajectory = TrajectoryGroup(axis_trajectory)

    def prepare(self):
        self.trajectory.prepare()
        self.trajectory.move_to_start()

    def start(self):
        if self.parent:
            return
        self.trigger()

    def trigger(self):
        self.trigger_slaves()
        self.trajectory.move_to_end()

    def trigger_ready(self):
        return not self.trajectory.is_moving

    def wait_ready(self):
        self.trajectory.wait_move()

    def stop(self):
        self.trajectory.stop()


class MotorListMaster(AcquisitionMaster, UndershootMixin):
    """
    Motor Master for fscan2d.
    """
    def __init__(
        self,
        axis,
        start_list,
        end_list,
        time=0,
        undershoot=None,
        undershoot_start_margin=0,
        undershoot_end_margin=0,
        trigger_type=AcquisitionMaster.SOFTWARE,
        **keys
    ):
        AcquisitionMaster.__init__(
            self, axis, trigger_type=trigger_type, **keys
        )
        UndershootMixin.__init__(
            self, undershoot, undershoot_start_margin, undershoot_end_margin
        )

        if not isinstance(start_list, list) or not isinstance(end_list, list):
            raise ValueError("MotorMasterList: start_pos and end_pos must be list")

        if len(start_list) != len(end_list):
            raise ValueError("MotorMasterList: start_pos and end_pos list must have same length")

        self.movable = axis
        self.start_list = start_list
        self.end_list = end_list
        self.time = time

        self.start_pos = self.start_list[0]
        self.end_pos = self.end_list[0]

        self.velocity = (
                abs(self.end_pos - self.start_pos) / float(self.time)
                if self.time > 0
                else self.movable.velocity
            )

    def __iter__(self):
        self._iter_index = 0
        iter_pos = zip(self.start_list, self.end_list)
        niter = len(self.start_list)
        while self._iter_index < niter:
            (self.start_pos, self.end_pos) = next(iter_pos)
            yield self
            self._iter_index += 1

    def prepare(self):
        start = self._calculate_undershoot(self.start_pos)
        self.movable.move(start)

    def start(self):
        if self.parent:
            return
        self.trigger()

    def trigger(self):
        self.trigger_slaves()
        return self._start_move()

    def _start_move(self):
        initial_velocity = self.movable.velocity
        try:
            with disable_print():
                self.movable.velocity = self.velocity
            end = self._calculate_undershoot(self.end_pos, end=True)
            self.movable.move(end)
        finally:
            with disable_print():
                self.movable.velocity = initial_velocity

    def trigger_ready(self):
        return not self.movable.is_moving

    def wait_ready(self):
        self.movable.wait_move()

    def stop(self):
        self.movable.stop()


class ExternalMotorListMaster(MotorListMaster):
    """
    MotorListMaster class for fscan2d with fast motor started by an hardware signal.
    """
    def __init__(
        self,
        axis,
        start_list,
        end_list,
        time,
        fast_npoints,
        undershoot=None,
        undershoot_start_margin=0,
        undershoot_end_margin=0,
        trigger_type=AcquisitionMaster.SOFTWARE,
        **keys
    ):
        
        self.fast_npoints = fast_npoints

        super().__init__(
            axis=axis,
            start_list=start_list,
            end_list=end_list,
            time=time,
            undershoot=undershoot,
            undershoot_start_margin=undershoot_start_margin,
            undershoot_end_margin=undershoot_end_margin,
            trigger_type=trigger_type,
            **keys
        )

        self.axis_channame = f"axis:{self.movable.name}"
        self.channels.append(AcquisitionChannel(self.axis_channame, numpy.float64, ()))        
        
    def build_theoritical_motor_positions(self):
        """ Calculate time-based centered positions of fast axis """
        self.axis_pos = numpy.linspace(self.start_pos, self.end_pos, self.fast_npoints + 1)[:-1]
        delta = self.axis_pos[1]-self.axis_pos[0]
        self.axis_pos += delta/2.

    def __iter__(self):
        self._iter_index = 0
        iter_pos = zip(self.start_list, self.end_list)
        niter = len(self.start_list)
        while self._iter_index < niter:
            (self.start_pos, self.end_pos) = next(iter_pos)
            self.build_theoritical_motor_positions()
            yield self
            self._iter_index += 1
    
    def prepare(self):
        self.enable_gate_output()
        start = self._calculate_undershoot(self.start_pos)
        self.movable.move(start)

    def trigger(self):
        self.trigger_slaves()
        return self._start_move()
    
    def _start_move(self):
        self.initial_velocity = self.movable.velocity
        try:
            with disable_print():
                self.movable.velocity = self.velocity
            end = self._calculate_undershoot(self.end_pos, end=True)
            self.movable.move(end, wait=False)
            gevent.time.sleep(0.05)
            self.channels.update({self.axis_channame: self.axis_pos})
            self.movable.wait_move()
        finally:
            with disable_print():
                self.movable.velocity = self.initial_velocity

    def stop(self):
        self.movable.stop()
        self.disable_gate_output()

    def enable_gate_output(self, **keys):
        """
        Motor-depending method to generate external trigger to start scan acquisition.
        To overload depending on controller.
        """
        pass

    def disable_gate_output(self, **keys):
        """
        To overload depending on controller.
        """
        pass
    
        
class MotorZigZagMaster(AcquisitionMaster, UndershootMixin):
    def __init__(
        self,
        axis,
        start_pos,
        end_pos,
        time=0,
        undershoot=None,
        undershoot_start_margin=0,
        undershoot_end_margin=0,
        trigger_type=AcquisitionMaster.SOFTWARE,
        **keys
    ):
        AcquisitionMaster.__init__(
            self, axis, trigger_type=trigger_type, **keys
        )
        UndershootMixin.__init__(
            self, undershoot, undershoot_start_margin, undershoot_end_margin
        )

        self.movable = axis
        self.start_pos = start_pos
        self.end_pos = end_pos
        self.time = time
        self.velocity = (
                abs(self.end_pos - self.start_pos) / float(self.time)
                if self.time > 0
                else self.movable.velocity
            )
        self._move_task = None

    def prepare(self):
        self.real_start = self._calculate_undershoot(self.start_pos)
        self.real_stop = self._calculate_undershoot(self.end_pos, end=True)
        self.movable.move(self.real_start)

    def start(self):
        if self.parent:
            return
        self.trigger()

    def trigger(self):
        self.trigger_slaves()
        self._move_task = gevent.spawn(self._start_move())

    def _start_move(self):
        initial_velocity = self.movable.velocity
        try:
            with disable_print():
                self.movable.velocity = self.velocity
            self.movable.move(self.real_stop)
            self.movable.wait_move()
            self.movable.move(self.real_start)
            self.movable.wait_move()
        finally:
            with disable_print():
                self.movable.velocity = initial_velocity

    def trigger_ready(self):
        return not self.movable.is_moving

    def wait_ready(self):
        if self._move_task is not None:
            self._move_task.join()

    def stop(self):
        self.movable.stop()


class TwoMotorMaster(AcquisitionMaster):
    """
    Master for two independent motors.
    Synchronization only by acceleration and speed calculation.
    """

    def __init__(
        self,
        master_axis,
        master_start,
        master_end,
        slave_axis,
        slave_start,
        slave_end,
        time=0,
        master_undershoot=None,
        master_undershoot_start_margin=0,
        master_undershoot_end_margin=0,
        slave_undershoot=None,
        slave_undershoot_start_margin=0,
        slave_undershoot_end_margin=0,
        trigger_type=AcquisitionMaster.SOFTWARE,
        **keys,
    ):
        AcquisitionMaster.__init__(
            self, master_axis, slave_axis, trigger_type=trigger_type, **keys
        )

        self.master_movable = master_axis
        self.master = {}
        self.master["start_pos"] = master_start
        self.master["end_pos"] = master_end
        self.master["undershoot"] = master_undershoot
        self.master["undershoot_start_margin"] = master_undershoot_start_margin
        self.master["undershoot_end_margin"] = master_undershoot_end_margin
        self.master["velocity"] = master_axis.velocity
        self.master["acceleration"] = master_axis.acceleration

        self.slave_movable = slave_axis
        self.slave = {}
        self.slave["start_pos"] = slave_start
        self.slave["end_pos"] = slave_end
        self.slave["undershoot"] = slave_undershoot
        self.slave["undershoot_start_margin"] = slave_undershoot_start_margin
        self.slave["undershoot_end_margin"] = slave_undershoot_end_margin
        self.slave["velocity"] = slave_axis.velocity
        self.slave["acceleration"] = slave_axis.acceleration

        self.time = time

        #
        # calculate velocities
        #
        if self.time > 0:
            self.master["velocity"] = abs(master_end - master_start) / float(self.time)
        else:
            self.master["velocity"] = self.master_movable.velocity
            self.time = abs(master_end - master_start) / self.master_movable.velocity

        self.slave["velocity"] = abs(slave_end - slave_start) / float(self.time)

        #
        # calculate full acceleration times
        #
        master_acc_time = self.master["velocity"] / self.master["acceleration"]
        slave_acc_time = self.slave["velocity"] / self.slave["acceleration"]

        #
        # Calculate the time correction for the faster motor
        #
        if master_acc_time > slave_acc_time:
            time_diff = master_acc_time - slave_acc_time
            add_margin = time_diff * self.slave["velocity"]
            self.slave["undershoot_start_margin"] += add_margin
        else:
            time_diff = slave_acc_time - master_acc_time
            add_margin = time_diff * self.master["velocity"]
            self.master["undershoot_start_margin"] += add_margin

        #
        # Calculate real start/end pos
        #
        self.master["real_start"] = self.calculate_undershoot(self.master)
        self.master["real_end"] = self.calculate_undershoot(self.master, end=True)
        self.slave["real_start"] = self.calculate_undershoot(self.slave)
        self.slave["real_end"] = self.calculate_undershoot(self.slave, end=True)

    def calculate_undershoot(self, mot_dict, end=False):
        d = 1 if mot_dict["end_pos"] >= mot_dict["start_pos"] else -1
        d *= -1 if end else 1

        if mot_dict["undershoot"] is None:
            acctime = float(mot_dict["velocity"]) / mot_dict["acceleration"]
            undershoot = mot_dict["velocity"] * acctime / 2
        else:
            undershoot = mot_dict["undershoot"]

        if end:
            pos = mot_dict["end_pos"] - (d * undershoot)
            margin = d * mot_dict["undershoot_end_margin"]
        else:
            pos = mot_dict["start_pos"] - (d * undershoot)
            margin = d * mot_dict["undershoot_start_margin"]
        return pos - margin

    def prepare(self):
        self.device.move(
            {self.master_movable: self.master["real_start"], 
             self.slave_movable: self.slave["real_start"],
            }
        )

    def start(self):
        if self.parent:
            return
        self.trigger()

    def trigger(self):
        self.trigger_slaves()
        return self._start_move()

    def _start_move(self):
        master_init_velocity = self.master_movable.velocity
        slave_init_velocity = self.slave_movable.velocity
        try:
            with disable_print():
                self.master_movable.velocity = self.master["velocity"]
                self.slave_movable.velocity = self.slave["velocity"]

            self.device.move(
                {self.master_movable: self.master["real_end"],
                 self.slave_movable: self.slave["real_end"],
                }
            )
        finally:
            with disable_print():
                self.master_movable.velocity = master_init_velocity
                self.slave_movable.velocity = slave_init_velocity

    def trigger_ready(self):
        return not self.device.is_moving

    def wait_ready(self):
        self.device.wait_move()

    def stop(self):
        self.device.stop()

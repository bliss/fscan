from fscan.synchro.base import SyncDevice
from fscan.synchro.maestrio.base import MaestrioMotorChannel


def check_motor(syncdev, motor, relative_motion=1.):
    sdev = SyncDevice(syncdev)
    if sdev.is_musst():
        check_motor_on_musst(syncdev, motor, relative_motion)
    else:
        check_motor_on_maestrio(syncdev, motor, relative_motion)

def check_motor_on_maestrio(maestrio, motor, relative_motion=1.0):
    warn = 0

    munit = motor.unit is None and "unit" or motor.unit
    motres = motor.steps_per_unit * motor.sign
    try:
        encres = motor.encoder.steps_per_unit
    except AttributeError:
        encres = motres
    print(f"motor resolution   = {motres} steps/{munit}")
    print(f"encoder resolution = {encres} steps/{munit}")

    print("\n> Search motor on MAESTRIO:")
    try:
        chan = maestrio.get_channel_by_name(motor.name)
    except RuntimeError:
        print(f"!!! ERROR !!! Cannot find [{motor.name}] on maestrio [{maestrio.name}]")
        raise

    print(f"found on channel     = {chan.channel_id}")
    print(f"channel type         = {chan.mode_str}")
    if chan.mode_str not in ['BISS', 'ENDAT', 'HSSL', 'PULSE', 'QUAD', 'SSI']:
        print("!!! WARNING !!! maestrip channel is not an encoder. Check config.")
        warn += 1
    motchan = MaestrioMotorChannel(motor, chan)
    matres = motchan.sign * motchan.resolution
    print(f"maestrio resolution  = {matres} steps/{munit}")

    chan.sync(int(motchan.unit2raw(motor.position) + 0.5))

    def check_motion(motion):
        warn = 0
        print(f"\n> Move relative {motion:+g}:")
        steps_start = int(float(chan.value))
        motor.rmove(motion)
        steps_stop = int(float(chan.value))
        steps_done = steps_stop - steps_start
        steps_theo = motion * matres

        print(f"Steps seen on MAESTRIO  = {steps_done} steps")
        if steps_done == 0:
            print("!!! WARNING !!! No steps seen on maestrio. Check cabling.")
            warn += 1
        else:
            if (steps_done > 0 and steps_theo < 0) or (steps_done < 0 and steps_theo > 0):
                print("!!! WARNING !!! Direction on maestrio is opposite to motion.")
                warn += 1
            if abs(steps_done - steps_theo) > 2:
                print(f"!!! WARNING !!! Seen {steps_done} steps. Should get {steps_theo} steps.")
                warn += 1
        return warn

    warn += check_motion(relative_motion)
    warn += check_motion(-1.*relative_motion)

    print("\n> CHECK RESULT:")
    if not warn:
        print("All tests OK. You can use this motor/maestrio in fscans.")
    else:
        print(f"!!! {warn} WARNINGS !!! This motor/maestrio won't work in fscans.")
    print()


def check_motor_on_musst(musst, motor, relative_motion=1.0):
    warn = 0

    munit = motor.unit is None and "unit" or motor.unit
    mres = motor.steps_per_unit * motor.sign
    try:
        eres = motor.encoder.steps_per_unit
    except AttributeError:
        eres = mres
    print(f"motor resolution   = {mres} steps/{munit}")
    print(f"encoder resolution = {eres} steps/{munit}")

    print("\n> Search motor on MUSST:")
    try:
        chan = musst.get_channel_by_name(motor.name)
    except RuntimeError:
        print(f"!!! ERROR !!! Cannot find [{motor.name}] on musst [{musst.name}]")
        raise

    print(f"found on channel  = {chan.channel_id}")

    mtype = musst.putget(f"?CHCFG CH{chan.channel_id}")
    print(f"channel type      = {mtype}")
    if mtype.find("ENC") != 0:
        print("!!! WARNING !!! musst channel type is not an encoder. Check config.")
        warn += 1

    def check_motion(motion):
        warn = 0
        renc = int(motion * eres + 0.5)
        print(f"\n> Move relative {motion:+g}:")
        chan.value = 0
        motor.rmove(motion)
        steps = chan.value
        print(f"Steps seen on MUSST  = {steps} steps")
        if steps == 0:
            print("!!! WARNING !!! No steps seen on musst. Check cabling.")
            warn += 1
        else:
            if (steps > 0 and renc < 0) or (steps < 0 and renc > 0):
                print("!!! WARNING !!! Direction on musst is opposite to motion.")
                warn += 1
            if abs(steps - renc) > 2:
                print(f"!!! WARNING !!! Seen {steps} steps. Should get {renc} steps.")
                warn += 1
        return warn

    warn += check_motion(relative_motion)
    warn += check_motion(-1.*relative_motion)

    print("\n> CHECK RESULT:")
    if not warn:
        print("All tests OK. You can use this motor/musst in fscans.")
    else:
        print(f"!!! {warn} WARNINGS !!! This motor/musst won't work in fscans.")
    print()




from bliss.scanning.toolbox import ChainBuilder
from bliss.controllers.lima.lima_base import Lima
from bliss.controllers.lima2.controller import DetectorController as Lima2
from bliss.controllers.ct2.client import CT2Controller
from bliss.controllers.mca.base import BaseMCA
from bliss.controllers.mca.mythen import Mythen
from bliss.controllers.mosca.base import McaCounterController
from bliss.controllers.musst import musst, MusstSamplingCounterController
from bliss.controllers.tango_elettra import Elettra
from bliss.controllers.counter import (
    SamplingCounterController, 
    CalcCounterController, 
    CalcCounter,
)
from bliss.scanning.acquisition.timer import SoftwareTimerMaster
from bliss.common.measurementgroup import get_active

class ChainTool(object):
    ControllerClassMap = {
        "lima" : Lima,
        "lima2": Lima2,
        "mosca" : McaCounterController,
        "mca" : BaseMCA,
        "mythen" : Mythen,
        "ct2" : CT2Controller,
        "musst" : musst,
        "sampling" : SamplingCounterController,
        "elettra" : Elettra,
        "calc" : CalcCounterController,
    }

    def __init__(self, counters=None, chain_config=dict()):
        if counters is None:
            counters = [get_active()]
        elif not isinstance(counters, list):
            counters = [ counters, ]
        self.builder = ChainBuilder(counters)
        self.chain_config = chain_config
        self.__setup_done = dict()

    def type_list(self):
        return list(self.ControllerClassMap.keys())        

    def __get_ctrl_class(self, type_or_class):
        if type(type_or_class) is str:
            name = type_or_class.lower()
            klass = self.ControllerClassMap.get(name, None)
            if klass is None:
                raise ValueError(f"Unknown class type [{name}]")
            return klass
        return type_or_class

    def __get_ctrl_type(self, type_or_class):
        if type(type_or_class) is str:
            if type_or_class in self.ControllerClassMap:
                return type_or_class
            else:
                raise ValueError(f"Unknown class type [{type_or_class}]")
        else:
            return type_or_class.__class__.__name__

    def get_node_by_name(self, name):
        nodes = self.builder.get_nodes_by_controller_name(name)
        if not len(nodes):
            return None
        else:
            return nodes[0]

    def get_nodes_by_type(self, type_or_class, *names):
        node_list = list()
        klass = self.__get_ctrl_class(type_or_class)
        for node in self.builder.get_nodes_by_controller_type(klass):
            if len(names):
                if node.controller.name in names:
                    node_list.append(node)
            else:
                node_list.append(node)
        return node_list

    def get_nodes(self):
        return self.builder.get_top_level_nodes()

    def setup(self, type_or_class, chain, master, acq_pars, *names):
        typename = self.__get_ctrl_type(type_or_class)
        if typename not in self.__setup_done:
            self.__setup_done[typename] = list()

        for node in self.get_nodes_by_type(type_or_class, *names):
            # --- filter if already setup
            if node in self.__setup_done[typename]:
                continue

            # --- acq/ctrl parameters
            ctrl_params = dict()
            acq_params = dict(acq_pars)

            # --- update acq_pars with chain config
            ctrl_name = node.controller.name
            ctrl_cfg = self.chain_config.get(ctrl_name, dict())
            if "acq_params" in ctrl_cfg:
                acq_params.update(ctrl_cfg["acq_params"])
            if "ctrl_params" in ctrl_cfg:
                ctrl_params.update(ctrl_cfg["ctrl_params"])

            # --- lima case : frames_per_file and saving_history
            if typename == "lima":
                curr_pars = node.controller.get_current_parameters()
                acq_nb_frames = acq_params.get("acq_nb_frames", 1)
                frame_per_file = acq_params.pop("scan_frame_per_file", acq_nb_frames)
                if curr_pars["saving_frame_per_file"] == -1:
                    ctrl_params["saving_frame_per_file"] = frame_per_file
                elif curr_pars["saving_frame_per_file"] > acq_nb_frames:
                    ctrl_params["saving_frame_per_file"] = acq_nb_frames
                ctrl_params["saving_statistics_history"] = acq_nb_frames

            # --- lima2 case : everything is already awesome
            if typename == "lima2":
                curr_pars = node.controller.get_current_parameters()
                pass

            if typename == "ct2":
                acq_params.setdefault("wait_new_point_signal", False)

            # --- set parameters on nodes
            node.set_parameters(acq_params=acq_params, ctrl_params=ctrl_params)
            for child_node in node.children:
                child_node.set_parent_parameters(acq_params)
                if typename == "ct2":
                    child_pars = dict(count_time=acq_params.get("acq_expo_time", 0.1),
                                      npoints=acq_params.get("npoints", 1)
                                 )
                    child_node.set_parameters(child_pars)

            # --- add to chain
            if master is not None:
                chain.add(master, node)
            else:
                chain.add(node)
            self.__setup_done[typename].append(node)

    def setup_calc_counters(self, chain, master, *names):
        all_nodes = self.get_nodes_by_type("calc", *names)
        if not len(all_nodes):
            return

        all_done = self.get_controllers_done()
        for node in all_nodes:
            input_ctrls = self._get_calc_input_controllers(node.controller)
            setup_ctrls = [ ctrl in all_done for ctrl in input_ctrls ]
            if all(setup_ctrls):
                chain.add(master, node)

    def _get_calc_input_controllers(self, calc_controller):
        input_ctrls = list()
        for cnt in calc_controller.inputs:
            if isinstance(cnt, CalcCounter):
                input_ctrls.extend(self._get_calc_input_controllers(cnt._counter_controller))
            else:
                if cnt._counter_controller._master_controller is not None:
                    input_ctrls.append(cnt._counter_controller._master_controller)
                else:
                    input_ctrls.append(cnt._counter_controller)
        return list(set(input_ctrls))

    def setup_sampling(self, chain, count_time, *names):
        all_nodes = self.get_nodes_by_type("sampling", *names)

        # --- filter musst sampling counters
        nodes = list()
        for node in all_nodes:
            if not isinstance(node.controller, MusstSamplingCounterController):
                nodes.append(node)
            # --- filter only musst master ?
        
        if not len(nodes):
            return

        if "sampling" not in self.__setup_done:
            self.__setup_done["sampling"] = list()

        # --- create software timer
        acq_pars = {"count_time": count_time, "npoints": 0}
        master = SoftwareTimerMaster(
            count_time, npoints=0, sleep_time=0, name="sampling_timer"
        )

        # --- add sampling nodes
        for node in nodes:
            node.set_parameters(acq_pars)
            chain.add(master, node)
            self.__setup_done["sampling"].append(node)

        # -- add elettra nodes
        bpm_nodes = self.get_nodes_by_type("elettra", *names)
        acq_pars = {"count_time": count_time}
        for node in bpm_nodes:
            node.set_parameters(acq_pars)
            chain.add(master, node)
            self.__setup_done["sampling"].append(node)

    def get_nodes_done(self):
        node_list = list()
        for done_nodes in self.__setup_done.values():
            node_list += done_nodes
        return node_list

    def get_controllers_done(self, type_or_class=None):
        node_list = list()
        if type_or_class is None:
            for done_nodes in self.__setup_done.values():
                node_list += done_nodes
        else:
            typename = self.__get_ctrl_type(type_or_class)
            node_list = self.__setup_done.get(typename, node_list)
        ctrl_list = [ node.controller for node in node_list ]
        return ctrl_list

    def get_controllers_not_done(self, type_or_class=None):
        done_list = self.get_controllers_done(type_or_class)
        ctrl_list = self.get_controllers(type_or_class)
        not_done_list = [ ctrl for ctrl in ctrl_list if ctrl not in done_list ]
        return not_done_list

    def get_controllers(self, *type_or_class_list):
        if not len(type_or_class_list):
            nodes = self.builder.get_top_level_nodes()
        else:
            nodes = list()
            for type_or_class in type_or_class_list:
                nodes += self.get_nodes_by_type(type_or_class)
        ctrls = [ node.controller for node in nodes ]
        return ctrls

    def has_type(self, type_or_class):
        nodes = self.get_nodes_by_type(type_or_class)
        return len(nodes) > 0
        
    def has_name(self, name):
        return self.get_node_by_name(name) is not None
      
    def get_controller_channels(self, controller_name):
        node = self.get_node_by_name(controller_name)
        if node is None:
            return []
        chans = list(node.acquisition_obj.channels)
        for child in node.children:
            chans += child.acquisition_obj.channels
        return chans

    def get_controller_channel_by_name(self, controller_name, channel_short_name):
        chans = self.get_controller_channels(controller_name)
        for channel in chans:
            if channel.short_name == channel_short_name:
                return channel
        return None


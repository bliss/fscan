from louie import dispatcher

from bliss.scanning.scan import Scan
from bliss.scanning.chain import ChainPreset
from bliss.scanning.acquisition.motor import JogMotorMaster
from bliss.controllers.ct2.device import AcqMode as CT2AcqMode

from fscan.synchro.base import SyncDeviceList
from fscan.synchro.musst.programs import MusstProgFScanLoop
from fscan.musstcalc import MusstCalcAcqSlave

from fscan.fscantools import (
    FScanMode,
    FScanTrigMode,
    FScanParamBase,
    FScanParamStruct,
    FScanDisplay,
    FScanLoopMode,
    FScanLoopTrigMode,
    FScanLoopLimaMode,
)
from fscan.limatools import LimaCalib
from fscan.mcatools import get_fscan_mca_params
from fscan.chaintools import ChainTool
from fscan.motortools import (
    RotationMotorChainPreset,
    check_motor_limits,
    check_motor_velocity,
)
from fscan.fscaninfo import FScanInfo
from fscan.fscanrunner import FScanDiagRunner


class LimaReadyChainPreset(ChainPreset):
    def __init__(self, limaname, trigger_func):
        self.limaname = limaname
        self.trigger_func = trigger_func
        self.acqobj = None

    def prepare(self, chain):
        acqobjs = [ node for node in chain.nodes_list if node.name == self.limaname ]
        if len(acqobjs) == 1:
            self.acqobj = acqobjs[0]
            dispatcher.connect(self.lima_started_callback, "lima_started", self.acqobj)

    def lima_started_callback(self, event_dict=None, signal=None, sender=None):
        self.trigger_func()

    def stop(self, chain):
        if self.acqobj:
            dispatcher.disconnect(self.lima_started_callback, "lima_started", self.acqobj)


class FScanLoopPars(FScanParamBase):
    DEFAULT = {
        "motor": None,
        "start_pos": 0,
        "step_size": 0.1,
        "step_time": 1.0,
        "loop_range": 180.,
        "loop_delta": 180.,
        "nloop": 10,
        "acq_time": 0.8,
        "loop_trig_mode": FScanLoopTrigMode.INTERNAL,
        "lima_read_mode": FScanLoopLimaMode.PER_SCAN,
        "scan_mode": FScanMode.TIME,
        "gate_mode": FScanTrigMode.TIME,
        "save_flag": True,
        "display_flag": True,
        "sync_encoder": True,
        "home_rotation": False,
        "round_rotation": True,
        "acc_margin": 0.,
        "sampling_time": 0.5,
        "latency_time": 1e-5,
    }
    OBJKEYS = ["motor"]
    LISTVAL = {
        "loop_trig_mode": FScanLoopTrigMode.values,
        "lima_read_mode": FScanLoopLimaMode.values,
        "scan_mode": FScanMode.values,
        "gate_mode": FScanTrigMode.values,
    }
    NOSETTINGS = ["save_flag",]

    def __init__(self, name):
        FScanParamBase.__init__(
            self,
            name,
            FScanLoopPars.DEFAULT,
            FScanLoopPars.OBJKEYS,
            FScanLoopPars.LISTVAL,
            FScanLoopPars.NOSETTINGS,
        )

    def _validate_loop_range(self, value):
        if value < 0.:
            print("WARNING : Automatically set loop_range positive. Direction is given by sign of step_size only.")
        return abs(value)

    def _validate_loop_delta(self, value):
        if value < 0.:
            print("WARNING : Automatically set loop_delta positive. Direction is given by sign of step_size only.")
        return abs(value)

    def _validate_save_flag(self, value):
        return value and True or False

    def _validate_display_flag(self, value):
        return value and True or False

    def _validate_sync_encoder(self, value):
        return value and True or False

    def _validate_home_rotation(self, value):
        return value and True or False

    def _validate_round_rotation(self, value):
        return value and True or False

    def _validate_loop_trig_mode(self, value):
        return FScanLoopTrigMode.get(value, "loop_trig_mode")

    def _validate_lima_read_mode(self, value):
        return FScanLoopLimaMode.get(value, "loop_read_mode")

    def _validate_scan_mode(self, value):
        return FScanMode.get(value, "scan_mode")

    def _validate_gate_mode(self, value):
        return FScanTrigMode.get(value, "gate_mode")

    def _validate_acc_margin(self, value):
        return abs(value)

class FScanLoopMaster(object):
    def __init__(self, name, config):
        self.name = name
        self.config = config

        self.musst_list = SyncDeviceList(config["devices"]["synchro"])
        self.meas_group = config["devices"]["measgroup"]
        self.rot_motors = config["devices"]["rotation_motors"]
        self.chain_config = config["chain_config"]
        self.lima_calib = LimaCalib(config["lima_calib"])
        self.musst_data_rate_factor = config.get("musst_data_rate_factor", 0)

        self.pars = FScanLoopPars(self.name)
        self.inpars = None
        self.mgchain = None

        self._trigger_func = None

    def set_lima_ready_trigger_func(self, func=None):
        self._trigger_func = func

    def validate(self):
        self.inpars = FScanParamStruct(self.pars.to_dict())
        pars = self.inpars

        # --- check motor is a rotation
        if pars.motor not in self.rot_motors:
            raise ValueError("FScanLoop works only on rotation motors")

        # --- check motor is on musst
        self.musst = self.musst_list.find_musst_for_motors(pars.motor)

        # --- mode check
        if pars.loop_trig_mode in (FScanLoopTrigMode.EXTERNAL, FScanLoopTrigMode.LIMA_READY):
            if pars.loop_delta <= 0:
                pars.loop_delta = pars.step_size

        # --- range check
        pars.loop_npoints = abs(pars.loop_range / pars.step_size)
        if int(pars.loop_npoints) != pars.loop_npoints:
            raise ValueError("loop_range must be a multiple of step_size")
        pars.loop_npoints = int(pars.loop_npoints)
        pars.npoints = pars.nloop * pars.loop_npoints

        # --- check lima and minimum period
        limas = self.get_controllers_found("lima")
        self.lima_calib.validate(limas, pars, use_acc=False)

        if pars.lima_ndev:
            if pars.step_time < pars.lima_min_period:
                pars.step_time = pars.lima_min_period
        else:
            if pars.step_time < pars.acq_time:
                pars.step_time = pars.acq_time + pars.latency_time

        if pars.loop_trig_mode == FScanLoopTrigMode.LIMA_READY:
            if pars.lima_ndev != 1:
                raise RuntimeError("fscanloop : when loop_trig_mode == LIMA_READY, "\
                                   "need to have ONE lima device enabled")
            if self._trigger_func is None:
                raise RuntimeError("fscanloop : when loop_trig_mode == LIMA_READY, "\
                                   "you need to set a trigger function")
            pars.lima_read_mode = FScanLoopLimaMode.PER_LOOP
            pars.lima_trig_name = limas[0].name

        # --- calc params
        pars.motor_name = pars.motor.name
        pars.motor_unit = pars.motor.unit is None and "unit" or pars.motor.unit
        pars.jog_speed = pars.step_size / pars.step_time
        pars.speed = abs(pars.jog_speed)
        pars.acq_size = pars.acq_time * pars.speed
        pars.dead_time = pars.step_time - pars.acq_time
        pars.acc_time = pars.acc_margin / pars.speed + pars.speed / pars.motor.acceleration
        pars.acc_disp = pars.acc_margin + 0.5 * pars.speed ** 2 / pars.motor.acceleration
        if pars.step_size > 0:
            pars.scan_dir = 1
        else:
            pars.scan_dir = -1

        pars.move_range = (pars.scan_dir * (pars.nloop-1) * pars.loop_delta) + \
                          (pars.scan_dir * pars.nloop * pars.loop_range)
        pars.stop_pos = pars.start_pos + pars.move_range
        pars.move_time = abs(pars.move_range) / pars.speed
        pars.scan_time = 2 * pars.acc_time + pars.move_time

        # --- check motor limits
        pars.real_start = pars.start_pos - pars.scan_dir * pars.acc_disp
        pars.real_stop = pars.stop_pos + pars.scan_dir * pars.acc_disp
        check_motor_limits(pars.motor, pars.real_start, pars.real_stop)
        check_motor_velocity(pars.motor, pars.speed)

    def show(self):
        if self.inpars is None:
            raise RuntimeError(f"FScanLoopMaster[{self.name}]: Parameters not validated yet !!")

        txt = """
Motor : {motor_name}
    start pos = {start_pos:.4f} {motor_unit}
    step size = {step_size:.4f} {motor_unit}
    step time = {step_time:.6f} sec
    velocity  = {jog_speed:.4f} {motor_unit}/sec
    acceleration size = {acc_disp:.4f} {motor_unit} (margin = {acc_margin:.4f})
    acceleration time = {acc_time:.6f} sec

Acquisition Loop :
    loop trigger mode  = {loop_trig_mode}
    loop motor range   = {loop_range} {motor_unit}
    points per loop    = {loop_npoints}
    number of loop     = {nloop}
    delta between loop = {loop_delta} {motor_unit}

Acquisition :
    scan mode    = {scan_mode}
    acq time     = {acq_time:.6f} sec
    acq size     = {acq_size:.4f} {motor_unit}
    dead time    = {dead_time:.6f} sec
    total points = {npoints}
"""
        print(txt.format(**self.inpars.to_dict()))

    def init_scan(self):
        self.mgchain = ChainTool(self.meas_group, self.chain_config)
        self.lima_used = list()
        self.mca_used = list()
        self.home_preset = None

    def get_controllers_found(self, ctrl_type):
        mgchain = ChainTool(self.meas_group)
        return mgchain.get_controllers_not_done(ctrl_type)

    def get_controllers_used(self, ctrl_type):
        if self.mgchain is None:
            return []
        else:
            return self.mgchain.get_controllers_done(ctrl_type)


    def setup_motor_master(self, chain):
        pars = self.inpars
        if pars.home_rotation is True or pars.round_rotation:
            self.home_preset = RotationMotorChainPreset(pars.motor, 
                                                        homing=pars.home_rotation, 
                                                        rounding=pars.round_rotation)
            chain.add_preset(self.home_preset)

        start_once = (pars.lima_read_mode == FScanLoopLimaMode.PER_LOOP)
        master = JogMotorMaster(pars.motor, pars.start_pos, pars.jog_speed,
                                end_jog_func=self.stop_motor_jog,
                                undershoot_start_margin=pars.acc_margin,
                                start_once=start_once,
                               )
        return master

    def stop_motor_jog(self, motor):
        return (self.musst.STATE == self.musst.RUN_STATE)

    def setup_acq_master(self, chain, mot_master):
        pars = self.inpars

        if pars.scan_mode == FScanTrigMode.TIME:
            delta = pars.step_time
        else:
            delta = pars.step_size
        if pars.gate_mode == FScanTrigMode.TIME:
            gatewidth = pars.acq_time
        else:
            gatewidth = pars.acq_size
        if pars.loop_trig_mode == FScanLoopTrigMode.INTERNAL:
            loopmode = FScanLoopMode.FORWARD
            loopdelta = pars.loop_range + pars.loop_delta
            waitdelta = 0
        else:
            loopmode = FScanLoopMode.EXTERNAL
            loopdelta = pars.loop_range
            waitdelta = pars.loop_delta

        musstprog = MusstProgFScanLoop(self.musst, [pars.motor,])
        musstprog.check_max_timer(pars.scan_time)
        musstprog.set_max_data_rate(self.musst_data_rate_factor * 2./pars.step_time)
        musstprog.set_modes(pars.scan_mode, pars.gate_mode, loopmode)
        musstprog.set_params(pars.start_pos, pars.scan_dir, delta, gatewidth, 
                             pars.loop_npoints, pars.nloop,
                             loopdelta, waitdelta)

        musstpars = dict()
        if pars.lima_read_mode == FScanLoopLimaMode.PER_LOOP:
            musstpars["prepare_once"] = True
            musstpars["start_once"] = True
            musstpars["last_iter_index"] = pars.nloop-1

        # --- sync motors on musst
        if pars.home_rotation:
            self.home_preset.set_musst_sync_cb(musstprog.sync_one_motor)
        elif pars.sync_encoder:
            musstprog.sync_motors()

        # --- add to chain
        musstprog.setup(chain, mot_master, **musstpars)

        # --- calc device
        musstcalc = MusstCalcAcqSlave(musstprog.acq_data_master, musstprog.channels, musstprog.DataPerPoint)
        musstcalc.add_default_calc()
        musstcalc.add_calc("center", pars.motor_name)
        musstcalc.add_calc("cen360", pars.motor_name)
        chain.add(musstprog.acq_master, musstcalc)

        # --- motor external channel
        calc_name = musstcalc.get_channel_short_name("center", pars.motor_name)
        mot_master.add_external_channel(musstcalc, calc_name, f"axis:{pars.motor_name}")

        # --- lima ready trigger preset
        if pars.loop_trig_mode == FScanLoopTrigMode.LIMA_READY:
            preset = LimaReadyChainPreset(pars.lima_trig_name, self._trigger_func)
            chain.add_preset(preset, mot_master)

        self.syncprog = musstprog
        self.synccalc = musstcalc

        return musstprog.musst_master 

    def setup_acq_slaves(self, chain, acq_master):
        pars = self.inpars

        # --- ct2 counters
        ct2_params={
            "npoints": pars.npoints,
            "acq_mode": CT2AcqMode.ExtGate,
            "prepare_once": True,
            "start_once": True
        }
        self.mgchain.setup("ct2", chain, acq_master, ct2_params)

        # --- lima devices
        lima_params = dict(
            acq_mode=pars.lima_acq_mode,
            acq_trigger_mode="EXTERNAL_TRIGGER_MULTI",
            acq_expo_time=pars.acq_time,
            scan_frame_per_file=pars.loop_npoints,
        )

        if pars.lima_read_mode == FScanLoopLimaMode.PER_SCAN:
            lima_params.update(
                acq_nb_frames= pars.npoints,
                wait_frame_id= [pars.npoints-1,],
                prepare_once= True,
                start_once= True
            )
        else:
            # --- lima_read_mode = ScanLoopLimaMode.PER_LOOP
            lima_params.update(
                acq_nb_frames= pars.loop_npoints,
                prepare_once= False,
                start_once= False
            )

        self.mgchain.setup("lima", chain, acq_master, lima_params)
        self.lima_used = self.mgchain.get_controllers_done("lima")

        # --- mca devices
        for mca_ctrl in self.mgchain.get_controllers_not_done("mca"):
            mca_params = get_fscan_mca_params(mca_ctrl, pars.npoints, pars.step_time)
            self.mgchain.setup("mca", chain, acq_master, mca_params, mca_ctrl.name)
            if "trigger_mode" in mca_params:
                # --- in spectrum reading mode
                self.mca_used.append(mca_ctrl)

        # --- mosca devices
        mosca_params = {
            "trigger_mode": "GATE",
            "npoints": pars.npoints,
            "preset_time": pars.acq_time,
            "wait_frame_id": [pars.npoints,],
            "start_once": True,
        }
        self.mgchain.setup("mosca", chain, acq_master, mosca_params)

        # --- calc
        self.mgchain.setup_calc_counters(chain, acq_master)

        # --- sampling
        self.mgchain.setup_sampling(chain, pars.sampling_time)

    def setup_scan(self, chain, scan_name, user_info=None):
        inpars = self.inpars
        if inpars.display_flag:
            self.show()

        scan_info = FScanInfo(scan_name, "fscanloop", inpars.npoints)
        scan_info.set_fscan_pars(self.pars)
        #scan_info.set_fscan_title()
        scan_info.set_fscan_info(user_info)

        trig_name = self.synccalc.get_channel_name("trig", "timer")
        scan = Scan(
            chain,
            name=scan_name,
            save=inpars.save_flag,
            scan_info=scan_info,
            scan_progress=FScanDisplay(trig_name)
        )

        return scan

    def get_runner_class(self):
        return FScanLoopCustomRunner

class FScanLoopCustomRunner(FScanDiagRunner):
    def __call__(self, motor, start_pos, step_size, 
                 loop_range, loop_delta, nloop,
                 acq_time, step_time=0., **kwargs):
        pars = dict(motor=motor,
                    start_pos=start_pos,
                    step_size=step_size,
                    loop_range=loop_range,
                    loop_delta=loop_delta,
                    nloop=nloop,
                    acq_time=acq_time,
                    step_time=step_time,
               )
        pars.update(kwargs)
        self.run_with_pars(pars)

    def set_lima_ready_trigger_func(self, func=None):
        self.master.set_lima_ready_trigger_func(func)


from bliss.scanning.scan_info import ScanInfo

class FScanInfo(ScanInfo):
    def __init__(self, scan_name, scan_type, npoints):
        ScanInfo.__init__(self)
        self._scan_name = scan_name
        self._scan_info["type"] = scan_type
        self._scan_info["title"] = scan_name
        self._scan_info["npoints"] = npoints

    def set_fscan_title(self, template, pars={}):
        parstxt = template.format(**pars.to_dict())
        self._scan_info["title"] = "{0} {1}".format(self._scan_name, parstxt)

    def set_fscan_pars(self, pars):
        pars_dict = pars.to_dict(string_format=True)
        pars_dict["scan_type"] = self._scan_info["type"]
        pars_dict["scan_name"] = self._scan_name
        pars_dict["@NX_class"] = "NXcollection"

        self._scan_info.setdefault("instrument", {})
        self._scan_info["instrument"]["fscan_parameters"] = pars_dict

    def set_fscan_info(self, info):
        if info is not None:
            self._scan_info.update(info)


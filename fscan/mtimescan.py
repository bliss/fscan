from bliss.scanning.scan import Scan
from bliss.controllers.ct2.device import AcqMode as CT2AcqMode
from bliss.common.measurementgroup import get_active, _get_counters_from_measurement_group

from fscan.fscantools import (
    FScanParamBase, 
    FTimeScanMode,
    FScanCamSignal,
    FScanParamStruct,
    MScanDisplay,
)
from fscan.synchro.base import SyncDeviceList
from fscan.synchro.musst.programs import MusstProgMTimeScan
from fscan.mcatools import get_fscan_mca_params
from fscan.chaintools import ChainTool
from fscan.fscanrunner import FScanRunner
from fscan.fscaninfo import FScanInfo
from fscan.limatools import LimaCalib

from fscan.musstcalc import MusstCalcAcqSlave, MusstCalcFilterSlowRate, MusstCalcFilterFastRate, MusstCalcDummy

# idx = numpy.arange(offset, len(data))
# slow = data[numpy.where((idx%4==0)|(idx%4==3))]
# fast = data[numpy.where(idx%4!=3)]

class MTimeScanPars(FScanParamBase):
    DEFAULT = {
        "fast_acq_time": 0.08,
        "fast_npoints": 10,
        "fast_period": 0.0,
        "fast_latency_time": 0.,
        "slow_acq_time": 0.20,
        "slow_period": 0.0,
        "slow_latency_time": 0.,
        "start_delay": 0.001,
        "sampling_time": 0.5,
        "scan_mode": FTimeScanMode.TIME,
        "save_flag": True,
        "display_flag": True,
    }
    OBJKEYS = list()
    LISTVAL = {
        "scan_mode": FTimeScanMode.values,
    }
    NOSETTINGS = ["save_flag",]

    def __init__(self, name):
        FScanParamBase.__init__(
            self, 
            name, 
            MTimeScanPars.DEFAULT, 
            MTimeScanPars.OBJKEYS, 
            MTimeScanPars.LISTVAL,
            MTimeScanPars.NOSETTINGS,
        )

    def _validate_fast_npoints(self, value):
        return int(value)

    def _validate_scan_mode(self, value):
        return FTimeScanMode.get(value, "scan_mode")

    def _validate_save_flag(self, value):
        return value and True or False

    def _validate_display_flag(self, value):
        return value and True or False

class MTimeScanMaster(object):
    def __init__(self, name, config):
        self.name = name
        self.config = config

        sync_list = SyncDeviceList(config["devices"]["synchro"])
        self.musst = sync_list.first_musst()

        self.slow_meas_group = config["devices"]["measgroup"]
        self.fast_meas_group = config["devices"]["fast_measgroup"]

        self.chain_config = config["chain_config"]
        self.lima_calib = LimaCalib(config["lima_calib"])
        self.musst_data_rate_factor = config.get("musst_data_rate_factor", 0)
        
        self.pars = MTimeScanPars(self.name)
        self.inpars = None

        self.counters = dict()

    def validate(self):
        if self.slow_meas_group is None:
            self.slow_meas_group = get_active()

        if self.fast_meas_group is None:
            raise ValueError("MTimeScanMaster is missing fast_measgroup configuration")

        self.inpars = FScanParamStruct(self.pars.to_dict())
        pars = self.inpars

        # --- get fast and slow counter list
        self.counters = dict()
        self.counters["fast"] = _get_counters_from_measurement_group(self.fast_meas_group)
        self.counters["slow"] = [ cnt for cnt in _get_counters_from_measurement_group(self.slow_meas_group) 
                                  if cnt not in self.counters["fast"] ]

        # --- lima calibration on fast and slow group
        for key, cnts in self.counters.items():
            limas = self.get_controllers_found(["lima", "lima2"], cnts)
            limapars = FScanParamStruct()
            if key == "fast":
                limapars.scan_mode = pars.scan_mode
                limapars.acq_time = pars.fast_acq_time
                limapars.latency_time = pars.fast_latency_time
            else:
                limapars.scan_mode = FTimeScanMode.TIME
                limapars.acq_time = pars.slow_acq_time
                limapars.latency_time = pars.slow_latency_time
            self.lima_calib.validate(limas, limapars, use_acc=False)
            pars.add(key, limapars)

        # --- check min period on fast group
        if pars.fast_lima_ndev:
            if pars.scan_mode == FTimeScanMode.CAMERA:
                pars.fast_period = pars.fast_lima_min_period
            elif pars.fast_period < pars.fast_lima_min_period:
                pars.fast_period = pars.fast_lima_min_period
        else:
            if pars.fast_period < pars.fast_acq_time:
                pars.fast_period = pars.fast_acq_time + pars.fast_latency_time

        # --- check min period on slow group
        if pars.slow_lima_ndev:
            if pars.slow_period < pars.slow_lima_min_period:
                pars.slow_period = pars.slow_lima_min_period
        else:
            if pars.slow_period < pars.slow_acq_time:
                pars.slow_period = pars.slow_acq_time + pars.slow_latency_time

        # --- calculate fast detectors sampling
        if pars.slow_period < pars.fast_period:
            raise RuntimeError("slow detector minimum period < fast detector minimum period. Check parameters")

        pars.fast_sampling = int(pars.slow_period // pars.fast_period)
        if pars.slow_period % pars.fast_period:
            pars.fast_sampling += 1
        pars.slow_npoints = int(pars.fast_npoints // pars.fast_sampling)
        if pars.fast_npoints % pars.fast_sampling >= pars.fast_sampling/2.:
            pars.slow_npoints += 1
        pars.slow_gateidx = int(pars.slow_acq_time // pars.fast_period) + 1
        pars.npoints = pars.slow_npoints * pars.fast_sampling

        # --- extra infos
        pars.slow_real_period = pars.fast_sampling * pars.fast_period
        if self.inpars.scan_mode == FTimeScanMode.EXTSTART:
            pars.start_delay = 0.
        pars.total_time = pars.start_delay + pars.npoints * pars.fast_period

    def show(self):
        if self.inpars is None:
            raise RuntimeError(f"MTimeScanMaster[{self.name}]: Parameters not validated yet !!")

        txt = """
Fast detectors:
    scanning mode       = {scan_mode}
    acquisition time    = {fast_acq_time:g} sec
    acquisition period  = {fast_period:g} sec
    acq/slow points     = {fast_sampling:d}
    number of points    = {npoints}  (requested {fast_npoints})

Slow detectors:
    acquisition time    = {slow_acq_time:g} sec
    acquisition period  = {slow_real_period:g} sec (requested {slow_period:g} sec)
    number of points    = {slow_npoints}

Start delay   = {start_delay:.3f} sec
Total time    = {total_time:.3f} sec
"""
        print(txt.format(**self.inpars.to_dict()))

    def get_controllers_found(self, ctrl_type_list, counters=list()):
        if not counters:
            mgchain = ChainTool([self.slow_meas_group, self.fast_meas_group])
        else:
            mgchain = ChainTool(counters)
        return mgchain.get_controllers(*ctrl_type_list)

    def init_scan(self):
        self.fastchain = None
        self.slowchain = None
        self.calc_fast_prefix = ""
        self.calc_slow_prefix = ""

    def setup_acq_chain(self, chain):
        master = self.setup_acq_master(chain)
        self.setup_fast_acq_slaves(chain, master)
        self.setup_slow_acq_slaves(chain, master)

    def setup_acq_master(self, chain):
        pars = self.inpars
        musstprog = MusstProgMTimeScan(self.musst)
        musstprog.check_max_timer(pars.total_time)
        musstprog.set_max_data_rate(self.musst_data_rate_factor * (2./pars.fast_period))
        musstprog.set_params(pars.scan_mode, pars.start_delay, 
                             pars.fast_sampling, pars.fast_period, 
                             pars.slow_npoints, pars.slow_acq_time, pars.slow_gateidx)
        musstprog.setup(chain)

        slowcalc = MusstCalcAcqSlave(musstprog.musst_device, musstprog.channels, 2, "slow")
        slowcalc.set_input_filter(MusstCalcFilterSlowRate, pars.fast_sampling+1, pars.slow_gateidx)
        slowcalc.add_default_calc()
        chain.add(musstprog.musst_master, slowcalc)
        self.calc_slow_prefix = slowcalc.calc_prefix

        fastcalc = MusstCalcAcqSlave(musstprog.musst_device, musstprog.channels, 1, "fast")
        fastcalc.set_input_filter(MusstCalcFilterFastRate, pars.fast_sampling+1, pars.slow_gateidx)
        fastcalc.add_default_calc()
        chain.add(musstprog.musst_master, fastcalc)
        self.calc_fast_prefix = fastcalc.calc_prefix

#        musstprog.set_timer_external_channel()

        self.musstprog = musstprog

        return musstprog.musst_master

    def setup_fast_acq_slaves(self, chain, acq_master):
        pars = self.inpars
        self.fastchain = ChainTool(self.counters["fast"])

        # --- counters
        ct2pars = {
            "npoints": pars.npoints,
            "acq_expo_time": pars.fast_acq_time,
            "acq_mode": CT2AcqMode.ExtTrigMulti,
        }
        self.fastchain.setup("ct2", chain, acq_master, ct2pars)

        # --- lima devs
        if pars.scan_mode == FTimeScanMode.CAMERA:
            trig_mode = "EXTERNAL_TRIGGER"
        else:
            trig_mode = "EXTERNAL_TRIGGER_MULTI"

        limapars = {
            "acq_mode": pars.fast_lima_acq_mode,
            "acq_trigger_mode": trig_mode,
            "acq_expo_time": pars.fast_acq_time,
            "acq_nb_frames": pars.npoints,
            "wait_frame_id": [pars.npoints-1,],
            "prepare_once": True,
            "start_once": True,
        }
        if pars.scan_mode == FTimeScanMode.CAMERA:
            if pars.fast_period > pars.fast_lima_min_period:
                lat_time = pars.fast_period - pars.fast_acq_time
                limapars["latency_time"] = lat_time
        self.fastchain.setup("lima", chain, acq_master, limapars)

        # --- lima2 devices
        lima_params = {
            "trigger_mode": "external",
            "expo_time": pars.fast_acq_time,
            "nb_frames": pars.npoints,
            "nb_frames_per_trigger": 1,
            "wait_frame_id": [ pars.npoints - 1, ],
            "prepare_once": True,
            "start_once": True,
        }
        if pars.scan_mode == FTimeScanMode.CAMERA:
            lima_params["nb_frames_per_trigger"] = pars.npoints
            if pars.fast_period > pars.fast_lima_min_period:
                lat_time = pars.fast_period - pars.fast_acq_time
                lima_params["latency_time"] = lat_time
        self.fastchain.setup("lima2", chain, acq_master, lima_params)

        # --- mosca devices
        moscapars = {
            "trigger_mode": "GATE",
            "npoints": pars.npoints,
            "preset_time": pars.fast_acq_time,
            "wait_frame_id": [pars.npoints,],
            "start_once": True,
        }
        self.fastchain.setup("mosca", chain, acq_master, moscapars)

        # --- calc
        self.fastchain.setup_calc_counters(chain, acq_master)

    def setup_slow_acq_slaves(self, chain, acq_master):
        pars = self.inpars
        self.slowchain = ChainTool(self.counters["slow"])

        # --- counters
        ct2pars = {
            "npoints": pars.slow_npoints,
            "acq_expo_time": pars.slow_acq_time,
            "acq_mode": CT2AcqMode.ExtGate,
        }
        self.slowchain.setup("ct2", chain, acq_master, ct2pars)

        # --- lima devs
        limapars = {
            "acq_mode": pars.slow_lima_acq_mode,
            "acq_trigger_mode": "EXTERNAL_TRIGGER_MULTI",
            "acq_expo_time": pars.slow_acq_time,
            "acq_nb_frames": pars.slow_npoints,
            "wait_frame_id": [pars.slow_npoints-1,],
            "prepare_once": True,
            "start_once": True,
        }
        self.slowchain.setup("lima", chain, acq_master, limapars)

        # --- mosca devices
        moscapars = {
            "trigger_mode": "GATE",
            "npoints": pars.slow_npoints,
            "preset_time": pars.slow_acq_time,
            "wait_frame_id": [pars.slow_npoints,],
            "start_once": True,
        }
        self.slowchain.setup("mosca", chain, acq_master, moscapars)

        # --- calc
        self.slowchain.setup_calc_counters(chain, acq_master)

        # --- sampling
        self.slowchain.setup_sampling(chain, pars.sampling_time)

    def setup_scan(self, chain, scan_name, user_info=None):
        pars = self.inpars

        if pars.display_flag:
            self.show()

        scan_info = FScanInfo(scan_name, "mtimescan", pars.npoints)
        scan_info.set_fscan_title("{fast_acq_time:g} {slow_acq_time:g} {npoints:d}", pars)
        scan_info.add_curve_plot(x="timer_trig:timer_trig")
        scan_info.set_fscan_pars(self.pars)
        scan_info.set_fscan_info(user_info)

        fast_nodes = self.fastchain.get_nodes_done()
        slow_nodes = self.slowchain.get_nodes_done()
        fast_trig_name = f"{self.calc_fast_prefix}timer_trig"
        slow_trig_name = f"{self.calc_slow_prefix}timer_trig"
        display = MScanDisplay(fast_trigger_name=fast_trig_name, fast_acq_nodes=fast_nodes,
                               slow_trigger_name=slow_trig_name, slow_acq_nodes=slow_nodes
        )

        scan = Scan(
            chain,
            name=scan_name,
            save=pars.save_flag,
            scan_info=scan_info,
            scan_progress=display,
        )

        return scan

    def get_runner_class(self):
        return MTimeScanCustomRunner

class MTimeScanCustomRunner(FScanRunner):
    def __call__(self, fast_acq_time, fast_npoints, slow_acq_time, slow_period=0., **kwargs):
        pars = dict(fast_acq_time = fast_acq_time,
                    fast_npoints = fast_npoints,
                    fast_period = kwargs.pop("fast_period", 0.),
                    slow_acq_time = slow_acq_time,
                    slow_period = slow_period,
               )
        pars.update(kwargs)
        self.run_with_pars(pars)


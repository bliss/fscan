import numpy

from bliss.scanning.chain import AcquisitionSlave
from bliss.scanning.channel import AcquisitionChannel
from bliss.scanning.acquisition.calc import CalcAcquisitionSlaveBase

from fscan.synchro.musst.base import MusstMotorChannel, MusstCounterChannel
from fscan.synchro.maestrio.base import MaestrioMotorChannel, MaestrioCounterChannel

class MusstCalcListClass(object):
    def __init__(self):
        self.classes = dict()

    def register(self, klass, name):
        self.classes[name] = klass

    def get(self, name):
        return self.classes.get(name)

global MusstCalcList
MusstCalcList = MusstCalcListClass()
      
class MusstCalcAcqSlave(CalcAcquisitionSlaveBase):
    def __init__(self, musst_device, musst_channels, data_per_point, prefix_name=None):
        if prefix_name:
            name = f"{musst_device.name}_calc_{prefix_name}"
            prefix = f"{prefix_name}_"
        else:
            name = f"{musst_device.name}_calc"
            prefix = ""

        super().__init__(
            None,
            name=name,
            trigger_type=AcquisitionSlave.HARDWARE,
            prepare_once=False,
            start_once=False,
        )
        self._master = musst_device
        self.data_per_point = data_per_point
        self.prefix_name = prefix
        self.input_channels = musst_device.channels
        self.musst_channels = musst_channels
        self.input_filters = dict()
        self.output_filters = dict()
        self.calc_hook = dict()
        for chan in self.input_channels:
            self.calc_hook[chan.short_name] = list()
        self.calc_names = dict()

    @property
    def start_epoch(self):
        return self._master.start_epoch
            
    def _iter_input_channels(self):
        yield from self.input_channels

    def set_input_filter(self, filter_class, *args, **kwargs):
        self.input_filters = dict()
        for chan in self.input_channels:
            self.input_filters[chan.short_name] = filter_class(*args, **kwargs)

    def set_output_filter(self, filter_class, *args, **kwargs):
        self.output_filters = dict()
        for all_calc in self.calc_hook.values():
            for calc in all_calc:
                self.output_filters[calc.name] = filter_class(*args, **kwargs)

    def add_calc(self, calc_name, *chan_names):
        calc_hook_class = MusstCalcList.get(calc_name)
        if calc_hook_class is None:
            raise ValueError(f"Unknown musst calc name [{calc_name}]")

        if not len(chan_names):
            chan_names = self.musst_channels.keys()

        for name in chan_names:
            chan = self.musst_channels.get(name)
            if chan:
                calc_dev = calc_hook_class(self, chan, self.data_per_point)
                self.add_calc_device(calc_dev, name)

    def add_calc_device(self, calc_dev, name):
        chan = self.musst_channels.get(name)
        if chan is None:
            raise ValueError(f"no sync channels with name {name}")
        self.calc_hook[chan.store_name].append(calc_dev)
        calc_name = f"{self.calc_prefix}{calc_dev.name}"
        acq_channel = AcquisitionChannel(calc_name, numpy.float64, (), unit=calc_dev.unit)
        self.channels.append(acq_channel)

        save = self.calc_names.setdefault(calc_dev.calc_name, {})
        save[name] = calc_name

    def add_default_calc(self):
        self.add_calc("epoch_trig", "timer")
        self.add_calc("trig", "timer", *self.musst_motor_names)
        self.add_calc("period", "timer", *self.musst_motor_names)
        if self.data_per_point > 1:
            self.add_calc("delta", "timer", *self.musst_motor_names)
        cnt_names = self.integrating_counter_names
        if len(cnt_names):
            self.add_calc("count", *cnt_names)
        cnt_names = self.sampling_counter_names
        if len(cnt_names):
            self.add_calc("trig", *cnt_names)

    @property
    def calc_prefix(self):
        return f"{self.name}:{self.prefix_name}"
    
    @property
    def counter_channels(self):
        return [ chan for chan in self.musst_channels.values()
                 if isinstance(chan, MusstCounterChannel) or 
                    isinstance(chan, MaestrioCounterChannel) ]

    @property
    def sampling_counter_names(self):
        return [ chan.name for chan in self.counter_channels
                 if chan.is_sampling() ]

    @property
    def integrating_counter_names(self):
        return [ chan.name for chan in self.counter_channels
                 if not chan.is_sampling() ]

    @property
    def musst_motor_names(self):
        return [ name for name, chan in self.musst_channels.items() 
                 if isinstance(chan, MusstMotorChannel) or
                    isinstance(chan, MaestrioMotorChannel) ]
       
    def get_calc_channel(self, calc_name, chan_name):
        try:
            name = self.calc_names[calc_name][chan_name]
        except KeyError:
            return None
        for chan in self.channels:
            if chan.name == name:
                return chan
        return None

    def get_channel_name(self, calc_name, chan_name):
        chan = self.get_calc_channel(calc_name, chan_name)
        if chan is not None:
            return chan.name
        else:
            return None
        
    def get_channel_short_name(self, calc_name, chan_name):
        chan = self.get_calc_channel(calc_name, chan_name)
        if chan is not None:
            return chan.short_name
        else:
            return None
        
    def prepare(self):
        for filt in self.input_filters.values():
            filt.prepare()
        for chan, all_calc in self.calc_hook.items():
            for calc in all_calc:
                calc.prepare()
        for filt in self.output_filters.values():
            filt.prepare()
        super().prepare()

    def compute(self, sender, data):
        result = dict()

        filt = self.input_filters.get(sender.short_name)
        if filt:
            data = filt.filter_data(data)

        for calc in self.calc_hook[sender.short_name]:
            calc_data = calc.calculate(data)
            filt = self.output_filters.get(calc.name)
            if filt:
                calc_data = filt.filter_data(calc_data)
            calc_name = f"{self.calc_prefix}{calc.name}"
            result[calc_name] = calc_data

        return result

class MusstCalcFilterBase(object):
    def prepare(self):
        pass

    def filter_data(self, data):
        raise NotImplementedError("filter_data method not implemented")

class MusstCalcFilterDummy(MusstCalcFilterBase):
    def filter_data(self, data):
        return data

class MusstCalcFilterSlowRate(MusstCalcFilterBase):
    def __init__(self, modulo, keep_index):
        self._modulo = modulo
        self._keep = keep_index

    def prepare(self):
        self._offset = 0
        
    def filter_data(self, data):
        npts = len(data)
        idx = numpy.arange(npts)
        slow_idx = numpy.where(((self._offset+idx)%self._modulo==0)|((self._offset+idx)%self._modulo==self._keep))
        slow_data = data[slow_idx]
        self._offset += npts
        self._offset %= self._modulo
        return slow_data

class MusstCalcFilterFastRate(MusstCalcFilterBase):
    def __init__(self, modulo, remove_index):
        self._modulo = modulo
        self._remove = remove_index

    def prepare(self):
        self._offset = 0
        
    def filter_data(self, data):
        npts = len(data)
        idx = numpy.arange(npts)
        fast_idx = numpy.where(((self._offset+idx)%self._modulo)!=self._remove)
        fast_data = data[fast_idx]
        self._offset += npts
        self._offset %= self._modulo
        return fast_data

class MusstCalcFilterEveryNPoints(MusstCalcFilterFastRate):
    def __init__(self, npoints):
        MusstCalcFilterFastRate.__init__(self, npoints+1, npoints)

class MusstCalcFilterIndexes(MusstCalcFilterBase):
    def __init__(self, indexes):
        self._indexes = indexes

    def prepare(self):
        self._offset = 0

    def filter_data(self, data):
        npts = len(data)
        to_remove = [idx for idx in self._indexes if idx in range(self._offset, self._offset+npts)]
        if len(to_remove):
            delete_idx = numpy.array(to_remove) - self._offset
            keep_data = numpy.delete(data, delete_idx)
        else:
            keep_data = data
        self._offset += npts
        return keep_data
        
class MusstCalcHookBase:
    def __init__(self, calc_master, channel, data_per_point=1):
        self.master = calc_master
        self.channel = channel
        self.data_per_point = data_per_point
        self.__overflow = 0
        self.__last_data = None

    @property
    def start_epoch(self):
        return self.master.start_epoch

    @property
    def unit(self):
        return self.channel.unit

    @property
    def name(self):
        return f"{self.channel.name}_{self.calc_name}"

    @property
    def calc_name(self):
        raise NotImplementedError("Missing calc_name property")

    def prepare(self):
        pass

    def calculate(self, data):
        raise NotImplementedError("calculate method not implemented")

    def absolute(self, data):
        if self.__last_data is None:
            self.__last_data = data[0]
        raw_data = numpy.append(self.__last_data, data)
        abs_data = raw_data.astype(numpy.float64)
        if len(raw_data) >= 2:
            over_idxs = numpy.where(numpy.diff(abs_data) != numpy.diff(raw_data))[0]
            abs_data += self.__overflow * 2**32
            for idx in over_idxs:
                ovr_sign = raw_data[idx] > 0 and 1. or -1.
                abs_data[idx+1:] += ovr_sign * 2**32
                self.__overflow += ovr_sign
        self.__last_data = raw_data[-1:]
        return abs_data[len(self.__last_data):]

class MusstCalcDummy(MusstCalcHookBase):
    @property
    def calc_name(self):
        return "dummy"

    def calculate(self, data):
        return self.absolute(data)

MusstCalcList.register(MusstCalcDummy, "dummy")

class MusstTrigCalc(MusstCalcHookBase):
    @property
    def calc_name(self):
        return "trig"

    def prepare(self):
        self._data = numpy.array((), numpy.float64)

    def calculate(self, data):
        abs_data = self.absolute(data)
        self._data = numpy.append(self._data, abs_data)
        if self.data_per_point == 1:
            if len(self._data) < 2:
                return None
            calc_data = self.channel.raw2unit(self._data[:-1])
            self._data = self._data[-1:]
        else:
            dp = self.data_per_point
            len_used = dp*(len(self._data)//dp)
            if not len_used:
                return None
            calc_data = self.channel.raw2unit(self._data[:len_used:dp])
            self._data = self._data[len_used:]

        return calc_data

MusstCalcList.register(MusstTrigCalc, "trig")

class MusstEpochTrig(MusstTrigCalc):
    @property
    def calc_name(self):
        return "epoch_trig"

    @property
    def name(self):
        return "epoch_trig"

    def calculate(self, data):
        calc_data = super().calculate(data)
        if calc_data is not None and len(calc_data):
            return self.start_epoch + calc_data
        else:
            return calc_data

MusstCalcList.register(MusstEpochTrig, "epoch_trig")

class MusstPeriodCalc(MusstCalcHookBase):
    @property
    def calc_name(self):
        return "period"

    def prepare(self):
        self._data = numpy.array((), dtype=numpy.int32)

    def calculate(self, data):
        self._data = numpy.append(self._data, data)

        if self.data_per_point == 1:
            if len(self._data) < 2:
                return None
            calc_data = self.channel.delta2unit(self._data[1:] - self._data[:-1])
            self._data = self._data[-1:]
        else:
            dp = self.data_per_point
            if len(self._data) < (dp + 1):
                return None
            calc_data = self.channel.delta2unit(self._data[dp::dp] - self._data[0:-dp:dp])
            self._data = self._data[2 * len(calc_data) :]

        return calc_data

MusstCalcList.register(MusstPeriodCalc, "period")

class MusstDeltaCalc(MusstCalcHookBase):
    @property
    def calc_name(self):
        return "delta"

    def prepare(self):
        self._data = numpy.array((), dtype=numpy.int32)

    def calculate(self, data):
        self._data = numpy.append(self._data, data)

        if self.data_per_point == 1:
            if len(self._data) < 2:
                return None
            calc_data = self.channel.delta2unit(self._data[1:] - self._data[:-1])
            self._data = self._data[-1:]
        else:
            dp = self.data_per_point
            if len(self._data) < (dp + 1):
                return None
            data0 = self._data[0::dp]
            data1 = self._data[dp - 1 :: dp]
            calc_len = min(data0.shape[0], data1.shape[0])
            calc_data = self.channel.delta2unit(data1[:calc_len] - data0[:calc_len])
            self._data = self._data[dp * calc_len :]

        return calc_data

MusstCalcList.register(MusstDeltaCalc, "delta")

class MusstCenterCalc(MusstCalcHookBase):
    @property
    def calc_name(self):
        return "center"

    def prepare(self):
        self._data = numpy.array((), numpy.float64)

    def calculate(self, data):
        new_data = self.absolute(data)
        self._data = numpy.append(self._data, new_data)

        if self.data_per_point == 1:
            if len(self._data) < 2:
                return None
            calc_data = self.channel.raw2unit((self._data[1:] + self._data[:-1]) / 2.)
            self._data = self._data[-1:]
        else:
            dp = self.data_per_point
            if len(self._data) < (dp + 1):
                return None
            data0 = self._data[0::dp]
            data1 = self._data[dp - 1 :: dp]
            calc_len = min(data0.shape[0], data1.shape[0])
            calc_data = self.channel.raw2unit((data1[:calc_len] + data0[:calc_len]) / 2.)
            self._data = self._data[dp * calc_len :]

        return calc_data

MusstCalcList.register(MusstCenterCalc, "center")

class MusstCenter360Calc(MusstCenterCalc):
    @property
    def calc_name(self):
        return "cen360"

    def calculate(self, data):
        calc_data = MusstCenterCalc.calculate(self, data)
        if calc_data is not None:
            calc_data = calc_data % 360
        return calc_data

MusstCalcList.register(MusstCenter360Calc, "cen360")

class MusstCountCalc(MusstDeltaCalc):
    @property
    def calc_name(self):
        return "count"

MusstCalcList.register(MusstCountCalc, "count")


import numpy

from bliss.scanning.scan import Scan
from bliss.controllers.ct2.device import AcqMode as CT2AcqMode

from bliss.scanning.chain import AcquisitionSlave

from fscan.synchro.base import SyncDeviceList
from fscan.synchro.musst.programs import (
    MusstProgFScan,
    MusstProgFScanLoop,
    MusstProgTimeTrigger,
)
from fscan.synchro.maestrio.programs import (
    MaestrioProgFScan,
    MaestrioProgFScanLoop,
)
from fscan.musstcalc import MusstCalcAcqSlave

from fscan.fscantools import (
    FScanModeBase,
    FScanTrigMode,
    FScanParamBase,
    FScanParamStruct,
    FScanDisplay,
    FScanLoopMode,
)
from fscan.motortools import (
    RotationMotorChainPreset,
    MotorListMaster,
    MotorMaster,
)
from fscan.mcatools import get_fscan_mca_params
from fscan.chaintools import ChainTool
from fscan.fscanrunner import FScanDiagRunner
from fscan.fscaninfo import FScanInfo
from fscan.limatools import LimaCalib

FInterlacedMode = FScanModeBase("FInterlacedMode",
                                "REWIND",
                                "ZIGZAG",
                                "FORWARD"
                               )

class FInterlacedPars(FScanParamBase):
    DEFAULT = {
        "motor": None,
        "start_pos": 0,
        "acq_size": 0.1,
        "acq_time": 0.8,
        "npoints": 10,
        "mode": FInterlacedMode.REWIND,
        "shift_pos": 0.,
        "save_flag": True,
        "display_flag": True,
        "sync_encoder": True,
        "home_rotation": False,
        "acc_margin": 0.,
        "sampling_time": 0.5,
    }
    OBJKEYS = ["motor"]
    LISTVAL = {
        "mode": FInterlacedMode.values,
    }
    NOSETTINGS = ["save_flag",]

    def __init__(self, name):
        FScanParamBase.__init__(
            self, 
            name, 
            FInterlacedPars.DEFAULT, 
            FInterlacedPars.OBJKEYS, 
            FInterlacedPars.LISTVAL,
            FInterlacedPars.NOSETTINGS,
        )

    def _validate_npoints(self, value):
        if not int(value) > 0:
            raise ValueError("npoints should be > 0")
        return int(value)

    def _validate_save_flag(self, value):
        return value and True or False

    def _validate_display_flag(self, value):
        return value and True or False

    def _validate_sync_encoder(self, value):
        return value and True or False

    def _validate_scan_mode(self, value):
        return FInterlacedMode.get(value, "mode")

    def _validate_acc_margin(self, value):
        return abs(value)


class FInterlacedMaster(object):
    def __init__(self, name, config):
        self.name = name
        self.config = config

        self.sync_list = SyncDeviceList(config["devices"]["synchro"])
        self.meas_group = config["devices"]["measgroup"]
        self.rot_motors = config["devices"]["rotation_motors"]
        self.chain_config = config["chain_config"]
        self.lima_calib = LimaCalib(config["lima_calib"])
        self.musst_data_rate_factor = config.get("musst_data_rate_factor", 0)

        self.pars = FInterlacedPars(self.name)
        self.inpars = None
        self.mgchain = None

    def validate(self):
        self.inpars = FScanParamStruct(self.pars.to_dict())
        pars = self.inpars

        # --- check motor is on musst
        self.sync_dev = self.sync_list.find_device_for_motors(pars.motor)

        # --- forward mode on rotation motor only
        if pars.mode == FInterlacedMode.FORWARD:
            if pars.motor not in self.rot_motors:
                raise ValueError(f"In interlaced FORWARD mode, can only use rotation motors {self.rot_motors}")

        # --- with lima, acq_time must be > readout_time
        limas = self.get_controllers_found("lima")
        if len(limas):
            self.lima_calib.calibrate(limas, pars.acq_time)
            if self.lima_calib.external_time > pars.acq_time:
                raise RuntimeError("Acquisition time must be greater than readout time !!")

        # --- calc params
        pars.motor_name = pars.motor.name
        pars.motor_unit = pars.motor.unit is None and "unit" or pars.motor.unit

        pars.speed = abs(pars.acq_size) / pars.acq_time
        pars.acc_disp = pars.acc_margin + 0.5 * pars.speed ** 2 / pars.motor.acceleration
        # ensure a minimum of 4 steps on acceleration
        if abs(pars.acc_disp * pars.motor.steps_per_unit) < 4:
            pars.acc_margin += 4/pars.motor.steps_per_unit - pars.acc_disp
            pars.acc_disp = pars.acc_margin + 0.5 * pars.speed ** 2 / pars.motor.acceleration
        pars.acc_time = pars.acc_margin / pars.speed + pars.speed / pars.motor.acceleration

        pars.step_size = 2 * pars.acq_size
        pars.step_time = 2 * pars.acq_time

        if pars.mode == FInterlacedMode.FORWARD:
            pars.npoints = pars.npoints - pars.npoints%2
            pars.npoints_arr = numpy.array([int(pars.npoints / 2), ] * 2, numpy.int32)
            if pars.acq_size > 0:
                pars.scan_dir = 1
            else:
                pars.scan_dir = -1

            if pars.npoints_arr[0] * pars.step_size > 360.0:
                raise ValueError("First acquisition pass is more than 360 degrees")
            
            pars.start_arr = numpy.array([pars.start_pos, 
                             pars.start_pos + pars.scan_dir * 360.0 + pars.acq_size], numpy.float64)
            pars.stop_arr = pars.start_arr + pars.npoints_arr * pars.step_size
            pars.move_time = abs(pars.stop_arr[1] - pars.start_arr[0]) / pars.speed
            pars.scan_time = 2 * pars.acc_time + pars.move_time

        if pars.mode in (FInterlacedMode.REWIND, FInterlacedMode.ZIGZAG):
            pars.npoints_arr = numpy.array([int(pars.npoints / 2),] * 2, numpy.int32)
            pars.npoints_arr[0] += pars.npoints%2

            if pars.acq_size > 0:
                pars.scan_dir_arr = [ 1, 1 ]
            else:
                pars.scan_dir_arr = [ -1, -1 ]

            pars.start_arr = numpy.array([pars.start_pos, pars.start_pos + pars.acq_size], numpy.float64)
            pars.stop_arr = pars.start_arr + pars.npoints_arr * pars.step_size

            if pars.mode == FInterlacedMode.ZIGZAG:
                pars.scan_dir_arr[1] *= -1
                pars.start_arr[1] = pars.stop_arr[0] + pars.shift_pos
                pars.stop_arr[1] = pars.start_arr[0] + pars.shift_pos
                if pars.npoints%2:
                    pars.start_arr[1] -= pars.step_size

            pars.move_time_arr = abs(pars.stop_arr - pars.start_arr)/pars.speed
            pars.scan_time_arr = 2 * pars.acc_time + pars.move_time_arr
            pars.scan_time = pars.scan_time_arr.sum()
            
    def show(self):
        if self.inpars is None:
            raise RuntimeError(f"FInterlacedMaster[{self.name}]: Parameters not validated yet !!")

        mot_txt = """
Motor : {motor_name}
    velocity  = {speed:.4f} {motor_unit}/sec
    acceleration size = {acc_disp:.4f} {motor_unit} (margin = {acc_margin:.4f})
    acceleration time = {acc_time:.6f} sec
"""
        acq_txt = """
Acquisition :
    acq time  = {acq_time:.6f} sec
    acq size  = {acq_size:.4f} {motor_unit}
    total nb points = {npoints}
"""
        print(mot_txt.format(**self.inpars.to_dict()))
        pars = self.inpars
        print(f"Scan in {pars.mode} mode:")
        for ii in range(2):
            print("    Pass #{idx:d} : {npts:d} points from {start:.4f} to {stop:.4f}".format(
                  idx=ii+1, npts=pars.npoints_arr[ii], start=pars.start_arr[ii], stop=pars.stop_arr[ii]))
        print(acq_txt.format(**self.inpars.to_dict()))

    def init_scan(self):
        self.mgchain = ChainTool(self.meas_group, self.chain_config)
        self.lima_used = list()
        self.mca_used = list()
        self.home_preset = None

    def get_controllers_found(self, ctrl_type):
        mgchain = ChainTool(self.meas_group)
        return mgchain.get_controllers_not_done(ctrl_type)

    def get_controllers_used(self, ctrl_type):
        if self.mgchain is None:
            return []
        else:
            return self.mgchain.get_controllers_done(ctrl_type)

    def setup_acq_chain(self, chain):
        mot_master = self.setup_motor_master(chain)
        acq_master = self.setup_acq_master(chain, mot_master)
        self.setup_acq_slaves(chain, acq_master)

    def setup_motor_master(self, chain):
        pars = self.inpars
        if pars.home_rotation is True and pars.motor in self.rot_motors:
            self.home_preset = RotationMotorChainPreset(pars.motor, homing=True, rounding=False)
            chain.add_preset(self.home_preset)

        if pars.mode in (FInterlacedMode.REWIND, FInterlacedMode.ZIGZAG):
            master = MotorListMaster(pars.motor, pars.start_arr.tolist(), pars.stop_arr.tolist(),
                                     pars.move_time_arr[0],
                                     undershoot_start_margin=pars.acc_margin,
                                     undershoot_end_margin=pars.acc_margin)

        elif pars.mode == FInterlacedMode.FORWARD:
            master = MotorMaster(pars.motor, pars.start_arr[0], pars.stop_arr[1], 
                                 pars.move_time, 
                                 undershoot_start_margin=pars.acc_margin, 
                                 undershoot_end_margin=pars.acc_margin)
        return master

    def setup_acq_master(self, chain, mot_master):
        pars = self.inpars

        if pars.mode in (FInterlacedMode.REWIND, FInterlacedMode.ZIGZAG):
            # --- use fscan for each pass
            if self.sync_dev.is_musst():
                syncprog = MusstProgFScan(self.sync_dev.board, [pars.motor,])
                syncprog.check_max_timer(pars.scan_time_arr.max())
                syncprog.set_max_data_rate(self.musst_data_rate_factor * 1./pars.acq_time)
            else:
                syncprog = MaestrioProgFScan(self.sync_dev.board, [pars.motor,])

            syncprog.set_modes(FScanTrigMode.POSITION, FScanTrigMode.POSITION)
            syncprog.set_params(pars.start_arr[0], pars.scan_dir_arr[0], 
                                pars.step_size, pars.acq_size, pars.npoints_arr[0])
            syncprog.set_iter_params(start=pars.start_arr,
                                     direction=pars.scan_dir_arr,
                                     npulses=pars.npoints_arr)
        elif pars.mode == FInterlacedMode.FORWARD:
            if self.sync_dev.is_musst():
                syncprog = MusstProgFScanLoop(self.sync_dev.board, [pars.motor,])
                syncprog.check_max_timer(pars.scan_time)
                syncprog.set_max_data_rate(self.musst_data_rate_factor * 1./pars.acq_time)
            else:
                syncprog = MaestrioProgFScanLoop(self.sync_dev.board, [pars.motor,])

            syncprog.set_modes(FScanTrigMode.POSITION, FScanTrigMode.POSITION,
                               FScanLoopMode.FORWARD)
            syncprog.set_params(pars.start_arr[0], pars.scan_dir,
                                pars.step_size, pars.acq_size,
                                pars.npoints_arr[0], 2, pars.start_arr[1]-pars.start_arr[0])

        # --- sync motors on musst
        if self.home_preset:
            self.home_preset.set_musst_sync_cb(syncprog.sync_one_motor)
        elif pars.sync_encoder:
            syncprog.sync_motors()

        # --- add to chain
        syncprog.setup(chain, mot_master)

        # --- calc device
        synccalc = MusstCalcAcqSlave(syncprog.acq_data_master, syncprog.channels, syncprog.DataPerPoint)
        synccalc.add_default_calc()
        synccalc.add_calc("center", pars.motor_name)
        if pars.mode == FInterlacedMode.FORWARD:
            synccalc.add_calc("cen360", pars.motor_name)
        chain.add(syncprog.acq_master, synccalc)

        # --- motor external channel
        if pars.mode == FInterlacedMode.FORWARD:
            calc_name = synccalc.get_channel_short_name("cen360", pars.motor_name)
        else:
            calc_name = synccalc.get_channel_short_name("center", pars.motor_name)
        mot_master.add_external_channel(synccalc, calc_name, f"axis:{pars.motor_name}")

        self.syncprog = syncprog
        self.synccalc = synccalc

        return syncprog.acq_master

    def setup_acq_slaves(self, chain, acq_master):
        pars = self.inpars

        # --- ct2 counters
        ct2_params={
            "npoints": pars.npoints,
            "acq_mode": CT2AcqMode.ExtGate,
            "prepare_once": True,
            "start_once": True           
        }
        self.mgchain.setup("ct2", chain, acq_master, ct2_params)

        # --- lima devices
        lima_params = {
            "acq_trigger_mode": "EXTERNAL_GATE",
            "acq_expo_time": pars.acq_time,
            "acq_nb_frames": pars.npoints,
            "prepare_once": True,
            "start_once": True,
            "scan_frame_per_file": pars.npoints,
        }
        if pars.mode in (FInterlacedMode.REWIND, FInterlacedMode.ZIGZAG):
            wait_frame_id = [ pars.npoints_arr[0]-1, pars.npoints-1 ]
        else:
            wait_frame_id = [ pars.npoints-1, ]
        lima_params["wait_frame_id"] = wait_frame_id
            
        self.mgchain.setup("lima", chain, acq_master, lima_params)
        self.lima_used = self.mgchain.get_controllers_done("lima")

        # --- mca devices
        for mca_ctrl in self.mgchain.get_controllers_not_done("mca"):
            mca_params = get_fscan_mca_params(mca_ctrl, pars.npoints, pars.step_time)
            self.mgchain.setup("mca", chain, acq_master, mca_params, mca_ctrl.name)
            if "trigger_mode" in mca_params:
                # --- in spectrum reading mode
                self.mca_used.append(mca_ctrl)

        # --- mosca devices
        if pars.mode in (FInterlacedMode.REWIND, FInterlacedMode.ZIGZAG):
            wait_frame_id = [ pars.npoints_arr[0], pars.npoints ]
        else:
            wait_frame_id = [ pars.npoints, ]
        mosca_params = {
            "trigger_mode": "GATE",
            "npoints": pars.npoints,
            "preset_time": pars.acq_time,
            "wait_frame_id": wait_frame_id,
            "start_once": True,
        }
        self.mgchain.setup("mosca", chain, acq_master, mosca_params)

        
        # --- mythen
        mythen_params = {
            "trigger_type": AcquisitionSlave.HARDWARE,
            "npoints": pars.npoints,
            "count_time": pars.acq_time,
        }
        self.mgchain.setup("mythen", chain, acq_master, mythen_params)
           
        # --- calc
        self.mgchain.setup_calc_counters(chain, acq_master)
 
        # --- sampling
        self.mgchain.setup_sampling(chain, pars.sampling_time)

    def setup_acq_scope(self, chain, mot_master):
        pars = self.inpars

        # --- musst time prog
        musstprog = MusstProgTimeTrigger(self.musst, [pars.motor,])
        acqtime = max(0.001, pars.acq_time / 10.)

        if pars.mode in (FInterlacedMode.REWIND, FInterlacedMode.ZIGZAG):
            period_arr = [ acqtime, ] * 2
            npts_arr = ((pars.scan_time_arr + 0.1) / acqtime + 0.5).astype(numpy.int32)
            musstprog.set_iter_params(npts_arr, period_arr)
        else:
            npts = int((pars.scan_time + 0.1) / acqtime + 0.5)
            musstprog.set_params(npts, acqtime)

        # --- sync motors on musst
        if self.home_preset:
            self.home_preset.set_musst_sync_cb(musstprog.sync_one_motor)
        elif pars.sync_encoder:
            musstprog.sync_motors()

        # --- acq chain
        musstprog.setup(chain, mot_master)
        musstprog.add_default_calc(chain)

    def setup_scan(self, chain, scan_name, user_info=None):
        inpars = self.inpars
        if inpars.display_flag:
            self.show()

        scan_info = FScanInfo(scan_name, "finterlaced", inpars.npoints)
        scan_info.set_fscan_pars(self.pars)
        scan_info.set_fscan_title(
            "{motor_name} {start_pos:g} {acq_size:g} {acq_time:g} {npoints:d} mode={mode}",
            inpars,
        )
        scan_info.add_curve_plot(x=f"axis:{inpars.motor_name}")
        scan_info.set_fscan_info(user_info)

        trig_name = self.synccalc.get_channel_name("trig", "timer")
        scan = Scan(
            chain,
            name=scan_name,
            save=inpars.save_flag,
            scan_info=scan_info,
            scan_progress=FScanDisplay(trig_name),
        )

        return scan

    def get_runner_class(self):
        return FInterlacedCustomRunner

class FInterlacedCustomRunner(FScanDiagRunner):
    def __call__(self, motor, start_pos, acq_size, npoints, acq_time, mode="ZIGZAG", **kwargs):
        pars = dict(motor=motor,
                    start_pos=start_pos,
                    acq_size=acq_size,
                    npoints=npoints,
                    acq_time=acq_time,
                    mode=mode
               )
        pars.update(kwargs)
        self.run_with_pars(pars)


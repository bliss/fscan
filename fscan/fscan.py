from bliss.scanning.scan import Scan
from bliss.controllers.ct2.device import AcqMode as CT2AcqMode
from bliss.scanning.chain import AcquisitionSlave

from fscan.synchro.base import SyncDeviceList
from fscan.synchro.maestrio.programs import (
    MaestrioProgFScan,
    MaestrioProgFScanCamera,
    MaestrioProgFTimeScan,
)
from fscan.synchro.musst.programs import (
    MusstProgFScan, 
    MusstProgFScanCamera,
    MusstProgFTimeScan,
    MusstProgTimeTrigger,
)
from fscan.musstcalc import MusstCalcAcqSlave
from fscan.fscantools import (
    FScanMode,
    FScanTrigMode,
    FScanCamSignal,
    FScanParamBase,
    FScanParamStruct,
    FScanDisplay,
)
from fscan.mcatools import get_fscan_mca_params
from fscan.motortools import (
    MotorMaster,
    ExternalMotorMaster,
    RotationMotorChainPreset, 
    check_motor_limits,
    check_motor_velocity,
)
from fscan.chaintools import ChainTool
from fscan.fscanrunner import FScanDiagRunner
from fscan.fscaninfo import FScanInfo
from fscan.limatools import LimaCalib
from fscan.calctools import LimaRoiProfile2ScatterCalc


class FScanPars(FScanParamBase):
    DEFAULT = {
        "motor": None,
        "start_pos": 0,
        "step_size": 0.1,
        "step_time": 1.0,
        "acq_time": 0.8,
        "npoints": 10,
        "scan_mode": FScanMode.TIME,
        "gate_mode": FScanTrigMode.TIME,
        "camera_signal": FScanCamSignal.EDGE,
        "min_gate_low_time": 0.,
        "save_flag": True,
        "display_flag": True,
        "sync_encoder": True,
        "home_rotation": False,
        "acc_margin": 0.,
        "sampling_time": 0.5,
        "latency_time": 1e-5,
        "scatter_plot": True,
    }
    OBJKEYS = ["motor"]
    LISTVAL = {
        "scan_mode": FScanMode.values,
        "gate_mode": FScanTrigMode.values,
        "camera_signal": FScanCamSignal.values,
    }
    NOSETTINGS = ["save_flag",]

    def __init__(self, name):
        FScanParamBase.__init__(
            self,
            name,
            FScanPars.DEFAULT,
            FScanPars.OBJKEYS,
            FScanPars.LISTVAL,
            FScanPars.NOSETTINGS,
        )

    def _validate_npoints(self, value):
        if not int(value) > 0:
            raise ValueError("npoints should be > 0")
        return int(value)

    def _validate_save_flag(self, value):
        return value and True or False

    def _validate_display_flag(self, value):
        return value and True or False

    def _validate_sync_encoder(self, value):
        return value and True or False

    def _validate_scan_mode(self, value):
        return FScanMode.get(value, "scan_mode")

    def _validate_gate_mode(self, value):
        return FScanTrigMode.get(value, "gate_mode")

    def _validate_camera_signal(self, value):
        return FScanCamSignal.get(value, "camera_signal")

    def _validate_acc_margin(self, value):
        return abs(value)


class FScanMaster(object):
    def __init__(self, name, config):
        self.name = name
        self.config = config

        self.sync_list = SyncDeviceList(config["devices"]["synchro"])
        self.meas_group = config["devices"]["measgroup"]
        self.rot_motors = config["devices"]["rotation_motors"]
        self.ext_motors = config["devices"]["external_motors"]
        self.chain_config = config["chain_config"]
        self.lima_calib = LimaCalib(config["lima_calib"])
        self.musst_data_rate_factor = config.get("musst_data_rate_factor", 0)

        self.pars = FScanPars(self.name)
        self.inpars = None
        self.mgchain = None
        self.user_calc = list()
        
        # default master class for external motor
        self._external_motor_master_class = ExternalMotorMaster

    def set_external_motor_master_class(self, klass):
        """ Replace the default ExternalMotorMaster with a custom class derivated from ExternalMotorMaster """
        if not issubclass(klass, ExternalMotorMaster):
            raise ValueError("external motor master class musst derivate from ExternalMotorMaster")
        self._external_motor_master_class = klass
        
    def add_user_calc(self, calc_obj):
        self.user_calc.append(calc_obj)

    def remove_user_calc(self, calc_obj):
        self.user_calc.remove(calc_obj)

    def validate(self):
        self.inpars = FScanParamStruct(self.pars.to_dict())
        pars = self.inpars

        if pars.motor in self.ext_motors:
            # --- external motor, use first musst
            pars.external_motor = True
            self.sync_dev = self.sync_list.first()
            pars.scan_mode = FScanTrigMode.TIME
            pars.gate_mode = FScanTrigMode.TIME
        else:
            # --- check motor is on musst
            pars.external_motor = False
            self.sync_dev = self.sync_list.find_device_for_motors(pars.motor)

        # --- check lima and minimum period
        limas = self.get_controllers_found("lima", "lima2")
        self.lima_calib.validate(limas, pars, use_acc=True)

        if pars.lima_ndev:
            if pars.step_time < pars.lima_min_period:
                pars.step_time = pars.lima_min_period
        else:
            if pars.step_time < pars.acq_time:
                pars.step_time = pars.acq_time + pars.latency_time

        pars.dead_time = pars.step_time - pars.acq_time

        # --- check minimum gate low time
        pars.gate_low_time = 0.
        if pars.scan_mode == FScanMode.CAMERA:
            if pars.camera_signal == FScanCamSignal.FALL:
                pars.gate_low_time = max(pars.min_gate_low_time, pars.dead_time)
            elif pars.camera_signal == FScanCamSignal.RISE:
                pars.gate_low_time = pars.min_gate_low_time
            if pars.gate_low_time >= pars.step_time:
                raise ValueError("Minimum gate low time >= period is not possible")

        # --- calc params
        pars.motor_name = pars.motor.name
        pars.motor_unit = pars.motor.unit is None and "unit" or pars.motor.unit
        pars.speed = abs(pars.step_size) / pars.step_time
        pars.stop_pos = pars.start_pos + pars.npoints * pars.step_size
        pars.acq_size = pars.acq_time * pars.speed
        pars.move_time = pars.npoints * pars.step_time
        
        acc = pars.motor.acceleration
        if acc > 0:
            inv_acc = 1 / acc
        else:
            inv_acc = 0

        pars.acc_time = pars.acc_margin / pars.speed + pars.speed * inv_acc
        pars.acc_disp = pars.acc_margin + 0.5 * inv_acc * pars.speed ** 2
        
        pars.scan_time = 2 * pars.acc_time + pars.move_time
        if pars.step_size > 0:
            pars.scan_dir = 1
        else:
            pars.scan_dir = -1
        if pars.dead_time < 0.:
            raise ValueError("Acquisition Time >= Step Time !!")

        # --- check motor limits
        pars.real_start = pars.start_pos - pars.scan_dir * pars.acc_disp
        pars.real_stop = pars.stop_pos + pars.scan_dir * pars.acc_disp
        check_motor_limits(pars.motor, pars.real_start, pars.real_stop)
        check_motor_velocity(pars.motor, pars.speed)

    def show(self):
        if self.inpars is None:
            raise RuntimeError(f"FScanMaster[{self.name}]: Parameters not validated yet !!")

        mot_txt = ""
        if self.inpars.external_motor:
            mot_txt += "External "

        mot_txt += """Motor : {motor_name}
    start pos = {start_pos:.4f} {motor_unit}
    stop pos  = {stop_pos:.4f} {motor_unit}
    step size = {step_size:.4f} {motor_unit}
    step time = {step_time:.6f} sec
    velocity  = {speed:.4f} {motor_unit}/sec
    acceleration size = {acc_disp:.4f} {motor_unit} (margin = {acc_margin:.4f})
    acceleration time = {acc_time:.6f} sec
"""
        txt = mot_txt + """
Acquisition :
    scan mode   = {scan_mode} {signal_txt}
    camera mode = {lima_acq_mode}
    acq time  = {acq_time:.6f} sec
    acq size  = {acq_size:.4f} {motor_unit}
    dead time = {dead_time:.6f} sec
    nb points = {npoints}
"""
        if self.inpars.scan_mode == FScanMode.CAMERA:
            signal_txt = f"({self.inpars.camera_signal} signal)"
        else:
            signal_txt = ""

        print(txt.format(signal_txt=signal_txt, **self.inpars.to_dict()))

    def init_scan(self):
        self.mgchain = ChainTool(self.meas_group, self.chain_config)
        self.lima_used = list()
        self.lima2_used = list()
        self.mca_used = list()
        self.home_preset = None

    def get_controllers_found(self, *ctrl_type):
        mgchain = ChainTool(self.meas_group)
        return mgchain.get_controllers(*ctrl_type)

    def get_controllers_used(self, ctrl_type):
        if self.mgchain is None:
            return []
        else:
            return self.mgchain.get_controllers_done(ctrl_type)

    def setup_acq_chain(self, chain):
        mot_master = self.setup_motor_master(chain)
        acq_master = self.setup_acq_master(chain, mot_master)
        self.setup_acq_slaves(chain, acq_master)

    def setup_motor_master(self, chain):
        pars = self.inpars
        if pars.home_rotation is True and pars.motor in self.rot_motors:
            self.home_preset = RotationMotorChainPreset(pars.motor, homing=True, rounding=False)
            chain.add_preset(self.home_preset)

        # --- create motor master
        motor_pars = {
                "axis": pars.motor,
                "start": pars.start_pos,
                "end": pars.stop_pos,
                "time": pars.move_time,
                "undershoot_start_margin": pars.acc_margin,
                "undershoot_end_margin": pars.acc_margin,
        }

        if pars.external_motor:
            motor_pars["fast_npoints"] = pars.npoints
            master_class = self._external_motor_master_class
        else:
            master_class = MotorMaster
            
        return master_class(**motor_pars)

    def setup_acq_master(self, chain, mot_master):
        pars = self.inpars

        if pars.external_motor:
            # --- sync prog for external motor : ftimescan-like
            if self.sync_dev.is_musst():
                syncprog = MusstProgFTimeScan(self.sync_dev.board)
                syncprog.check_max_timer(pars.scan_time)
                syncprog.set_max_data_rate(self.musst_data_rate_factor * 2./pars.step_time)
            else:
                syncprog = MaestrioProgFTimeScan(self.sync_dev.board)
            syncprog.set_extstart_params(pars.npoints, pars.acq_time, pars.step_time)
            if pars.lima_acc_used:
                syncprog.set_acc_params(pars.lima_acc_nb)
        elif pars.scan_mode == FScanMode.CAMERA:
            # --- sync prog in CAMERA mode
            if self.sync_dev.is_musst():
                syncprog = MusstProgFScanCamera(self.sync_dev.board, [pars.motor,])
                syncprog.check_max_timer(pars.scan_time)
                syncprog.set_max_data_rate(self.musst_data_rate_factor * 2./pars.step_time)
            else:
                syncprog = MaestrioProgFScanCamera(self.sync_dev.board, [pars.motor,])
            syncprog.set_params(pars.start_pos, pars.scan_dir, pars.npoints, pars.camera_signal, pars.gate_low_time)
            if pars.lima_acc_used:
                syncprog.set_acc_params(pars.lima_acc_nb)
        else:
            # --- sync prog for either TIME or POSITION mode
            if self.sync_dev.is_musst():
                syncprog = MusstProgFScan(self.sync_dev.board, [pars.motor,])
                syncprog.check_max_timer(pars.scan_time)
                syncprog.set_max_data_rate(self.musst_data_rate_factor * 2./pars.step_time)
            else:
                syncprog = MaestrioProgFScan(self.sync_dev.board, [pars.motor,])

            syncprog.set_modes(pars.scan_mode, pars.gate_mode)
            if pars.scan_mode == FScanTrigMode.TIME:
                delta = pars.step_time
            else:
                delta = pars.step_size
            if pars.gate_mode == FScanTrigMode.TIME:
                gatewidth = pars.acq_time
            else:
                gatewidth = pars.acq_size
            syncprog.set_params(pars.start_pos, pars.scan_dir, delta, gatewidth, pars.npoints)
            if pars.lima_acc_used:
                syncprog.set_acc_params(pars.lima_acc_nb, pars.lima_acc_period)

        # --- sync motors on musst
        if self.home_preset:
            self.home_preset.set_musst_sync_cb(syncprog.sync_one_motor)
        elif pars.sync_encoder:
            syncprog.sync_motors()

        # --- add to chain
        syncprog.setup(chain, mot_master)

        # --- calc device
        synccalc = MusstCalcAcqSlave(syncprog.acq_data_master, syncprog.channels, syncprog.DataPerPoint)
        synccalc.add_default_calc()
        if not pars.external_motor:
            synccalc.add_calc("center", pars.motor_name)

        chain.add(syncprog.acq_master, synccalc)

#        if self.user_calc:
#            syncprog.add_user_calc(chain, self.user_calc, pars)

        # --- motor external channel
        if not pars.external_motor:
            calc_name = synccalc.get_channel_short_name("center", pars.motor_name)
            mot_master.add_external_channel(synccalc, calc_name, f"axis:{pars.motor_name}")

        self.syncprog = syncprog
        self.synccalc = synccalc

        return syncprog.acq_master

    def setup_acq_slaves(self, chain, acq_master):
        pars = self.inpars

        # --- ct2 counters
        ct2_params = {
            "npoints": pars.npoints,
            "acq_mode": CT2AcqMode.ExtGate,
            "prepare_once": True,
            "start_once": True,
        }
        self.mgchain.setup("ct2", chain, acq_master, ct2_params)

        # --- lima devices
        if pars.scan_mode == FScanMode.CAMERA:
            trig_mode = "EXTERNAL_TRIGGER"
        else:
            trig_mode = "EXTERNAL_TRIGGER_MULTI"

        lima_params = {
            "acq_mode": pars.lima_acq_mode,
            "acq_trigger_mode": trig_mode,
            "acq_expo_time": pars.acq_time,
            "acq_nb_frames": pars.npoints,
            "wait_frame_id": [pars.npoints-1,],
            "prepare_once": True,
            "start_once": True,
            "scan_frame_per_file": pars.npoints,
        }
        if pars.scan_mode == FScanMode.CAMERA:
            if pars.step_time > pars.lima_min_period:
                lat_time = pars.step_time - pars.acq_time
                lima_params["latency_time"] = lat_time
        self.mgchain.setup("lima", chain, acq_master, lima_params)
        self.lima_used = self.mgchain.get_controllers_done("lima")

        if pars.scatter_plot:
            self.scatter_calc = list()
            for lima_dev in self.lima_used:
                lima_master = self.mgchain.get_node_by_name(lima_dev.name)
                mot_channel = self.synccalc.get_calc_channel("center", pars.motor_name)
                for roi in lima_dev.roi_profiles.values():
                    channel = self.mgchain.get_controller_channel_by_name(lima_dev.name, roi.name)
                    if channel is not None:
                        calc = LimaRoiProfile2ScatterCalc(mot_channel, roi, channel)
                        chain.add(lima_master, calc)
                        self.scatter_calc.append(calc)

        # --- lima2 devices
        lima_params = {
            "trigger_mode": "external",
            "expo_time": pars.acq_time,
            "nb_frames": pars.npoints,
            "nb_frames_per_trigger": 1,
            "wait_frame_id": [ pars.npoints - 1, ],
            "prepare_once": True,
            "start_once": True,
        }

        if pars.scan_mode == FScanMode.CAMERA:
            lima_params["nb_frames_per_trigger"] = pars.npoints
            if pars.step_time > pars.lima_min_period:
                lat_time = pars.step_time - pars.acq_time
                lima_params["latency_time"] = lat_time

        self.mgchain.setup("lima2", chain, acq_master, lima_params)
        self.lima2_used = self.mgchain.get_controllers_done("lima2")

        # --- mca devices
        for mca_ctrl in self.mgchain.get_controllers_not_done("mca"):
            mca_params = get_fscan_mca_params(mca_ctrl, self.inpars.npoints, self.inpars.step_time)
            self.mgchain.setup("mca", chain, acq_master, mca_params, mca_ctrl.name)
            if "trigger_mode" in mca_params:
                # --- in spectrum reading mode
                self.mca_used.append(mca_ctrl)

        # --- mythen
        mythen_params = {
            "trigger_type": AcquisitionSlave.HARDWARE,
            "npoints": pars.npoints,
            "count_time": pars.acq_time,
        }
        self.mgchain.setup("mythen", chain, acq_master, mythen_params)

        # --- mosca devices
        mosca_params = {
            "trigger_mode": "GATE",
            "npoints": pars.npoints,
            "preset_time": pars.acq_time,
            "wait_frame_id": [pars.npoints,],
            "start_once": True,
        }
        self.mgchain.setup("mosca", chain, acq_master, mosca_params)

        # --- calc
        self.mgchain.setup_calc_counters(chain, acq_master)

        # --- sampling
        self.mgchain.setup_sampling(chain, pars.sampling_time)

    def setup_acq_scope(self, chain, mot_master):
        acqtime = max(0.001, self.inpars.step_time / 10.)
        npts = int((self.inpars.scan_time + 0.1) / acqtime + 0.5)

        # --- musst time prog
        musstprog = MusstProgTimeTrigger(self.musst, [self.inpars.motor,])
        musstprog.set_params(npts, acqtime)

        # --- sync motors on musst
        if self.home_preset:
            self.home_preset.set_musst_sync_cb(musstprog.sync_one_motor)
        elif self.inpars.sync_encoder:
            musstprog.sync_motors()

        # --- acq chain
        musstprog.setup(chain, mot_master)
        musstprog.add_default_calc(chain)

    def setup_scan(self, chain, scan_name, user_info=None):
        inpars = self.inpars
        if inpars.display_flag:
            self.show()

        scan_info = FScanInfo(scan_name, "fscan", inpars.npoints)
        scan_info.set_fscan_pars(self.pars)
        title = "{motor_name} {start_pos:g} {step_size:g} {npoints:d} {acq_time:g} {step_time:g}"
        scan_info.set_fscan_title(title, inpars)
        scan_info.set_channel_meta(
                f"axis:{inpars.motor_name}",
                start=inpars.start_pos,
                stop=inpars.stop_pos,
                points=inpars.npoints,
        )
        scan_info.add_curve_plot(x=f"axis:{inpars.motor_name}")

        if inpars.scatter_plot:
            for calc in self.scatter_calc:
                scan_info.set_channel_meta(
                    calc.calc_names["axis"],
                    start=inpars.start_pos,
                    stop=inpars.stop_pos,
                    points=inpars.npoints * calc.spectrum_size,
                    axis_points=inpars.npoints,
                    axis_id=calc.axis_id,
                    axis_kind="forth",
                    group=calc.name
                )
                scan_info.set_channel_meta(
                    calc.calc_names["spectrum_index"],
                    start=0,
                    stop=calc.spectrum_size,
                    points=inpars.npoints * calc.spectrum_size,
                    axis_points=calc.spectrum_size,
                    axis_id=(calc.axis_id+1) % 2,
                    group=calc.name
                )
                scan_info.set_channel_meta(calc.calc_names["spectrum_value"], group=calc.name)
                if calc.axis_id == 0:
                    xname = calc.calc_names["axis"]
                    yname = calc.calc_names["spectrum_index"]
                else:
                    xname = calc.calc_names["spectrum_index"]
                    yname = calc.calc_names["axis"]
                scan_info.add_scatter_plot(
                    x=xname,
                    y=yname,
                    value=calc.calc_names["spectrum_value"]
                )

        scan_info.set_fscan_info(user_info)

        trig_name = self.synccalc.get_channel_name("trig", "timer")
        scan = Scan(
            chain,
            name=scan_name,
            save=inpars.save_flag,
            scan_info=scan_info,
            scan_progress=FScanDisplay(trig_name),
        )

        return scan

    def get_runner_class(self):
        return FScanCustomRunner


class FScanCustomRunner(FScanDiagRunner):
    def __call__(self, motor, start_pos, step_size, npoints, acq_time, step_time=0., **kwargs):
        pars = dict(
                motor=motor,
                start_pos=start_pos,
                step_size=step_size,
                npoints=npoints,
                acq_time=acq_time,
                step_time=step_time,
        )
        pars.update(kwargs)
        self.run_with_pars(pars)

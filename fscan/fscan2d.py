import numpy

from bliss.scanning.scan import Scan
from bliss.scanning.acquisition.motor import LinearStepTriggerMaster
from bliss.controllers.ct2.device import AcqMode as CT2AcqMode
from bliss.scanning.chain import attach_channels

from fscan.synchro.base import SyncDeviceList
from fscan.synchro.maestrio.programs import (
    MaestrioProgFScan,
    MaestrioProgFScanCamera,
    MaestrioProgFTimeScan,
)
from fscan.synchro.musst.programs import (
    MusstProgFScan,
    MusstProgFScanCamera,
    MusstProgTimeTrigger,
    MusstProgFTimeScan,
)
from fscan.musstcalc import MusstCalcAcqSlave

from fscan.fscantools import (
    FScanMode,
    FScanAxisMode,
    FScanTrigMode,
    FScanCamSignal,
    FScanParamBase,
    FScanParamStruct,
    FScanDisplay,
)
from fscan.motortools import (
    MotorListMaster,
    ExternalMotorListMaster,
    RotationMotorChainPreset,
    estimate_move_time,
    check_motor_limits,
    check_motor_velocity,
)
from fscan.mcatools import get_fscan_mca_params
from fscan.chaintools import ChainTool
from fscan.fscanrunner import FScanDiagRunner
from fscan.fscaninfo import FScanInfo
from fscan.limatools import LimaCalib


class FScan2DPars(FScanParamBase):
    DEFAULT = {
        "slow_motor": None,
        "slow_start_pos": 0,
        "slow_step_size": 0.1,
        "slow_npoints": 10,
        "fast_motor": None,
        "fast_start_pos": 0,
        "fast_step_size": 0.1,
        "fast_step_time": 1.0,
        "fast_npoints": 10,
        "acq_time": 0.8,
        "fast_motor_mode": FScanAxisMode.REWIND,
        "fast_acc_margin": 0.,
        "scan_mode": FScanMode.TIME,
        "gate_mode": FScanTrigMode.TIME,
        "camera_signal": FScanCamSignal.EDGE,
        "min_gate_low_time": 0.,
        "save_flag": True,
        "display_flag": True,
        "sync_encoder": True,
        "home_rotation": False,
        "sampling_time": 0.5,
        "latency_time": 1e-5,
        "scatter_plot": True,
    }
    OBJKEYS = ["slow_motor", "fast_motor"]
    LISTVAL = {
        "fast_motor_mode": FScanAxisMode.values,
        "scan_mode": FScanMode.values,
        "gate_mode": FScanTrigMode.values,
        "camera_signal": FScanCamSignal.values,
    }
    NOSETTINGS = ["save_flag",]

    def __init__(self, name):
        FScanParamBase.__init__(
            self, 
            name, 
            FScan2DPars.DEFAULT, 
            FScan2DPars.OBJKEYS, 
            FScan2DPars.LISTVAL,
            FScan2DPars.NOSETTINGS,
        )

    def _validate_slow_npoints(self, value):
        if not int(value) > 0:
            raise ValueError("slow_npoints should be > 0")
        return int(value)

    def _validate_fast_npoints(self, value):
        if not int(value) > 0:
            raise ValueError("fast_npoints should be > 0")
        return int(value)

    def _validate_save_flag(self, value):
        return value and True or False

    def _validate_display_flag(self, value):
        return value and True or False

    def _validate_sync_encoder(self, value):
        return value and True or False

    def _validate_fast_motor_mode(self, value):
        return FScanAxisMode.get(value, "fast_motor_mode")

    def _validate_scan_mode(self, value):
        return FScanMode.get(value, "scan_mode")

    def _validate_gate_mode(self, value):
        return FScanTrigMode.get(value, "gate_mode")

    def _validate_camera_signal(self, value):
        return FScanCamSignal.get(value, "camera_signal")

    def _validate_fast_acc_margin(self, value):
        return abs(value)

    def _validate_scatter_plot(self, value):
        return value and True or False

class FScan2DMaster(object):
    def __init__(self, name, config):
        self.name = name
        self.config = config

        self.sync_list = SyncDeviceList(config["devices"]["synchro"])
        self.meas_group = config["devices"]["measgroup"]
        self.rot_motors = config["devices"]["rotation_motors"]
        self.ext_motors = config["devices"]["external_motors"]
        self.chain_config = config["chain_config"]
        self.lima_calib = LimaCalib(config["lima_calib"])
        self.musst_data_rate_factor = config.get("musst_data_rate_factor", 0)

        self.pars = FScan2DPars(self.name)
        self.inpars = None
        self.mgchain = None

        # default master class for external motor
        self._external_motor_list_master_class = ExternalMotorListMaster

    def set_external_motor_list_master_class(self, klass):
        """ Replace the default ExternalMotorListMaster with a custom class derivated from ExternalMotorListMaster """
        if not issubclass(klass, ExternalMotorListMaster):
            raise ValueError("external motor master class musst derivate from ExternalMotorListMaster")
        self._external_motor_list_master_class = klass

    def validate(self):
        self.inpars = FScanParamStruct(self.pars.to_dict())
        pars = self.inpars

        # --- check motors
        if pars.fast_motor is None:
            raise ValueError("Need to specify fast motor !!")
        if pars.slow_motor is None:
            raise ValueError("Need to specify slow motor !!")

        if pars.fast_motor in self.ext_motors:
            # --- external motor, use first musst
            pars.external_motor = True
            self.sync_dev = self.sync_list.first()
            pars.scan_mode = FScanTrigMode.TIME
            pars.gate_mode = FScanTrigMode.TIME
        else:
            # --- check motor is on musst
            pars.external_motor = False
            self.sync_dev = self.sync_list.find_device_for_motors(pars.fast_motor)

        # --- fast_start_pos is one value or a list/array
        try:
            iter_start_pos = iter(pars.fast_start_pos)
            pars.fall_start_pos = numpy.fromiter(iter_start_pos, numpy.float64)
            pars.is_fast_regular = False
            pars.fast_start_pos_txt = "[{0:g} .. {1:g}]".format(pars.fall_start_pos[0], pars.fall_start_pos[-1])
        except TypeError:
            pars.is_fast_regular = True
            pars.fall_start_pos = numpy.array([pars.fast_start_pos,] * pars.slow_npoints, dtype=numpy.float64)
            pars.fast_start_pos_txt = f"{pars.fast_start_pos:g}"

        if not pars.is_fast_regular and len(pars.fall_start_pos) != pars.slow_npoints:
            raise ValueError(f"fast_start_pos array should have length {pars.slow_npoints} !!")
        pars.first_start_pos = pars.fall_start_pos[0]

        # --- check period if not given
        limas = self.get_controllers_found("lima", "lima2")
        self.lima_calib.validate(limas, pars, use_acc=True)

        if pars.lima_ndev:
            if pars.fast_step_time < pars.lima_min_period:
                pars.fast_step_time = pars.lima_min_period
        else:
            if pars.fast_step_time < pars.acq_time:
                pars.fast_step_time = pars.acq_time + pars.latency_time
        pars.dead_time = pars.fast_step_time - pars.acq_time

        # --- check minimum gate low time
        pars.gate_low_time = 0.
        if pars.scan_mode == FScanMode.CAMERA:
            if pars.camera_signal == FScanCamSignal.FALL:
                pars.gate_low_time = max(pars.min_gate_low_time, pars.dead_time)
            elif pars.camera_signal == FScanCamSignal.RISE:
                pars.gate_low_time = pars.min_gate_low_time
            if pars.gate_low_time >= pars.fast_step_time:
                raise ValueError("Minimum gate low time >= period is not possible")

        # --- calculate parameters
        pars.slow_motor_name = pars.slow_motor.name
        pars.slow_motor_unit = pars.slow_motor.unit is None and "unit" or pars.slow_motor.unit
        pars.slow_stop_pos = pars.slow_start_pos + (pars.slow_npoints - 1) * pars.slow_step_size
        pars.slow_step_time = estimate_move_time(pars.slow_motor, pars.slow_step_size)
        pars.slow_move_time = (pars.slow_npoints - 1) * pars.slow_step_time

        pars.fast_motor_name = pars.fast_motor.name
        pars.fast_motor_unit = pars.fast_motor.unit is None and "unit" or pars.fast_motor.unit

        pars.first_start_pos = pars.fall_start_pos[0]
        pars.fast_speed = abs(pars.fast_step_size) / pars.fast_step_time

        acc = pars.fast_motor.acceleration
        if acc > 0:
            inv_acc = 1 / acc
        else:
            inv_acc = 0

        pars.fast_acc_time = pars.fast_acc_margin / pars.fast_speed + pars.fast_speed * inv_acc
        pars.fast_acc_size = pars.fast_acc_margin + 0.5 * inv_acc * pars.fast_speed ** 2

        pars.fast_move_time = pars.fast_npoints * pars.fast_step_time

        if pars.fast_step_size < 0.:
            pars.fast_scan_dir = -1
        else:
            pars.fast_scan_dir = 1

        pars.acq_size = pars.acq_time * pars.fast_speed
        pars.npoints = pars.fast_npoints * pars.slow_npoints
        if pars.dead_time < 0.:
            raise ValueError("Acquisition Time >= Step Time !!")

        if pars.fast_motor_mode == FScanAxisMode.ZIGZAG:
            if pars.fast_step_size > 0:
                step_sign = 1.
            else:
                step_sign = -1.
            pars.loop_delta = step_sign * (abs(pars.fast_step_size) - pars.acq_size)
        else:
            pars.loop_delta = 0

        # --- scan time
        pars.fast_scan_time = 2 * pars.fast_acc_time + pars.fast_move_time
        pars.scan_time = pars.slow_move_time + (pars.slow_npoints * pars.fast_scan_time)

        # --- fast motor position arrays
        start_pos = numpy.array(pars.fall_start_pos)
        stop_pos = start_pos + pars.fast_npoints * pars.fast_step_size
        scan_dir = numpy.array([pars.fast_scan_dir,] * pars.slow_npoints)

        pars.fast_pos_range = [ min(start_pos.min(), stop_pos.min()) + pars.fast_step_size/2.,
                                max(start_pos.max(), stop_pos.max()) - pars.fast_step_size/2.,
                              ]

        if pars.fast_motor_mode == FScanAxisMode.ZIGZAG:
            start_pos[1::2] = stop_pos[1::2] - pars.loop_delta
            stop_pos[1::2] = pars.fall_start_pos[1::2] - pars.loop_delta
            scan_dir[1::2] *= -1

        pars.fall_start_pos = start_pos
        pars.fall_stop_pos = stop_pos
        pars.fall_scan_dir = scan_dir

        # --- check motor limits
        fast_real_start = pars.fall_start_pos - pars.fall_scan_dir * pars.fast_acc_size
        fast_real_stop = pars.fall_stop_pos + pars.fall_scan_dir * pars.fast_acc_size
        check_motor_limits(pars.fast_motor, fast_real_start.min(), fast_real_start.max(),
                                            fast_real_stop.min(), fast_real_stop.min())
        check_motor_limits(pars.slow_motor, pars.slow_start_pos, pars.slow_stop_pos)
        check_motor_velocity(pars.fast_motor, pars.fast_speed)

    def show(self):
        if self.inpars is None:
            raise RuntimeError(f"FScan2DMaster[{self.name}]: Parameters not validated yet !!")

        pars = self.inpars
        if pars.is_fast_regular:
            start_pos_txt = "{0:.4f} {1}".format(pars.fall_start_pos[0], pars.fast_motor_unit)
            stop_pos_txt = "{0:.4f} {1}".format(pars.fall_stop_pos[0], pars.fast_motor_unit)
        else:
            start_pos_txt = "[{0:.4f} .. {1:.4f}] {2}".format(pars.fall_start_pos[0], pars.fall_start_pos[-1], pars.fast_motor_unit)
            stop_pos_txt = "[{0:.4f} .. {1:.4f}] {2}".format(pars.fall_stop_pos[0], pars.fall_stop_pos[-1], pars.fast_motor_unit)

        slow_mot_txt = """
Slow Motor : {slow_motor_name}
    start pos = {slow_start_pos:.4f} {slow_motor_unit}
    stop pos  = {slow_stop_pos:.4f} {slow_motor_unit}
    step size = {slow_step_size:.4f} {slow_motor_unit}

"""
        
        fast_mot_txt = ""
        if self.inpars.external_motor:
            fast_mot_txt += "External "

        fast_mot_txt += """Fast Motor : {fast_motor_name}
    start pos = {start_pos_txt}
    stop pos  = {stop_pos_txt}
    step size = {fast_step_size:.4f} {fast_motor_unit}
    step time = {fast_step_time:.6f} sec
    velocity  = {fast_speed:.4f} {fast_motor_unit}/sec
    acceleration size = {fast_acc_size:.4f} {fast_motor_unit} (margin= {fast_acc_margin:.4f})
    acceleration time = {fast_acc_time:.6f} sec
    mode      = {fast_motor_mode}
"""

        if self.inpars.scan_mode == FScanMode.CAMERA:
            signal_txt = f"({self.inpars.camera_signal} signal)"
        else:
            signal_txt = ""

        acq_txt = """
Acquisition :
    scan mode   = {scan_mode} {signal_txt}
    camera mode = {lima_acq_mode}
    acq time  = {acq_time:.6f} sec
    acq size  = {acq_size:.4f} {fast_motor_unit}
    dead time = {dead_time:.6f} sec
    nb points = {npoints} (slow:{slow_npoints} * fast:{fast_npoints})
"""
        txt = slow_mot_txt + fast_mot_txt + acq_txt
        print(txt.format(start_pos_txt=start_pos_txt, stop_pos_txt=stop_pos_txt, signal_txt=signal_txt, **self.inpars.to_dict()))

    def get_controllers_found(self, *ctrl_type):
        mgchain = ChainTool(self.meas_group)
        return mgchain.get_controllers(*ctrl_type)

    def get_controllers_used(self, ctrl_type):
        if self.mgchain is None:
            return []
        else:
            return self.mgchain.get_controllers_done(ctrl_type)

    def init_scan(self):
        self.mgchain = ChainTool(self.meas_group, self.chain_config)
        self.lima_used = list()
        self.mca_used = list()

    def setup_acq_chain(self, chain):
        mot_master = self.setup_motor_master(chain)
        acq_master = self.setup_acq_master(chain, mot_master)
        self.setup_acq_slaves(chain, acq_master)

    def setup_rotation_preset(self, chain, motors):
        self.home_preset = dict()
        if self.inpars.home_rotation is True:
            for motor in motors:
                if motor in self.rot_motors:
                    self.home_preset[motor] = RotationMotorChainPreset(motor, homing=True, rounding=False)
                    chain.add_preset(self.home_preset[motor])

    def setup_motor_master(self, chain):
        pars = self.inpars

        slow_master = LinearStepTriggerMaster(pars.slow_npoints, pars.slow_motor, pars.slow_start_pos, pars.slow_stop_pos)

        
        motor_pars = { "axis": pars.fast_motor, 
                       "start_list": pars.fall_start_pos.tolist(), 
                       "end_list": pars.fall_stop_pos.tolist(),
                       "time": pars.fast_move_time,
                       "undershoot_start_margin": pars.fast_acc_margin,
                       "undershoot_end_margin": pars.fast_acc_margin,
        }
        if pars.external_motor:
            motor_pars["fast_npoints"] = pars.fast_npoints
            master_class = self._external_motor_list_master_class
        else:
            master_class = MotorListMaster
        
        fast_master = master_class(**motor_pars)
        chain.add(slow_master, fast_master)

        self.setup_rotation_preset(chain, (pars.slow_motor, pars.fast_motor))

        self.slow_master = slow_master
        return fast_master

    def setup_acq_master(self, chain, mot_master):
        pars = self.inpars

        # --- sync prog
        scan_motors = [pars.fast_motor, pars.slow_motor]
        if pars.external_motor:
            # --- sync prog for external motor : ftimescan-like
            if self.sync_dev.is_musst():
                syncprog = MusstProgFTimeScan(self.sync_dev.board)
                syncprog.check_max_timer(pars.scan_time)
                syncprog.set_max_data_rate(self.musst_data_rate_factor * 2./pars.fast_step_time)
            else:
                syncprog = MaestrioProgFTimeScan(self.sync_dev.board)
            syncprog.set_extstart_params(pars.fast_npoints, pars.acq_time, pars.fast_step_time)
            if pars.lima_acc_used:
                syncprog.set_acc_params(pars.lima_acc_nb)
        elif pars.scan_mode == FScanMode.CAMERA:
            # --- sync prog in CAMERA mode
            if self.sync_dev.is_musst():
                syncprog = MusstProgFScanCamera(self.sync_dev.board, scan_motors)
                syncprog.check_max_timer(pars.scan_time)
                syncprog.set_max_data_rate(self.musst_data_rate_factor * 2./pars.fast_step_time)
            else:
                syncprog = MaestrioProgFScanCamera(self.sync_dev.board, scan_motors)
            syncprog.set_params(pars.fall_start_pos[0], pars.fall_scan_dir[0], pars.fast_npoints, pars.camera_signal, pars.gate_low_time)
            if pars.lima_acc_used:
                syncprog.set_acc_params(pars.lima_acc_nb)
        else:
            # --- sync prog for either TIME or POSITION mode
            if self.sync_dev.is_musst():
                syncprog = MusstProgFScan(self.sync_dev.board, scan_motors)
                syncprog.check_max_timer(pars.scan_time)
                syncprog.set_max_data_rate(self.musst_data_rate_factor * 2./pars.fast_step_time)
            else:
                syncprog = MaestrioProgFScan(self.sync_dev.board, scan_motors)

            syncprog.set_modes(pars.scan_mode, pars.gate_mode)
            if pars.scan_mode == FScanMode.TIME:
                delta = pars.fast_step_time
            else:
                delta = pars.fast_step_size
            if pars.gate_mode == FScanTrigMode.TIME:
                gatewidth = pars.acq_time
            else:
                gatewidth = pars.acq_size
            syncprog.set_params(pars.fall_start_pos[0], pars.fall_scan_dir[0], delta, gatewidth, pars.fast_npoints)
            if pars.lima_acc_used:
                syncprog.set_acc_params(pars.lima_acc_nb, pars.lima_acc_period)

        # --- iter position/direction if needed
        if not pars.external_motor:
            if pars.fast_motor_mode == FScanAxisMode.ZIGZAG or not pars.is_fast_regular:
                syncprog.set_iter_params(start=pars.fall_start_pos, direction=pars.fall_scan_dir)

        # --- sync motors on musst
        if pars.sync_encoder:
            syncprog.sync_motors()
        for (motor, preset) in self.home_preset.items():
            if motor in syncprog.motors:
                preset.set_musst_sync_cb(syncprog.sync_one_motor)

        # --- add to chain
        syncprog.setup(chain, mot_master)

        # --- calc device
        synccalc = MusstCalcAcqSlave(syncprog.acq_data_master, syncprog.channels, syncprog.DataPerPoint)
        synccalc.add_default_calc()
        if not pars.external_motor:
            synccalc.add_calc("center", pars.fast_motor_name)

        chain.add(syncprog.acq_master, synccalc)

        # --- add motor external channels
        if not pars.external_motor:
            fast_chan = synccalc.get_calc_channel("center", pars.fast_motor_name)
            mot_master.add_external_channel(synccalc, fast_chan.short_name, f"axis:{pars.fast_motor_name}")
            attach_channels(self.slow_master.channels, fast_chan)
        else:
            fast_chan = mot_master.channels[0]
            attach_channels(self.slow_master.channels, fast_chan)

        self.syncprog = syncprog
        self.synccalc = synccalc

        return syncprog.acq_master

    def setup_acq_slaves(self, chain, acq_master):
        pars = self.inpars

        # --- ct2 counters
        ct2_params={
            "npoints": pars.npoints,
            "acq_mode": CT2AcqMode.ExtGate,
            "prepare_once": True,
            "start_once": True
        }
        self.mgchain.setup("ct2", chain, acq_master, ct2_params)

        # --- lima devices
        if pars.scan_mode == FScanMode.CAMERA:
            lima_params = {
                "acq_mode": pars.lima_acq_mode,
                "acq_trigger_mode": "EXTERNAL_TRIGGER",
                "acq_expo_time": pars.acq_time,
                "acq_nb_frames": pars.fast_npoints,
                "prepare_once": False,
                "start_once": False,
                "scan_frame_per_file": pars.fast_npoints,
            }
        else:
            wait_frame_id = range(pars.fast_npoints-1, pars.npoints, pars.fast_npoints)
            lima_params = {
                "acq_mode": pars.lima_acq_mode,
                "acq_trigger_mode": "EXTERNAL_TRIGGER_MULTI",
                "acq_expo_time": pars.acq_time,
                "acq_nb_frames": pars.npoints,
                "wait_frame_id": wait_frame_id,
                "prepare_once": True,
                "start_once": True,
                "scan_frame_per_file": pars.fast_npoints,
            }
        self.mgchain.setup("lima", chain, acq_master, lima_params)
        self.lima_used = self.mgchain.get_controllers_done("lima")

        # --- lima2 devices
        if pars.scan_mode == FScanMode.CAMERA:
            lima_params = {
                "trigger_mode": "external",
                "expo_time": pars.acq_time,
                "nb_frames": pars.fast_npoints,
                "nb_frames_per_trigger": pars.fast_npoints,
                "wait_frame_id": [ pars.fast_npoints - 1, ],
                "prepare_once": False,
                "start_once": False,
            }
        else:
            wait_frame_id = range(pars.fast_npoints-1, pars.npoints, pars.fast_npoints)
            lima_params = {
                "trigger_mode": "external",
                "expo_time": pars.acq_time,
                "nb_frames": pars.npoints,
                "nb_frames_per_trigger": 1,
                "wait_frame_id": wait_frame_id,
                "prepare_once": True,
                "start_once": True,
            }
        self.mgchain.setup("lima2", chain, acq_master, lima_params)
        self.lima2_used = self.mgchain.get_controllers_done("lima2")

        # --- mca devices
        for mca_ctrl in self.mgchain.get_controllers_not_done("mca"):
            mca_params = get_fscan_mca_params(mca_ctrl, pars.npoints, pars.fast_step_time)
            self.mgchain.setup("mca", chain, acq_master, mca_params, mca_ctrl.name)
            if "trigger_mode" in mca_params:
                # --- in spectrum reading mode
                self.mca_used.append(mca_ctrl)

        # --- mosca devices
        wait_frame_id = range(pars.fast_npoints, pars.npoints+1, pars.fast_npoints)
        mosca_params = {
            "trigger_mode": "GATE",
            "npoints": pars.npoints,
            "preset_time": pars.acq_time,
            "wait_frame_id": wait_frame_id,
            "start_once": True,
        }
        self.mgchain.setup("mosca", chain, acq_master, mosca_params)

        # --- calc
        self.mgchain.setup_calc_counters(chain, acq_master)

        # --- sampling counters
        self.mgchain.setup_sampling(chain, pars.sampling_time)

    def setup_acq_scope(self, chain, mot_master):
        acqtime = max(0.001, self.inpars.fast_step_time / 10.)
        npts = int((self.inpars.fast_scan_time + 0.1) / acqtime + 0.5)

        # --- musst time prog
        musstprog = MusstProgTimeTrigger(self.musst, [self.inpars.fast_motor, self.inpars.slow_motor])
        musstprog.check_max_timer(self.inpars.scan_time)
        musstprog.set_params(npts, acqtime)

        # --- sync motors on musst
        if self.inpars.sync_encoder:
            musstprog.sync_motors()
        for (motor, preset) in self.home_preset.items():
            if motor in musstprog.motors:
                preset.set_musst_sync_cb(musstprog.sync_one_motor)

        # --- acq chain
        musstprog.setup(chain, mot_master, prepare_once=True)
        musstprog.add_default_calc(chain)

    def setup_scan(self, chain, scan_name, user_info=None):
        inpars = self.inpars
        if inpars.display_flag:
            self.show()

        scan_info = FScanInfo(scan_name, "fscan2d", inpars.npoints)
        scan_info.set_fscan_pars(self.pars)
        title = "{slow_motor_name} {slow_start_pos:g} {slow_step_size:g} {slow_npoints:d} " +\
                "{fast_motor_name} {fast_start_pos_txt} {fast_step_size:g} {fast_npoints:d} " +\
                "{acq_time:g} {fast_step_time:g}"
        scan_info.set_fscan_title(title, inpars)
        fast_name = f"axis:{inpars.fast_motor_name}"
        scan_info.set_channel_meta(fast_name,
                                   start=inpars.fast_pos_range[0],
                                   stop=inpars.fast_pos_range[1],
                                   points=inpars.npoints,
                                   axis_id=0,
                                   axis_points=inpars.fast_npoints,
                                  )
        slow_name = f"axis:{inpars.slow_motor_name}"
        scan_info.set_channel_meta(slow_name,
                                   start=inpars.slow_start_pos,
                                   stop=inpars.slow_stop_pos,
                                   points=inpars.npoints,
                                   axis_id=1,
                                   axis_points=inpars.slow_npoints,
                                  )
        if inpars.scatter_plot is True:
            scan_info.add_scatter_plot(x=fast_name, y=slow_name)

        timer_name = self.synccalc.get_channel_name("trig", "timer")
        scan_info.add_curve_plot(x=timer_name)

        scan_info.set_fscan_info(user_info)

        scan = Scan(
            chain,
            name=scan_name,
            save=inpars.save_flag,
            scan_info=scan_info,
            scan_progress=FScanDisplay(timer_name),
        )

        return scan

    def get_runner_class(self):
        return FScan2DCustomRunner


class FScan2DCustomRunner(FScanDiagRunner):
    def __call__(self, slow_motor, slow_start_pos, slow_step_size, slow_npoints,
                       fast_motor, fast_start_pos, fast_step_size, fast_npoints,
                       acq_time, fast_step_time=0., **kwargs):
        pars = dict(slow_motor= slow_motor,
                    slow_start_pos = slow_start_pos,
                    slow_step_size = slow_step_size,
                    slow_npoints = slow_npoints,
                    fast_motor = fast_motor,
                    fast_start_pos = fast_start_pos,
                    fast_step_size = fast_step_size,
                    fast_npoints = fast_npoints,
                    acq_time = acq_time,
                    fast_step_time = fast_step_time,
               )
        pars.update(**kwargs)
        self.run_with_pars(pars)


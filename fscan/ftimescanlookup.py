import numpy
import tabulate

from bliss.scanning.scan import Scan
from bliss.controllers.ct2.device import AcqMode as CT2AcqMode
from bliss.scanning.acquisition.motor import VariableStepTriggerMaster

from fscan.fscantools import (
    FScanParamBase, 
    FTimeScanMode,
    FScanCamSignal,
    FScanParamStruct,
    FScanDisplay,
)
from fscan.synchro.base import SyncDeviceList
from fscan.synchro.maestrio.programs import MaestrioProgFTimeScan
from fscan.synchro.musst.programs import MusstProgFTimeScan
from fscan.musstcalc import MusstCalcAcqSlave

from fscan.mcatools import get_fscan_mca_params
from fscan.chaintools import ChainTool
from fscan.fscanrunner import FScanRunner
from fscan.fscaninfo import FScanInfo
from fscan.limatools import LimaCalib


class MotorLookupPars:
    def __init__(self, motpos=None):
        self.set(motpos)

    def reset(self):
        self._lookup = dict()

    def set(self, motpos=None):
        self.reset()
        if motpos is not None:
            axes = set()
            npts = None
            for (mot, pos) in motpos:
                if mot in axes:
                    raise ValueError(f"Duplicated axis {mot.name} in motor_lookup parameter")
                axes.add(mot)
                if npts is None:
                    npts = len(pos)
                else:
                    if len(pos) != npts:
                        raise ValueError(f"Invalid number of positions for {mot.name}. Should be {npts}.")
            self._lookup = motpos

    @property
    def nmot(self):
        return len(self._lookup)

    @property
    def npos(self):
        if len(self._lookup):
            return len(self._lookup[0][1])
        else:
            return 0

    @property
    def motors(self):
        return [ mot for (mot, _) in self._lookup ]

    def __info__(self):
        if not self.nmot:
            return "No motor lookup defined !!"
        vals = [ [f"- {mot.name} :"]+pos for (mot,pos) in self._lookup ]
        return tabulate.tabulate(vals, tablefmt="plain")

    def __str__(self):
        infos = self.__info__()
        if self.nmot:
            infos = "\n    " + infos.replace("\n", "\n    ")
        return infos

    def as_list(self):
        motor_positions = list()
        for (mot, pos) in self._lookup:
            motor_positions.extend((mot, pos))
        return motor_positions


class FTimeScanLookupPars(FScanParamBase):
    DEFAULT = {
        "acq_time": 0.08,
        "npoints": 10,
        "period": 0.10,
        "motor_lookup": MotorLookupPars(),
        "start_delay": 0.10,
        "latency_time": 0.,
        "sampling_time": 0.5,
        "scan_mode": FTimeScanMode.TIME,
        "camera_signal": FScanCamSignal.EDGE,
        "min_gate_low_time": 0.,
        "save_flag": True,
        "display_flag": True,
    }
    OBJKEYS = list()
    LISTVAL = {
        "scan_mode": FTimeScanMode.values,
        "camera_signal": FScanCamSignal.values,
    }
    NOSETTINGS = ["save_flag", "motor_lookup",]

    def __init__(self, name):
        FScanParamBase.__init__(
            self, 
            name, 
            FTimeScanLookupPars.DEFAULT, 
            FTimeScanLookupPars.OBJKEYS, 
            FTimeScanLookupPars.LISTVAL,
            FTimeScanLookupPars.NOSETTINGS,
        )

    def _validate_npoints(self, value):
        return int(value)

    def _validate_scan_mode(self, value):
        return FTimeScanMode.get(value, "scan_mode")

    def _validate_camera_signal(self, value):
        return FScanCamSignal.get(value, "camera_signal")

    def _validate_save_flag(self, value):
        return value and True or False

    def _validate_display_flag(self, value):
        return value and True or False

    def _validate_motor_lookup(self, value):
        lookup = MotorLookupPars()
        lookup.set(value)
        return lookup
            

class FTimeScanLookupMaster(object):
    def __init__(self, name, config):
        self.name = name
        self.config = config

        sync_list = SyncDeviceList(config["devices"]["synchro"])
        self.sync_dev = sync_list.first()

        self.meas_group = config["devices"]["measgroup"]
        self.chain_config = config["chain_config"]
        self.lima_calib = LimaCalib(config["lima_calib"])
        self.musst_data_rate_factor = config.get("musst_data_rate_factor", 0)
        
        self.pars = FTimeScanLookupPars(self.name)
        self.inpars = None
        self.mgchain = None

    def validate(self):
        self.inpars = FScanParamStruct(self.pars.to_dict())
        pars = self.inpars

        if not pars.motor_lookup.nmot:
            raise ValueError("Need to specify motor_lookup !!")

        limas = self.get_controllers_found("lima", "lima2")
        self.lima_calib.validate(limas, pars, use_acc=True)

        if pars.lima_ndev:
            if pars.period < pars.lima_min_period:
                pars.period = pars.lima_min_period
        else:
            if pars.period < pars.acq_time:
                pars.period = pars.acq_time + pars.latency_time
        pars.dead_time = pars.period - pars.acq_time

        # --- check minimum gate low time
        pars.gate_low_time = 0.
        if pars.scan_mode == FTimeScanMode.CAMERA:
            if pars.camera_signal == FScanCamSignal.FALL:
                pars.gate_low_time = max(pars.min_gate_low_time, pars.dead_time)
            elif pars.camera_signal == FScanCamSignal.RISE:
                pars.gate_low_time = pars.min_gate_low_time
            if pars.gate_low_time >= pars.period:
                raise ValueError("Minimum gate low time >= period is not possible")

        pars.nsteps = pars.motor_lookup.npos
        pars.total_npoints = pars.nsteps * pars.npoints
        pars.total_time = pars.nsteps * (pars.start_delay + pars.npoints*pars.period)

    def show(self):
        if self.inpars is None:
            raise RuntimeError(f"FTimeScanMaster[{self.name}]: Parameters not validated yet !!")


        txt = """
Acquisiton  = {acq_time:g} sec
Period      = {period:g} sec
NPoints     = {npoints:d}

Scan mode       = {scan_mode}
"""
        if self.inpars.lima_ndev:
            txt += "Camera mode     = {lima_acq_mode}\n"
            if self.inpars.scan_mode == FTimeScanMode.CAMERA:
                txt += "Camera signal   = {camera_signal}\n"
        if self.inpars.scan_mode != FTimeScanMode.EXTSTART:
            txt += "Start delay     = {start_delay:.3f} sec\n"

        txt += """
Motor Lookup = {motor_lookup}

Total NPoints = {total_npoints}
Total Time    = {total_time:.3f} sec
"""
        print(txt.format(**self.inpars.to_dict()))

    def init_scan(self):
        self.mgchain = ChainTool(self.meas_group, self.chain_config)
        self.lima_used = list()
        self.mca_used = list()

    def get_controllers_found(self, *ctrl_type):
        mgchain = ChainTool(self.meas_group)
        return mgchain.get_controllers(*ctrl_type)

    def get_controllers_used(self, ctrl_type):
        if self.mgchain is None:
            return []
        else:
            return self.mgchain.get_controllers_done(ctrl_type)

    def setup_acq_chain(self, chain):
        pars = self.inpars

        # --- motor master
        motor_positions = pars.motor_lookup.as_list()
        motor_master = VariableStepTriggerMaster(*motor_positions, broadcast_len=pars.npoints)

        # --- sync prog
        if self.sync_dev.is_musst():
            syncprog = MusstProgFTimeScan(self.sync_dev.board)
            syncprog.check_max_timer(pars.total_time)
            syncprog.set_max_data_rate(self.musst_data_rate_factor * (2./pars.period))
        else:
            syncprog = MaestrioProgFTimeScan(self.sync_dev.board)

        if pars.scan_mode == FTimeScanMode.TIME:
            syncprog.set_time_params(pars.start_delay, pars.npoints, pars.acq_time, pars.period)
        elif pars.scan_mode == FTimeScanMode.CAMERA:
            syncprog.set_camera_params(pars.start_delay, pars.npoints, pars.camera_signal, pars.gate_low_time)
        else: # EXTSTART
            syncprog.set_extstart_params(pars.npoints, pars.acq_time, pars.period)

        if pars.lima_acc_used:
            syncprog.set_acc_params(pars.lima_acc_nb, pars.lima_acc_period)

        syncprog.setup(chain, motor_master)
        acq_master = syncprog.acq_master

        # --- calc device
        synccalc = MusstCalcAcqSlave(syncprog.acq_data_master, syncprog.channels, syncprog.DataPerPoint)
        synccalc.add_default_calc()

        chain.add(acq_master, synccalc)

        if self.sync_dev.is_musst():
            timer_chan = synccalc.get_channel_short_name("trig", "timer")
            epoch_chan = synccalc.get_channel_short_name("epoch_trig", "timer")

            acq_master.add_external_channel(synccalc, timer_chan, "timer:elapsed_time")
            acq_master.add_external_channel(synccalc, epoch_chan, "timer:epoch")

        # --- counters
        ct2pars = {
            "npoints": pars.total_npoints,
            "acq_expo_time": pars.acq_time,
            "acq_mode": CT2AcqMode.ExtGate,
        }
        self.mgchain.setup("ct2", chain, acq_master, ct2pars)

        # --- lima devs
        if pars.scan_mode == FTimeScanMode.CAMERA:
            limapars = {
                "acq_mode": pars.lima_acq_mode,
                "acq_trigger_mode": "EXTERNAL_TRIGGER",
                "acq_expo_time": pars.acq_time,
                "acq_nb_frames": pars.npoints,
                "prepare_once": False,
                "start_once": False,
            }
            if pars.period > pars.lima_min_period:
                lat_time = pars.period - pars.acq_time
                limapars["latency_time"] = lat_time
        else:
            wait_frame_id = range(pars.npoints-1, pars.total_npoints, pars.npoints)
            limapars = {
                "acq_mode": pars.lima_acq_mode,
                "acq_trigger_mode": "EXTERNAL_TRIGGER_MULTI",
                "acq_expo_time": pars.acq_time,
                "acq_nb_frames": pars.total_npoints,
                "wait_frame_id": wait_frame_id,
                "prepare_once": True,
                "start_once": True,
            }
        self.mgchain.setup("lima", chain, acq_master, limapars)
        self.lima_used = self.mgchain.get_controllers_done("lima")

        # --- lima2 devs
        if pars.scan_mode == FTimeScanMode.CAMERA:
            limapars = {
                "trigger_mode": "external",
                "expo_time": pars.acq_time,
                "nb_frames": pars.npoints,
                "nb_frames_per_trigger": pars.npoints,
                "wait_frame_id": [ pars.npoints - 1, ],
                "prepare_once": False,
                "start_once": False,
            }
        else:
            wait_frame_id = range(pars.npoints-1, pars.total_npoints, pars.npoints)
            limapars = {
                "trigger_mode": "external",
                "expo_time": pars.acq_time,
                "nb_frames": pars.total_npoints,
                "nb_frames_per_trigger": 1,
                "wait_frame_id": wait_frame_id,
                "prepare_once": True,
                "start_once": True,
            }
        self.mgchain.setup("lima2", chain, acq_master, limapars)

        # --- mca devices
        for mca_ctrl in self.mgchain.get_controllers_not_done("mca"):
            mca_params = get_fscan_mca_params(mca_ctrl, pars.total_npoints, pars.period)
            self.mgchain.setup("mca", chain, acq_master, mca_params, mca_ctrl.name)
            if "trigger_mode" in mca_params:
                # --- in spectrum reading mode
                self.mca_used.append(mca_ctrl)

        # --- mosca devices
        wait_frame_id = range(pars.npoints, pars.total_npoints+1, pars.npoints)
        mosca_params = {
            "trigger_mode": "GATE",
            "npoints": pars.total_npoints,
            "preset_time": pars.acq_time,
            "wait_frame_id": wait_frame_id,
            "start_once": True,
        }
        self.mgchain.setup("mosca", chain, acq_master, mosca_params)

        # --- calc
        self.mgchain.setup_calc_counters(chain, acq_master)

        # --- sampling
        self.mgchain.setup_sampling(chain, pars.sampling_time)

        self.syncprog = syncprog
        self.synccalc = synccalc

    def setup_scan(self, chain, scan_name, user_info=None):
        pars = self.inpars

        if pars.display_flag:
            self.show()

        scan_info = FScanInfo(scan_name, "ftimescanlookup", self.inpars.npoints)
        scan_info.set_fscan_title("{acq_time:g} {npoints:d} {period:g}", self.pars)
        timer_name = self.synccalc.get_channel_name("trig", "timer")
        scan_info.add_curve_plot(x=timer_name)
        scan_info.set_fscan_pars(self.pars)
        scan_info.set_fscan_info(user_info)

        # motors = pars.motor_lookup.motors
        # channames = [ f"axis:{mot.name}" for mot in motors ]
        scan = Scan(
            chain,
            name=scan_name,
            save=pars.save_flag,
            scan_info=scan_info,
            scan_progress=FScanDisplay(timer_name),
        )

        return scan

    def get_runner_class(self):
        return FTimeScanLookupCustomRunner

class FTimeScanLookupCustomRunner(FScanRunner):
    def __call__(self, motor_lookup, acq_time, npoints, period=0, **kwargs):
        pars = dict(motor_lookup = motor_lookup,
                    acq_time = acq_time,
                    npoints = npoints,
                    period = period,
               )
        pars.update(kwargs)
        self.run_with_pars(pars)


import numpy

from bliss.scanning.acquisition.calc import CalcHook
from bliss.scanning.chain import AcquisitionChannel
from fscan.synchro.musst.legacy.base import MusstChanTrigCalc

class MusstLinearTrajCalcFromTime(MusstChanTrigCalc):
    def __init__(self, linear_traj, axis, master, time_chan, data_per_point=1, delay=0.05):
        self.traj = linear_traj
        self.unit = axis.unit
        self.axis_name = axis.name
        self.delay = delay
        super().__init__(master, time_chan, data_per_point)

    @property
    def dest_name(self):
        return f"{self.name}:{self.axis_name}_trig"

    def calculate(self, data):
        conv_data = super().calculate(data)
        if conv_data is not None:
            conv_data -= self.delay
            calc_data = numpy.fromiter(map(self.traj.position, conv_data), dtype=numpy.float64)
            return calc_data
        else:
            return None

class MusstTrajCalcFromOneAxis(MusstChanTrigCalc):
    def __init__(self, traj, axis, master, channel, data_per_point=1):
        self.traj = traj
        self.axis = axis
        super().__init__(master, channel, data_per_point)
        self.unit = self.traj.calc_axis.unit

    @property
    def dest_name(self):
        return f"{self.name}:{self.traj.calc_axis.name}_trig"

    def calculate(self, data):
        conv_data = super().calculate(data)
        if conv_data is not None:
            calc_data = self.traj.calc_pos_from_axis(self.axis, conv_data)
            return calc_data
        else:
            return None

class MusstTrajCalcFromTime(MusstChanTrigCalc):
    def __init__(self, traj, master, channel, data_per_point=1, delay=0.05):
        self.traj = traj
        self.delay = delay
        super().__init__(master, channel, data_per_point)
        self.unit = self.traj.calc_axis.unit

    @property
    def dest_name(self):
        return f"{self.name}:{self.traj.calc_axis.name}_trig"

    def calculate(self, data):
        conv_data = super().calculate(data)
        if conv_data is not None:
            conv_data -= self.delay
            calc_data = self.traj.calc_pos_from_time(conv_data)
            return calc_data
        else:
            return None

class MusstTrajCalcFromAllAxis(CalcHook):
    def __init__(self, calc_axis, axes, musst_prog, data_per_point=1):
        self.calc_axis = calc_axis
        self.axes = axes
        self.master = musst_prog.musst_device
        self.name = self.master.name
        self.unit = self.traj.calc_axis.unit

        self.calc_tag = calc_axis.controller._axis_tag(calc_axis)
        self.trig_calc = dict()
        for axis in self.axes:
            axis_tag = self.calc_axis.controller._axis_tag(axis)
            chan = musst_prog.get_channel(axis)
            self.trig_calc[axis_tag] = MusstChanTrigCalc(self.master, chan, data_per_point)

    @property
    def dest_name(self):
        return f"{self.name}:{self.calc_axis.name}_trig"

    @property
    def acquisition_channels(self):
        return [AcquisitionChannel(self.dest_name, numpy.float64, (), unit=self.unit)]

    def prepare(self):
        self.__data = dict()
        for (tagname, calc) in self.trig_calc.items():
            calc.prepare()
            self.__data[tagname] = numpy.array((), numpy.float64)

    def compute(self, sender, data_dict):
        for (name, calc) in self.trig_calc.items():
            trigcomp = calc.compute(sender, data_dict)
            trigdata = trigcomp.get(calc.dest_name, None)
            if trigdata is not None:
                numpy.append(self.__data[name], trigdata)

        size = min([ len(data) for data in self.__data.values() ])
        if size < 1:
            return dict()

        real_data = dict()
        for (name, data) in self.__data.items():
            real_data[name] = data[:size]

        calc_data = self.calc_axis.controller.calc_from_real(real_data)
        calc_axis_data = calc_data[self.calc_tag]

        for name, data in self.__data.items():
            self.__data[name] = data[size:]

        return { self.dest_name: calc_axis_data }

class CalcTrajTool(object):
    def __init__(self, calc_axis, traj_group):
        self.calc_axis = calc_axis
    
        self.tag_axes = dict()
        for axis in traj_group.axes:
            self.tag_axes[axis] = calc_axis.controller._axis_tag(axis)
        self.tag_pseudo = calc_axis.controller._axis_tag(calc_axis)

        self.axis_time = traj_group.trajectories[0].pvt["time"]

        self.traj_pos = dict()
        for axis, traj in zip(traj_group.axes, traj_group.trajectories):
            self.traj_pos[axis] = traj.pvt["position"]

    def axis_ordered_list(self):
        mean_diff = dict()
        for axis, traj in self.traj_pos.items():
            if axis.encoder:
                steps = axis.encoder.steps_per_unit
            else:
                steps = axis.steps_per_unit
            pos_diff = numpy.diff(traj[1:-1]).mean()
            mean_diff[axis] = abs(pos_diff * steps)

        axis_list = [ ax for (ax, res) in 
                      sorted(mean_diff.items(), key=lambda item: item[1]) ]
        axis_list.reverse()
        return axis_list

    def get_trigger_position(self, axis):
        return self.traj_pos[axis][1]

    def get_trigger_direction(self, axis):
        if self.traj_pos[axis][1] >= self.traj_pos[axis][0]:
            direction = 1
        else:
            direction = -1
        return direction

    def total_time(self):
        return self.axis_time[-1]
      
    def calc_pos_from_time(self, time_arr):
        real_pos = dict()
        for axis, pos in self.traj_pos.items():
            real_pos[self.tag_axes[axis]] = numpy.interp(time_arr, self.axis_time, pos)

        calc_pos = self.calc_axis.controller.calc_from_real(real_pos)
        return calc_pos[self.tag_pseudo]

    def calc_pos_from_axis(self, axis, pos_arr):
        real_pos = dict()
        real_pos[self.tag_axes[axis]] = pos_arr

        x_pos = self.traj_pos[axis]
        for y_axis, y_pos in self.traj_pos.items():
            if y_axis != axis:
                real_pos[self.tag_axes[y_axis]] = numpy.interp(pos_arr, x_pos, y_pos)

        calc_pos = self.calc_axis.controller.calc_from_real(real_pos)
        return calc_pos[self.tag_pseudo]


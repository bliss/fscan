
from ruamel.yaml import YAML
from bliss import setup_globals

def fscan_save_state(master, filename, save_counters=True, save_limas=True):
    state = dict()
    state["master"] = master.name
    state["parameters"] = master.pars.to_dict(string_format=True)

    if save_counters:
        state["counters"] = master.meas_group.enabled

    if save_limas:
        limas = master.get_controllers_found("lima")
        if limas:
            lima_state = state.setdefault("lima_detectors", {})
            for lima in limas:
                lima_pars = lima_state.setdefault(lima.name, {})
                lima_pars.update(lima_get_state(lima))

    with open(filename, "w") as ymlfile:
        yaml = YAML()
        yaml.dump(state, ymlfile)

def fscan_load_state(master, filename, load_counters=True, load_limas=True):
    with open(filename) as ymlfile:
        yaml = YAML()
        state = yaml.load(ymlfile)

    name = state.get("master")
    if master.name != name:
        raise RuntimeError(f"Loading state of [{name}] onto [{master.name}] not allowed")

    pars = state.get("parameters", dict())
    master.pars.from_dict(pars, string_format=True)

    limas = state.get("lima_detectors")
    if load_limas and limas:
        for limaname in limas:
            try:
                limaobj = getattr(setup_globals, limaname)
            except AttributeError:
                print("Lima [{limaname}] not in session : cannot restore its state !!")
                continue
            lima_set_state(limaobj, limas[limaname])

    counters = state.get("counters")
    if load_counters and counters:
        measgroup = master.meas_group
        measgroup.disable_all()
        measgroup.enable(*counters)

def lima_get_state(lima):
    state = dict()
    state["image"] = lima.image.to_dict()
    state["processing"] = lima.processing.to_dict()
    rois = lima.roi_counters.get_rois()
    if rois:
        rois_dict = state.setdefault("roi_counters", {})
        for roi in rois:
            rois_dict[roi.name] = [ roi.x, roi.y, roi.width, roi.height ]
    return state

def lima_set_state(lima, state):
    img_pars = state.get("image")
    if img_pars:
        for (name, value) in img_pars.items():
            setattr(lima.image, name.lstrip("image_"), value)
    proc_pars = state.get("processing")
    if proc_pars:
        for (name, value) in proc_pars.items():
            setattr(lima.processing, name, value)
    rois_pars = state.get("roi_counters")
    if rois_pars:
        rois_ctrl = lima.roi_counters
        rois_ctrl.clear()
        for (name, values) in rois_pars.items():
            rois_ctrl.set(name, values)

def lima_save_state(lima, filename):
    state = lima_get_state(lima)
    with open(filename, "w") as ymlfile:
        yaml = YAML()
        yaml.dump(state, ymlfile)

def lima_load_state(lima, filename):
    with open(filename) as ymlfile:
        yaml = YAML()
        state = yaml.load(ymlfile)
    lima_set_state(lima, state)


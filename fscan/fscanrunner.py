from bliss.common.utils import autocomplete_property
from bliss.scanning.scan import ScanPreset
from bliss.scanning.chain import AcquisitionChain, ChainPreset

from fscan.fscanstate import fscan_save_state, fscan_load_state

import tabulate

class FScanRunner(object):
    def __init__(self, scanname, scanmaster, *args):
        self._master = scanmaster
        self._scan_name = scanname or self._master.scan_name
        self._chain = None
        self._scan = None
        self._scan_presets = dict()
        self._chain_presets = dict()
        self._use_presets = True

    def __setattr__(self, name, value):
        if not name.startswith("_"):
            msg = "Setting [{0}] on [{1}] not allowed.".format(name, self._scan_name)
            if name in self._master.pars.to_dict().keys():
                msg += "\nLooking for [{0}.pars.{1}] maybe ??".format(self._scan_name, name)
            raise RuntimeError(msg)
        else:
            object.__setattr__(self, name, value)

    @autocomplete_property
    def pars(self):
        return self._master.pars

    @autocomplete_property
    def inpars(self):
        return self._master.inpars

    def validate(self):
        self.local_validate()
        self._master.validate()

    # --- BL specific validate
    def local_validate(self):
        pass

    # --- BL default pars
    def get_local_pars(self):
        return {}

    def __info__(self):
        info = f"Scan Type : {self._scan_name}\n"
        info += self.pars.__info__()
        return info

    def show(self):
        self.validate()
        self._master.show()

    def prepare(self, scan_info=None):
        self.validate()
        self._chain = AcquisitionChain(parallel_prepare=True)
        self._master.init_scan()
        self._master.setup_acq_chain(self._chain)
        self._scan = self._master.setup_scan(self._chain, self._scan_name, scan_info)
        self._setup_presets()

    def start(self):
        self._scan.run()

    def run(self, scan_info=None):
        self.prepare(scan_info)
        self.start()

    def run_with_pars(self, user_pars):
        scan_info = user_pars.pop("scan_info", None)
        pars = self.get_local_pars()
        pars.update(user_pars)
        self.pars.set(**pars)
        self.run(scan_info)

    def _setup_presets(self):
        if self._use_presets:
            self._prepare_presets()

            # --- chain presets
            for preset in self._chain_presets.values():
                self._chain.add_preset(preset)

            # --- scan presets
            for preset in self._scan_presets.values():
                self._scan.add_preset(preset)

    def _prepare_presets(self):
        # --- add fscan master reference to presets
        for preset in self._chain_presets.values():
            set_master_func = getattr(preset, "set_fscan_master", None)
            if set_master_func is not None:
                set_master_func(self._master)

        for preset in self._scan_presets.values():
            set_master_func = getattr(preset, "set_fscan_master", None)
            if set_master_func is not None:
                set_master_func(self._master)

    def add_scan_preset(self, preset, name=None):
        if not isinstance(preset, ScanPreset):
            raise ValueError("preset should be an instance of ScanPreset !!")
        preset_name = id(preset) if name is None else name
        self._scan_presets[preset_name] = preset

    def get_scan_preset(self, name):
        return self._scan_presets.get(name, None)

    def remove_scan_preset(self, name):
        if name in self._scan_presets:
            self._scan_presets.pop(name)

    def add_chain_preset(self, preset, name=None):
        if not isinstance(preset, ChainPreset):
            raise ValueError("preset should be an instance of ChainPreset !!")
        preset_name = id(preset) if name is None else name
        self._chain_presets[preset_name] = preset

    def get_chain_preset(self, name):
        return self._chain_presets.get(name, None)

    def remove_chain_preset(self, name):
        if name in self._chain_presets:
            self._chain_presets.pop(name)

    def disable_presets(self):
        self._use_presets = False

    def enable_presets(self):
        self._use_presets = True

    @autocomplete_property
    def master(self):
        return self._master

    @autocomplete_property
    def scan(self):
        if self._scan is None:
            raise RuntimeError("No scan performed yet !!")
        return self._scan

    @autocomplete_property
    def chain(self):
        if self._chain is None:
            raise RuntimeError("No chain setup yet !!")
        return self._chain

    @property
    def streams(self):
        return self.scan.streams

    def chain_show(self):
        self.chain._tree.show()

    def get_data(self, dataname=None):
        if dataname is None:
            data = dict()
            for name in self.streams.keys():
                stream_data = self.streams.get(name)
                if stream_data.info["dim"] == 0:
                    data[name] = stream_data[:]
            return data
        else:
            stream = self.streams.get(dataname)
            if stream is None:
                raise ValueError(f"Invalid dataname !! use {self._scan_name}.list_data() to get a list of valid data.")
            return stream[:]

    def list_data(self):
        heads = ["Data names", "NbEmitted"]
        infos = list()
        for name, stream in self.streams.items():
            infos.append([name, len(stream)])
        print("\n"+tabulate.tabulate(infos, headers=heads)+"\n")
           
    def save_state(self, filename, save_counters=True, save_limas=True):
        fscan_save_state(self._master, filename, save_counters, save_limas)

    def load_state(self, filename, load_counters=True, load_limas=True):
        fscan_load_state(self._master, filename)

class FScanDiagRunner(FScanRunner):
    def prepare(self, scan_info=None):
        scan_name = self._scan_name
        self.validate()
        self._master.init_scan()
        self._chain = AcquisitionChain(parallel_prepare=True)
        mot_master = self._master.setup_motor_master(self._chain)
        acq_master = self._master.setup_acq_master(self._chain, mot_master)
        self._master.setup_acq_slaves(self._chain, acq_master)
        self._scan = self._master.setup_scan(self._chain, scan_name, scan_info)
        self._setup_presets()

    def run(self, scan_info=None):
        self.prepare(scan_info)
        self.start()

    def prepare_dry(self):
        scan_name = "{0}.dry_run".format(self._scan_name)
        self.validate()
        self._chain = AcquisitionChain(parallel_prepare=True)
        self._master.init_scan()
        mot_master = self._master.setup_motor_master(self._chain)
        self._master.setup_acq_master(self._chain, mot_master)
        self._scan = self._master.setup_scan(self._chain, scan_name)
        self._setup_presets()

    def dry_run(self):
        self.prepare_dry()
        self.start()

    def prepare_scope(self):
        scan_name = "{0}.scope_run".format(self._scan_name)
        self.validate()
        self._chain = AcquisitionChain(parallel_prepare=True)
        self._master.init_scan()
        mot_master = self._master.setup_motor_master(self._chain)
        self._master.setup_acq_scope(self._chain, mot_master)
        self._scan = self._master.setup_scan(self._chain, scan_name)
        self._setup_presets()

    def scope_run(self):
        self.prepare_scope()
        self.start()


class FScanNoPreset:
    def __init__(self, scanrunner):
        self._runner = scanrunner

    def __enter__(self):
        self._runner.disable_presets()

    def __exit__(self, *args):
        self._runner.enable_presets()

class FScanExecPresetOnce:
    def __init__(self, scanrunner):
        self._runner = scanrunner

    def __enter__(self):
        # --- add fscan master to presets
        self._runner._prepare_presets()
     
        # --- disable presets and prepare scan
        self._runner.disable_presets()
        self._runner.prepare()

        # --- call prepare/start on all presets
        for preset in self._runner._scan_presets.values():
            preset.prepare(self._runner._scan)
        for preset in self._runner._chain_presets.values():
            preset.prepare(self._runner._chain)
        for preset in self._runner._scan_presets.values():
            preset.start(self._runner._scan)
        for preset in self._runner._chain_presets.values():
            preset.start(self._runner._chain)

    def __exit__(self, *args):
        # --- call stop on all presets
        for preset in self._runner._chain_presets.values():
            preset.stop(self._runner._scan)
        for preset in self._runner._scan_presets.values():
            preset.stop(self._runner._scan)

        # --- enable presets
        self._runner.enable_presets()

from bliss.scanning.acquisition.maestrio import MaestrioAcquisitionMaster


def _get_list(value):
    try:
        iter(value)
        return list(value)
    except TypeError:
        return [value,]


class MaestrioBaseChannel(object):
    def __init__(self, name, chanid, unit=None):
        self.name = name
        self.channel_id = chanid
        self.unit = unit

    @property
    def store_name(self):
        return f"{self.name}_raw"

    def raw2unit(self, data):
        raise NotImplementedError

    def unit2raw(self, data):
        raise NotImplementedError

    def delta2raw(self, delta):
        return self.unit2raw(delta)

    def delta2unit(self, delta):
        return self.raw2unit(delta)


class MaestrioTimerChannel(MaestrioBaseChannel):
    def __init__(self, clock_factor):
        super().__init__("timer", 0, "s")
        self.set_clock(clock_factor)

    def set_clock(self, clock_factor):
        self.resolution = clock_factor

    def raw2unit(self, data):
        return data / self.resolution

    def unit2raw(self, data):
        return int(data * self.resolution)


class MaestrioCounterChannel(MaestrioBaseChannel):
    def __init__(self, cnt, channel):
        super().__init__(cnt.name, channel.channel_id, cnt.unit)

        self.channel = None
        self.resolution = 1.

    def init(self):
        if self.channel is not None:
            self.channel.stop()
            self.channel.value = 0
            self.channel.run()

    def raw2unit(self, data):
        return data / self.resolution

    def unit2raw(self, data):
        return data * self.resolution


class MaestrioMotorChannel(MaestrioBaseChannel):
    def __init__(self, motor, channel):
        super().__init__(motor.name, channel.channel_id)
        self.resolution = None
        self.offset = 0
        self.sign = 1.
        self.modulo = None
        self.modulo_offset = 0

        conv = channel.encoder_conversion_param
        if conv is not None:
            # assume an external absolute encoder
            self.resolution = conv.get("steps_per_unit", None)
            self.offset = conv.get("offset", 0.)
            self.offset += motor.offset
            self.sign = conv.get("sign", 1.)
            self.unit = conv.get("unit", motor.unit)
            modcfg = _get_list(conv.get("modulo", list()))
            if not len(modcfg):
                self.modulo = None
            elif len(modcfg)==1:
                self.modulo = [0, modcfg[0]]
            elif len(modcfg)==2:
                self.modulo = modcfg
            else:
                raise ValueError(f"Wrong modulo definition on maestrio channel for {motor.name}")
        else:
            # assume a relative encoder : no offset, work in user pos
            self.resolution = None
            self.offset = 0.
            self.sign = 1.
            self.unit = motor.unit

        if self.resolution is None:
            if motor.encoder:
                self.resolution = motor.encoder.steps_per_unit
            else:
                self.resolution = motor.steps_per_unit
            self.sign = float(motor.sign)

        if self.resolution < 0.:
            self.resolution = abs(self.resolution)
            self.sign *= -1.

    def _calc_modulo(self, data):
        (low, high) = self.modulo
        return (data-low)%(high-low)+low

    def _calc_modulo_offset(self, data):
        (low, high) = self.modulo
        return (data-low)//(high-low)*(high-low)

    def set_pos_reference(self, data):
        if data is None or self.modulo is None:
            self.modulo_offset = 0.
        else:
            self.modulo_offset = self._calc_modulo_offset(data - self.offset)

    def raw2unit(self, data):
        return self.sign * data / self.resolution + self.offset + self.modulo_offset

    def unit2raw(self, data):
        pos = data - self.offset
        if self.modulo is not None:
            pos = self._calc_modulo(pos)
        return self.sign * pos * self.resolution

    def delta2raw(self, data):
        return self.sign * data * self.resolution

    def delta2unit(self, data):
        return self.sign * data / self.resolution


class MaestrioProgBase(object):
    ProgramData = None
    ProgramName = None
    ProgramAbort = None
    DataPerPoint = 1
    NbMotorUsed = 0

    TIMER_CLOCK = 1e6

    def __init__(self, maestrio, motors=None, counters=None):
        self.maestrio = maestrio

        self.params = dict()
        self.iter_params = list()
        self.macro_defs = dict()

        self.channels = dict()
        self.channels["timer"] = MaestrioTimerChannel(self.TIMER_CLOCK)

        if motors is None:
            self.motors = list()
        else:
            self.set_motors(*motors)
        if counters is None:
            self.set_counters(*maestrio.counters)
        else:
            self.set_counters(*counters)

    def set_motors(self, *motors):
        use_chans = list()
        self.motors = list()
        names = list()
        for motor in motors:
            name = getattr(motor, "original_name", motor.name)
            try:
                chan = self.maestrio.get_channel_by_name(name)
            except Exception:
                continue
            chan_id = chan.channel_id
            if chan_id not in use_chans:
                use_chans.append(chan_id)
                self.channels[motor.name] = MaestrioMotorChannel(motor, chan)
                self.motors.append(motor)
                names.append(name)
        # ensure switches are set
        self.maestrio.get_channel_by_names(*names)

    def set_counters(self, *counters):
        # use_chans = [ chan.channel_id for chan in self.channels.values() ]
        # self.counters = list()
        # for counter in counters:
        #     name = getattr(counter, "original_name", counter.name)
        #     try:
        #         chan = self.maestrio.get_channel_by_name(name)
        #     except:
        #         continue
        #     chan_id = chan.channel_id
        #     if chan_id not in use_chans:
        #         use_chans.append(chan_id)
        #         self.channels[counter.name] = MaestrioCounterChannel(counter, chan)
        #         self.counters.append(counter.name)
        pass

    def get_channel(self, mot_or_cnt):
        try:
            return self.channels[mot_or_cnt.name]
        except KeyError:
            raise ValueError(f"No channel found for {mot_or_cnt:r}")

    def get_timer_channel(self):
        return self.channels["timer"]

    def get_motor_channel(self, motidx=0):
        if motidx >= len(self.motors):
            raise ValueError("Motor index does not exist")
        return self.channels.get(self.motors[motidx].name)

    def channel_list(self):
        return list(self.channels.keys())

    def sync_motors(self, *motors):
        if not len(motors):
            motors = self.motors
        for motor in motors:
            self.sync_one_motor(motor)

    def sync_one_motor(self, motor):
        mot_chan = self.channels[motor.name]
        enc_chan = self.maestrio.get_channel(mot_chan.channel_id)

        motor.wait_move()
        motor_pos = motor.position

        enc_chan.sync(int(mot_chan.unit2raw(motor_pos) + 0.5))

    def set_params(self, param_dict):
        self.params = param_dict

    def set_iter_params(self, param_list):
        self.iter_params = param_list

    def set_macro_defs(self, macro_defs):
        self.macro_defs = macro_defs

    # musst compatibility
    def set_max_data_rate(self, data_rate):
        pass

    # musst compatibility
    def check_max_timer(self, scan_time):
        pass

    def setup(self, chain, master=None, **keys):
        # --- create store list and template
        self.__setup_macdef_and_store_list()

        # --- get parameters list or dict
        pars = self.__get_params()

        # --- musst acquisition master
        self.__prepare_once = keys.pop("prepare_once", False)
        self.__start_once = keys.pop("start_once", False)
        # last_iter_index = keys.pop("last_iter_index", 1)

        self._acq_master = MaestrioAcquisitionMaster(
            self.maestrio,
            program_data=self.ProgramData,
            program_name=self.ProgramName,
            program_abort_name=self.ProgramAbort,
            variables=pars,
            macrodefs=self.macro_defs,
            channel_names=self.chan_names,
            prepare_once=self.__prepare_once,
            start_once=self.__start_once,
            **keys
        )

        # --- add to acq. chain
        if master is not None:
            chain.add(master, self._acq_master)
        else:
            chain.add(self._acq_master)

    @property
    def acq_master(self):
        return self._acq_master

    @property
    def acq_data_master(self):
        return self._acq_master

#    def add_user_calc(self, chain, calc_objs, scan_pars):
#        raw_calc = [ calc for calc in calc_objs if isinstance(calc, MusstChanUserRawCalc) ]
#        for calc in raw_calc:
#            channel = calc.select_channel(self.channels, scan_pars)
#            if channel is not None and isinstance(channel, MaestrioBaseChannel):
#                calc.init(self.musst_device, channel, self.DataPerPoint)
#                self.add_calc_dev(chain, calc)

#        user_calc = [ calc for calc in calc_objs if isinstance(calc, MusstChanUserCalc) ]
#        all_chans = list(self.calc_devices.keys())
#        for calc in user_calc:
#            input_channels = calc.init(self.musst_device, all_chans, scan_pars)
#            if len(input_channels):
#                input_devs = [ self.calc_devices[name] for name in input_channels ]
#                calc_dev = CalcChannelAcquisitionSlave(
#                    calc.name,
#                    input_devs,
#                    calc,
#                    calc.acquisition_channels,
#                    prepare_once=self.__prepare_once,
#                    start_once=self.__start_once,
#                )
#                chain.add(self.musst_master, calc_dev)

    def __setup_macdef_and_store_list(self):
        data_store = list()
        macro_defs = dict()
        chan_names = [self.channels["timer"].store_name,]

        motidx = 0
        for motor in self.motors:
            chan = self.channels.get(motor.name)
            if motidx < self.NbMotorUsed:
                motidx += 1
                macro_defs[f"CH_MOT{motidx:d}"] = f"CH{chan.channel_id:d}"
            data_store.append(f"CH{chan.channel_id:d}")
            chan_names.append(chan.store_name)

#        cntidx = 0
#        for counter in self.counters:
#            chan = self.channels.get(counter)
#            cntidx += 1
#            data_alias += "ALIAS DATA{0:d} = CH{1:d}\n".format(cntidx, chan.channel_id)
#            data_store += "DATA{0:d} ".format(cntidx)
#            chan.init()

        macro_defs["DATA_STORE"] = " ".join(data_store)
        self.macro_defs.update(macro_defs)
        self.chan_names = chan_names

    def __get_params(self):
        if not len(self.iter_params):
            return self.params
        else:
            par0 = dict(self.params)
            par0.update(self.iter_params[0])
            parlist = [par0,] + self.iter_params[1:]
            return parlist

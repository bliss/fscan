
from .base import MaestrioProgBase
from .mprg.ftimescan_mprg import ftimescan_mprg_data
from fscan.fscantools import FScanCamSignal


class MaestrioProgFTimeScan(MaestrioProgBase):
    ProgramData = ftimescan_mprg_data
    ProgramName = "FTIMESCAN"
    ProgramAbort = None
    DataPerPoint = 2
    NbMotorUsed = 0

    def set_time_params(self, start, npoints, acqtime, period):
        clock_chan = self.get_timer_channel()
        self.params = dict()
        self.params["STARTTIME"] = clock_chan.unit2raw(start)
        self.params["NPTS"] = int(npoints)
        self.params["ACQTIME"] = clock_chan.unit2raw(acqtime)
        self.params["PERIOD"] = clock_chan.unit2raw(period)
        self.params["MODE"] = 0
        self.params["ACCNB"] = 1
        self.params["ACCTIME"] = 0

    def set_camera_params(self, start, npoints, signal_type, gatelowtime=0.):
        clock_chan = self.get_timer_channel()
        self.params = dict()
        self.params["STARTTIME"] = clock_chan.unit2raw(start)
        self.params["NPTS"] = int(npoints)
        self.params["MODE"] = 1
        self.params["SIGNAL"] = FScanCamSignal.index(signal_type)
        self.params["ACCNB"] = 1
        self.params["ACCTIME"] = 0
        self.params["GATE_LOWTIME"] = max(1, clock_chan.unit2raw(gatelowtime))
        if signal_type in [FScanCamSignal.FALL, FScanCamSignal.RISE]:
            self.DataPerPoint = 1
        else:
            self.DataPerPoint = 2

    def set_extstart_params(self, npoints, acqtime, period):
        clock_chan = self.get_timer_channel()
        self.params = dict()
        self.params["STARTTIME"] = 0
        self.params["NPTS"] = int(npoints)
        self.params["ACQTIME"] = clock_chan.unit2raw(acqtime)
        self.params["PERIOD"] = clock_chan.unit2raw(period)
        self.params["MODE"] = 2
        self.params["ACCNB"] = 1
        self.params["ACCTIME"] = 0

    def set_acc_params(self, accnb, accperiod=None):
        self.params["ACCNB"] = int(accnb)
        if accperiod is not None:
            clock_chan = self.get_timer_channel()
            self.params["ACCTIME"] = clock_chan.unit2raw(accperiod)


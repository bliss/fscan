
from .base import MaestrioProgBase
from .mprg.timetrig_mprg import timetrig_mprg_data


class MaestrioProgTimeTrigger(MaestrioProgBase):
    ProgramData = timetrig_mprg_data
    ProgramName = "TIMETRIG"
    ProgramAbort = None
    DataPerPoint = 1
    NbMotorUsed = 0

    def set_params(self, npts, period):
        self.params = dict()

        clock_chan = self.get_timer_channel()
        self.params["PERIOD"] = clock_chan.unit2raw(abs(period))
        self.params["NPTS"] = int(npts)

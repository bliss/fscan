
from .base import MaestrioProgBase
from .mprg.itrigrecord_mprg import itrigrecord_mprg_data


class MaestrioProgITrigRecord(MaestrioProgBase):
    ProgramData = itrigrecord_mprg_data
    ProgramName = "ITRIGRECORD"
    ProgramAbort = None
    DataPerPoint = 2
    NbMotorUsed = 0

    def set_params(self, npts, mode=0, record=0):
        self.params = dict(NPTS=int(npts), MODE=mode, RECORD=record)
        if record == 0 or record == 3:
            self.DataPerPoint = 2
        else:
            self.DataPerPoint = 1

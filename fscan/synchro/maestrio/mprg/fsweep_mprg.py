"""
// ****************************************************************
// FSWEEP :
// ****************************************************************
// Description:
//   The program generates NPULSES gates on OutB starting from TRIGSTART.
//   Gate width is given by TRIGDELTA.
//   Second gate is generated exactly at end position of previous one.
//   Data are stored at rising and falling edge of each gate.
//   Scanning can be done in both direction. Direction is given by
//   sign of TRIGDELTA
// ****************************************************************
//
// OutA     |
//          |_________
// OutB     |        |
//          |        |
// Pos  --------------------------------------->
//          ^
//       TRIGSTART
//
// OutA              |
//                   |_________
// OutB              |        |
//                   |        |
// Pos  --------------------------------------->
//                   |<------>|
//                    TRIGDELTA
//
//      ... repeated NPULSES times ...
//
// Buffer   *        *
//                   *        *
//                            *       * ...
// ****************************************************************
"""

fsweep_mprg_data = """
// --- INPUT PARAMETERS
MACRODEF OUT_ATRIG  := OUT7
MACRODEF OUT_BTRIG  := OUT8
MACRODEF OUT_CTRIG  := OUT6
MACRODEF CH_MOT1    := CH6
MACRODEF DATA_STORE := CH_MOT1 CH2

// --- INPUT VARIABLES
SIGNED   TRIGSTART   = 0
SIGNED   TRIGDELTA   = 10
UNSIGNED NPULSES     = 2
UNSIGNED POSERR      = 0
UNSIGNED UNDERSHOOT  = 5

// --- INTERNAL VARIABLES
SIGNED SCANDIR
SIGNED LAST_TG
SIGNED NEXT_TG
UNSIGNED CUMERR
SIGNED POSCORR
SIGNED BACKDELTA

// --- OUTPUT VARIABLES
UNSIGNED NPOINTS

// --- PROGRAM ALIASES
ALIAS MOT1  = CH_MOT1
ALIAS ATRIG = OUT_ATRIG
ALIAS BTRIG = OUT_BTRIG
ALIAS CTRIG = OUT_CTRIG


// --- initialise store list
SUB STORE_INIT
    STORELIST TIMER DATA_STORE
    CHACTION ALLEVENTS SAVE TIMER
    CHACTION ALLEVENTS SAVE DATA_STORE
ENDSUB

// --- init event source
SUB SET_NORMAL_DIR
    IF (SCANDIR > 0) THEN
        EVSOURCE MOT1 UP
    ELSE
        EVSOURCE MOT1 DOWN
    ENDIF
ENDSUB

// --- inverse event direction
SUB SET_INVERT_DIR
    IF (SCANDIR > 0) THEN
        EVSOURCE MOT1 DOWN
    ELSE
        EVSOURCE MOT1 UP
    ENDIF
ENDSUB

// --- increment target position
// --- and correct error if needed
SUB POS_INC_TARGET
    NEXT_TG = LAST_TG + TRIGDELTA

    CUMERR += POSERR
    IF ((CUMERR & 0x80000000) != 0) THEN
        CUMERR &= 0x7FFFFFFF
        NEXT_TG += POSCORR
    ENDIF

    LAST_TG = NEXT_TG
    @MOT1= NEXT_TG
ENDSUB

// --- main program loop
PROG
    // --- reset triggers
    OUTBITACTION NOW !ATRIG !BTRIG !CTRIG

    // --- reset timer
    BASETIME TIMER 1us
    CHACTION NOW START CLEAR TIMER

    // --- init store values
    GOSUB STORE_INIT

    // --- init variables
    NPOINTS = 0
    CUMERR = 0
    LAST_TG = TRIGSTART
    NEXT_TG = TRIGSTART

    IF (TRIGDELTA < 0) THEN
        SCANDIR= -1
        POSCORR= -1
        BACKDELTA= UNDERSHOOT
    ELSE
        SCANDIR= 1
        POSCORR= 1
        BACKDELTA= -UNDERSHOOT
    ENDIF

    // --- init event and first target
    GOSUB SET_NORMAL_DIR
    @MOT1 = NEXT_TG

    // --- main loop
    WHILE (NPOINTS < NPULSES) DO
        AT MOT1 DO STORE CHANNELS *ATRIG ~BTRIG *CTRIG
        GOSUB POS_INC_TARGET
        AT MOT1 DO STORE CHANNELS ~BTRIG
        IF (NPOINTS < NPULSES-1) THEN
            GOSUB SET_INVERT_DIR
            @MOT1= LAST_TG + BACKDELTA
            AT MOT1 DO NOTHING
            GOSUB SET_NORMAL_DIR
            @MOT1= LAST_TG
        ENDIF
        NPOINTS += 1
    ENDWHILE

    @TIMER = $TIMER+1
    AT TIMER DO STORE CHANNELS

    EXIT NPOINTS
ENDPROG

// --- cleanup function
PROG FSWEEP_CLEAN
    CHACTION NOW STOP CLEAR TIMER
    OUTBITACTION NOW !ATRIG !BTRIG !CTRIG
ENDPROG

"""


from bliss.controllers.musst import Musst
from bliss.controllers.maestrio import Maestrio, MaestrioError

from fscan.fscantools import FScanModeBase

SyncDeviceTypes = FScanModeBase("SyncDeviceTypes", "MUSST", "MAESTRIO")


class SyncProgListClass(object):
    def __init__(self):
        self.classes = dict()

    def register(self, klass, sync_type, prog_name):
        self.classes[sync_type][prog_name] = klass

    def get(self, sync_type, name):
        return self.classes[sync_type].get(name)


global SyncProgList
SyncProgList = SyncProgListClass()

# SyncProgList.register(klass, "MUSST", "fscan")
# SyncProgList.register(klass, "MUSST", "fscancamera")


class SyncDevice(object):
    def __init__(self, board):
        if isinstance(board, Musst):
            self.__type = SyncDeviceTypes.MUSST
        elif isinstance(board, Maestrio):
            self.__type = SyncDeviceTypes.MAESTRIO
        else:
            raise RuntimeError("Unknown SyncDevice type")
        self.__board = board

    @property
    def name(self):
        return self.__board.name

    @property
    def board(self):
        return self.__board

    @property
    def type(self):
        return self.__type

    def get_program_class(self, prog_name):
        prog_class = SyncProgList.get(self.type, prog_name)
        if prog_class is None:
            raise ValueError(
                f"Program [{prog_name}] not implemented on {self.type}]"
            )
        return prog_class

    def has_motors(self, *motors):
        motor_names = [
            getattr(motor, "original_name", motor.name)
            for motor in motors
        ]
        try:
            self.__board.get_channel_by_names(*motor_names)
            return True
        except (RuntimeError, MaestrioError):
            return False

    def is_maestrio(self):
        return self.__type == SyncDeviceTypes.MAESTRIO

    def is_musst(self):
        return self.__type == SyncDeviceTypes.MUSST


class SyncDeviceList(object):
    def __init__(self, board_list):
        self.sync_devs = [SyncDevice(board) for board in board_list]

    @property
    def board_names(self):
        return [dev.board.name for dev in self.sync_devs]

    def first(self, sync_type=None):
        if sync_type is not None:
            devs = self.get_device_of_type(sync_type)
            if not len(devs):
                raise RuntimeError(f"No synchro device if type {sync_type} configured")
            return devs[0]
        return self.sync_devs[0]

    def get_device_of_type(self, sync_type):
        return [dev for dev in self.sync_devs if dev.type == sync_type]

    def get_board_of_type(self, sync_type):
        return [dev.board for dev in self.get_device_of_type(sync_type)]

    def __find_device(self, devlist, *motors):
        for dev in devlist:
            if dev.has_motors(*motors):
                return dev
        motor_names = [motor.name for motor in motors]
        raise RuntimeError(
            f"Cannot find all motors {motor_names} on sync device !!"
        )

    def find_device_for_motors(self, *motors):
        return self.__find_device(self.sync_devs, *motors)

    def __check_device(self, devlist, *motors):
        reply = list()
        for mot in motors:
            found = None
            for dev in devlist:
                if dev.has_motors(mot):
                    found = dev
                    break
            reply.append(found)
        return reply

    def check_device_for_motors(self, *motors):
        return self.__check_device(self.sync_devs, *motors)

    # for musst backward compatibility

    def first_musst(self):
        dev = self.first(SyncDeviceTypes.MUSST)
        return dev.board

    def find_musst_for_motors(self, *motors):
        devlist = self.get_device_of_type(SyncDeviceTypes.MUSST)
        dev = self.__find_device(devlist, *motors)
        return dev.board

    def check_musst_for_motors(self, *motors):
        devlist = self.get_device_of_type(SyncDeviceTypes.MUSST)
        devs = self.__check_device(devlist, *motors)
        return [dev is not None and dev.board or dev for dev in devs]

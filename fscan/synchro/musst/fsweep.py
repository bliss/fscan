from .base import MusstProgBase
from .mprg.fsweep_mprg import fsweep_mprg_data


class MusstProgFSweep(MusstProgBase):
    ProgramData = fsweep_mprg_data
    ProgramStart = "FSWEEP"
    ProgramAbort = "FSWEEP_CLEAN"
    DataPerPoint = 2

    def set_params(self, start, delta, npulses, backdelta):
        motor_chan = self.get_motor_channel(0)

        trigstart = motor_chan.unit2raw(start)
        trigdelta = motor_chan.delta2raw(delta)
        poserr = ((int(abs(trigdelta)) - abs(trigdelta)) * 2**31)
        undershoot = abs(motor_chan.delta2raw(backdelta))
        if undershoot < 1:
            undershoot = 1

        self.params = dict()
        self.params["TRIGSTART"] = int(trigstart)
        self.params["TRIGDELTA"] = int(trigdelta)
        self.params["NPULSES"] = int(npulses)
        self.params["POSERR"] = int(poserr)
        self.params["UNDERSHOOT"] = int(undershoot)



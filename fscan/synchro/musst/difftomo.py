from .base import MusstProgBase
from .mprg.difftomostep_mprg import difftomostep_mprg_data
from .mprg.difftomocont_mprg import difftomocont_mprg_data

from fscan.fscantools import FScanMode, FScanTrigMode


class MusstProgDiffTomoStep(MusstProgBase):
    ProgramData = difftomostep_mprg_data
    ProgramStart = "DIFFTOMOSTEP"
    ProgramAbort = "DIFFTOMOSTEP_CLEAN"
    DataPerPoint = 1


    def set_params(self, scanmode, start, delta, gate, npulses, nloop, loopdelta):
        clock_chan = self.get_timer_channel()
        motor_chan = self.get_motor_channel(0)

        enc_start = motor_chan.unit2raw(start)
        enc_loopdelta = abs(motor_chan.unit2raw(loopdelta))

        if scanmode == FScanMode.TIME:
            trigdelta = clock_chan.unit2raw(delta)
            poserr = 0
        else:
            enc_delta = abs(motor_chan.unit2raw(delta))
            trigdelta = int(enc_delta)
            poserr = int((enc_delta - trigdelta) * 2 ** 31)

        timegate = clock_chan.unit2raw(abs(gate))

        self.params = dict()
        self.params["SCANMODE"] = FScanMode.index(scanmode)
        self.params["TRIGSTART"] = int(enc_start)
        self.params["TRIGDELTA"] = int(trigdelta)
        self.params["GATEWIDTH"] = int(timegate)
        self.params["POSERR"] = int(poserr)
        self.params["NLOOP"] = int(nloop)
        self.params["NPULSES"] = int(npulses)
        self.params["LOOPDELTA"] = int(enc_loopdelta)

class MusstProgDiffTomoCont(MusstProgBase):
    ProgramData = difftomocont_mprg_data
    ProgramStart = "DIFFTOMOCONT"
    ProgramAbort = "DIFFTOMOCONT_CLEAN"
    DataPerPoint = 1

    def set_params(self, scanmode, start, delta, gate, npulses_list, loopdelta_list):
        nloop = len(npulses_list)
        if len(loopdelta_list) != nloop:
            raise ValueError("MusstProgDiffTomoStep: npulses_list and loopdelta_list have not the same size")

        clock_chan = self.get_timer_channel()
        motor_chan = self.get_motor_channel(0)

        trigstart = motor_chan.unit2raw(start)

        if scanmode == FScanTrigMode.TIME:
            trigdelta = clock_chan.unit2raw(delta)
            poserr = 0
        else:
            enc_delta = abs(motor_chan.unit2raw(delta))
            trigdelta = int(enc_delta)
            poserr = int((enc_delta - trigdelta) * 2 ** 31)

        gatewidth = clock_chan.unit2raw(gate)

        self.params = dict()
        self.params["SCANMODE"] = FScanMode.index(scanmode)
        self.params["TRIGSTART"] = int(trigstart)
        self.params["TRIGDELTA"] = int(trigdelta)
        self.params["GATEWIDTH"] = int(gatewidth)
        self.params["POSERR"] = int(poserr)
        self.params["NLOOP"] = int(nloop)

        for idx in range(nloop):
            self.params[f"LOOPPULSES[{idx:d}]"] = int(npulses_list[idx])
            enc_loopdelta = abs(motor_chan.unit2raw(loopdelta_list[idx]))
            self.params[f"LOOPDELTA[{idx:d}]"] = int(enc_loopdelta)


import numpy

from bliss.scanning.acquisition.musst import MusstAcquisitionMaster
from bliss.scanning.acquisition.musst import MusstAcquisitionSlave
from bliss.scanning.acquisition.calc import CalcChannelAcquisitionSlave, CalcHook
from bliss.scanning.chain import AcquisitionChannel, ChainPreset


def _get_iter_var(val):
    try:
        _ = iter(val)
    except TypeError:
        return (val,)
    return val


class MusstBaseCalc(CalcHook):
    def __init__(self, master=None, channel=None, data_per_point=1):
        self.init(master, channel, data_per_point)

    def init(self, master, channel, data_per_point):
        self.master = master
        self.channel = channel
        self.data_per_point = data_per_point
        self.__overflow = 0
        self.__last_data = None

    @property
    def unit(self):
        return self.channel.unit

    @property
    def dest_name(self):
        raise NotImplementedError("Implement dest_name property in {0}".format(self.__class__))

    def compute(self, sender, data_dict):
        data = data_dict.get(self.channel.store_name)
        
        if data is None:
            return dict()

        calc_data = self.calculate(data)
        if calc_data is not None:
            return { self.dest_name: calc_data }
        else:
            return dict()

    def absolute(self, data):
        if self.__last_data is None:
            self.__last_data = data[0]
        raw_data = numpy.append(self.__last_data, data)
        abs_data = raw_data.astype(numpy.float64)
        if len(raw_data) >= 2:
            over_idxs = numpy.where(numpy.diff(abs_data) != numpy.diff(raw_data))[0]
            abs_data += self.__overflow * 2**32
            for idx in over_idxs:
                ovr_sign = raw_data[idx] > 0 and 1. or -1.
                abs_data[idx+1:] += ovr_sign * 2**32
                self.__overflow += ovr_sign
        self.__last_data = raw_data[-1:]
        return abs_data[len(self.__last_data):]
       
    def calculate(self, data):
        raise NotImplementedError("Implement calculate method in {0}".format(self.__class__))

    @property
    def acquisition_channels(self):
        return [AcquisitionChannel(self.dest_name, numpy.float64, (), unit=self.unit)]
 
class MusstChanUserRawCalc(MusstBaseCalc):
    def select_channel(self, channels, scan_pars):
        """ Return one channel to be used or None if the calc is not used
        """
        raise NotImplementedError("Missing select_channel method in {0}".format(self.__class__.__name__))

class MusstChanUserCalc(CalcHook):

    @property
    def name(self):
        return self.__class__.__name__

    def select_input_channels(self, channels_list, scan_pars):
        raise NotImplementedError("Missing select_input_channels method in {0}".format(self.__class__.__name__))

    def get_output_channels(self):
        raise NotImplementedError("Missing get_output_channels method in {0}".format(self.__class__.__name__))

    def calculate(self, input_data_dict):
        raise NotImplementedError("Missing calculate method in {0}".format(self.__class__.__name__))

    def init(self, musstdev, channels_list, scan_pars):
        filter_list = [ name.split(":")[1] for name in channels_list ]
        input_channels = self.select_input_channels(filter_list, scan_pars)
        if input_channels is None:
            return list()
        self._input_channels = input_channels
        self._data = dict()
        for channel in input_channels:
            self._data[channel] = numpy.array((), numpy.float64)
        self.musst_name = musstdev.name
        return [ f"{self.musst_name}:{name}" for name in input_channels ]
            
    def compute(self, sender, data_dict):
        name = sender.short_name
        data = data_dict.get(name)
        
        if data is None:
            return dict()

        self._data[name] = numpy.append(self._data[name], data)
        max_aligned = min([len(data) for data in self._data.values()])
        if max_aligned == 0:
            return dict()

        input_data = dict()
        for channel in self._input_channels:
            input_data[channel] = self._data[channel][:max_aligned]
            self._data[channel] = self._data[channel][max_aligned:]

        calc_data = self.calculate(input_data)
        ret_data = dict()
        for (name, value) in calc_data.items():
            ret_data[f"{self.musst_name}:{name}"] = value
        return ret_data

    @property
    def acquisition_channels(self):
        out_channels = list()
        for name in self.get_output_channels():
            if name.find(":")<0:
                out_channels.append(f"{self.musst_name}:{name}")
            else:
                out_channels.append(name)
        acq_channels = [ AcquisitionChannel(name, numpy.float64, ()) for name in out_channels ]
        return acq_channels

class MusstChanConvCalc(MusstBaseCalc):
    @property
    def dest_name(self):
        return f"{self.master.name}:{self.channel.name}"

    def calculate(self, data):
        abs_data = self.absolute(data)
        conv_data = self.channel.raw2unit(abs_data[:])
        return conv_data

class MusstEpochCalc(MusstBaseCalc):
    @property
    def dest_name(self):
        return f"{self.master.name}:epoch"

    @property
    def start_epoch(self):
        return self.master.start_epoch

    def calculate(self, data):
        conv_data = self.start_epoch + self.channel.raw2unit(data[:])
        return conv_data

class MusstEpochTrigCalc(MusstBaseCalc):
    @property
    def dest_name(self):
        return f"{self.master.name}:epoch_trig"

    @property
    def start_epoch(self):
        return self.master.start_epoch

    def prepare(self):
        self._data = numpy.array((), numpy.float64)

    def calculate(self, data):
        self._data = numpy.append(self._data, data)
        if self.data_per_point == 1:
            if len(self._data) < 2:
                return None
            calc_data = self.start_epoch + self.channel.raw2unit(self._data[:-1])
            self._data = self._data[-1:]
        else:
            dp = self.data_per_point
            len_used = dp*(len(self._data)//dp)
            if not len_used:
                return None
            calc_data = self.start_epoch + self.channel.raw2unit(self._data[:len_used:dp])
            self._data = self._data[len_used:]

        return calc_data

class MusstChanTrigCalc(MusstBaseCalc):
    @property
    def dest_name(self):
        return f"{self.master.name}:{self.channel.name}_trig"

    def prepare(self):
        self._data = numpy.array((), numpy.float64)

    def calculate(self, data):
        abs_data = self.absolute(data)
        self._data = numpy.append(self._data, abs_data)
        if self.data_per_point == 1:
            if len(self._data) < 2:
                return None
            calc_data = self.channel.raw2unit(self._data[:-1])
            self._data = self._data[-1:]
        else:
            dp = self.data_per_point
            len_used = dp*(len(self._data)//dp)
            if not len_used:
                return None
            calc_data = self.channel.raw2unit(self._data[:len_used:dp])
            self._data = self._data[len_used:]

        return calc_data

class MusstChanTrig360Calc(MusstChanTrigCalc):
    @property
    def dest_name(self):
        return f"{self.master.name}:{self.channel.name}_trig360"

    def calculate(self, data):
        calc_data = MusstChanTrigCalc.calculate(self, data)
        if calc_data is not None:
            calc_data = calc_data % 360
        return calc_data

class MusstChanPeriodCalc(MusstBaseCalc):
    @property
    def dest_name(self):
        return f"{self.master.name}:{self.channel.name}_period"

    def prepare(self):
        self._data = numpy.array((), dtype=numpy.int32)

    def calculate(self, data):
        self._data = numpy.append(self._data, data)

        if self.data_per_point == 1:
            if len(self._data) < 2:
                return None
            calc_data = self.channel.raw2unit(self._data[1:] - self._data[:-1])
            self._data = self._data[-1:]
        else:
            dp = self.data_per_point
            if len(self._data) < (dp + 1):
                return None
            calc_data = self.channel.raw2unit(self._data[dp::dp] - self._data[0:-dp:dp])
            self._data = self._data[2 * len(calc_data) :]

        return calc_data

class MusstChanDeltaCalc(MusstBaseCalc):
    @property
    def dest_name(self):
        return f"{self.master.name}:{self.channel.name}_delta"

    def prepare(self):
        self._data = numpy.array((), dtype=numpy.int32)

    def calculate(self, data):
        self._data = numpy.append(self._data, data)

        if self.data_per_point == 1:
            if len(self._data) < 2:
                return None
            calc_data = self.channel.raw2unit(self._data[1:] - self._data[:-1])
            self._data = self._data[-1:]
        else:
            dp = self.data_per_point
            if len(self._data) < (dp + 1):
                return None
            data0 = self._data[0::dp]
            data1 = self._data[dp - 1 :: dp]
            calc_len = min(data0.shape[0], data1.shape[0])
            calc_data = self.channel.raw2unit(data1[:calc_len] - data0[:calc_len])
            self._data = self._data[dp * calc_len :]

        return calc_data

class MusstChanCenterCalc(MusstBaseCalc):
    @property
    def dest_name(self):
        return f"{self.master.name}:{self.channel.name}_center"

    def prepare(self):
        self._data = numpy.array((), numpy.float64)

    def calculate(self, data):
        new_data = self.absolute(data)
        self._data = numpy.append(self._data, new_data)

        if self.data_per_point == 1:
            if len(self._data) < 2:
                return None
            calc_data = self.channel.raw2unit((self._data[1:] + self._data[:-1]) / 2.)
            self._data = self._data[-1:]
        else:
            dp = self.data_per_point
            if len(self._data) < (dp + 1):
                return None
            data0 = self._data[0::dp]
            data1 = self._data[dp - 1 :: dp]
            calc_len = min(data0.shape[0], data1.shape[0])
            calc_data = self.channel.raw2unit((data1[:calc_len] + data0[:calc_len]) / 2.)
            self._data = self._data[dp * calc_len :]

        return calc_data

class MusstChanCenter360Calc(MusstChanCenterCalc):
    @property
    def dest_name(self):
        return f"{self.master.name}:{self.channel.name}_cen360"

    def calculate(self, data):
        calc_data = MusstChanCenterCalc.calculate(self, data)
        if calc_data is not None:
            calc_data = calc_data % 360
        return calc_data

class MusstBaseChannel(object):
    def __init__(self, name, chanid, unit=None):
        self.name = name
        self.channel_id = chanid
        self.unit = unit

    @property
    def store_name(self):
        return f"{self.name}_raw"

    def raw2unit(self, data):
        raise NotImplementedError

    def unit2raw(self, data):
        raise NotImplementedError

    def delta2raw(self, delta):
        return self.unit2raw(delta)

    def delta2unit(self, delta):
        return self.raw2unit(delta)

class MusstTimerChannel(MusstBaseChannel):
    def __init__(self, clock_factor):
        super().__init__("timer", 0, "s")
        self.set_clock(clock_factor)

    def set_clock(self, clock_factor):
        self.resolution = clock_factor

    def raw2unit(self, data):
        return data / self.resolution

    def unit2raw(self, data):
        return int(data * self.resolution)
        
class MusstCounterChannel(MusstBaseChannel):
    def __init__(self, cnt, channel):
        super().__init__(cnt.name, channel.channel_id, cnt.unit)

        self.resolution = 1.
        self.offset = 0.
        self.sign = 1

        if channel.mode == channel.ADC5:
            self._sampling = True
            self.resolution = 1. / (5. / 0x7fffffff)
            self.unit = "V"
        elif channel.mode == channel.ADC10:
            self._sampling = True
            self.resolution = 1. / (10. / 0x7fffffff)
            self.unit = "V"
        elif channel.mode == channel.COUNTER:
            self._sampling = False
            self.channel = channel
        elif channel.mode in [channel.ENCODER, channel.SSI]:
            self._sampling = True
            conv = channel.encoder_conversion_param
            if conv is None:
                conv = {}
            self.resolution = conv.get("steps_per_unit", self.resolution)
            self.offset = conv.get("offset", self.offset)
            self.sign = conv.get("sign", self.sign)
            self.unit = conv.get("unit", self.unit)
        else:
            self._sampling = True

    def is_sampling(self):
        return self._sampling

    def init(self):
        if not self._sampling:
            self.channel.stop()
            self.channel.value = 0
            self.channel.run()

    def raw2unit(self, data):
        return self.sign * data / self.resolution + self.offset

    def unit2raw(self, data):
        return self.sign * (data - self.offset) * self.resolution

    def delta2raw(self, data):
        return self.sign * data * self.resolution

    def delta2unit(self, data):
        return self.sign * data / self.resolution


def getMusstMotorChannel(motor, channel):
    class_name = motor.controller.__class__.__name__
    if class_name == "PI_E712":
        return MusstPi712MotorChannel(motor,channel)
    else:
        return MusstStepperMotorChannel(motor, channel)

class MusstMotorChannel(MusstBaseChannel):
    def __init__(self, motor, channel):
        super().__init__(motor.name, channel.channel_id, motor.unit)

class MusstStepperMotorChannel(MusstMotorChannel):
    def __init__(self, motor, channel):
        super().__init__(motor, channel)

        conv = channel.encoder_conversion_param
        if conv is None:
            conv = {}
        
        self.resolution = conv.get("steps_per_unit", None)
        self.offset = conv.get("offset", 0.)
        self.sign = conv.get("sign", motor.sign)
        self.unit = conv.get("unit", motor.unit)

        if self.resolution is None:
            if motor.encoder:
                self.resolution = motor.encoder.steps_per_unit
            else:
                self.resolution = motor.steps_per_unit

        if self.resolution < 0.:
            self.resolution = abs(self.resolution)
            self.sign *= -1

        if channel.mode == channel.SSI:
            self.offset += motor.offset

    def raw2unit(self, data):
        return self.sign * data / self.resolution + self.offset

    def unit2raw(self, data):
        return self.sign * (data - self.offset) * self.resolution

    def delta2raw(self, data):
        return self.sign * data * self.resolution

    def delta2unit(self, data):
        return self.sign * data / self.resolution


class MusstPi712MotorChannel(MusstMotorChannel):
    def __init__(self,motor,channel):
        super().__init__(motor, channel)

        # conversion musst raw to voltage
        mode = channel.chan_config
        if mode == channel.ADC10:
            self._musst_scale = 1. / (10. / 0x7fffffff)
        elif mode == channel.ADC5:
            self._musst_scale = 1. / (5. / 0x7fffffff)
        else:
            raise TypeError("PI E712 piezo musst be connected to a musst ADC channel")

        # conversion voltage to position
        scaling, offset = channel.switch.scaling_and_offset
        self._volt_scaling = scaling
        self._volt_offset = offset
      
        # conversion dial to user 
        # assume steps_per_unit is either +1 or -1 for PIE712 
        if motor.steps_per_unit < 0.:
            self._motor_sign = -1
        else:
            self._motor_sign = 1
        self._motor_sign *= motor.sign
        self._motor_offset = motor.offset

        self.sign = self._motor_sign
        
    def raw2unit(self, musst_data):
        data = musst_data / self._musst_scale
        data = data / self._volt_scaling - self._volt_offset
        data = self._motor_sign * data + self._motor_offset
        return data
    
    def unit2raw(self, data):
        musst_data = self._motor_sign * (data - self._motor_offset)
        musst_data = musst_data * self._volt_scaling + self._volt_offset
        musst_data = musst_data * self._musst_scale
        return musst_data
    
    def delta2raw(self, delta):
        volt_delta = delta * self._volt_scaling
        musst_data = self._motor_sign * volt_delta * self._musst_scale
        return musst_data
        
    def delta2unit(self, delta):
        musst_data = self._motor_sign * delta / self._musst_scale
        volt_data = musst_data / self._volt_scaling
        return volt_data
    
class MusstRevertClock(ChainPreset):
    def __init__(self, musstprog):
        self.musstprog = musstprog

    def stop(self, chain):
        self.musstprog.set_back_timer()

class MusstProgBase(object):
    ProgramData = None
    ProgramStart = None
    ProgramAbort = None
    DataPerPoint = 1

    TIMER_CLOCK = {
        "1KHZ": 1e3, 
        "10KHZ": 10e3,
        "100KHZ": 100e3,
        "1MHZ": 1e6, 
        "10MHZ": 10e6, 
        "50MHZ": 50e6,
    }

    def __init__(self, musst, motors=None, counters=None):
        self.musst = musst

        self.params = dict()
        self.iter_params = list()
        self.template = dict()
        self.user_template = dict()
        (self.__initial_clock, clock_factor) = self.musst.TMRCFG
        self.__last_clock = self.__initial_clock
        self.__max_data_rate = 0

        self.channels = dict()
        self.channels["timer"] = MusstTimerChannel(clock_factor)

        if motors is None:
            self.motors = list()
        else:
            self.set_motors(*motors)
        if counters is None:
            self.set_counters(*musst.counters)
        else:
            self.set_counters(*counters)

    def set_motors(self, *motors):
        use_chans = list()
        self.motors = list()
        names = list()
        for motor in motors:
            name = getattr(motor, "original_name", motor.name)
            try:
                chan = self.musst.get_channel_by_name(name)
            except:
                continue
            chan_id = chan.channel_id
            if chan_id not in use_chans:
                use_chans.append(chan_id)
                self.channels[motor.name] = getMusstMotorChannel(motor, chan)
                self.motors.append(motor)
                names.append(name)
        # ensure switches are set
        chans = self.musst.get_channel_by_names(*names)

    def set_counters(self, *counters):
        use_chans = [ chan.channel_id for chan in self.channels.values() ]
        self.counters = list()
        for counter in counters:
            name = getattr(counter, "original_name", counter.name)
            try:
                chan = self.musst.get_channel_by_name(name)
            except:
                continue
            chan_id = chan.channel_id
            if chan_id not in use_chans:
                use_chans.append(chan_id)
                self.channels[counter.name] = MusstCounterChannel(counter, chan)
                self.counters.append(counter.name)

    def get_channel(self, mot_or_cnt):
        try:
            return self.channels[mot_or_cnt.name]
        except KeyError:
            raise ValueError(f"No channel found for {mot_or_cnt:r}")

    def get_timer_channel(self):
        return self.channels["timer"]

    def get_motor_channel(self, motidx=0):
        if motidx >= len(self.motors):
            raise ValueError("Motor index does not exist")
        return self.channels.get(self.motors[motidx].name)

    def channel_list(self):
        return list(self.channels.keys())

    def sync_motors(self, *motors):
        if not len(motors):
            motors = self.motors
        for motor in motors:
            self.sync_one_motor(motor)

    def sync_one_motor(self, motor):
        mot_chan = self.channels[motor.name]
        enc_chan = self.musst.get_channel(mot_chan.channel_id)

        if enc_chan.mode == enc_chan.SSI:
            return

        motor.wait_move()
        motor_pos = motor.position
        enc_chan.value = int(mot_chan.unit2raw(motor_pos) + 0.5)
       
    def set_params(self, param_dict):
        self.params = param_dict

    def set_iter_params(self, param_list):
        self.iter_params = param_list

    def set_template(self, template_dict):
        self.user_template = template_dict

    def check_max_timer(self, scan_time):
        if self.__initial_clock in ["1KHZ", "10KHZ", "100KHZ"]:
            self.set_timer_clock("1MHZ")
        if self.get_max_timer() < scan_time:
            return self.set_max_timer(scan_time)
        return False

    def set_max_timer(self, total_time):
        (clock_str, clock) = self.musst.TMRCFG
        clock_list = list(self.TIMER_CLOCK.values())
        clock_cmds = list(self.TIMER_CLOCK.keys())
        clock_index = clock_cmds.index(clock_str)
        changed = False

        while clock_index > 0 and total_time > (pow(2, 32) / clock_list[clock_index]):
            clock_index -= 1
            changed = True

        if changed:
            self.set_timer_clock(clock_cmds[clock_index])
        return changed

    def set_timer_clock(self, clock):
        self.musst.TMRCFG = clock
        self.__last_clock = clock
        self.channels["timer"].set_clock(self.TIMER_CLOCK[clock])

    def get_max_timer(self):
        return pow(2, 32) / self.musst.get_timer_factor()

    def set_back_timer(self):
        self.musst.TMRCFG = self.__initial_clock

    def set_max_data_rate(self, data_rate):
        self.__max_data_rate = data_rate

    def setup(self, chain, master=None, **keys):
        # --- create store list and template
        self.__setup_store_list_and_template()

        # --- get parameters list or dict
        pars = self.__get_params()

        # --- check musst state
        if self.musst.STATE != self.musst.IDLE_STATE:
            self.musst.ABORT

        # --- musst acquisition master
        self.__prepare_once = keys.pop("prepare_once", False)
        self.__start_once = keys.pop("start_once", False)
        last_iter_index = keys.pop("last_iter_index", 1)

        self.musst_master = MusstAcquisitionMaster(
            self.musst,
            program_data=self.ProgramData,
            program_start_name=self.ProgramStart,
            program_abort_name=self.ProgramAbort,
            program_template_replacement=self.template,
            vars=pars,
            prepare_once=self.__prepare_once,
            start_once=self.__start_once,
            last_iter_index=last_iter_index,
            **keys
        )

        # --- musst acquisition device
        self.musst_device = MusstAcquisitionSlave(
            self.musst, store_list=self.store_list, max_data_rate=self.__max_data_rate,
        )

        # --- add to acq. chain
        if master is not None:
            chain.add(master, self.musst_master)
        chain.add(self.musst_master, self.musst_device)

        # --- revert musst clock if needed
        if self.__last_clock != self.__initial_clock:
            chain.add_preset(MusstRevertClock(self))

        # --- holder to keep track of devices for ext channels
        self.calc_devices = dict()

    @property
    def acq_master(self):
        return self.musst_master

    @property
    def acq_data_master(self):
        return self.musst_device

    def get_calc_dev(self, name, calc_type):
        if calc_type is not None and calc_type != "conv":
            chan_name = f"{name}_{calc_type}"
        else:
            chan_name = f"{name}"
        calc_name = f"{self.musst.name}:{chan_name}"
        return (self.calc_devices.get(calc_name, None), chan_name)

    def get_calc_name(self, name, calc_type):
        if calc_type is not None and calc_type != "conv":
            chan_name = f"{name}_{calc_type}"
        else:
            chan_name = f"{name}"
        return f"{self.musst.name}:{chan_name}"
       
    def get_calc_channel(self, name, calc_type):
        calc_name = self.get_calc_name(name, calc_type)
        try:
            calc_dev = self.calc_devices[calc_name]
            return calc_dev.channels[0]
        except KeyError:
            return None

    def set_motor_external_channel(self, mot_name, mot_master, calc_type="trig"):
        (dev, name) = self.get_calc_dev(mot_name, calc_type)
        if dev is not None:
            mot_master.add_external_channel(dev, name, f"axis:{mot_name}")

    def set_timer_external_channel(self):
        (dev, name) = self.get_calc_dev("timer", "trig")
        self.musst_master.add_external_channel(dev, name, "timer:elapsed_time")
        (dev, name) = self.get_calc_dev("epoch", "trig")
        self.musst_master.add_external_channel(dev, name, "timer:epoch")

    def add_default_calc(self, chain):
        motors = [motor.name for motor in self.motors]
        self.add_epoch_trig_calc(chain)
        self.add_trig_calc(chain, "timer", *motors)
        if self.DataPerPoint == 2:
            self.add_period_calc(chain, "timer", *motors)
        self.add_delta_calc(chain)

    def add_epoch_conv_calc(self, chain):
        self.__add_calc_hook(chain, "timer", MusstEpochCalc)

    def add_epoch_trig_calc(self, chain):
        self.__add_calc_hook(chain, "timer", MusstEpochTrigCalc)

    def add_conv_calc(self, chain, *chan_names):
        if not len(chan_names):
            chan_names = self.channels.keys()
        for name in chan_names:
            self.__add_calc_hook(chain, name, MusstChanConvCalc)

    def add_trig_calc(self, chain, *chan_names):
        if not len(chan_names):
            chan_names = self.channels.keys()
        for name in chan_names:
            self.__add_calc_hook(chain, name, MusstChanTrigCalc)

    def add_trig360_calc(self, chain, *chan_names):
        if not len(chan_names):
            chan_names = self.channels.keys()
        for name in chan_names:
            self.__add_calc_hook(chain, name, MusstChanTrig360Calc)

    def add_delta_calc(self, chain, *chan_names):
        if not len(chan_names):
            chan_names = self.channels.keys()
        for name in chan_names:
            self.__add_calc_hook(chain, name, MusstChanDeltaCalc)

    def add_period_calc(self, chain, *chan_names):
        if not len(chan_names):
            chan_names = self.channels.keys()
        for name in chan_names:
            self.__add_calc_hook(chain, name, MusstChanPeriodCalc)

    def add_center_calc(self, chain, *chan_names):
        if not len(chan_names):
            chan_names = self.channels.keys()
        for name in chan_names:
            self.__add_calc_hook(chain, name, MusstChanCenterCalc)

    def add_cen360_calc(self, chain, *chan_names):
        if not len(chan_names):
            chan_names = self.channels.keys()
        for name in chan_names:
            self.__add_calc_hook(chain, name, MusstChanCenter360Calc)

    def add_user_calc(self, chain, calc_objs, scan_pars):
        raw_calc = [ calc for calc in calc_objs if isinstance(calc, MusstChanUserRawCalc) ]
        for calc in raw_calc:
            channel = calc.select_channel(self.channels, scan_pars)
            if channel is not None and isinstance(channel, MusstBaseChannel):
                calc.init(self.musst_device, channel, self.DataPerPoint)
                self.add_calc_dev(chain, calc)

        user_calc = [ calc for calc in calc_objs if isinstance(calc, MusstChanUserCalc) ]
        all_chans = list(self.calc_devices.keys())
        for calc in user_calc:
            input_channels = calc.init(self.musst_device, all_chans, scan_pars)
            if len(input_channels):
                input_devs = [ self.calc_devices[name] for name in input_channels ]
                calc_dev = CalcChannelAcquisitionSlave(
                    calc.name,
                    input_devs,
                    calc,
                    calc.acquisition_channels,
                    prepare_once=self.__prepare_once,
                    start_once=self.__start_once,
                )
                chain.add(self.musst_master, calc_dev)

    def __add_calc_hook(self, chain, name, klass):
        calc_hook = klass(self.musst_device, self.channels[name], self.DataPerPoint)
        self.add_calc_dev(chain, calc_hook)

    def add_calc_dev(self, chain, calc_hook):
        calc_dev = CalcChannelAcquisitionSlave(
            calc_hook.dest_name,
            (self.musst_device,),
            calc_hook,
            calc_hook.acquisition_channels,
            prepare_once=self.__prepare_once,
            start_once=self.__start_once,
        )
        chain.add(self.musst_master, calc_dev)
        self.calc_devices[calc_hook.dest_name] = calc_dev

    def __setup_store_list_and_template(self):
        data_alias = ""
        data_store = ""
        chan_names = dict()

        motidx = 0
        for motor in self.motors:
            chan = self.channels.get(motor.name)
            motidx += 1
            data_alias += "ALIAS MOT{0:d} = CH{1:d}\n".format(motidx, chan.channel_id)
            data_store += "MOT{0:d} ".format(motidx)

        cntidx = 0
        for counter in self.counters:
            chan = self.channels.get(counter)
            cntidx += 1
            data_alias += "ALIAS DATA{0:d} = CH{1:d}\n".format(cntidx, chan.channel_id)
            data_store += "DATA{0:d} ".format(cntidx)
            chan.init()

        self.template = {"$DATA_ALIAS$": data_alias, "$DATA_STORE$": data_store}
        self.template.update(self.user_template)

        chan_names = dict(
            ((chan.channel_id, chan.store_name) for chan in self.channels.values())
        )
        sorted_chans = list(chan_names.keys())
        sorted_chans.sort()
        self.store_list = [chan_names[chid] for chid in sorted_chans]

    def __get_params(self):
        if not len(self.iter_params):
            return self.params
        else:
            par0 = dict(self.params)
            par0.update(self.iter_params[0])
            parlist = [ par0, ] + self.iter_params[1:]
            return parlist


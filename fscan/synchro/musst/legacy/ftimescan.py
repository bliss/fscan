from .base import MusstProgBase
from .mprg.ftimescan_mprg import ftimescan_mprg_data
from fscan.fscantools import FScanCamSignal


class MusstProgFTimeScan(MusstProgBase):
    ProgramData = ftimescan_mprg_data
    ProgramStart = "FTIMESCAN"
    ProgramAbort = "FTIMESCAN_CLEAN"
    DataPerPoint = 2

    def set_time_params(self, start, npoints, acqtime, period):
        mclock = self.musst.get_timer_factor()
        self.params = dict()
        self.params["STARTTIME"] = int(start * mclock)
        self.params["NPTS"] = int(npoints)
        self.params["ACQTIME"] = int(acqtime * mclock)
        self.params["PERIOD"] = int(period * mclock)
        self.params["MODE"] = 0
        self.params["ACCNB"] = 1
        self.params["ACCTIME"] = 0

    def set_camera_params(self, start, npoints, signal_type):
        mclock = self.musst.get_timer_factor()
        self.params = dict()
        self.params["STARTTIME"] = int(start * mclock)
        self.params["NPTS"] = int(npoints)
        self.params["MODE"] = 1
        self.params["SIGNAL"] = FScanCamSignal.index(signal_type)
        self.params["ACCNB"] = 1
        self.params["ACCNB"] = 0

    def set_extstart_params(self, npoints, acqtime, period):
        mclock = self.musst.get_timer_factor()
        self.params = dict()
        self.params["STARTTIME"] = 0
        self.params["NPTS"] = int(npoints)
        self.params["ACQTIME"] = int(acqtime * mclock)
        self.params["PERIOD"] = int(period * mclock)
        self.params["MODE"] = 2
        self.params["ACCNB"] = 1
        self.params["ACCTIME"] = 0


    def set_acc_params(self, accnb, accperiod=None):
        self.params["ACCNB"] = int(accnb)
        if accperiod is not None:
            mclock = self.musst.get_timer_factor()
            self.params["ACCTIME"] = int(accperiod * mclock)


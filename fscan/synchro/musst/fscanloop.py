from .base import MusstProgBase
from .mprg.fscanloop_mprg import fscanloop_mprg_data

from fscan.fscantools import FScanTrigMode, FScanLoopMode


class MusstProgFScanLoop(MusstProgBase):
    ProgramData = fscanloop_mprg_data
    ProgramStart = "FSCANLOOP"
    ProgramAbort = "FSCANLOOP_CLEAN"
    DataPerPoint = 2

    def set_modes(
        self,
        scanmode=FScanTrigMode.TIME,
        gatemode=FScanTrigMode.TIME,
        loopmode=FScanLoopMode.NONE,
    ):
        self._scan_mode = scanmode
        self._gate_mode = gatemode
        self._loop_mode = loopmode
        if self._scan_mode == FScanTrigMode.TIME:
            self._gate_mode = FScanTrigMode.TIME

    def set_params(self, start, direction, delta, gatewidth, npulses, nloop, loopdelta, waitdelta=0):
        clock_chan = self.get_timer_channel()
        motor_chan = self.get_motor_channel(0)

        trigstart = motor_chan.unit2raw(start)

        if self._scan_mode == FScanTrigMode.TIME:
            trigdelta = clock_chan.unit2raw(delta)
            poserr = 0
        else:
            enc_delta = abs(motor_chan.unit2raw(delta))
            trigdelta = int(enc_delta)
            poserr = int((enc_delta - trigdelta) * 2 ** 31)

        if self._gate_mode == FScanTrigMode.TIME:
            gate = clock_chan.unit2raw(abs(gatewidth))
        else:
            gate = abs(motor_chan.unit2raw(gatewidth))

        if self._loop_mode == FScanLoopMode.NONE:
            nloop = 1
        enc_loopdelta = abs(motor_chan.unit2raw(loopdelta))
        enc_waitdelta = abs(motor_chan.unit2raw(waitdelta))

        self.params = dict()
        self.params["SCANMODE"] = FScanTrigMode.index(self._scan_mode)
        self.params["GATEMODE"] = FScanTrigMode.index(self._gate_mode)
        self.params["TRIGSTART"] = int(trigstart)
        self.params["TRIGDELTA"] = int(trigdelta)
        self.params["GATEWIDTH"] = int(gate)
        self.params["NPULSES"] = int(npulses)
        self.params["SCANDIR"] = int(direction * motor_chan.sign)
        self.params["POSERR"] = int(poserr)
        self.params["LOOPMODE"] = FScanLoopMode.index(self._loop_mode)
        self.params["NLOOP"] = int(nloop)
        self.params["LOOPDELTA"] = int(enc_loopdelta)
        self.params["WAITDELTA"] = int(enc_waitdelta)


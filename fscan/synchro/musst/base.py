import numpy

from bliss.scanning.acquisition.musst import MusstAcquisitionMaster
from bliss.scanning.acquisition.musst import MusstAcquisitionSlave
from bliss.scanning.chain import ChainPreset


def _get_iter_var(val):
    try:
        _ = iter(val)
    except TypeError:
        return (val,)
    return val


class MusstBaseChannel(object):
    def __init__(self, name, chanid, unit=None):
        self.name = name
        self.channel_id = chanid
        self.unit = unit

    @property
    def store_name(self):
        return f"{self.name}_raw"

    def raw2unit(self, data):
        raise NotImplementedError

    def unit2raw(self, data):
        raise NotImplementedError

    def delta2raw(self, delta):
        return self.unit2raw(delta)

    def delta2unit(self, delta):
        return self.raw2unit(delta)

class MusstTimerChannel(MusstBaseChannel):
    def __init__(self, clock_factor):
        super().__init__("timer", 0, "s")
        self.set_clock(clock_factor)

    def set_clock(self, clock_factor):
        self.resolution = clock_factor

    def raw2unit(self, data):
        return data / self.resolution

    def unit2raw(self, data):
        return int(data * self.resolution)
        
class MusstCounterChannel(MusstBaseChannel):
    def __init__(self, cnt, channel):
        super().__init__(cnt.name, channel.channel_id, cnt.unit)

        self.resolution = 1.
        self.offset = 0.
        self.sign = 1

        if channel.mode == channel.ADC5:
            self._sampling = True
            self.resolution = 1. / (5. / 0x7fffffff)
            self.unit = "V"
        elif channel.mode == channel.ADC10:
            self._sampling = True
            self.resolution = 1. / (10. / 0x7fffffff)
            self.unit = "V"
        elif channel.mode == channel.COUNTER:
            self._sampling = False
            self.channel = channel
        elif channel.mode in [channel.ENCODER, channel.SSI]:
            self._sampling = True
            conv = channel.encoder_conversion_param
            if conv is None:
                conv = {}
            self.resolution = conv.get("steps_per_unit", self.resolution)
            self.offset = conv.get("offset", self.offset)
            self.sign = conv.get("sign", self.sign)
            self.unit = conv.get("unit", self.unit)
        else:
            self._sampling = True

    def is_sampling(self):
        return self._sampling

    def init(self):
        if not self._sampling:
            self.channel.stop()
            self.channel.value = 0
            self.channel.run()

    def raw2unit(self, data):
        return self.sign * data / self.resolution + self.offset

    def unit2raw(self, data):
        return self.sign * (data - self.offset) * self.resolution

    def delta2raw(self, data):
        return self.sign * data * self.resolution

    def delta2unit(self, data):
        return self.sign * data / self.resolution


def getMusstMotorChannel(motor, channel):
    class_name = motor.controller.__class__.__name__
    if class_name == "PI_E712":
        return MusstPi712MotorChannel(motor,channel)
    else:
        return MusstStepperMotorChannel(motor, channel)

class MusstMotorChannel(MusstBaseChannel):
    def __init__(self, motor, channel):
        super().__init__(motor.name, channel.channel_id, motor.unit)

class MusstStepperMotorChannel(MusstMotorChannel):
    def __init__(self, motor, channel):
        super().__init__(motor, channel)

        conv = channel.encoder_conversion_param
        if conv is None:
            conv = {}
        
        self.resolution = conv.get("steps_per_unit", None)
        self.offset = conv.get("offset", 0.)
        self.sign = conv.get("sign", motor.sign)
        self.unit = conv.get("unit", motor.unit)

        if self.resolution is None:
            if motor.encoder:
                self.resolution = motor.encoder.steps_per_unit
            else:
                self.resolution = motor.steps_per_unit

        if self.resolution < 0.:
            self.resolution = abs(self.resolution)
            self.sign *= -1

        if channel.mode == channel.SSI:
            self.offset += motor.offset

    def raw2unit(self, data):
        return self.sign * data / self.resolution + self.offset

    def unit2raw(self, data):
        return self.sign * (data - self.offset) * self.resolution

    def delta2raw(self, data):
        return self.sign * data * self.resolution

    def delta2unit(self, data):
        return self.sign * data / self.resolution


class MusstPi712MotorChannel(MusstMotorChannel):
    def __init__(self,motor,channel):
        super().__init__(motor, channel)

        # conversion musst raw to voltage
        mode = channel.chan_config
        if mode == channel.ADC10:
            self._musst_scale = 1. / (10. / 0x7fffffff)
        elif mode == channel.ADC5:
            self._musst_scale = 1. / (5. / 0x7fffffff)
        else:
            raise TypeError("PI E712 piezo musst be connected to a musst ADC channel")

        # conversion voltage to position
        scaling, offset = channel.switch.scaling_and_offset
        self._volt_scaling = scaling
        self._volt_offset = offset
      
        # conversion dial to user 
        # assume steps_per_unit is either +1 or -1 for PIE712 
        if motor.steps_per_unit < 0.:
            self._motor_sign = -1
        else:
            self._motor_sign = 1
        self._motor_sign *= motor.sign
        self._motor_offset = motor.offset

        self.sign = self._motor_sign
        
    def raw2unit(self, musst_data):
        data = musst_data / self._musst_scale
        data = data / self._volt_scaling - self._volt_offset
        data = self._motor_sign * data + self._motor_offset
        return data
    
    def unit2raw(self, data):
        musst_data = self._motor_sign * (data - self._motor_offset)
        musst_data = musst_data * self._volt_scaling + self._volt_offset
        musst_data = musst_data * self._musst_scale
        return musst_data
    
    def delta2raw(self, delta):
        volt_delta = delta * self._volt_scaling
        musst_data = self._motor_sign * volt_delta * self._musst_scale
        return musst_data
        
    def delta2unit(self, delta):
        musst_data = self._motor_sign * delta / self._musst_scale
        volt_data = musst_data / self._volt_scaling
        return volt_data
    
class MusstRevertClock(ChainPreset):
    def __init__(self, musstprog):
        self.musstprog = musstprog

    def stop(self, chain):
        self.musstprog.set_back_timer()

class MusstProgBase(object):
    ProgramData = None
    ProgramStart = None
    ProgramAbort = None
    DataPerPoint = 1

    TIMER_CLOCK = {
        "1KHZ": 1e3, 
        "10KHZ": 10e3,
        "100KHZ": 100e3,
        "1MHZ": 1e6, 
        "10MHZ": 10e6, 
        "50MHZ": 50e6,
    }

    def __init__(self, musst, motors=None, counters=None):
        self.musst = musst

        self.params = dict()
        self.iter_params = list()
        self.template = dict()
        self.user_template = dict()
        (self.__initial_clock, clock_factor) = self.musst.TMRCFG
        self.__last_clock = self.__initial_clock
        self.__max_data_rate = 0

        self.channels = dict()
        self.channels["timer"] = MusstTimerChannel(clock_factor)

        if motors is None:
            self.motors = list()
        else:
            self.set_motors(*motors)
        if counters is None:
            self.set_counters(*musst.counters)
        else:
            self.set_counters(*counters)

    def set_motors(self, *motors):
        use_chans = list()
        self.motors = list()
        names = list()
        for motor in motors:
            name = getattr(motor, "original_name", motor.name)
            try:
                chan = self.musst.get_channel_by_name(name)
            except:
                continue
            chan_id = chan.channel_id
            if chan_id not in use_chans:
                use_chans.append(chan_id)
                self.channels[motor.name] = getMusstMotorChannel(motor, chan)
                self.motors.append(motor)
                names.append(name)
        # ensure switches are set
        chans = self.musst.get_channel_by_names(*names)

    def set_counters(self, *counters):
        use_chans = [ chan.channel_id for chan in self.channels.values() ]
        self.counters = list()
        for counter in counters:
            name = getattr(counter, "original_name", counter.name)
            try:
                chan = self.musst.get_channel_by_name(name)
            except:
                continue
            chan_id = chan.channel_id
            if chan_id not in use_chans:
                use_chans.append(chan_id)
                self.channels[counter.name] = MusstCounterChannel(counter, chan)
                self.counters.append(counter.name)

    def get_channel(self, mot_or_cnt):
        try:
            return self.channels[mot_or_cnt.name]
        except KeyError:
            raise ValueError(f"No channel found for {mot_or_cnt:r}")

    def get_timer_channel(self):
        return self.channels["timer"]

    def get_motor_channel(self, motidx=0):
        if motidx >= len(self.motors):
            raise ValueError("Motor index does not exist")
        return self.channels.get(self.motors[motidx].name)

    def channel_list(self):
        return list(self.channels.keys())

    def sync_motors(self, *motors):
        if not len(motors):
            motors = self.motors
        for motor in motors:
            self.sync_one_motor(motor)

    def sync_one_motor(self, motor):
        mot_chan = self.channels[motor.name]
        enc_chan = self.musst.get_channel(mot_chan.channel_id)

        if enc_chan.mode == enc_chan.SSI:
            return

        motor.wait_move()
        motor_pos = motor.position
        enc_chan.value = int(mot_chan.unit2raw(motor_pos) + 0.5)
       
    def set_params(self, param_dict):
        self.params = param_dict

    def set_iter_params(self, param_list):
        self.iter_params = param_list

    def set_template(self, template_dict):
        self.user_template = template_dict

    def check_max_timer(self, scan_time):
        if self.__initial_clock in ["1KHZ", "10KHZ", "100KHZ"]:
            self.set_timer_clock("1MHZ")
        if self.get_max_timer() < scan_time:
            return self.set_max_timer(scan_time)
        return False

    def set_max_timer(self, total_time):
        (clock_str, clock) = self.musst.TMRCFG
        clock_list = list(self.TIMER_CLOCK.values())
        clock_cmds = list(self.TIMER_CLOCK.keys())
        clock_index = clock_cmds.index(clock_str)
        changed = False

        while clock_index > 0 and total_time > (pow(2, 32) / clock_list[clock_index]):
            clock_index -= 1
            changed = True

        if changed:
            self.set_timer_clock(clock_cmds[clock_index])
        return changed

    def set_timer_clock(self, clock):
        self.musst.TMRCFG = clock
        self.__last_clock = clock
        self.channels["timer"].set_clock(self.TIMER_CLOCK[clock])

    def get_max_timer(self):
        return pow(2, 32) / self.musst.get_timer_factor()

    def set_back_timer(self):
        self.musst.TMRCFG = self.__initial_clock

    def set_max_data_rate(self, data_rate):
        self.__max_data_rate = data_rate

    def setup(self, chain, master=None, **keys):
        # --- create store list and template
        self.__setup_store_list_and_template()

        # --- get parameters list or dict
        pars = self.__get_params()

        # --- check musst state
        if self.musst.STATE != self.musst.IDLE_STATE:
            self.musst.ABORT

        # --- musst acquisition master
        self.__prepare_once = keys.pop("prepare_once", False)
        self.__start_once = keys.pop("start_once", False)
        last_iter_index = keys.pop("last_iter_index", 1)

        self.musst_master = MusstAcquisitionMaster(
            self.musst,
            program_data=self.ProgramData,
            program_start_name=self.ProgramStart,
            program_abort_name=self.ProgramAbort,
            program_template_replacement=self.template,
            vars=pars,
            prepare_once=self.__prepare_once,
            start_once=self.__start_once,
            last_iter_index=last_iter_index,
            **keys
        )

        # --- musst acquisition device
        self.musst_device = MusstAcquisitionSlave(
            self.musst, store_list=self.store_list, max_data_rate=self.__max_data_rate,
        )

        # --- add to acq. chain
        if master is not None:
            chain.add(master, self.musst_master)
        chain.add(self.musst_master, self.musst_device)

        # --- revert musst clock if needed
        if self.__last_clock != self.__initial_clock:
            chain.add_preset(MusstRevertClock(self))

    @property
    def acq_master(self):
        return self.musst_master

    @property
    def acq_data_master(self):
        return self.musst_device

    def __setup_store_list_and_template(self):
        data_alias = ""
        data_store = ""
        chan_names = dict()

        motidx = 0
        for motor in self.motors:
            chan = self.channels.get(motor.name)
            motidx += 1
            data_alias += "ALIAS MOT{0:d} = CH{1:d}\n".format(motidx, chan.channel_id)
            data_store += "MOT{0:d} ".format(motidx)

        cntidx = 0
        for counter in self.counters:
            chan = self.channels.get(counter)
            cntidx += 1
            data_alias += "ALIAS DATA{0:d} = CH{1:d}\n".format(cntidx, chan.channel_id)
            data_store += "DATA{0:d} ".format(cntidx)
            chan.init()

        self.template = {"$DATA_ALIAS$": data_alias, "$DATA_STORE$": data_store}
        self.template.update(self.user_template)

        chan_names = dict(
            ((chan.channel_id, chan.store_name) for chan in self.channels.values())
        )
        sorted_chans = list(chan_names.keys())
        sorted_chans.sort()
        self.store_list = [chan_names[chid] for chid in sorted_chans]

    def __get_params(self):
        if not len(self.iter_params):
            return self.params
        else:
            par0 = dict(self.params)
            par0.update(self.iter_params[0])
            parlist = [ par0, ] + self.iter_params[1:]
            return parlist


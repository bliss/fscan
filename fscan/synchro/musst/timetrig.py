from .base import MusstProgBase, _get_iter_var
from .mprg.timetrig_mprg import timetrig_mprg_data


class MusstProgTimeTrigger(MusstProgBase):
    ProgramData = timetrig_mprg_data
    ProgramStart = "TIMETRIG"
    ProgramAbort = None
    DataPerPoint = 1

    def set_params(self, npts, period):
        self.params = self.__params_to_vars(npts, period)

    def __params_to_vars(self, npts, period):
        l_npts = _get_iter_var(npts)
        l_period = _get_iter_var(period)
        if len(l_npts) > 10:
            raise ValueError("MusstTimeTrigger is limited to 10 regions")

        params = dict()
        nzone = 0
        mclock = self.musst.get_timer_factor()

        for (nb, per) in zip(l_npts, l_period):
            params["ZNPTS[{0}]".format(nzone)] = nb
            params["ZPERIOD[{0}]".format(nzone)] = int(per * mclock)
            nzone += 1

        params["ZNB"] = nzone
        return params

    def set_iter_params(self, npts, period):
        self.iter_params = list()
        for p, t in zip(npts, period):
            self.iter_params.append(self.__params_to_vars(p, t))



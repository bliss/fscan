fscanloop_mprg_data = """

// --- INPUT VARIABLES
UNSIGNED SCANMODE
UNSIGNED GATEMODE
UNSIGNED LOOPMODE

SIGNED TRIGSTART
UNSIGNED TRIGDELTA
UNSIGNED NPULSES
SIGNED SCANDIR

UNSIGNED GATEWIDTH

UNSIGNED NLOOP
UNSIGNED LOOPDELTA
UNSIGNED WAITDELTA

UNSIGNED POSERR

// --- OUPUT VARIABLES
UNSIGNED NPOINTS

// --- INTERNAL VARIABLES
SIGNED NEXT_TG
SIGNED START_TG
SIGNED S_TRIGDELTA
SIGNED S_LOOPDELTA
SIGNED S_WAITDELTA
SIGNED S_REWIND
SIGNED S_GATEPOS

UNSIGNED CUMERR

UNSIGNED ILOOP
UNSIGNED IPULSE

UNSIGNED TRIGRECV


// --- PROGRAM ALIASES
$DATA_ALIAS$

// --- initialise store list
SUB STORE_INIT
    STORELIST TIMER $DATA_STORE$
    EMEM 0 AT 0
ENDSUB

// --- external event source
EVENT WAIT_SOURCE = ANYOF MOT1 ITRIG

CONSTANT EV_MASK = 0x0000FFFF
CONSTANT EV_ITRIG = 0x00000100


// --- init position event
SUB POS_EVENT_INIT
    IF (SCANDIR > 0) THEN
        EVSOURCE MOT1 UP
    ELSE
        EVSOURCE MOT1 DOWN
    ENDIF
    DEFEVENT MOT1
ENDSUB

// --- increment position target
SUB POS_INC_TARGET
    NEXT_TG = NEXT_TG + S_TRIGDELTA

    CUMERR += POSERR
    IF ((CUMERR & 0x80000000) != 0) THEN
        CUMERR &= 0x7FFFFFFF
        NEXT_TG += SCANDIR
    ENDIF

    @MOT1 = NEXT_TG
ENDSUB

// --- single acquisition in position
SUB ACQ_IN_POS
    AT DEFEVENT DO STORE ATRIG BTRIG
    IF (GATEMODE == 0) THEN
        // --- gate in time
        @TIMER = $TIMER + GATEWIDTH
        AT TIMER DO STORE BTRIG
        DEFEVENT MOT1
    ELSE
        // --- gate in position
        @MOT1 = $MOT1 + S_GATEPOS
        AT MOT1 DO STORE BTRIG
    ENDIF
ENDSUB

// --- single acquisition in time
SUB ACQ_IN_TIME
    AT DEFEVENT DO STORE ATRIG BTRIG
    NEXT_TG = $TIMER + TRIGDELTA
    @TIMER = $TIMER + GATEWIDTH
    AT TIMER DO STORE BTRIG
    @TIMER = NEXT_TG
    DEFEVENT TIMER
ENDSUB

SUB WAIT_EXT_TRIG
    TRIGRECV = 0
    EVSOURCE ITRIG RISE
    IF (SCANDIR > 0) THEN
        EVSOURCE MOT1 UP
    ELSE
        EVSOURCE MOT1 DOWN
    ENDIF

    NEXT_TG = START_TG + S_LOOPDELTA
    WHILE (TRIGRECV == 0) DO
       NEXT_TG = NEXT_TG + S_WAITDELTA
       @MOT1 = NEXT_TG
       AT WAIT_SOURCE DO NOTHING
       IF ((.ESRC & EV_MASK) == EV_ITRIG) THEN
           TRIGRECV = 1
       ENDIF
    ENDWHILE
    START_TG = NEXT_TG
ENDSUB

SUB INVERT_DIR
    SCANDIR = -SCANDIR
    S_TRIGDELTA = -S_TRIGDELTA
    S_LOOPDELTA = -S_LOOPDELTA
    S_REWIND = -S_REWIND
    S_GATEPOS = -S_GATEPOS
ENDSUB

PROG FSCANLOOP
   // --- reset Trigger Out B signal
   BTRIG 0

   // --- reset timer
   CTSTOP TIMER
   TIMER = 0
   CTSTART TIMER

   // --- init store values
   GOSUB STORE_INIT

   // --- init variables
   ILOOP = 0
   NPOINTS = 0
   CUMERR = 0
   START_TG = TRIGSTART
   IF (SCANDIR > 0) THEN
       S_TRIGDELTA = TRIGDELTA
       S_LOOPDELTA = LOOPDELTA
       S_WAITDELTA = WAITDELTA
       S_REWIND = 10
       S_GATEPOS = GATEWIDTH
   ELSE
       S_TRIGDELTA = -TRIGDELTA
       S_LOOPDELTA = -LOOPDELTA
       S_WAITDELTA = -WAITDELTA
       S_REWIND = -10
       S_GATEPOS = -GATEWIDTH
   ENDIF

   // --- main loop
   WHILE (ILOOP < NLOOP) DO
       // --- first target in position
       GOSUB POS_EVENT_INIT
       NEXT_TG = START_TG
       @MOT1 = START_TG

       IPULSE = 0
       WHILE (IPULSE < NPULSES) DO
          IF (SCANMODE == 1) THEN
             // --- trigger in position
             GOSUB ACQ_IN_POS
             GOSUB POS_INC_TARGET
          ELSE
             // --- trigger in time
             GOSUB ACQ_IN_TIME
          ENDIF
          IPULSE += 1
          NPOINTS += 1
       ENDWHILE

       ILOOP += 1

       IF (ILOOP == NLOOP) THEN
           AT DEFEVENT DO STORE
       ELSE
           IF (LOOPMODE == 1) THEN
               // --- forward
               START_TG = START_TG + S_LOOPDELTA
           ELSEIF (LOOPMODE == 2) THEN
               // --- rewind
               START_TG = START_TG + S_LOOPDELTA
               GOSUB INVERT_DIR
               GOSUB POS_EVENT_INIT
               @MOT1 = START_TG + S_REWIND
               AT DEFEVENT DO NOTHING
               GOSUB INVERT_DIR
           ELSEIF (LOOPMODE == 3) THEN
               // --- backnforth
               START_TG = START_TG + S_LOOPDELTA
               GOSUB INVERT_DIR
           ELSEIF (LOOPMODE == 4) THEN
               // --- forward + external trigger
               GOSUB WAIT_EXT_TRIG
           ELSE
               EXIT NPOINTS
           ENDIF
       ENDIF
   ENDWHILE

   EXIT NPOINTS
ENDPROG

// --- cleanup function
PROG FSCANLOOP_CLEAN
   CTSTOP TIMER
   TIMER = 0

   BTRIG 0
ENDPROG

"""


itrigrecord_mprg_data = """
UNSIGNED NPTS
UNSIGNED IPT
UNSIGNED MODE
UNSIGNED RECORD

// MODE = 0 : no trigger sent
// MODE = 1 : send one trigger at start

// RECORD = 0 : record both rise and fall itrig
// RECORD = 1 : record only rise itrig
// RECORD = 2 : record only fall itrig
// RECORD = 3 : record high and low transitions

$DATA_ALIAS$

SUB STORE_INIT
    STORELIST TIMER $DATA_STORE$
    EMEM 0 AT 0
ENDSUB

PROG ITRIGRECORD

    GOSUB STORE_INIT

    BTRIG 0

    CTSTOP TIMER
    CTRESET TIMER
    CTSTART TIMER

    IF (MODE == 1) THEN
       DOACTION ATRIG
    ENDIF

    IF (RECORD == 0) THEN
       EVSOURCE ITRIG EDGE
       FOR IPT FROM 1 TO NPTS
          AT ITRIG DO STORE
          AT ITRIG DO STORE
       ENDFOR
    ELSEIF (RECORD == 1) THEN
       EVSOURCE ITRIG RISE
       FOR IPT FROM 1 TO NPTS
          AT ITRIG DO STORE
       ENDFOR
    ELSEIF (RECORD == 2) THEN
       EVSOURCE ITRIG FALL
       FOR IPT FROM 1 TO NPTS
          AT ITRIG DO STORE
       ENDFOR
    ELSE
       FOR IPT FROM 1 TO NPTS
          EVSOURCE ITRIG HIGH
          AT ITRIG DO STORE
          EVSOURCE ITRIG LOW
          AT ITRIG DO STORE
       ENDFOR
    ENDIF

ENDPROG
"""

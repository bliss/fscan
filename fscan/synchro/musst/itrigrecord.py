from .base import MusstProgBase
from .mprg.itrigrecord_mprg import itrigrecord_mprg_data

class MusstProgITrigRecord(MusstProgBase):
    ProgramData = itrigrecord_mprg_data
    ProgramStart = "ITRIGRECORD"
    ProgramAbort = None
    DataPerPoint = 2

    def set_params(self, npts, mode=0, record=0):
        self.params = dict(NPTS=int(npts), MODE=mode, RECORD=record)
        if record == 0 or record == 3:
            self.DataPerPoint = 2
        else:
            self.DataPerPoint = 1


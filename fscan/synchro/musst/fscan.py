import numpy

from .base import MusstProgBase
from .mprg.fscan_mprg import fscan_mprg_data
from .mprg.fscancamera_mprg import fscancamera_mprg_data

from fscan.fscantools import FScanTrigMode, FScanCamSignal

class MusstProgFScan(MusstProgBase):
    ProgramData = fscan_mprg_data
    ProgramStart = "FSCAN"
    ProgramAbort = "FSCAN_CLEAN"
    DataPerPoint = 2

    def set_modes(self, scanmode=FScanTrigMode.TIME, gatemode=FScanTrigMode.TIME):
        self._scan_mode = scanmode
        self._gate_mode = gatemode
        if self._scan_mode == FScanTrigMode.TIME:
            self._gate_mode = FScanTrigMode.TIME

    def set_params(self, start, direction, delta, gatewidth, npulses):
        clock_chan = self.get_timer_channel()
        motor_chan = self.get_motor_channel(0)

        trigstart = motor_chan.unit2raw(start)

        if self._scan_mode == FScanTrigMode.TIME:
            trigdelta = clock_chan.unit2raw(abs(delta))
            poserr = 0
        else:
            enc_delta = abs(motor_chan.delta2raw(delta))
            trigdelta = int(enc_delta)
            poserr = int((enc_delta - trigdelta) * 2 ** 31)

        if self._gate_mode == FScanTrigMode.TIME:
            gate = clock_chan.unit2raw(abs(gatewidth))
        else:
            gate = abs(motor_chan.delta2raw(gatewidth))

        self.params = dict()
        self.params["SCANMODE"] = FScanTrigMode.index(self._scan_mode)
        self.params["GATEMODE"] = FScanTrigMode.index(self._gate_mode)
        self.params["TRIGSTART"] = int(trigstart)
        self.params["TRIGDELTA"] = int(trigdelta)
        self.params["GATEWIDTH"] = int(gate)
        self.params["NPULSES"] = int(npulses)
        self.params["SCANDIR"] = int(direction * motor_chan.sign)
        self.params["POSERR"] = int(poserr)
        self.params["ACCNB"] = 1
        self.params["ACCTIME"] = 0

    def set_acc_params(self, accnb, accperiod):
        clock_chan = self.get_timer_channel()
        self.params["ACCNB"] = int(accnb)
        self.params["ACCTIME"] = int(clock_chan.unit2raw(accperiod))

    def set_iter_params(self, start=None, direction=None, delta=None, gatewidth=None, npulses=None):
        clock_chan = self.get_timer_channel()
        motor_chan = self.get_motor_channel(0)
        set_dict = dict()

        if start is not None:
            trigstart = motor_chan.unit2raw(numpy.array(start))
            set_dict["TRIGSTART"] = trigstart.astype(numpy.int32)

        if direction is not None:
            set_dict["SCANDIR"] = numpy.array(direction, numpy.int32) * motor_chan.sign

        if delta is not None:
            trigdelta = numpy.array(delta)
            if self._scan_mode == FScanTrigMode.TIME:
                trigdelta = clock_chan.unit2raw(trigdelta)
                set_dict["TRIGDELTA"] = trigdelta.astype(numpy.int32)
            else:
                enc_delta = abs(motor_chan.delta2raw(trigdelta))
                trigdelta = enc_delta.astype(numpy.int32)
                pos_error = ((enc_delta - trigdelta) * 2**31).astype(numpy.int32)
                set_dict["TRIGDELTA"] = trigdelta
                set_dict["POSERR"] = pos_error

        if gatewidth is not None:
            gatedata = numpy.array(gatewidth)
            if self._gate_mode == FScanTrigMode.TIME:
                gatedata = clock_chan.unit2raw(gatedata)
            else:
                gatedata = motor_chan.delta2raw(gatedata)
            set_dict["GATEWIDTH"] = gatedata.astype(numpy.int32)

        if npulses is not None:
            set_dict["NPULSES"] = numpy.array(npulses, numpy.int32)

        self.iter_params = list()
        for data in zip(*set_dict.values()):
            self.iter_params.append(dict(zip(set_dict.keys(), data)))


class MusstProgFScanCamera(MusstProgBase):
    ProgramData = fscancamera_mprg_data
    ProgramStart = "FSCANCAMERA"
    ProgramAbort = "FSCANCAMERA_CLEAN"
    DataPerPoint = 2

    def set_params(self, start, direction, npulses, signal, gatelowtime=0.):
        clock_chan = self.get_timer_channel()
        motor_chan = self.get_motor_channel(0)

        trigstart = motor_chan.unit2raw(start)

        self.params = dict()
        self.params["TRIGSTART"] = int(trigstart)
        self.params["SCANDIR"] = int(direction * motor_chan.sign)
        self.params["NPULSES"] = int(npulses)
        self.params["SIGNAL"] = FScanCamSignal.index(signal)
        self.params["ACCNB"] = 1
        self.params["GATE_LOWTIME"] = max(1, int(clock_chan.unit2raw(gatelowtime)))
        if signal in [FScanCamSignal.FALL, FScanCamSignal.RISE]:
            self.DataPerPoint = 1
        else:
            self.DataPerPoint = 2

    def set_acc_params(self, accnb):
        self.params["ACCNB"] = int(accnb)

    def set_iter_params(self, start=None, direction=None, npulses=None):
        motor_chan = self.get_motor_channel(0)
        set_dict = dict()

        if start is not None:
            trigstart = motor_chan.unit2raw(numpy.array(start))
            set_dict["TRIGSTART"] = trigstart.astype(numpy.int32)

        if direction is not None:
            set_dict["SCANDIR"] = numpy.array(direction, numpy.int32) * motor_chan.sign

        if npulses is not None:
            set_dict["NPULSES"] = numpy.array(npulses, numpy.int32)

        self.iter_params = list()
        for data in zip(*set_dict.values()):
            self.iter_params.append(dict(zip(set_dict.keys(), data)))


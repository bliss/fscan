from .base import MusstProgBase
from .mprg.mtimescan_mprg import mtimescan_mprg_data

from fscan.fscantools import FTimeScanMode

class MusstProgMTimeScan(MusstProgBase):
    ProgramData = mtimescan_mprg_data
    ProgramStart = "MTIMESCAN"
    ProgramAbort = "MTIMESCAN_CLEAN"
    DataPerPoint = 2

    def set_params(self, scan_mode, start, fast_npts, fast_period, slow_npts, slow_gate, slow_idx):
        if scan_mode == FTimeScanMode.TIME:
            mode = 0
        elif scan_mode == FTimeScanMode.CAMERA:
            mode = 1
        elif scan_mode == FTimeScanMode.EXTSTART:
            mode = 1
        else:
            raise ValueError("invalid scan_mode for mtimescan musst program")

        mclock = self.musst.get_timer_factor()
        self.params = dict()
        self.params["MODE"] = mode
        self.params["START_TIME"] = int(start * mclock)
        self.params["FAST_NPTS"] = int(fast_npts)
        self.params["FAST_PERIOD"] = int(fast_period * mclock)
        self.params["SLOW_NPTS"] = int(slow_npts)
        self.params["SLOW_GATETIME"] = int(slow_gate * mclock)
        self.params["SLOW_GATEIDX"] = int(slow_idx)



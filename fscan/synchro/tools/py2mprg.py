
import sys
import os.path

if len(sys.argv) < 2:
    print("Usage: {0} <filename>".format(sys.argv[0]))
    sys.exit(0)

filename = sys.argv[1]
name = os.path.splitext(os.path.basename(filename))[0]
mprgname = "{0}.mprg".format(name.replace("_mprg", ""))

print("Reading {0}".format(filename))
with open(filename, "r") as rfile:
    lines = rfile.readlines()

print("Writing {0}".format(mprgname))
with open(mprgname, "w") as wfile:
    for line in lines:
        if line.find('"""') < 0:
            wfile.write(line)

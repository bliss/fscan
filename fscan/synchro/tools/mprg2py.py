
import sys
import os.path

if len(sys.argv) < 2:
    print("Usage: {0} <filename>".format(sys.argv[0]))
    sys.exit(0)

filename = sys.argv[1]
name = os.path.splitext(os.path.basename(filename))[0]
pyname = f"{name}_mprg.py"

print(f"Reading {filename}")
with open(filename, "r") as rfile:
    lines = rfile.readlines()

print(f"Writing {pyname}")
with open(pyname, "w") as wfile:
    wfile.write(f'{name}_mprg_data = """\n')
    wfile.writelines(lines)
    wfile.write('"""\n')

import tabulate

from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.scan import Scan
from bliss.scanning.scan_info import ScanInfo
from bliss.scanning.toolbox import ChainBuilder

from fscan.fscantools import FScanDisplay
from fscan.motortools import calc_move_time, MotorMaster, MotorZigZagMaster

from fscan.synchro.base import SyncDevice
from fscan.synchro.maestrio.programs import MaestrioProgTimeTrigger, MaestrioProgITrigRecord
from fscan.synchro.musst.programs import MusstProgTimeTrigger, MusstProgITrigRecord
from fscan.musstcalc import MusstCalcAcqSlave

__all__ = ["timescope", "itrigscope", "limascope", "motorscope"]


def timescope(syncdev, npts, period, *motors, **kwargs):
    name = "timescope"

    # --- parameters
    sync_motors = kwargs.pop("sync_motors", True)
    save_flag = kwargs.pop("save", False)
    if len(kwargs):
        raise ValueError("Invalid argument : {0}".format(list(kwargs.keys())))

    # --- acquisition chain
    chain = AcquisitionChain()

    # --- sync program
    sync = SyncDevice(syncdev)
    if sync.is_musst():
        syncprog = MusstProgTimeTrigger(syncdev)
    else:
        syncprog = MaestrioProgTimeTrigger(syncdev)
    syncprog.set_motors(*motors)
    syncprog.set_params(npts, period)
    syncprog.setup(chain)

    if sync_motors is True:
        syncprog.sync_motors()

    # --- sync calc
    synccalc = MusstCalcAcqSlave(syncprog.acq_data_master, syncprog.channels, 1)
    synccalc.add_default_calc()
    chain.add(syncprog.acq_master, synccalc)

    # --- scan info
    scan_info = ScanInfo()
    scan_info["title"] = f"{name} {syncdev.name} {npts} {period}"
    scan_info["type"] = name
    timechan = synccalc.get_channel_name("trig", "timer")
    if len(motors):
        motchan = synccalc.get_channel_name("trig", motors[0].name)
        scan_info.add_curve_plot(x=timechan, yleft=motchan)

    # --- scan
    scan = Scan(chain, name=name, scan_info=scan_info, save=save_flag, scan_progress=FScanDisplay(timechan))
    scan.run()


RECORD_MODES = ["RISE_AND_FALL", "RISE", "FALL", "HIGH_AND_LOW"]


def _check_record_mode(record):
    try:
        irecord = RECORD_MODES.index(record.upper())
        return irecord
    except (AttributeError, ValueError):
        raise ValueError(f"record can only one of {RECORD_MODES}")


def itrigscope(syncdev, npts, **kwargs):
    name = "itrigscope"

    # --- parameters
    record = kwargs.pop("record", RECORD_MODES[0])
    save_flag = kwargs.pop("save", False)
    if len(kwargs):
        raise ValueError("Invalid argument : {0}".format(list(kwargs.keys())))

    irecord = _check_record_mode(record)

    # --- acquisition chain
    chain = AcquisitionChain()

    # --- sync program
    sync = SyncDevice(syncdev)
    if sync.is_musst():
        syncprog = MusstProgITrigRecord(syncdev)
    else:
        syncprog = MaestrioProgITrigRecord(syncdev)
    syncprog.set_params(npts, 0, irecord)
    syncprog.setup(chain)

    # --- sync calc
    synccalc = MusstCalcAcqSlave(syncprog.acq_data_master, syncprog.channels, syncprog.DataPerPoint)
    synccalc.add_default_calc()
    chain.add(syncprog.acq_master, synccalc)

    # --- scan info
    scan_info = ScanInfo()
    scan_info["title"] = f"{name} {syncdev.name} {npts}"
    scan_info["type"] = name
    timechan = synccalc.get_channel_name("trig", "timer")
    deltachan = synccalc.get_channel_name("period", "timer")
    scan_info.add_curve_plot(x=timechan, yleft=deltachan)

    # --- scan
    scan = Scan(chain, name=name, scan_info=scan_info, save=save_flag,
                scan_progress=FScanDisplay(timechan))
    scan.run()

    # --- simple report
    period = scan.streams["timer_period"][:]
    print(f"Recorded signal       : {record.upper()}")
    print(f"Recorded period [sec] : {period.mean()}, {period.min()}, {period.max()}")
    print()


def limascope(syncdev, limadev, expotime, npoints, **kwargs):
    name = "limascope"

    # --- parameters
    external_trigger = kwargs.pop("external_trigger", False)
    record = kwargs.pop("record", RECORD_MODES[0])
    period = kwargs.pop("period", None)
    save_flag = kwargs.pop("save", False)
    if len(kwargs):
        raise ValueError("Invalid argument : {0}".format(list(kwargs.keys())))

    irecord = _check_record_mode(record)

    if period is not None:
        latency = period - expotime
        if latency < 0:
            raise ValueError("Period must be > expotime !!")

    # --- limadev
    if external_trigger:
        trig_mode = "EXTERNAL_TRIGGER"
        ext_trig = 1
    else:
        trig_mode = "INTERNAL_TRIGGER"
        ext_trig = 0

    lima_pars = {
        "acq_nb_frames": npoints,
        "acq_expo_time": expotime,
        "acq_mode": "SINGLE",
        "acq_trigger_mode": trig_mode,
        "prepare_once": False,
        "start_once": False,
    }
    if period is not None:
        lima_pars["latency_time"] = latency
    builder = ChainBuilder([limadev,])
    limanode = builder.nodes[0]
    limanode.set_parameters(acq_params=lima_pars)

    # --- sync program
    sync = SyncDevice(syncdev)
    if sync.is_musst():
        syncprog = MusstProgITrigRecord(syncdev)
    else:
        syncprog = MaestrioProgITrigRecord(syncdev)
    syncprog.set_params(npoints, ext_trig, irecord)

    # --- acquisition chain
    chain = AcquisitionChain(parallel_prepare=True)

    if external_trigger:
        syncprog.setup(chain)
        chain.add(syncprog.acq_master, limanode)
    else:
        chain.add(limanode)
        syncprog.setup(chain, limanode)

    # --- sync calc
    synccalc = MusstCalcAcqSlave(syncprog.acq_data_master, syncprog.channels, syncprog.DataPerPoint)
    synccalc.add_default_calc()
    chain.add(syncprog.acq_master, synccalc)

    # --- scan info
    scan_info = ScanInfo()
    scan_info["title"] = f"{name} {syncdev.name} {limadev.name} {expotime} {npoints}"
    scan_info["type"] = name

    xname = synccalc.get_channel_name("trig", "timer")
    yname = synccalc.get_channel_name("period", "timer")
    scan_info.add_curve_plot(x=xname, yleft=yname)

    # --- scan
    scan = Scan(chain, name=name, scan_info=scan_info, save=save_flag,
                scan_progress=FScanDisplay(xname))
    scan.run()
    _limascope_print_results(record, scan.streams)


def _limascope_print_results(record, streams):
    heads = ["", "Mean", "Min", "Max"]
    values = list()

    if record in ("RISE_AND_FALL", "HIGH_AND_LOW"):
        delta = streams["timer_delta"][:]
        period = streams["timer_period"][:]
        nb = min(len(delta), len(period))
        dtime = period[:nb] - delta[:nb]

        values.append(("AcqTime [sec]", delta.mean(), delta.min(), delta.max()))
        values.append(("Period [sec]", period.mean(), period.min(), period.max()))
        values.append(("DeadTime [sec]", dtime.mean(), dtime.min(), dtime.max()))
    else:
        period = streams["timer_period"][:]
        values.append(("Period [sec]", period.mean(), period.min(), period.max()))

    print(f"Limascope recorded signals : {record.upper()}")
    print()
    print(tabulate.tabulate(values, headers=heads))
    print()


def motorscope(syncdev, motor, start, stop, speed, period, **kwargs):
    name = "motorscope"

    # --- arguments
    save_flag = kwargs.pop("save", False)
    sync_motors = kwargs.pop("sync_motors", True)
    constant_speed = kwargs.pop("constant_speed", False)
    acc_margin = kwargs.pop("acc_margin", None)
    extra_time = kwargs.pop("extra_time", 0)
    zigzag_mode = kwargs.pop("zigzag", False)

    if len(kwargs):
        raise ValueError("Invalid argument : {0}".format(list(kwargs.keys())))

    # --- check motor located on sync device
    syncdev.get_channel_by_name(motor.name)

    # --- motor calc params
    motor_unit = motor.unit is None and "unit" or motor.unit
    if constant_speed is True:
        acc_time = speed / motor.acceleration
        acc_disp = 0.5 * speed**2 / motor.acceleration
        if acc_margin is not None:
            acc_disp += acc_margin
            acc_time += acc_margin/speed
        move_range = abs(stop-start) + 2*acc_disp
        move_time = calc_move_time(motor, move_range, speed)
        real_start = start
        real_stop = stop
        undershoot = acc_disp
        print("\nmotorscope type : constant speed from {0} {1} to {2} {3}".format(
              start, motor_unit, stop, motor_unit))
        print("    * constant speed     = {0:f} {1}/sec".format(speed, motor_unit))
        print("    * acceleration delta = {0:f} {1}".format(acc_disp, motor_unit))
        print("    * acceleration time  = {0:f} sec".format(acc_time))
        print("    * estimated time     = {0:f} sec".format(move_time))
        print()
    else:
        move_time = calc_move_time(motor, abs(stop-start), speed)
        real_start = start
        real_stop = stop
        undershoot = 0.
        print("\nmotorscope type : simple move from {0:f} {1} to {2:f} {3}".format(
               start, motor_unit, stop, motor_unit))
        print("    * maximum speed  = {0:f} {1}/sec".format(speed, motor_unit))
        print("    * estimated time = {0:f} sec".format(move_time))
        print()

    # --- acq chain
    chain = AcquisitionChain()

    # --- setup motor dev
    if zigzag_mode is False:
        npts = int((move_time + extra_time) / period + 0.5)
        motor_master = MotorMaster(motor, real_start, real_stop, abs(stop-start)/speed, undershoot=undershoot)
    else:
        npts = 2 * int((move_time + extra_time) / period + 0.5)
        motor_master = MotorZigZagMaster(motor, real_start, real_stop, abs(stop-start)/speed, undershoot=undershoot)

    # --- sync program
    sync = SyncDevice(syncdev)
    if sync.is_musst():
        syncprog = MusstProgTimeTrigger(syncdev)
    else:
        syncprog = MaestrioProgTimeTrigger(syncdev)
    syncprog.set_motors(motor)
    syncprog.set_params(npts, period)
    syncprog.setup(chain, motor_master)

    if sync_motors is True:
        syncprog.sync_motors()

    synccalc = MusstCalcAcqSlave(syncprog.acq_data_master, syncprog.channels, 1)
    synccalc.add_default_calc()
    chain.add(syncprog.acq_master, synccalc)

    # --- scanning
    scan_info = ScanInfo()
    scan_info["title"] = f"{name} {syncdev.name} {motor.name} {start} {stop} {speed} {period}"
    scan_info["type"] = name

    timechan = synccalc.get_channel_name("trig", "timer")
    motorchan = synccalc.get_channel_name("trig", motor.name)
    scan_info.add_curve_plot(x=timechan, yleft=motorchan)

    # --- scan
    scan = Scan(chain, name=name, scan_info=scan_info, save=save_flag,
                scan_progress=FScanDisplay(timechan))
    scan.run()

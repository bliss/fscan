from bliss.scanning.scan import Scan
from bliss.scanning.acquisition.motor import SweepMotorMaster
from bliss.controllers.ct2.device import AcqMode as CT2AcqMode

from bliss.scanning.chain import AcquisitionSlave

from fscan.synchro.base import SyncDeviceList
from fscan.synchro.musst.programs import (
    MusstProgFSweep,
    MusstProgTimeTrigger,
)
from fscan.synchro.maestrio.programs import MaestrioProgFSweep
from fscan.musstcalc import MusstCalcAcqSlave
from fscan.fscantools import (
    FScanParamBase,
    FScanParamStruct,
    FScanDisplay,
)
from fscan.motortools import (
    RotationMotorChainPreset,
    calc_move_time,
)
from fscan.mcatools import get_fscan_mca_params
from fscan.chaintools import ChainTool
from fscan.fscanrunner import FScanDiagRunner
from fscan.fscaninfo import FScanInfo
from fscan.limatools import LimaCalib


class FSweepPars(FScanParamBase):
    DEFAULT = {
        "motor": None,
        "start_pos": 0,
        "acq_size": 0.1,
        "acq_time": 0.8,
        "npoints": 10,
        "save_flag": True,
        "display_flag": True,
        "sync_encoder": True,
        "home_rotation": False,
        "acc_margin": 0.,
        "sampling_time": 0.5,
    }
    OBJKEYS = ["motor"]
    LISTVAL = dict()
    NOSETTINGS = ["save_flag",]

    def __init__(self, name):
        FScanParamBase.__init__(
            self, 
            name, 
            FSweepPars.DEFAULT, 
            FSweepPars.OBJKEYS, 
            FSweepPars.LISTVAL,
            FSweepPars.NOSETTINGS,
        )

    def _validate_npoints(self, value):
        if not int(value) > 0:
            raise ValueError("npoints should be > 0")
        return int(value)

    def _validate_save_flag(self, value):
        return value and True or False

    def _validate_display_flag(self, value):
        return value and True or False

    def _validate_sync_encoder(self, value):
        return value and True or False

    def _validate_acc_margin(self, value):
        return abs(value)


class FSweepMaster(object):
    def __init__(self, name, config):
        self.name = name
        self.config = config

        self.sync_list = SyncDeviceList(config["devices"]["synchro"])
        self.meas_group = config["devices"]["measgroup"]
        self.rot_motors = config["devices"]["rotation_motors"]
        self.chain_config = config["chain_config"]
        self.lima_calib = LimaCalib(config["lima_calib"])

        self.pars = FSweepPars(self.name)
        self.inpars = None
        self.mgchain = None

    def validate(self):
        self.inpars = FScanParamStruct(self.pars.to_dict())
        pars = self.inpars

        # --- check motor is on musst
        self.sync_dev = self.sync_list.find_device_for_motors(pars.motor)

        # --- with lima, acq_time must be > readout_time
        limas = self.get_controllers_found("lima")
        if len(limas):
            self.lima_calib.calibrate(limas, pars.acq_time)
            pars.min_period = pars.acq_time + self.lima_calib.external_time

        # --- calc params
        pars.motor_name = pars.motor.name
        pars.motor_unit = pars.motor.unit is None and "unit" or pars.motor.unit

        pars.speed = abs(pars.acq_size) / pars.acq_time
        pars.acc_disp = pars.acc_margin + 0.5 * pars.speed ** 2 / pars.motor.acceleration
        # ensure acc_disp is at least 6 steps
        if abs(pars.acc_disp * pars.motor.steps_per_unit) < 6:
            pars.acc_margin += 6/pars.motor.steps_per_unit - pars.acc_disp
            pars.acc_disp = pars.acc_margin + 0.5 * pars.speed ** 2 / pars.motor.acceleration

        pars.acc_time = pars.acc_margin / pars.speed + pars.speed / pars.motor.acceleration
        pars.stop_pos = pars.start_pos + pars.acq_size * pars.npoints

        pars.back_delta = pars.acc_disp * 0.5

        pars.move_time = 2 * pars.acc_time + pars.acq_time
        pars.back_time = calc_move_time(pars.motor, 2 * pars.acc_disp)
        pars.scan_time = pars.npoints * pars.move_time + (pars.npoints - 1) * pars.back_time

    def show(self):
        if self.inpars is None:
            raise RuntimeError(f"FSweepMaster[{self.name}]: Parameters not validated yet !!")

        txt = """
Motor : {motor_name}
    start pos = {start_pos:.4f} {motor_unit}
    stop pos  = {stop_pos:.4f} {motor_unit}
    velocity  = {speed:.4f} {motor_unit}/sec
    acceleration size = {acc_disp:.4f} {motor_unit} (margin = {acc_margin:.4f})
    acceleration time = {acc_time:.6f} sec

Acquisition :
    acq time  = {acq_time:.6f} sec
    acq size  = {acq_size:.4f} {motor_unit}
    nb points = {npoints}
"""
        print(txt.format(**self.inpars.to_dict()))

    def init_scan(self):
        self.mgchain = ChainTool(self.meas_group, self.chain_config)
        self.lima_used = list()
        self.mca_used = list()
        self.home_preset = None

    def get_controllers_found(self, ctrl_type):
        mgchain = ChainTool(self.meas_group)
        return mgchain.get_controllers_not_done(ctrl_type)

    def get_controllers_used(self, ctrl_type):
        if self.mgchain is None:
            return []
        else:
            return self.mgchain.get_controllers_done(ctrl_type)

    def setup_acq_chain(self, chain):
        mot_master = self.setup_motor_master(chain)
        acq_master = self.setup_acq_master(chain, mot_master)
        self.setup_acq_slaves(chain, acq_master)

    def setup_motor_master(self, chain):
        pars = self.inpars
        if pars.home_rotation is True and pars.motor in self.rot_motors:
            self.home_preset = RotationMotorChainPreset(pars.motor, homing=True, rounding=False)
            chain.add_preset(self.home_preset)

        master = SweepMotorMaster(
            pars.motor,
            pars.start_pos,
            pars.stop_pos,
            pars.acq_time,
            pars.npoints,
            undershoot_start_margin=pars.acc_margin,
            undershoot_end_margin=pars.acc_margin,
        )
        return master

    def setup_acq_master(self, chain, mot_master):
        pars = self.inpars

        # --- create sync master
        if self.sync_dev.is_musst():
            syncprog = MusstProgFSweep(self.sync_dev.board, [pars.motor,])
            syncprog.check_max_timer(1.2 * pars.scan_time)
        else:
            syncprog = MaestrioProgFSweep(self.sync_dev.board, [pars.motor,])
        syncprog.set_params(pars.start_pos, pars.acq_size, pars.npoints, pars.back_delta)

        # --- sync motors on musst
        if self.home_preset:
            self.home_preset.set_musst_sync_cb(syncprog.sync_one_motor)
        elif pars.sync_encoder:
            syncprog.sync_motors()

        # --- add to chain
        syncprog.setup(chain, mot_master)

        # --- musst calc
        synccalc = MusstCalcAcqSlave(syncprog.acq_data_master, syncprog.channels, syncprog.DataPerPoint)
        synccalc.add_default_calc()
        synccalc.add_calc("center", pars.motor_name)
        chain.add(syncprog.acq_master, synccalc)

        # --- motor external channel
        calc_name = synccalc.get_channel_short_name("center", pars.motor_name)
        mot_master.add_external_channel(synccalc, calc_name, f"axis:{pars.motor_name}")

        self.syncprog = syncprog
        self.synccalc = synccalc
        
        return syncprog.acq_master

    def setup_acq_slaves(self, chain, acq_master):
        pars = self.inpars

        # --- ct2 counters
        ct2_params = {
            "npoints": pars.npoints,
            "acq_mode": CT2AcqMode.ExtGate,
            "prepare_once": True,
            "start_once": True,
        }
        self.mgchain.setup("ct2", chain, acq_master, ct2_params)

        # --- lima devices
        lima_params = {
            "acq_trigger_mode": "EXTERNAL_GATE",
            "acq_expo_time": pars.acq_time,
            "acq_nb_frames": pars.npoints,
            "wait_frame_id": [pars.npoints - 1, ],
            "prepare_once": True,
            "start_once": True,
            "scan_frame_per_file": pars.npoints,
        }
        self.mgchain.setup("lima", chain, acq_master, lima_params)
        self.lima_used = self.mgchain.get_controllers_done("lima")

        # --- mca devices
        for mca_ctrl in self.mgchain.get_controllers_not_done("mca"):
            mca_params = get_fscan_mca_params(mca_ctrl, pars.npoints, pars.acq_time)
            self.mgchain.setup("mca", chain, acq_master, mca_params, mca_ctrl.name)
            if "trigger_mode" in mca_params:
                # --- in spectrum reading mode
                self.mca_used.append(mca_ctrl)

        # --- mosca devices
        mosca_params = {
            "trigger_mode": "GATE",
            "npoints": pars.npoints,
            "preset_time": pars.acq_time,
            "wait_frame_id": [pars.npoints,],
            "start_once": True,
        }
        self.mgchain.setup("mosca", chain, acq_master, mosca_params)

        # --- mythen
        mythen_params = {
            "trigger_type": AcquisitionSlave.HARDWARE,
            "npoints": pars.npoints,
            "count_time": pars.acq_time,
        }
        self.mgchain.setup("mythen", chain, acq_master, mythen_params)

        # --- calc
        self.mgchain.setup_calc_counters(chain, acq_master)

        # --- sampling
        self.mgchain.setup_sampling(chain, pars.sampling_time)

    def setup_acq_scope(self, chain, mot_master):
        pars = self.inpars

        # --- musst time prog
        musstprog = MusstProgTimeTrigger(self.musst, [pars.motor,])
        acqtime = max(0.001, pars.acq_time / 10.)

        npts = int((pars.scan_time + 0.5) / acqtime + 0.5)
        musstprog.set_params(npts, acqtime)

        # --- sync motors on musst
        if self.home_preset:
            self.home_preset.set_musst_sync_cb(musstprog.sync_one_motor)
        elif pars.sync_encoder:
            musstprog.sync_motors()

        # --- acq chain
        musstprog.setup(chain, mot_master)
        musstprog.add_default_calc(chain)

    def setup_scan(self, chain, scan_name, user_info=None):
        inpars = self.inpars
        if inpars.display_flag:
            self.show()

        scan_info = FScanInfo(scan_name, "fsweep", inpars.npoints)
        scan_info.set_fscan_pars(self.pars)
        scan_info.set_fscan_title(
            "{motor_name} {start_pos:g} {acq_size:g} {acq_time:g} {npoints:d}",
            inpars,
        )
        scan_info.set_fscan_info(user_info)
        scan_info.add_curve_plot(x=f"axis:{inpars.motor.name}")

        trig_name = self.synccalc.get_channel_name("trig", "timer")
        scan = Scan(
            chain,
            name=scan_name,
            save=inpars.save_flag,
            scan_info=scan_info,
            scan_progress=FScanDisplay(trig_name),
        )

        return scan

    def get_runner_class(self):
        return FSweepCustomRunner


class FSweepCustomRunner(FScanDiagRunner):
    def __call__(self, motor, start_pos, acq_size, npoints, acq_time, **kwargs):
        pars = dict(
            motor=motor,
            start_pos=start_pos,
            acq_size=acq_size,
            npoints=npoints,
            acq_time=acq_time,
        )
        pars.update(kwargs)
        self.run_with_pars(pars)

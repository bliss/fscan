
from bliss.controllers.mca import DetectorType, AcquisitionMode

def mercury_calc_max_block_size(mca, mem_in_MB=2.):
    mem = mem_in_MB * 1024. * 1024.
    size = mca.spectrum_size
    npts = ((mem / 2.) - 256) / (256 + size * len(mca.elements))
    return int(npts)

def mercury_estimate_block_size(mca, acq_time):
    maxpts = mercury_calc_max_block_size(mca)
    estime = int(0.5 / acq_time)
    estime = max(estime, 5)
    return min(estime, maxpts)

def estimate_block_size(mca, acq_time):
    if mca.detector_type == DetectorType.MERCURY:
        return mercury_estimate_block_size(mca, acq_time)
    if mca.detector_type == DetectorType.FALCONX:
        return max(int(1.0/acq_time), 1)
    if mca.detector_type == DetectorType.DANTE:
        return max(int(1.0/acq_time), 1)
    return None

def get_fscan_mca_params(mca, npoints, point_time):
    if mca.acquisition_mode == AcquisitionMode.HWSCA:
        mca_params = {
            "npoints": npoints,
            "prepare_once": True,
            "start_once": True,
        }
    else:
        block_size = estimate_block_size(mca, point_time)
        mca_params = {
            "npoints": npoints,
            "trigger_mode": "GATE",
            "block_size": block_size,
            "prepare_once": True,
            "start_once": True,
        }
    return mca_params
    
    
